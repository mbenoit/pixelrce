//
//      test-client.cc
//
//      test application for IPC library
//
//      Sergei Kolos January 2000
//
//      description:
//              Shows how to access remote objects and call their methods
//	    Performs timing for the lookup and remote methods invocations
/////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>

#include <ers/ers.h>

#include <ipc/partition.h>
#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include <owl/timer.h>
#include <cmdl/cmdargs.h>
#include <unistd.h>


#include "IPCScanAdapter.hh"
#include "IPCConfigIFAdapter.hh"
#include "IPCFEI3Adapter.hh"
#include "ScanOptions.hh"

using namespace std;

static bool echo = true;
static int iter=1;
static int delay=0;
static int asynch=1;
static int synch=0;
static const char *object_name="scanCtrl";

static int s_error_num;
static int a_error_num;

static void s_error(CORBA::Exception & ex)
{
    s_error_num++;
    std::cerr << "ERROR [synch_ping] exception " << ex._name() << std::endl;
}

static void a_error(CORBA::Exception & ex)
{
    a_error_num++;
    std::cerr << "ERROR [asynch_ping] exception " << ex._name() << std::endl;
}


static void 
TestIterators( IPCPartition p )
{
    std::map< std::string, ipc::IPCScanAdapter_var >	scanobjects;
    std::map< std::string, ipc::IPCConfigIFAdapter_var >	confobjects;
    std::map< std::string, ipc::IPCFEI3Adapter_var >	modobjects;

    
    OWLTimer time;

    time.start();
    try {
        p.getObjects<ipc::IPCScanAdapter>( scanobjects );
        p.getObjects<ipc::IPCConfigIFAdapter>( confobjects );
        p.getObjects<ipc::IPCFEI3Adapter>( modobjects );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
    	ers::error( ex );
    }
    time.stop();
    
    if ( echo )
    {
  	std::cout << "There are " << confobjects.size() << " objects of type \"ipc::IPCConfigIFAdapter\" registered with the \""
	      << p.name() << "\" partition" << std::endl;
  	std::cout << "There are " << scanobjects.size() << " objects of type \"ipc::IPCScanAdapter\" registered with the \""
	      << p.name() << "\" partition" << std::endl;
  	std::cout << "There are " << modobjects.size() << " objects of type \"ipc::IPCFEI3Adapter\" registered with the \""
	      << p.name() << "\" partition" << std::endl;
	std::cout << "getObjects operation took (in seconds): " << std::endl
	      << "\t" << time.userTime() << "(user time)" << std::endl
	      << "\t" << time.systemTime() << "(system time)" << std::endl
	      << "\t" << time.totalTime() << "(total time)" << std::endl;
    }
    
    std::map< std::string, ipc::IPCScanAdapter_var >::iterator scanit;
    std::map< std::string, ipc::IPCConfigIFAdapter_var >::iterator confit;
    std::map< std::string, ipc::IPCFEI3Adapter_var >::iterator modit;
    
    if ( echo ) 
    	std::cout << "Objects are:" << std::endl;
    for ( scanit = scanobjects.begin(); scanit != scanobjects.end(); scanit++ )
    {
  	if ( echo )
	    std::cout << "\"" << (*scanit).first << "\"" << std::endl;
    }	
    for ( confit = confobjects.begin(); confit != confobjects.end(); confit++ )
    {
  	if ( echo )
	    std::cout << "\"" << (*confit).first << "\"" << std::endl;
    }	
    for ( modit = modobjects.begin(); modit != modobjects.end(); modit++ )
    {
  	if ( echo )
	    std::cout << "\"" << (*modit).first << "\"" << std::endl;
    }	
}


static void TestLookupMethod( IPCPartition p )
{
    char	oname[32];
    int		i;
    OWLTimer	time;
  
    time.start();
    for ( i = 0; i < 1 ; i++ )
    {
  	sprintf( oname, "scanCtrl", i );
  	try {
	    ipc::IPCScanAdapter_var handle = p.lookup<ipc::IPCScanAdapter>( oname );
  	}
  	catch( daq::ipc::InvalidPartition & ex ) {
  	    ers::error( ex );
  	}
  	catch( daq::ipc::ObjectNotFound & ex ) {
  	    ers::error( ex );
  	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
    time.stop();
    if ( echo )
    {
	std::cout	<< "object resolution (first time) takes (in seconds): " << std::endl
	    << "\t" << time.userTime() << "(user time)" << std::endl
	    << "\t" << time.systemTime() << "(system time)" << std::endl
	    << "\t" << time.totalTime() << "(total time)" << std::endl;   
    }
    
    time.reset();
    time.start();
    for ( i = 0; i < 1 ; i++ )
    {
  	sprintf( oname, "scanCtrl", i );
  	try {
	    ipc::IPCScanAdapter_var handle = p.lookup<ipc::IPCScanAdapter>( oname );
  	}
  	catch( daq::ipc::InvalidPartition & ex ) {
  	    ers::error( ex );
  	}
  	catch( daq::ipc::ObjectNotFound & ex ) {
  	    ers::error( ex );
  	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
    time.stop();
    if ( echo )
    {
	std::cout	<< "object resolution (second time) takes (in seconds): " << std::endl
	    << "\t" << time.userTime() << "(user time)" << std::endl
	    << "\t" << time.systemTime() << "(system time)" << std::endl
	    << "\t" << time.totalTime() << "(total time)" << std::endl;   
    }
}

static void TestResolve( IPCPartition p )
{
    char	oname[32] = "scanCtrl";
    int		i;
    OWLTimer	time;
  
    time.start();
    for ( i = 0; i < iter; i++ )
    {
  	try {
	    ipc::IPCScanAdapter_var handle = p.lookup<ipc::IPCScanAdapter,ipc::no_cache,ipc::narrow>( oname );
  	}
  	catch( daq::ipc::InvalidPartition & ex ) {
  	    ers::error( ex );
  	}
  	catch( daq::ipc::ObjectNotFound & ex ) {
  	    ers::error( ex );
  	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
    time.stop();
    if ( echo )
    {
	std::cout	<< "object resolution takes (in seconds): " << std::endl
	    << "\t" << time.userTime()/iter << "(user time)" << std::endl
	    << "\t" << time.systemTime()/iter << "(system time)" << std::endl
	    << "\t" << time.totalTime()/iter << "(total time)" << std::endl;   
    }
}

static void TestIsObjectValid( IPCPartition p )
{
    char	oname[32];
    int		i;
 
    for ( i = 0; i < iter ; i++ )
    {
  	if ( echo )
	    std::cout << " p.isValid( ) = " << p.isValid( ) << std::endl;
    }
    
    sprintf( oname, "scanCtrl" );
    for ( i = 0; i < iter ; i++ )
    {
	try {
	    bool v = p.isObjectValid<ipc::IPCScanAdapter>( oname );
	    if ( echo )
		std::cout << " p.isObjectValid( ) = " << v << std::endl;
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
    }
    
    for ( i = 0; i < 1 ; i++ )
    {
  	sprintf( oname, "scanCtrl", i );
	try {
	    bool v = p.isObjectValid<ipc::IPCScanAdapter>( oname );
	    if ( echo )
		std::cout << " p.isObjectValid( ) = " << v << std::endl;
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
    }
    
}

static void TestRemoteInvocation( IPCPartition p )
{
    char    line[2];
    int	    i;
    	
    memset( line, 55, 1 );
    line[1] = 0;

    OWLTimer time;

    ipc::ScanOptions options;
    char* pointer=(char*)&options;
    for (int i=0;i<sizeof(ipc::ScanOptions);i++){
      *pointer++=0;
    }
    options.nMaskStages=5;
    if ( !synch )
    {
  	if ( echo )
	{
	    std::cout << "Asynchronous method test ..." << std::endl;
	}
	
	time.start();

  	for ( i = 0; i < 1; i++ )
	{
	    sleep( delay );
	    ipc::IPCScanAdapter_var handle;
	    try {
	      //   	std::clog << "calling lookup ... {" << std::endl;
		handle = p.lookup<ipc::IPCScanAdapter>( (const char*)object_name );
	    		      //std::clog << "} done" << std::endl;
	    }
	    catch( daq::ipc::InvalidPartition & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::ObjectNotFound & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::InvalidObjectName & ex ) {
		ers::error( ex );
                continue;
	    }
	    
            try
	    {
	    		      //std::clog << "calling ping_a function ... {" << std::endl;
	        handle -> IPCabort( );
                //handle -> IPCconfigureScan( "Regular", options  );
                handle -> IPCpause( );
	    		      //std::clog << "} done" << std::endl;
                
	    }
	    catch(CORBA::Exception & ex)
	    {
	    	a_error( ex );
	    }
  	}
	time.stop();

  	if ( echo )
	{
	    std::cout << "passing 1024 bytes (asynchronously) takes (in seconds): " << std::endl
	    	 << "\t" << time.userTime()/iter << "(user time)" << std::endl
	    	 << "\t" << time.systemTime()/iter << "(system time)" << std::endl
	    	 << "\t" << time.totalTime()/iter << "(total time)" << std::endl;   
	}
    }
  
    if ( !asynch )
    {
  	if ( echo )
	{	
	    std::cout << "Synchronous method test ..." << std::endl;
	}
  
	time.reset();
	time.start();               // Record starting time
	
   	for ( i = 0; i < iter; i++ )
	{
	    sleep( delay );
	    ipc::IPCScanAdapter_var handle;
	    try {
	    	std::clog << "calling lookup ... {" << std::endl;
		handle = p.lookup<ipc::IPCScanAdapter>( (const char*)object_name );
	    	std::clog << "} done" << std::endl;
	    }
	    catch( daq::ipc::InvalidPartition & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::ObjectNotFound & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::InvalidObjectName & ex ) {
		ers::error( ex );
                continue;
	    }

	    try
	    {
	    	std::clog << "calling ping_s function ... {" << std::endl;
	    	handle -> IPCresume(  );
	    	std::clog << "} done" << std::endl;
	    }
	    catch(CORBA::Exception & ex)
	    {
	    	s_error( ex );
	    }
  	}
	time.stop();
  	if ( echo )
	{
	    std::cout << "passing 1024 bytes (synchronously) takes (in seconds): " << std::endl
	    	 << "\t" << time.userTime()/iter << "(user time)" << std::endl
	    	 << "\t" << time.systemTime()/iter << "(system time)" << std::endl
	    	 << "\t" << time.totalTime()/iter << "(total time)" << std::endl;   
	}
    }  
}

static void TestExceptions( IPCPartition p )
{
    char    line[1025];
    int	    i;
    	
    memset( line, 55, 1024 );
    line[1024] = 0;

    for ( i = 0; i < iter; i++ )
    {
	ipc::IPCScanAdapter_var handle;
	try {
	    handle = p.lookup<ipc::IPCScanAdapter>( (const char*)object_name );
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	    break;
	}
	catch( daq::ipc::ObjectNotFound & ex ) {
	    ers::error( ex );
	    break;
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	    break;
	}
        
	try
	{
	    handle -> IPCabort(  );
	}
	catch(CORBA::SystemException & ex)
	{
	    std::cerr << std::endl << "ERROR [ipc_IPCScanAdapter_client::TestExceptions] system exception " << ex._name() << std::endl;
	}
    }
}
/*
class IPCCallback : public IPCServer,
		    public IPCObject<POA_ipc::callback>
{
  public:
    void notify( const char * name )
    {
    	std::cout << "Callback from employee \"" << name << "\" received" << std::endl;
    }
    
    ~IPCCallback()
    {
    	std::cout << "IPCCallback destructor is called" << std::endl;
    }
};


bool stop_test( void * param )
{
    IPCCallback * cb = static_cast<IPCCallback*>( param );
    cb->stop();
    return false;
}

void TestCallbacks( IPCPartition p )
{
    IPCCallback * cb = new IPCCallback;
    
    std::map< std::string, ipc::person_var >	persons;
    try {
        p.getObjects<ipc::person>( persons );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
    	ers::error( ex );
    }

    std::map< std::string, ipc::person_var >::iterator pit;
    
    std::cout << "Persons are:" << std::endl;
    
    for ( pit = persons.begin(); pit != persons.end(); pit++ )
    {
	std::cout << "\"" << (*pit).first << "\"" << std::endl;
    }	
    
    std::map< std::string, ipc::employee_var >	employees;
    try {
        p.getObjects<ipc::employee>( employees );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
    	ers::error( ex );
    }

    std::map< std::string, ipc::employee_var >::iterator eit;
    
    std::cout << "Employees are:" << std::endl;
    
    for ( eit = employees.begin(); eit != employees.end(); eit++ )
    {
	std::cout << "\"" << (*eit).first << "\"" << std::endl;
	(*eit).second->subscribe( cb->_this(), 1 );
    }	
    
    IPCAlarm * alarm = new IPCAlarm( 10, 0, stop_test, cb );
    
    cb->run();
    
    delete alarm;
    cb->_destroy();
    std::cout << "Callbacks test completed." << std::endl;
}
*/
static void TestCache( IPCPartition p )
{
    char	line[2];
    int	    	i;
    	
    memset( line, 55, 1 );
    line[2] = 0;

    if ( echo )
    {
	std::cout << "Cache test ..." << std::endl;
    }

    ipc::IPCScanAdapter_var handle1;
    ipc::IPCScanAdapter_var handle2;
    try {
	std::clog << "calling lookup for handle 1 ... {" << std::endl;
	handle1 = p.lookup<ipc::IPCScanAdapter>( (const char*)object_name );
	std::clog << "} done" << std::endl;
        
	std::clog << "calling lookup for handle 2 ... {" << std::endl;
	handle2 = p.lookup<ipc::IPCScanAdapter>( (const char*)object_name );
	std::clog << "} done" << std::endl;
    }
    catch( daq::ipc::Exception & ex ) {
	ers::error( ex );
        return;
    }

    for ( i = 0; i < iter; i++ )
    {
	try
	{
	    sleep( delay );
	    std::clog << "calling ping_s function using handle 1 ... {" << std::endl;
	    handle1 -> IPCpause( );
	    std::clog << "} done" << std::endl;
	    
            sleep( delay );
            std::clog << "calling ping_s function using handle 2 ... {" << std::endl;
	    handle2 -> IPCresume( );
	    std::clog << "} done" << std::endl;
	}
	catch(CORBA::Exception & ex)
	{
	    s_error( ex );
	}
            
	try
	{
            sleep( delay );
	    std::clog << "calling lookup for local handle ... {" << std::endl;
	    ipc::IPCScanAdapter_var handle = p.lookup<ipc::IPCScanAdapter>( (const char*)object_name );
	    std::clog << "} done" << std::endl;

            sleep( delay );
            std::clog << "calling ping_s function using local handle ... {" << std::endl;
	    handle -> IPCpause( );
	    std::clog << "} done" << std::endl;            
	}
	catch(CORBA::Exception & ex)
	{
	    s_error( ex );
	}
	catch( daq::ipc::Exception & ex ) {
	    ers::error( ex );
	    return;
	}
    }
    
    std::cout << "Cache test completed." << std::endl;
}

static void TestPerformance( IPCPartition p, size_t size )
{
    char * line = new char[size];
    	
    memset( line, 55, size );
    line[size-1] = 0;

    double total_time = 0;
    double user_time = 0;
    double system_time = 0;
    if ( !synch )
    {
  	if ( echo )
	{
	    std::cout << "Asynchronous method test ..." << std::endl;
	}
	
  	for ( int i = 0; i < iter; i++ )
	{
	    if ( delay) usleep( delay );
	    ipc::IPCScanAdapter_var handle;
	    try {
		handle = p.lookup<ipc::IPCScanAdapter>( (const char*)object_name );
	    }
	    catch( daq::ipc::InvalidPartition & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::ObjectNotFound & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::InvalidObjectName & ex ) {
		ers::error( ex );
                continue;
	    }
	    
            try
	    {
		OWLTimer one_time;
		one_time.start();
                handle -> IPCpause( );
		one_time.stop();
		std::cout << one_time.totalTime()*1000 << std::endl;
                total_time += one_time.totalTime();
                user_time += one_time.userTime();
                system_time += one_time.systemTime();
	    }
	    catch(CORBA::Exception & ex)
	    {
	    	a_error( ex );
	    }
  	}

  	if ( echo )
	{
	    std::cout << "passing " << size << " bytes (asynchronously) takes (in milliseconds): " << std::endl
	    	 << "\t" << user_time/iter*1000 << "(user time)" << std::endl
	    	 << "\t" << system_time/iter*1000 << "(system time)" << std::endl
	    	 << "\t" << total_time/iter*1000 << "(total time)" << std::endl;   
	}
    }
  
    total_time = 0;
    user_time = 0;
    system_time = 0;
    if ( !asynch )
    {
  	if ( echo )
	{	
	    std::cout << "Synchronous method test ..." << std::endl;
	}
  
   	for ( int i = 0; i < iter; i++ )
	{
	    if ( delay) usleep( delay );
	    ipc::IPCScanAdapter_var handle;
	    try {
		handle = p.lookup<ipc::IPCScanAdapter>( (const char*)object_name );
	    }
	    catch( daq::ipc::InvalidPartition & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::ObjectNotFound & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::InvalidObjectName & ex ) {
		ers::error( ex );
                continue;
	    }

	    try
	    {
		OWLTimer one_time;
		one_time.start();
	    	handle -> IPCpause( );
		one_time.stop();
		std::cout << one_time.totalTime()*1000 << std::endl;
                total_time += one_time.totalTime();
                user_time += one_time.userTime();
                system_time += one_time.systemTime();
	    }
	    catch(CORBA::Exception & ex)
	    {
	    	s_error( ex );
	    }
  	}

  	if ( echo )
	{
	    std::cout << "passing " << size << " bytes (asynchronously) takes (in milliseconds): " << std::endl
	    	 << "\t" << user_time/iter*1000 << "(user time)" << std::endl
	    	 << "\t" << system_time/iter*1000 << "(system time)" << std::endl
	    	 << "\t" << total_time/iter*1000 << "(total time)" << std::endl;   
	}
    }
    
    delete line;
}

int main( int argc, char ** argv )
{	
 
    CmdArgStr	partition_name ('p', "partition", "partition-name", "partition to work in.");
    CmdArgInt	length ('l', "length", "data-length", "size of data for timing test (1024 by default).");

//
// Initialize command line parameters with default values
//
   
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmd(*argv, &partition_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmd.parse(arg_iter);
  
    IPCPartition   p( (const char*)partition_name );
  
    std::cout<<"***********TestIterators"<<std::endl;
    TestIterators(p);
    std::cout<<"***********TestLookupMethod"<<std::endl;
    TestLookupMethod(p);
    std::cout<<"***********TestIsObjectValid"<<std::endl;
    TestIsObjectValid(p);
    std::cout<<"***********TestRemoteInvocation"<<std::endl;
    TestRemoteInvocation(p);
    //TestExceptions(p);
    //    TestCallbacks(p);
    //    TestCache(p);
    //TestPerformance( p, length );    
        //TestResolve(p);
    
    return 0;
    
    std::cout << "# of failed methods invocations: " << std::endl
	<< "\tasynchronous - " << a_error_num << std::endl
	<< "\tsynchronous - " << s_error_num << std::endl;
  
    ipc::IPCScanAdapter_var handle;
    try {
	handle = p.lookup<ipc::IPCScanAdapter>( (const char*)object_name );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
	ers::error( ex );
	return 1;
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
	ers::error( ex );
	return 1;
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
	ers::error( ex );
	return 1;
    }

    try
    {
    	handle -> shutdown( );
    }
    catch(CORBA::SystemException & ex) 
    {
	std::cerr << "ERROR [ipc_test_client::main] Call to method shutdown failed " << ex._name() << std::endl;
	return 1;
    }
    	
    return 0;
}
