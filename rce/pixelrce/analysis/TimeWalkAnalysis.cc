#include "analysis/TimeWalkAnalysis.hh"
#include "server/PixScan.hh"

#include <TFile.h>
#include <TStyle.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TF1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>

namespace{
  const double threshold=0;
}
using namespace RCE;

void TimeWalkAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  //unsigned int numval=scan->getLoopVarValues(0).size();
  gStyle->SetOptFit(10);
  gStyle->SetOptStat(0);
  std::cout<<"TimeWalk analysis"<<std::endl;

  const int totalScans = scan->getLoopVarValues(1).size();

  float xVals[totalScans];
  float yVals[totalScans];


  TH1D* t0mean[totalScans];
  //  TH1D* t0sigma[totalScans];

  TH1D* histoTW(0);


  float xMin = scan->getLoopVarValues(1).at(0);
  float xMax = scan->getLoopVarValues(1).at(totalScans-1);
  
  if(xMax<xMin){
    float xtemp = xMax;
    xMax = xMin;
    xMin = xtemp;
  }
  
  histoTW = new TH1D(Form("TimeWalk"), Form("Time Walk integrated over all pixels"), 50, xMin - 1, xMax + 1);
  histoTW->GetXaxis()->SetTitle("N(e^{-})");
  histoTW->GetYaxis()->SetTitle("t0 (strobe delay counts)");

  histoTW->SetOption("P");
  histoTW->SetMarkerStyle(2);
  histoTW->SetMarkerSize(2);




  //loop over the different T0 scans
  for(int iScan = 0; iScan<totalScans; iScan++){

    file->cd(Form("loop1_%d",iScan));

  
    TIter nextkey(gDirectory->GetListOfKeys());
    TKey *key;
    boost::regex re("_(\\d+)_Mean");
    boost::regex re2("Mean");
    while ((key=(TKey*)nextkey())) {
      std::string name(key->GetName());
      boost::cmatch matches;
      if(boost::regex_search(name.c_str(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	
	
	//	std::cout<<"match = "<<match.c_str()<<std::endl;
	int id=strtol(match.c_str(),0,10);
	
	//	std::cout<<"id = "<<id<<std::endl;
	
	std::string sigmaHistoName = boost::regex_replace (name, re2, "Sigma");

	TH2* histo = (TH2*)key->ReadObj();
	TH2* sigmaHisto=(TH2*)gDirectory->Get(sigmaHistoName.c_str());
	assert(sigmaHisto);



	//Get histogram ranges
	float maxval[2] = {0,0};
	float minval[2] = {1e8, 1e8};
	float vals[2] = {0,0};
	
	for (int i=1;i<=histo->GetNbinsX();i++){
	  for(int j=1;j<=histo->GetNbinsY();j++){
	    
	    vals[0] = histo->GetBinContent(i, j);
	    vals[1] = sigmaHisto->GetBinContent(i, j);
	    
	    for(int kpar = 0; kpar<2; kpar++){
	      if(vals[kpar]>maxval[kpar]){
		maxval[kpar] = vals[kpar];
	      }
	      if(vals[kpar]<minval[kpar]&&vals[kpar]>0){
		minval[kpar] = vals[kpar];
	      }
	    }
	    
	  }
	}
	
	//  TH1D* t0sigma[2];
	
	
	t0mean[iScan] =new TH1D(Form("T0_%d_Mod_%d",iScan,id), 
				Form("T0 for each pixel in Mod %d at %s (Scan %d)"
				     , id, findFieldName(cfg, id), iScan),
				50, minval[0]-1, maxval[0]+1);
	//      t0sigma[iScan] =new TH1D(Form("Sigma_%d_Mod_%d",iScan,id), Form("T0 S-CURVE sigma for each pixel in Mod %d (Scan %d)", id, iScan), 50, minval[1]-1, maxval[1]+1);
	
	t0mean[iScan]->GetXaxis()->SetTitle("t0 (strobe delay counts)");
	//	t0sigma[iScan]->GetXaxis()->SetTitle("t0 S-Curve sigma (strobe delay counts)");
	
	
	for (int i=1;i<=histo->GetNbinsX();i++){
	  for(int j=1;j<=histo->GetNbinsY();j++){
	    
	    float val=0;
	    
	    if(histo->GetBinContent(i, j)!=0 ){
	      val = histo->GetBinContent(i, j);
	      t0mean[iScan]->Fill(val);
	    }
	    
	    
	    // 	  if(sigmaHisto->GetBinContent(i, j)!=0 ){
	    // 	    val = sigmaHisto->GetBinContent(i, j);
	    // 	    t0sigma[iScan]->Fill(val);
	    // 	  }
	    
	  }
	  
	}
	
	t0mean[iScan]->Fit("gaus");
	yVals[iScan] = t0mean[iScan]->GetFunction("gaus")->GetParameter(1);
	std::cout<<"T0 for module "<<id<<" for scan "<<iScan<<" is "<<yVals[iScan]<<std::endl;
	
	


	xVals[iScan] = scan->getLoopVarValues(1).at(iScan);

	//	std::cout<<"at scan "<<iScan<<" , x = "<<xVals[iScan]<<std::endl;

	
	//     for (int i=1;i<=histo->GetNbinsX();i++){
	// 	for(int j=1;j<=histo->GetNbinsY();j++){
	
	// 	  float val=0;
	
	// 	  if(histo->GetBinContent(i, j)!=0 ){
	// 	    val = histo->GetBinContent(i, j)*convFactor;
	// 	    t0mean_ns[0]->Fill(val);
	// 	  }
	// 	  if(histo2->GetBinContent(i, j)!=0 ){
	// 	    val = histo2->GetBinContent(i, j)*convFactor;
	// 	    t0mean_ns[1]->Fill(val);
	// 	  }  
	
	// 	  if(sigmaHisto->GetBinContent(i, j)!=0 ){
	// 	    val = sigmaHisto->GetBinContent(i, j)*convFactor;
	// 	    t0sigma_ns[0]->Fill(val);
	// 	  }
	// 	  if(sigmaHisto2->GetBinContent(i, j)!=0 ){
	// 	    val = sigmaHisto2->GetBinContent(i, j)*convFactor;
	// 	    t0sigma_ns[1]->Fill(val);
	// 	  } 
	
	// 	}
	
	//       }
	      

	delete histo;
	anfile->cd();

	t0mean[iScan]->Write();
	t0mean[iScan]->SetDirectory(gDirectory);	
// 	t0sigma[scan]->Write();
// 	t0sigma[scan]->SetDirectory(gDirectory);

// 	t0mean_ns[scan]->Write();
// 	t0mean_ns[scan]->SetDirectory(gDirectory);
// 	t0sigma_ns[scan]->Write();
// 	t0sigma_ns[scan]->SetDirectory(gDirectory);
	file->cd(Form("loop1_%d",iScan));
	
      }

    }

  }  //end loop over iScan


  for(int iScan = 0; iScan<totalScans; iScan++){
    
    histoTW->Fill(xVals[iScan], yVals[iScan]);

  }
  anfile->cd();
  histoTW->Write();
  histoTW->SetDirectory(gDirectory);

}


void TimeWalkAnalysis::fillHit1d(std::string &name, TH1* hits1d, unsigned numval){
for(unsigned int k=0;k<numval;k++){
  boost::regex re2("Mean");
  std::string histoName = boost::regex_replace (name, re2, Form("Occupancy_Point_%03d", k));
  TH2* occHisto=(TH2*)gDirectory->Get(histoName.c_str());
  if(occHisto==0){
    std::cout<<"No Occupancy histograms found. Won't fill 1-d hit histo."<<std::endl;
    break;
  }
  hits1d->SetBinContent(k+1, occHisto->GetSumOfWeights());
 }
}
