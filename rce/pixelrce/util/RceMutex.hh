
#ifndef RCEMUTEX_HH
#define RCEMUTEX_HH
#include <stdint.h>
#include <pthread.h>

///////////////////////////////////////////////////////////////////////////
//
// Mutex
//
///////////////////////////////////////////////////////////////////////////
class rce_mutex {

public:
    rce_mutex(void);
    ~rce_mutex(void);

    inline void lock(void)    { pthread_mutex_lock(&posix_mutex); }
    inline void unlock(void)  { pthread_mutex_unlock(&posix_mutex);}
    inline void acquire(void) { lock(); }
    inline void release(void) { unlock(); }
	// the names lock and unlock are preferred over acquire and release
	// since we are attempting to be as POSIX-like as possible.

    friend class rce_condition;

private:
    // dummy copy constructor and operator= to prevent copying
    rce_mutex(const rce_mutex&);
    rce_mutex& operator=(const rce_mutex&);

private:
    pthread_mutex_t posix_mutex;
};

//
// As an alternative to:
// {
//   mutex.lock();
//   .....
//   mutex.unlock();
// }
//
// you can use a single instance of the rce_mutex_lock class:
//
// {
//   rce_mutex_lock l(mutex);
//   ....
// }
//
// This has the advantage that mutex.unlock() will be called automatically
// when an exception is thrown.
//

class rce_mutex_lock {
    rce_mutex& mutex;
public:
    rce_mutex_lock(rce_mutex& m) : mutex(m) { mutex.lock(); }
    ~rce_mutex_lock(void) { mutex.unlock(); }
private:
    // dummy copy constructor and operator= to prevent copying
    rce_mutex_lock(const rce_mutex_lock&);
    rce_mutex_lock& operator=(const rce_mutex_lock&);
};


///////////////////////////////////////////////////////////////////////////
//
// Condition variable
//
///////////////////////////////////////////////////////////////////////////

class rce_condition {

    rce_mutex* mutex;

public:
    rce_condition(rce_mutex* m);
	// constructor must be given a pointer to an existing mutex. The
	// condition variable is then linked to the mutex, so that there is an
	// implicit unlock and lock around wait() and timed_wait().

    ~rce_condition(void);

    void wait(void);
	// wait for the condition variable to be signalled.  The mutex is
	// implicitly released before waiting and locked again after waking up.
	// If wait() is called by multiple threads, a signal may wake up more
	// than one thread.  See POSIX threads documentation for details.

    int timedwait(int32_t secs, int32_t nanosecs = 0);
	// timedwait() is given an absolute time to wait until.  To wait for a
	// relative time from now, use rce_thread::get_time. See POSIX threads
	// documentation for why absolute times are better than relative.
	// Returns 1 (true) if successfully signalled, 0 (false) if time
	// expired.

    void signal(void);
	// if one or more threads have called wait(), signal wakes up at least
	// one of them, possibly more.  See POSIX threads documentation for
	// details.

    void broadcast(void);
	// broadcast is like signal but wakes all threads which have called
	// wait().

private:
    // dummy copy constructor and operator= to prevent copying
    rce_condition(const rce_condition&);
    rce_condition& operator=(const rce_condition&);

private:
    pthread_cond_t posix_cond;
};


void rce_get_time(int32_t* abs_sec, int32_t* abs_nsec,
		      int32_t rel_sec = 0, int32_t rel_nsec=0);
	// calculates an absolute time in seconds and nanoseconds, suitable for
	// use in timed_waits on condition variables, which is the current time
	// plus the given relative offset.
#endif
