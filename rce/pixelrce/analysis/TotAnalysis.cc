#include "analysis/TotAnalysis.hh"
#include <boost/regex.hpp>

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TKey.h>
#include <TStyle.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

void TotAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  gStyle->SetOptFit(111);
  printf("Begin TOT Analysis\n");
  std::map<int, odata> histomap;
  TKey *key;
  std::vector<std::string> pos;
  std::vector<float> tots;
  std::vector<float> toterrs;
  std::vector<float> sigmas;
  std::vector<float> sigmaerrs;

  int binsx=0, binsy=0;
  TIter nextkey(gDirectory->GetListOfKeys()); // TDAC settings
  while ((key=(TKey*)nextkey())) {
    std::string name(key->GetName());
    boost::regex re(Form("_(\\d+)_Occupancy_Point_%03d", 0));
    boost::cmatch matches;
    if(boost::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int lnm=strtol(match.c_str(),0,10);
      std::string match2=addPosition(name.c_str(), cfg).substr(0,5);
      pos.push_back(match2);
      histomap[lnm].occ = (TH2*)key->ReadObj();
      boost::regex re2("Occupancy");
      std::string totHistoName = boost::regex_replace (name, re2, "ToT");
      histomap[lnm].tot=(TH2*)gDirectory->Get(totHistoName.c_str());
      std::string tot2HistoName = boost::regex_replace (name, re2, "ToT2");
      histomap[lnm].tot2=(TH2*)gDirectory->Get(tot2HistoName.c_str());
      assert(histomap[lnm].tot2);
      binsx=histomap[lnm].tot2->GetNbinsX();
      binsy=histomap[lnm].tot2->GetNbinsY();
    }
  }
  for(std::map<int, odata>::iterator it=histomap.begin();it!=histomap.end();it++){ 
    int id=it->first;
    int max=15;
    if(findFEType(cfg, id)=="FEI3")max=63;
    TH2D* ahis = new TH2D(Form("ToT_Mean_Mod_%i", id),
			  Form("ToT Mean Mod %d at %s", id, findFieldName(cfg, id)),
			  binsx,0,binsx,binsy,0,binsy);
    ahis->GetXaxis()->SetTitle("Column");
    ahis->GetYaxis()->SetTitle("Row");
    ahis->SetMinimum(0);
    ahis->SetMaximum(max+1);
    TH2D* shis = new TH2D(Form("ToT_Sigma_Mod_%i", id),
			  Form("ToT Sigma Mod %d at %s", id, findFieldName(cfg, id)),
			  binsx,0,binsx,binsy,0,binsy);
    ahis->GetXaxis()->SetTitle("Column");
    ahis->GetYaxis()->SetTitle("Row");
    TH2D* badpix = new TH2D(Form("BadPixels_Mod_%i", id), Form("Bad Pixel Map Mod %d", id),binsx,0,binsx,binsy,0,binsy);
    badpix->GetXaxis()->SetTitle("Column");
    badpix->GetYaxis()->SetTitle("Row");
    TH1D* channel = new TH1D(Form("ToT_Mean_Channel_Mod_%i", id),
			     Form("ToT Mean Mod %d at %s", id, findFieldName(cfg, id)),
			     binsx*binsy,0,binsx*binsy);
    channel->GetXaxis()->SetTitle("Channel");
    channel->SetOption("p9");
    channel->SetMinimum(0);
    channel->SetMaximum(max+1);
    TH1D* schannel = new TH1D(Form("ToT_Sigma_Channel_Mod_%i", id),
			      Form("ToT Sigma Mod %d at %s", id, findFieldName(cfg, id)),
			      binsx*binsy,0,binsx*binsy);
    schannel->GetXaxis()->SetTitle("Channel");
    schannel->SetOption("p9");
    TH1D* dist=new TH1D(Form("ToT_Mean_Dist_Mod_%i", id), 
			Form("ToT Distribution Mod %d at %s", id, findFieldName(cfg, id)), 
			100, 0, max);
    dist->GetXaxis()->SetTitle("ToT");
    TH1D* sdist=new TH1D(Form("ToT_Sigma_Dist_Mod_%i", id), 
			 Form("ToT Sigma Distribution Mod %d at %s", id, findFieldName(cfg, id)), 
			 100, 0, 3);
    sdist->GetXaxis()->SetTitle("ToT");
    TH2* occ_histo=histomap[id].occ;
    TH2* tot_histo=histomap[id].tot;
    TH2* tot2_histo=histomap[id].tot2;
    for(int j = 1; j <= ahis->GetNbinsX(); j++){
      for(int k = 1; k <= ahis->GetNbinsY(); k++){
	//printf("Point %3i\n",i);
	float nhits = occ_histo->GetBinContent(j,k);
	float tot=0;
	if(nhits != 0){
	  tot = float(tot_histo->GetBinContent(j,k)) / nhits;
	  float tot2=tot2_histo->GetBinContent(j,k);
	  float sig=0;
	  if(tot2>0)sig=sqrt(tot2/nhits-tot*tot);
	  ahis->SetBinContent(j,k,tot);
	  shis->SetBinContent(j,k,sig);
	  channel->SetBinContent(k+(j-1)*binsy, tot);
	  schannel->SetBinContent(k+(j-1)*binsy, sig);
	  dist->Fill(tot);
	  sdist->Fill(sig);
	}
	else badpix->SetBinContent(j,k,1);
      }
    }
    dist->Fit("gaus");
    tots.push_back(dist->GetMean());
    std::cout<<"Dist Mean value: "<<dist->GetMean()<<", Dist Err: "<<dist->GetRMS()<<"|| Module: "<<findFieldName(cfg,id)<<std::endl; 
    toterrs.push_back(dist->GetRMS());
    anfile->cd();
    ahis->Write();
    ahis->SetDirectory(gDirectory);
    shis->Write();
    shis->SetDirectory(gDirectory);
    badpix->Write();
    badpix->SetDirectory(gDirectory);
    channel->Write();
    channel->SetDirectory(gDirectory);
    schannel->Write();
    schannel->SetDirectory(gDirectory);
    dist->Write();
    dist->SetDirectory(gDirectory);
    sdist->Write();
    sdist->SetDirectory(gDirectory);
  }
  TH1D* mhis[2];
  mhis[0]=new TH1D(Form("1-Tots"), Form("Tots 1st stave"), 32, 0, 32);
  mhis[1]=new TH1D(Form("2-Tots"), Form("Tots 2nd stave"), 32, 0, 32);

  char position[10];
  for(int i=0;i<4;i++){ //half stave
    if(i%2==0)position[0]='A';
    else position[0]='C';
    //mhis[i]->GetYaxis()->SetTitle("Threshold (e)");
    for(int j=1;j<=8;j++){ //module
      for(int k=1;k<=2;k++){ //FE
	sprintf(&position[1], "%d-%d", j,k);
	int bin=0;
	if(i%2==0)bin=17-((j-1)*2+k); // A side
	else bin=16+(j-1)*2+k;
	mhis[i/2]->GetXaxis()->SetBinLabel(bin,position);
	for(size_t l=0;l<pos.size();l++){
	  if(std::string(position)==pos[l].substr(1,4) && 
	     pos[l].substr(0,1)==Form("%d", i/2+1)){
	    mhis[i/2]->SetBinContent(bin, tots[l]);
	    mhis[i/2]->SetBinError(bin, toterrs[l]);
	    
	  } 
	}
      }
    }
    if(i%2==1){
      anfile->cd();
      mhis[i/2]->Write();
      mhis[i/2]->SetDirectory(gDirectory);
    }
  }




  printf("Done Analysis\n");
}

