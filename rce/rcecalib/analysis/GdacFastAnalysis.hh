#ifndef GDACFASTANALYSIS_HH
#define GDACFASTANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"
#include <map>
#include <vector>

class ConfigGui;
class TFile;
class TH2;
class TH1;

namespace RCE{
  class PixScan;
}
namespace{
  struct hdatagdacfast{
    TH2* occupancy;
    TH1* gdac;
  };
}

class GdacFastAnalysis: public CalibAnalysis{
public:
  GdacFastAnalysis(): CalibAnalysis(){}
  ~GdacFastAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
