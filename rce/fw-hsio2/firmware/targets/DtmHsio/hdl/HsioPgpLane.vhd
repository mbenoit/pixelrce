-------------------------------------------------------------------------------
-- Title         : PGP Lane For Data DPM
-- File          : HpsPgpCtrlLane.vhd
-- Author        : Ryan Herbst, rherbst@slac.stanford.edu
-- Created       : 05/21/2014
-------------------------------------------------------------------------------
-- Description:
-- PGP Lane
-------------------------------------------------------------------------------
-- Copyright (c) 2013 by Ryan Herbst. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 05/21/2014: created.
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.numeric_std.all;

library UNISIM;
use UNISIM.VCOMPONENTS.all;

use work.StdRtlPkg.all;
use work.Pgp2bPkg.all;
use work.AxiStreamPkg.all;
use work.AxiLitePkg.all;
use work.SsiPkg.all;
use work.RceG3Pkg.all;
use work.Gtx7CfgPkg.all;

entity HsioPgpLane is
   generic (
      TPD_G  : time := 1 ns
   );
   port (

      -- Sys Clocks
      sysClk200       : in  sl;
      sysClk200Rst    : in  sl;

      -- AXI Bus
      axiClk          : in  sl;
      axiClkRst       : in  sl;
      axiReadMaster   : in  AxiLiteReadMasterType;
      axiReadSlave    : out AxiLiteReadSlaveType;
      axiWriteMaster  : in  AxiLiteWriteMasterType;
      axiWriteSlave   : out AxiLiteWriteSlaveType;

      -- AXI Streaming
      pgpAxisClk      : out sl;
      pgpAxisRst      : out sl;
      pgpDataRxMaster : out AxiStreamMasterType;
      pgpDataRxSlave  : in  AxiStreamSlaveType;
      pgpDataTxMaster : in  AxiStreamMasterType;
      pgpDataTxSlave  : out AxiStreamSlaveType;

      cpllLock        : out sl;
      mmcmLock        : out sl;
      txclkout        : out sl;
      pgpclkout       : out sl;
      inclk           : out sl;

      -- Reference Clock
      locRefClkP  : in    sl;
      locRefClkM  : in    sl;

      -- PHY
      pgpTxP      : out sl;
      pgpTxM      : out sl;
      pgpRxP      : in  sl;
      pgpRxM      : in  sl
   );
end HsioPgpLane;

architecture STRUCTURE of HsioPgpLane is

   signal pgpClkRst          : sl;
   signal pgpClk             : sl;
   signal pgpFbClk           : sl;
   signal ipgpClk            : sl;
   signal pgpTxMmcmReset     : sl;
   signal pgpTxMmcmLocked    : sl;
   signal locRefClk          : sl;
   signal locRefClkG         : sl;
   signal pgpRxIn            : Pgp2bRxInType;
   signal pgpRxOut           : Pgp2bRxOutType;
   signal pgpTxIn            : Pgp2bTxInType;
   signal pgpTxOut           : Pgp2bTxOutType;
   signal muxTxMaster        : AxiStreamMasterType;
   signal muxTxSlave         : AxiStreamSlaveType;
   signal pgpTxMasters       : AxiStreamMasterArray(3 downto 0);
   signal pgpTxSlaves        : AxiStreamSlaveArray(3 downto 0);
   signal pgpRxMastersMuxed  : AxiStreamMasterType;
   signal ipgpRxMastersMuxed : AxiStreamMasterType;
   signal pgpRxCtrl          : AxiStreamCtrlType;
   signal ipgpDataRxMaster   : AxiStreamMasterType;
   signal ipgpDataRxSlave    : AxiStreamSlaveType;
   signal ipgpDataTxMaster   : AxiStreamMasterType;
   signal ipgpDataTxSlave    : AxiStreamSlaveType;
   signal tmpTxMasters       : AxiStreamMasterArray(3 downto 0);
   signal tmpTxSlaves        : AxiStreamSlaveArray(3 downto 0);
   signal tmpRxMaster        : AxiStreamMasterType;
   signal tmpRxSlave         : AxiStreamSlaveType;

   constant DATA_PGP_LINE_RATE_C : real            := 3.125e9;
   constant CPLL_REFCLK_FREQ_C   : real            := 250.0E6;
   constant CPLL_CONFIG_C        : Gtx7CPllCfgType := getGtx7CPllCfg(CPLL_REFCLK_FREQ_C, DATA_PGP_LINE_RATE_C);
   constant QPLL_CONFIG_C        : Gtx7QPllCfgType := getGtx7QPllCfg(250.0E6, DATA_PGP_LINE_RATE_C);
   constant GTX_CONFIG_C         : Gtx7CfgType     := getGtx7Cfg("CPLL", "CPLL", CPLL_CONFIG_C, QPLL_CONFIG_C);

begin

   pgpAxisClk  <= sysClk200;
   pgpAxisRst  <= sysClk200Rst;


   --------------------------------------------------
   -- FIFOs
   --------------------------------------------------

   U_RxFifo : entity work.AxiStreamFifo
      generic map (
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 1,
         --SLAVE_READY_EN_G    => true, -- local loop
         SLAVE_READY_EN_G    => false, -- pgp enable
         VALID_THOLD_G       => 1,
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         ALTERA_SYN_G        => false,
         ALTERA_RAM_G        => "M9K",
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 9,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 255,
         SLAVE_AXI_CONFIG_G  => SSI_PGP2B_CONFIG_C,
         MASTER_AXI_CONFIG_G => RCEG3_AXIS_DMA_CONFIG_C)
      port map (
         sAxisClk        => pgpClk,
         sAxisRst        => pgpClkRst,
         --sAxisMaster     => tmpRxMaster, -- local loop
         --sAxisSlave      => tmpRxSlave, -- local loop
         sAxisMaster     => pgpRxMastersMuxed, -- pgp enable
         sAxisSlave      => open, -- pgp enable
         sAxisCtrl       => pgpRxCtrl,
         fifoPauseThresh => (others => '1'),
         mAxisClk        => sysClk200,
         mAxisRst        => sysClk200Rst,
         mAxisMaster     => pgpDataRxMaster,
         mAxisSlave      => pgpDataRxSlave);

   U_TxFifo : entity work.AxiStreamFifo
      generic map (
         TPD_G               => TPD_G,
         PIPE_STAGES_G       => 1,
         SLAVE_READY_EN_G    => true,
         VALID_THOLD_G       => 1,
         BRAM_EN_G           => true,
         XIL_DEVICE_G        => "7SERIES",
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => false,
         ALTERA_SYN_G        => false,
         ALTERA_RAM_G        => "M9K",
         CASCADE_SIZE_G      => 1,
         FIFO_ADDR_WIDTH_G   => 9,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 255,
         SLAVE_AXI_CONFIG_G  => RCEG3_AXIS_DMA_CONFIG_C,
         MASTER_AXI_CONFIG_G => SSI_PGP2B_CONFIG_C)
      port map (
         sAxisClk        => sysClk200,
         sAxisRst        => sysClk200Rst,
         sAxisMaster     => pgpDataTxMaster,
         sAxisSlave      => pgpDataTxSlave,
         sAxisCtrl       => open,
         fifoPauseThresh => (others => '1'),
         mAxisClk        => pgpClk,
         mAxisRst        => pgpClkRst,
         mAxisMaster     => muxTxMaster,
         mAxisSlave      => muxTxSlave);


   U_PgpTxMux : entity work.AxiStreamDeMux 
      generic map (
         TPD_G         => TPD_G,
         NUM_MASTERS_G => 4
      ) port map (
         axisClk      => pgpClk,
         axisRst      => pgpClkRst,
         sAxisMaster  => muxTxMaster, 
         sAxisSlave   => muxTxSlave, 
         mAxisMasters => pgpTxMasters, 
         mAxisSlaves  => pgpTxSlaves
         --mAxisMasters => tmpTxMasters,
         --mAxisSlaves  => tmpTxSlaves
      );

   -- Loopback removing PGP
   --tmpRxMaster    <= tmpTxMasters(0);
   --tmpTxSlaves(0) <= tmpRxSlave;
   --tmpTxSlaves(1) <= AXI_STREAM_SLAVE_FORCE_C;
   --tmpTxSlaves(2) <= AXI_STREAM_SLAVE_FORCE_C;
   --tmpTxSlaves(3) <= AXI_STREAM_SLAVE_FORCE_C;

   -- Looping back pgp on itself
   --pgpTxMasters(0) <= pgpRxMastersMuxed;
   --pgpTxMasters(1) <= pgpRxMastersMuxed;
   --pgpTxMasters(2) <= pgpRxMastersMuxed;
   --pgpTxMasters(3) <= pgpRxMastersMuxed;


   --------------------------------------------------
   -- PGP Lane
   --------------------------------------------------

   -- Local Ref Clk 
   U_LocRefClk : IBUFDS_GTE2
      port map(
         O       => locRefClk,
         ODIV2   => open,
         I       => locRefClkP,
         IB      => locRefClkM,
         CEB     => '0'
      );

   -- Buffer for ref clk
   U_RefBug : BUFG
      port map (
         I     => locRefClk,
         O     => locRefClkG
      );
   mmcmLock<=pgpTxMmcmLocked;
   inclk<=locRefClkG;
   pgpclkout<=pgpClk;
   -- PGP Core
   U_Pgp: entity work.Pgp2bGtx7MultiLane
      generic map (
         TPD_G                 => 1 ns,
         -----------------------------------------
         -- GT Settings
         -----------------------------------------
         -- Sim Generics
         SIM_GTRESET_SPEEDUP_G => "FALSE",
         SIM_VERSION_G         => "4.0",
         CPLL_REFCLK_SEL_G     => "001",

         -- 3.125Gbps
         STABLE_CLOCK_PERIOD_G => 4.0E-9,
         CPLL_FBDIV_G          => GTX_CONFIG_C.CPLL_FBDIV_G,       --2,
         CPLL_FBDIV_45_G       => GTX_CONFIG_C.CPLL_FBDIV_45_G,    --5,
         CPLL_REFCLK_DIV_G     => GTX_CONFIG_C.CPLL_REFCLK_DIV_G,  --1,
         RXOUT_DIV_G           => GTX_CONFIG_C.RXOUT_DIV_G,        --1,
         TXOUT_DIV_G           => GTX_CONFIG_C.TXOUT_DIV_G,        --1,
         RX_CLK25_DIV_G        => GTX_CONFIG_C.RX_CLK25_DIV_G,     --10,
         TX_CLK25_DIV_G        => GTX_CONFIG_C.TX_CLK25_DIV_G,     --10,
         RXCDR_CFG_G           => x"03000023ff20400020",           -- Set by wizard
--         PMA_RSV_G             => X"0018480",
--         RXDFEXYDEN_G          => '0',                             -- Set by wizard
         RX_DFE_KL_CFG2_G      => x"301148AC",                     -- Set by wizard
         -- Configure PLL sourc
         TX_PLL_G              => "CPLL",
         RX_PLL_G              => "CPLL",
         -- Configure Number of
         LANE_CNT_G            => 1,

         --CPLL_FBDIV_G          => 5,
         --CPLL_FBDIV_45_G       => 5,
         --CPLL_REFCLK_DIV_G     => 2,
         --RXOUT_DIV_G           => 2,
         --TXOUT_DIV_G           => 2,
         --RX_CLK25_DIV_G        => 10,
         --TX_CLK25_DIV_G        => 10,
         --RXCDR_CFG_G           => x"03000023ff40200020",    -- Set by wizard
         --RX_OS_CFG_G           => "0000010000000",          -- Set by wizard
         --RXDFEXYDEN_G          => '0',                      -- Set by wizard
         --RX_DFE_KL_CFG2_G      => x"3010D90C",              -- Set by wizard

         -- Configure PLL sourc
         --TX_PLL_G              => "CPLL",
         --RX_PLL_G              => "CPLL",
         -- Configure Number of
         --LANE_CNT_G            => 1,
         ----------------------------------------
         -- PGP Settings
         ----------------------------------------
         PAYLOAD_CNT_TOP_G     => 7,  -- Top bit for payload counter
         VC_INTERLEAVE_G       => 0
      ) port map (
         -- GT Clocking
         stableClk        => sysClk200,    -- GT needs a stable clock to "boot up"
         gtCPllRefClk     => locRefClk,    -- Drives CPLL if used
         gtCPllLock       => cpllLock,
         gtQPllRefClk     => '0',          -- Signals from QPLL if used
         gtQPllClk        => '0',  
         gtQPllLock       => '0',  
         gtQPllRefClkLost => '0',  
         gtQPllReset      => open,
         -- Gt Serial IO
         gtTxP(0)         => pgpTxP,
         gtTxN(0)         => pgpTxM,
         gtRxP(0)         => pgpRxP,
         gtRxN(0)         => pgpRxM,
         -- Tx Clocking
         pgpTxReset        => pgpClkRst,
         pgpTxClk          => pgpClk,
         pgpTxRecClk       => txclkout,                            -- recovered clock
         pgpTxMmcmReset    => pgpTxMmcmReset,
         pgpTxMmcmLocked   => pgpTxMmcmLocked,
         -- Rx clocking
         pgpRxReset        => pgpClkRst,
         pgpRxRecClk       => open,         -- recovered clock
         pgpRxClk          => pgpClk,
         pgpRxMmcmReset    => open,
         pgpRxMmcmLocked   => '1',
         -- Non VC Rx Signals
         pgpRxIn           => pgpRxIn,
         pgpRxOut          => pgpRxOut,
         -- Non VC Tx Signals
         pgpTxIn           => pgpTxIn,
         pgpTxOut          => pgpTxOut,
         -- Frame Transmit Interface - 1 Lane, Array of 4 VCs
         pgpTxMasters      => pgpTxMasters,
         pgpTxSlaves       => pgpTxSlaves,
         -- Frame Receive Interface - 1 Lane, Array of 4 VCs
         pgpRxMasters      => open,
         pgpRxMasterMuxed  => pgpRxMastersMuxed,
         pgpRxCtrl         => (others=>pgpRxCtrl)
      );


   -- Register Control
   U_Pgp2bAxi : entity work.Pgp2bAxi
      generic map (
         TPD_G              => TPD_G,
         COMMON_TX_CLK_G    => false,
         COMMON_RX_CLK_G    => false,
         WRITE_EN_G         => true,
         AXI_CLK_FREQ_G     => 125.0E+6,
         STATUS_CNT_WIDTH_G => 32,
         ERROR_CNT_WIDTH_G  => 16
      ) port map (
         pgpTxClk        => pgpClk,
         pgpTxClkRst     => pgpClkRst,
         pgpTxIn         => pgpTxIn,
         pgpTxOut        => pgpTxOut,
         pgpRxClk        => pgpClk,
         pgpRxClkRst     => pgpClkRst,
         pgpRxIn         => pgpRxIn,
         pgpRxOut        => pgpRxOut,
         axilClk         => axiClk,
         axilRst         => axiClkRst,
         axilReadMaster  => axiReadMaster,
         axilReadSlave   => axiReadSlave,
         axilWriteMaster => axiWriteMaster,
         axilWriteSlave  => axiWriteSlave
      );

   -- PLL
   U_PgpClkGen : MMCME2_ADV
      generic map (
         BANDWIDTH            => "OPTIMIZED",
         CLKOUT4_CASCADE      => FALSE,
         COMPENSATION         => "ZHOLD",
         STARTUP_WAIT         => FALSE,
         DIVCLK_DIVIDE        => 1,
         CLKFBOUT_MULT_F      => 5.0,
         CLKFBOUT_PHASE       => 0.000,
         CLKFBOUT_USE_FINE_PS => FALSE,
         CLKOUT0_DIVIDE_F     => 8.000,
         CLKOUT0_PHASE        => 0.000,
         CLKOUT0_DUTY_CYCLE   => 0.5,
         CLKOUT0_USE_FINE_PS  => FALSE,
         CLKOUT1_DIVIDE       => 5,
         CLKOUT1_PHASE        => 0.000,
         CLKOUT1_DUTY_CYCLE   => 0.5,
         CLKOUT1_USE_FINE_PS  => FALSE,
         CLKOUT2_DIVIDE       => 8,
         CLKOUT2_PHASE        => 0.000,
         CLKOUT2_DUTY_CYCLE   => 0.5,
         CLKOUT2_USE_FINE_PS  => FALSE,
         CLKIN1_PERIOD        => 4.0,
         REF_JITTER1          => 0.010

         --BANDWIDTH            => "OPTIMIZED",
         --CLKOUT4_CASCADE      => FALSE,
         --COMPENSATION         => "ZHOLD",
         --STARTUP_WAIT         => FALSE,
         --DIVCLK_DIVIDE        => 8,
         --CLKFBOUT_MULT_F      => 31.875,
         --CLKFBOUT_PHASE       => 0.000,
         --CLKFBOUT_USE_FINE_PS => FALSE,
         --CLKOUT0_DIVIDE_F     => 6.375,
         --CLKOUT0_PHASE        => 0.000,
         --CLKOUT0_DUTY_CYCLE   => 0.5,
         --CLKOUT0_USE_FINE_PS  => FALSE,
         --CLKOUT1_DIVIDE       => 5,
         --CLKOUT1_PHASE        => 0.000,
         --CLKOUT1_DUTY_CYCLE   => 0.5,
         --CLKOUT1_USE_FINE_PS  => FALSE,
         --CLKOUT2_DIVIDE       => 8,
         --CLKOUT2_PHASE        => 0.000,
         --CLKOUT2_DUTY_CYCLE   => 0.5,
         --CLKOUT2_USE_FINE_PS  => FALSE,
         --CLKIN1_PERIOD        => 4.0,
         --REF_JITTER1          => 0.010

      )
      port map (
         CLKFBOUT             => pgpFbClk,
         CLKFBOUTB            => open,
         CLKOUT0              => ipgpClk,
         CLKOUT0B             => open,
         CLKOUT1              => open,
         CLKOUT1B             => open,
         CLKOUT2              => open,
         CLKOUT2B             => open,
         CLKOUT3              => open,
         CLKOUT3B             => open,
         CLKOUT4              => open,
         CLKOUT5              => open,
         CLKOUT6              => open,
         CLKFBIN              => pgpFbClk,
         CLKIN1               => locRefClkG,
         CLKIN2               => '0',
         CLKINSEL             => '1',
         DADDR                => (others => '0'),
         DCLK                 => '0',
         DEN                  => '0',
         DI                   => (others => '0'),
         DO                   => open,
         DRDY                 => open,
         DWE                  => '0',
         PSCLK                => '0',
         PSEN                 => '0',
         PSINCDEC             => '0',
         PSDONE               => open,
         LOCKED               => pgpTxMmcmLocked,
         CLKINSTOPPED         => open,
         CLKFBSTOPPED         => open,
         PWRDWN               => '0',
         RST                  => axiClkRst
      );

   -- Clock Buffer
   U_pgpClkBuf : BUFG
      port map (
         I     => ipgpClk,
         O     => pgpClk
      );

   -- Reset Gen
   U_pgpClkRstGen : entity work.RstSync
      generic map (
         TPD_G           => 1 ns,
         IN_POLARITY_G   => '1',
         OUT_POLARITY_G  => '1',
         RELEASE_DELAY_G => 16
      )
      port map (
        clk      => pgpClk,
        asyncRst => axiClkRst,
        syncRst  => pgpClkRst
      );

end architecture STRUCTURE;

