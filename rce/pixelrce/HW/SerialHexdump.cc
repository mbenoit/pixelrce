#include "HW/SerialHexdump.hh"
#include "HW/BitStream.hh"
#include <stdio.h>
#include <iostream>




SerialHexdump::SerialHexdump(): SerialIF() {
  m_flog=fopen("dbg.txt","w+");
}


void SerialHexdump::Send(BitStream* bs, int opt){
    char txt[28];
    if(!m_flog) return;
    //  fprintf(m_flog,"sendWaitX\n");
    //      std::cout<<"sendWaitX"<<std::endl;
  for (unsigned int i=0;i<bs->size();i++){
    if(opt & SerialIF::BYTESWAP){
      unsigned tmp=(*bs)[i];
      unsigned tmp2 = ((tmp&0xff)<<24) | ((tmp&0xff00)<<8) |
        ((tmp&0xff0000)>>8) | ((tmp&0xff000000)>>24);
      sprintf(txt,"%x\n",tmp2);
    } else{
      sprintf(txt,"%x\n",(*bs)[i]);
    }
    fprintf(m_flog,txt);
    //    std::cout<<txt;
  }
  if((opt&SerialIF::DONT_CLEAR)==0)bs->clear();
}
void SerialHexdump::SetChannelInMask(unsigned linkmask){
}
void SerialHexdump::SetChannelOutMask(unsigned linkmask){
}
int SerialHexdump::EnableTrigger(bool on){
  return 0;
}
unsigned SerialHexdump::SendCommand(unsigned char opcode) {
  if(opcode==77)fclose(m_flog);
  std::cout<<"Opcode: "<<(unsigned)opcode<<std::endl;
  return 0;
}
unsigned SerialHexdump::WriteRegister(unsigned addr, unsigned val){
  std::cout<<"Address: "<<addr<<" Value: "<<val<<std::endl;
  return 0;
}
unsigned SerialHexdump::ReadRegister(unsigned addr, unsigned &val){
  std::cout<<"Address: "<<addr<<" setting to 55"<<std::endl;
  val=55;
  return 0;
}
unsigned SerialHexdump::WriteBlockData(std::vector<unsigned>& data){
  //std::cout<<"Block data"<<std::endl;
  for (size_t i=0;i<data.size();i++){
    // std::cout<<"Word "<<i<<": "<<data[i]<<std::endl;
    fprintf(m_flog, "%x\n", data[i]);
  }
  return 0;
}
unsigned SerialHexdump::ReadBlockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec){
  //std::cout<<"Block data"<<std::endl;
  for (size_t i=0;i<data.size();i++){
  fprintf(m_flog, "%x\n", data[i]);
  //  std::cout<<"Word "<<i<<": "<<data[i]<<std::endl;
  }
  //  for (int i=0;i<1000;i++)retvec.push_back(i);
  return 0;
}

