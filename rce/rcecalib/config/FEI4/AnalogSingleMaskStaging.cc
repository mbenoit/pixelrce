#include "rcecalib/config/FEI4/AnalogSingleMaskStaging.hh"
#include "rcecalib/config/FEI4/Module.hh"

namespace FEI4{
  
  AnalogSingleMaskStaging::AnalogSingleMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
  }
  
  void AnalogSingleMaskStaging::setupMaskStageHW(int maskStage){
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    GlobalRegister* global=m_module->global();
    global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
    global->setField("Colpr_Addr", maskStage, GlobalRegister::HW); 
  }
}
