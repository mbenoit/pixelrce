#ifndef RCE_HISTO2D_HH
#define RCE_HISTO2D_HH

#include "util/AbsRceHisto.hh"

class Provider;


template<typename T, typename TE> class RceHisto2d: public AbsRceHisto{
public:
  RceHisto2d(std::string name, std::string title, unsigned int nbin, float xmin, float xmax,    //! Constructor 2D
	     unsigned int nbin2, float xmin2, float xmax2, bool isreversed=false, bool hasErrors=false);
  RceHisto2d(const RceHisto2d<T, TE> &h);                                                                      //! Copy contructor
  virtual ~RceHisto2d();
  RceHisto2d &operator=(const RceHisto2d<T, TE>&h);
  T operator()(unsigned i, unsigned j);
  TE getBinError(unsigned i, unsigned j);
  void setBinError(unsigned int i, unsigned j, TE val);
  void setBinErrorFast(unsigned int i, unsigned j, TE val);
  void set(unsigned int i, unsigned j, T val);                 //! Bin write access 2d
  void setFast(unsigned int i, unsigned j, T val);                 //! Bin write access 2d
  void fill(unsigned int i, unsigned j, T val);
  void fillFast(unsigned int i, unsigned j, T val);
//  void fillByValue(float x, float y, T val);
  void increment(unsigned int i, unsigned j);
  void incrementFast(unsigned int i, unsigned j);
  void clear();
  void publish(Provider* p);
private:
  int index(int x, int y);
  T *m_data;
  TE *m_err;
  bool m_hasErrors;
  bool m_reverse;
};


#endif
