#ifndef SETUPTRIGGERACTION_HH
#define SETUPTRIGGERACTION_HH


#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/config/ConfigIF.hh"
#include <boost/property_tree/ptree.hpp>
#include <string>
#include "rcecalib/util/exceptions.hh"

class SetupTriggerAction: public LoopAction{
public:
  SetupTriggerAction(std::string name, ConfigIF* cif, boost::property_tree::ptree *pt):
    LoopAction(name),m_configIF(cif){
    try{
      m_interval=pt->get<int>("trigOpt.eventInterval");
      m_l1delay = pt->get<int>("trigOpt.CalL1ADelay");
      m_triggermask = pt->get<int>("trigOpt.triggerMask");
      m_repetitions = pt->get<int>("trigOpt.nTriggers");
      m_deadtime = pt->get<int>("trigOpt.deadtime");
      m_hitbusconfig = pt->get<int>("trigOpt.hitbusConfig");
      m_protectFifo = pt->get("trigOpt.optionsMask.PROTECT_FIFO", 0);
    }
    catch(boost::property_tree::ptree_bad_path ex){
      rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
      ers::error(issue);
    }
  }
  int execute(int i){
    std::cout<<"Configuring trigger with interval="<<std::dec<<m_interval<<", l1 delay="<<m_l1delay<<
      ", number of repetitions="<<m_repetitions<<", triggermask="<<std::hex<<m_triggermask<<
      ", hitbus config="<<m_hitbusconfig<<", protectFifo="<<m_protectFifo<<std::dec<<std::endl;
    m_configIF->writeHWregister(9,(unsigned)m_interval); //period
    m_configIF->writeHWregister(14,(unsigned)m_repetitions); //n-1 events only
    m_configIF->writeHWregister(8,(unsigned)m_l1delay); 
    m_configIF->writeHWregister(15,(unsigned)m_deadtime); 
    //m_configIF->writeHWregister(21,(unsigned)m_hitbusconfig); // per RCE
    m_configIF->writeHWregister(26,(unsigned)m_protectFifo); 
    m_configIF->writeHWregister(11,(unsigned)m_triggermask); //trigger mask 2=cyclic
    return 0;
  }
private: 
  ConfigIF* m_configIF;
  int m_interval;
  int m_l1delay;
  int m_triggermask;
  int m_repetitions;
  int m_deadtime;
  int m_hitbusconfig;
  int m_protectFifo;
};

#endif
