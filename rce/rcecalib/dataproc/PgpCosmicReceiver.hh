#ifndef PGPCOSMICRECEIVER_HH
#define PGPCOSMICRECEIVER_HH

#include "rcecalib/dataproc/AbsReceiver.hh"
#include "rcecalib/HW/Receiver.hh"
#include <iostream>
#include "namespace_aliases.hh"

class PgpCosmicReceiver: public AbsReceiver, public PgpTrans::Receiver{
public:
  PgpCosmicReceiver(AbsDataHandler* handler);
  virtual ~PgpCosmicReceiver(){
    std::cout<<"Received "<<std::dec<<m_counter<<" buffers in previous run."<<std::endl;
    delete [] m_buffer;
  }
  void receive(PgpTrans::PgpData *pgpdata);
private:
  unsigned* m_buffer;
  unsigned m_counter;
};
#endif
