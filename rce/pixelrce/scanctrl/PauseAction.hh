#ifndef PAUSEACTION_HH
#define PAUSEACTION_HH

#include "scanctrl/LoopAction.hh"
#include "scanctrl/Scan.hh"

class PauseAction: public LoopAction{
public:
  PauseAction(std::string name, Scan* scan):
    LoopAction(name),m_scan(scan){}
  int execute(int i){
    //std::cout<<"Pausing scan"<<std::endl;
    return m_scan->pause();
    //std::cout<<"Done pausing scan"<<std::endl;
  }
private:
  Scan* m_scan;
};

#endif
