#ifndef CHANGEBINACTION_HH
#define CHANGEBINACTION_HH

#include "scanctrl/LoopAction.hh"
#include "dataproc/AbsDataProc.hh"

class ChangeBinAction: public LoopAction{
public:
  ChangeBinAction(std::string name, AbsDataProc* proc):
    LoopAction(name),m_dataProc(proc){}
  int execute(int i){
    //std::cout<<"Changed bin"<<std::endl;
    return m_dataProc->changeBin(i);
  }
private:
  AbsDataProc* m_dataProc;
};

#endif
