#ifndef PIXEL_REGISTER_FEI3_HH
#define PIXEL_REGISTER_FEI3_HH

#include <string>
#include <map>


namespace FEI3{

  static const unsigned fieldPos[]={0,1,2,3,10,13};
  static const unsigned fieldMask[]={1,1,1,0x7f,0x7,1};

  class PixelRegister{
  public:
    enum Field{hitbus, select, enable, tdac, fdac, kill, Nfields, not_found};
    PixelRegister(): m_register(0){
      if(!m_initialized)initialize();
    }
    unsigned getBit(unsigned i){
      return (m_register>>i)&0x1;
    }
    void setBit(unsigned i, bool val){
      if(val){
	m_register|=(1<<i);
      } else{
	m_register&=~(1<<i);
      }
    }
    void setField(Field f, unsigned val){
      m_register&=~(fieldMask[f]<<fieldPos[f]); //clear
      m_register|=(fieldMask[f]&val)<<fieldPos[f]; //set
    }
    unsigned getField(Field f){
      return (m_register>>fieldPos[f])&fieldMask[f];
    }
    unsigned short get(){
      return m_register;
    }
    void set(unsigned short val){
      m_register=val;
    }

    static Field lookupParameter(const char* name);
    
  private:
    unsigned short m_register;
    static std::string m_cachedName; 
    static Field m_cachedField;
    static std::map<std::string, Field> m_parameters;
    static bool m_initialized;
    static void initialize();
  };

};

#endif
