#ifndef RCEPRODUCER_HH
#define RCEPRODUCER_HH

#include "eudaq/Producer.hh"

#include "rcecalib/server/IPCController.hh"

class RceProducer : public eudaq::Producer {
public:
  RceProducer(const std::string & name, const std::string & runcontrol, IPCPartition& p) 
    : eudaq::Producer(name, runcontrol), m_run(0), m_PlaneMask(0), m_conseq(1), m_controller(new IPCController(p)),
      m_options(0){std::cout<<"Rce producer constructor "<< m_options<<std::endl;}
  virtual ~RceProducer(){
    delete m_controller;
  }
  virtual void OnConfigure(const eudaq::Configuration & config) ;
  virtual void OnStartRun(unsigned param) ;
  virtual void OnStopRun() ;
  virtual void OnTerminate() ;

private:
  unsigned m_run;
  unsigned m_PlaneMask;
  unsigned m_conseq;
  IPCController* m_controller;
  ipc::ScanOptions* m_options;
};

#endif
