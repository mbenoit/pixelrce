build_date:=$(shell  date +%Y/%m/%d-%H:%M:%S)
#ugly hack to link ers streams
ifeq ($(TDAQ_VERSION),4)
ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)
extract_archive:=$(shell cd $(RELEASE)/build/rcecalib/obj/$(tgt_arch)/server; $(AR)  x $(TDAQC_INST_PATH)/$(tgt_arch)/lib/libErsBaseStreams.a)
endif
endif
CPPFLAGS+=-DBUILDDATE=\"$(build_date)\" -DBUILDUSER=\"$(USER)\"

ifeq ($(RCE_CORE_VERSION),2.2)
bootloader_cc:=bootloader_2.2.cc
rtems_config_cc:=rtems_config_2.2.cc
rce_service:=tool/exception  
rce_net:= oldPpi/ethernet oldPpi/bsdNet oldPpi/net  
else
bootloader_cc:=bootloader.cc
rtems_config_cc:=rtems_config.cc
rce_service:=rce/rceservice
rce_net:=rce/rcenet
endif
CPPFLAGS+=-I$(RELEASE_DIR)/rcecalib

ifdef RCE_INTERFACE
CPPFLAGS+= '-DRCE_INTERFACE=$(RCE_INTERFACE)'
else 
CPPFLAGS+= '-DRCE_INTERFACE=0'
endif
ifdef ELOG
CPPFLAGS+= '-DELOG'
endif
ifdef SQLDB
CPPFLAGS+= '-DSQLDB'
endif
ifdef SQLDBDBG
CPPFLAGS+= '-DSQLDBDBG'
endif
ifeq ($(profiler),y)
CPPFLAGS+='-D__PROFILER_ENABLED__'
profiler_lib=rcecalib/profiler
endif

ifneq ($(findstring linux,$(tgt_os)),)
libnames := ipcclient ipcrootclient rcecontrol
endif
libsrcs_ipcclient:=  IPCController.cc \
                     TurboDaqFile.cc \
                     FEI4AConfigFile.cc \
                     FEI4BConfigFile.cc \
                     HitbusConfigFile.cc \
                     AFPHPTDCConfigFile.cc \
                     PixScan.cc \
		     PrimList.cc \
                     IPCRegularCallback.cc 

libincs_ipcclient := \
		rcecalib 


libsrcs_rcecontrol := RceControl.cc
libincs_rcecontrol : = rcecalib
tgtslibs_rcecontrol: = $(rce_service) $(rce_net)

libsrcs_ipcrootclient:=  IPCHistoController.cc
libincs_ipcrootclient := \
		rcecalib 

ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)


tgtnames := bootloader #calibserver eudaqserver 

# need to fix this
ifneq ($(RCE_CORE_VERSION),2.2)
tgtnames += calibserver
endif

CPPFLAGS += -DNFSDIR=\"/nfsexport/home/$(USER)\"
ifeq ($(TDAQ_VERSION),4)
LXFLAGS +=  $(RELEASE_DIR)/build/rcecalib/obj/$(tgt_arch)/server/ThrottleStream.o  $(RELEASE_DIR)/build/rcecalib/obj/$(tgt_arch)/server/StandardStream.o  
endif
LXFLAGS+=-L$(RELEASE_DIR)/build/rcecalib/modlib/$(tgt_arch)
ifeq ($(RCE_CORE_VERSION),2.2)
LXFLAGS += -u  _ZTIN7service3fci9ExceptionE  -u pthread_attr_destroy -u sem_trywait -u _ZTIi -u sem_getvalue -u pthread_getspecific -u __ltsf2 -u pthread_mutex_trylock -u clearerr -u _ZTIt -u pipe -u __mulsf3 -u toupper -u pthread_cond_wait -u __floatsisf -u _ZN5boost6thread4joinEv -u __floatundisf -u _ZN3ers11LocalStream5fatalERKNS_5IssueE -u pthread_cond_destroy -u tolower -u pthread_getschedparam -u _ZTIN5boost6detail16thread_data_baseE -u _ZN5boost11this_thread5sleepERKNS_10posix_time5ptimeE -u _ZN5boost6thread12start_threadEv -u sem_init -u isalnum -u pthread_cond_signal -u feof -u pthread_setspecific -u _ZTVN5boost6detail16thread_data_baseE -u sem_post -u sem_wait -u _ZN3ers11LocalStream7warningERKNS_5IssueE -u pthread_cond_broadcast -u _ZN5boost6thread9interruptEv -u _ZTIj -u ferror -u _ZN5boost6threadD1Ev -u sched_yield -u __gtsf2 -u sched_get_priority_min -u freeifaddrs -u _ZN3ers11LocalStream8instanceEv -u pthread_detach -u pthread_setschedparam -u sem_destroy -u _ZN5boost6detail16thread_data_baseD2Ev -u _ZN3ers11LocalStream5errorERKNS_5IssueE -u pthread_cond_init -u getifaddrs -u isspace -u pthread_attr_getstacksize -u pthread_cond_timedwait -u pthread_key_create -u pthread_mutexattr_init -u pthread_join -u sched_get_priority_max -u  _ZNK5boost9gregorian10greg_month15as_short_stringEv
else
LXFLAGS +=    -u pthread_attr_destroy -u _ZN3ers13Configuration15verbosity_levelEi -u _ZN3ers5IssueD2Ev -u sem_trywait -u _ZTIi -u _ZN3ers5Issue15prepend_messageERKSs -u sem_getvalue -u pthread_getspecific -u sem_post -u sem_wait -u __lesf2 -u _ZN3ers11LocalStream7warningERKNS_5IssueE -u pthread_mutex_trylock -u clearerr -u pthread_cond_broadcast -u _ZN3ers13StreamManager3logERKNS_5IssueE -u _ZTIj -u ferror -u __eqsf2 -u _ZN3ers13StreamManager8instanceEv -u sched_yield -u pthread_mutex_destroy -u pthread_mutex_init -u _ZN3ers13Configuration8instanceEv -u sched_get_priority_min -u _ZTIt -u freeifaddrs -u pipe -u __mulsf3 -u _ZN3ers5IssueC2ERKNS_7ContextERKSs -u pthread_mutex_lock -u _ZN3ers11LocalStream8instanceEv -u _ZTIm -u pthread_detach -u _ZN3ers13StreamManager12report_issueENS_8severityERKNS_5IssueE -u _ZTIl -u _ZN3ers12IssueFactory14register_issueERKSsPFPNS_5IssueERKNS_7ContextEE -u _ZN3ers12LocalContext9c_processE -u sem_destroy -u pthread_setschedparam -u pthread_cond_wait -u __floatsisf -u pthread_mutex_unlock -u strtoll -u _ZN3ers11LocalStream5errorERKNS_5IssueE -u pthread_cond_init -u getifaddrs -u __floatundisf -u _ZN3ers11LocalStream5fatalERKNS_5IssueE -u isspace -u pthread_cond_destroy -u __moddi3 -u pthread_attr_getstacksize -u tolower -u pthread_getschedparam -u _ZN3ers12IssueFactory8instanceEv -u __divdi3 -u _ZN3erslsERSoRKNS_5IssueE -u pthread_cond_timedwait -u _ZN3ers5IssueC2ERKNS_7ContextERKSt9exception -u _ZTIh -u _ZN3ers5IssueC2ERKS0_ -u select -u _ZN3ers13StreamManager5debugERKNS_5IssueEi -u _ZTIN3ers5IssueE -u pthread_key_create -u sem_init -u _ZN3ers12LocalContextC1EPKcS2_iS2_ -u pthread_mutexattr_init -u isalnum -u pthread_join -u pthread_cond_signal -u feof -u sched_get_priority_max -u pthread_setspecific -u malloc_report_statistics  -L$(RELEASE_DIR)/build/rcecalib/modlib/$(tgt_arch) 
endif



tgtsrcs_bootloader := $(bootloader_cc) $(rtems_config_cc) EthPrimitive.cc CmdDecoder.cc

tgtincs_bootloader := 
ifeq ($(RCE_CORE_VERSION),2.2)
tgtlibs_bootloader := \
    oldPpi/bsdNet \
    oldPpi/net \
    service/fci \
    service/shell \
    service/dynalink \
    service/debug \
    service/logger \
    tool/concurrency \
    tool/exception \
    tool/io \
    tool/string \
    tool/time \
    oldPpi/ethernet \
    oldPpi/init \
    oldPpi/pic \
    oldPpi/pgp
else
tgtlibs_bootloader := rce/rceshell \
    $(rce_service) \
    $(rce_net) \
    rce/rcefci \
    rce/rcedebug \
    rce/rcepic \
    rce/rcepgp \
    rce/rceethernet \
    rce/rcebsdnet \
    rce/rceinit \
    rce/dynalink \
    rce/gdbstub \
    rceusr/rceusrinit \
    rceusr/rcezcpnet 
endif
ifeq ($(RCE_CORE_VERSION),2.2)
tgtslib_bootloader:=  $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(ers_lib) \
    $(RTEMS_DIR)/nfs \
    $(RTEMS_DIR)/rtemscpu \
    $(RTEMS_DIR)/telnetd \
    $(RTEMS_DIR)/rtemsbsp \
    $(RTEMS_DIR)/rtems-gdb-stub 
else 
tgtslib_bootloader:=  $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(ers_lib) \
    $(RTEMS_DIR)/nfs \
    $(RTEMS_DIR)/rtemscpu \
    $(RTEMS_DIR)/telnetd
endif
managrs_bootloader := io event msg sem ext region
#===================================================================

tgtsrcs_calibserver :=  runserver.cc $(rtems_config_cc) calibserver.cc 
tgtincs_calibserver := \
		rcecalib 

tgtlibs_calibserver := rce/rceshell \
    $(rce_service) \
    $(rce_net) \
    rce/rcefci \
    rce/rcedebug \
    rce/rcepic \
    rce/rcepgp \
    rce/rceethernet \
    rce/rcebsdnet \
    rce/rceinit \
    rce/dynalink \
    rce/gdbstub \
    rceusr/rceusrinit \
    rceusr/rcezcpnet 

tgtslib_calibserver += \
    $(RTEMS_DIR)/nfs \
    $(RTEMS_DIR)/rtemscpu \
    $(RTEMS_DIR)/telnetd \
    $(z_lib) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(ipc_lib)	\
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(rdb_lib) \
    $(is_lib) \
    $(oh_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(boost_regex_lib) \
    rcecalib/eudaq \
    rcecalib/util \
    rcecalib/HW \
    rcecalib/dataproc \
    rcecalib/scanctrl \
    rcecalib/idl \
    rcecalib/rceconfig \
    $(profiler_lib)



managrs_calibserver := io event msg sem ext region

MLXFLAGS += -mlongcall
LXFLAGS += -mlongcall

modnames := calibservermod # eudaqservermod
#=========================================================================================
ifeq ($(RCE_CORE_VERSION),2.2)
majorv_calibservermod := 2
minorv_calibservermod := 2
else 
majorv_calibservermod := 1
minorv_calibservermod := 0
endif
branch_calibservermod := prod
modsrcs_calibservermod :=  runservermod.cc calibserver.cc 
modincs_calibservermod := \
		rcecalib 

modlibs_calibservermod :=  \
    $(profiler_lib) \
    rcecalib/util \
    rcecalib/HW \
    rcecalib/dataproc \
    rcecalib/scanctrl \
    rcecalib/idl \
    rcecalib/rceconfig \
    rcecalib/eudaq 

TDAQ_DIR=$(RELEASE_DIR)/build/tdaq/lib/$(tgt_arch)
modslib_calibservermod := \
    $(z_lib) \
    $(omnithread_lib) \
    $(ipc_lib) \
    $(rdb_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(boost_regex_lib)\
    $(omniorb_lib) \
    $(z_lib) \
    $(oh_lib) \
    $(owl_lib)  


#=========================================================================================
MLXFLAGS += -mlongcall
majorv_eudaqservermod := 1
minorv_eudaqservermod := 0
branch_eudaqservermod := prod
modsrcs_eudaqservermod :=  runeudaqservermod.cc eudaqserver.cc RceProducer.cc
modincs_eudaqservermod := \
		rcecalib 

modlibs_eudaqservermod :=  \
    $(profiler_lib) \
    rcecalib/util \
    rcecalib/HW \
    rcecalib/dataproc \
    rcecalib/scanctrl \
    rcecalib/idl \
    rcecalib/rceconfig \
    rcecalib/ipcclient \
    rcecalib/eudaq 

TDAQ_DIR=$(RELEASE_DIR)/build/tdaq/lib/$(tgt_arch)
modslib_eudaqservermod := \
    $(z_lib) \
    $(omnithread_lib) \
    $(ipc_lib) \
    $(rdb_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(boost_regex_lib)\
    $(omniorb_lib) \
    $(z_lib) \
    $(oh_lib) \
    $(owl_lib) 




    

#--------------------------------------------
tgtsrcs_eudaqserver := runeudaqserver.cc $(rtems_config_cc) eudaqserver.cc RceProducer.cc 
tgtincs_eudaqserver := \
		rcecalib 

tgtlibs_eudaqserver := rce/rceshell \
    $(rce_service) \
    $(rce_net) \
    rce/rcefci \
    rce/rcedebug \
    rce/rcepic \
    rce/rcepgp \
    rce/rceethernet \
    rce/rcebsdnet \
    rce/rceinit \
    rce/dynalink \
    rce/gdbstub \
    rceusr/rceusrinit \
    rceusr/rcezcpnet \
    rcecalib/util \
    rcecalib/HW \
    rcecalib/dataproc \
    rcecalib/scanctrl \
    rcecalib/idl \
    rcecalib/rceconfig \
    rcecalib/eudaq \
    rcecalib/ipcclient \
    $(omniorb_lib)

tgtslib_eudaqserver := \
    $(RTEMS_DIR)/nfs \
    $(RTEMS_DIR)/rtemscpu \
    $(RTEMS_DIR)/z \
    $(RTEMS_DIR)/telnetd \
    $(z_lib) \
    $(profiler_lib) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(omnithread_lib) \
    $(boost_regex_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) 
managrs_eudaqserver := io event msg sem ext region

endif


ifneq ($(findstring linux,$(tgt_os)),)

LXFLAGS +=-pthread -lm -ldl -rdynamic 
rootlibs:= \
    $(RELEASE_DIR)/build/root/lib/Gui \
    $(RELEASE_DIR)/build/root/lib/Thread \
    $(RELEASE_DIR)/build/root/lib/MathCore \
    $(RELEASE_DIR)/build/root/lib/Physics \
    $(RELEASE_DIR)/build/root/lib/Matrix \
    $(RELEASE_DIR)/build/root/lib/Postscript \
    $(RELEASE_DIR)/build/root/lib/Rint \
    $(RELEASE_DIR)/build/root/lib/Tree \
    $(RELEASE_DIR)/build/root/lib/Gpad \
    $(RELEASE_DIR)/build/root/lib/Graf3d \
    $(RELEASE_DIR)/build/root/lib/Graf \
    $(RELEASE_DIR)/build/root/lib/Hist \
    $(RELEASE_DIR)/build/root/lib/Net \
    $(RELEASE_DIR)/build/root/lib/RIO \
    $(RELEASE_DIR)/build/root/lib/Cint \
    $(RELEASE_DIR)/build/root/lib/Core

tgtnames := host_bootloader \
            calibGui \
            cosmicGui \
            rebootHSIO \
	    cosmicMonitoringGui \
            test_sctrl_server  \
            rceOfflineProducer  \
            sendBitStream \
            sendRandomPattern \
            calibclient \
            tdccalib \
            delaycalib \
            dumpRceMod dumpRceScan

tgtsrcs_calibclient := calibclient.cc

tgtincs_calibclient := \
		rcecalib 

tgtlibs_calibclient := \
    rcecalib/idl \
    rcecalib/ipcclient \
    rcecalib/ipcrootclient \
 

tgtslib_calibclient := \
    dl \
    $(z_lib) \
    $(rootlibs) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(rdb_lib) \
    $(is_lib) \
    $(oh_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib) \
    $(oh_root_lib) 

#--------------------------------------------

tgtsrcs_tdccalib := tdccalib.cc 

tgtincs_tdccalib := \
		rcecalib 

tgtlibs_tdccalib := \
    rcecalib/idl \
    rcecalib/ipcclient

tgtslib_tdccalib := \
    dl \
    $(rootlibs) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(owl_lib) \
    $(ers_lib) \
    rcecalib/idl \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(oh_root_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib) 
#--------------------------------------------
tgtsrcs_delaycalib := delaycalib.cc

tgtincs_delaycalib := \
		rcecalib 

tgtlibs_delaycalib := \
    rcecalib/idl \
    rcecalib/ipcclient \
    rcecalib/ipcrootclient 
  
tgtslib_delaycalib := \
    dl \
    $(rootlibs) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(oh_root_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib)
#--------------------------------------------

GUIHEADERSDEP = CosmicGui.hh ConfigGui.hh CalibGui.hh ScanGui.hh PrimListGui.hh CosmicMonitoringGui.hh \
                CosmicDataReceiver.hh CosmicMonitoringGuiEmbedded.hh GlobalConfigGui.hh

tgtsrcs_cosmicGui := CosmicGui.cc \
                     ConfigGui.cc \
                     GlobalConfigGui.cc \
                     ScanLog.cc \
                     cosmicGui_rootDict.cc \
                     CosmicDataReceiver.cc  \
                     CosmicOfflineDataProc.cc \
                     CosmicMonitoringGuiEmbedded.cc

guiheaders_cosmicGui := CosmicGui.hh ConfigGui.hh CosmicMonitoringGuiEmbedded.hh GlobalConfigGui.hh



tgtincs_cosmicGui := \
		rcecalib \
                rcecalib/server 

tgtlibs_cosmicGui := \
    rcecalib/idl \
    rcecalib/eudaq \
    rcecalib/rceconfig \
    rcecalib/dataproc \
    rcecalib/HW \
    rcecalib/util \
    rcecalib/ipcclient

tgtslib_cosmicGui := \
    dl\
    $(z_lib) \
    $(rootlibs) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(omniorb_lib)\
    $(omnithread_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(oh_root_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib) 

ifdef SQLDB
    tgtslib_cosmicGui +=$(mysql_lib) 
endif
#--------------------------------------------

tgtsrcs_dumpRceMod := dumpRceMod.cc
tgtslib_dumpRceMod :=  dl\
    $(z_lib) \
    $(rootlibs) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(oh_root_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib)
tgtincs_dumpRceMod := \
		rcecalib \
		rcecalib/server 
tgtlibs_dumpRceMod := \
    rcecalib/idl \
    rcecalib/ipcclient

tgtsrcs_dumpRceScan := dumpRceScan.cc
tgtslib_dumpRceScan :=  dl\
    $(z_lib) \
    $(rootlibs) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(oh_root_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib)
tgtincs_dumpRceScan := \
		rcecalib \
		rcecalib/server 
tgtlibs_dumpRceScan := \
    rcecalib/idl \
    rcecalib/ipcclient

tgtsrcs_calibGui := CalibGui.cc  \
                    ConfigGui.cc \
                    GlobalConfigGui.cc \
                    ScanGui.cc \
		    PrimListGui.cc \
                    DataExporter.cc \
                    ScanLog.cc \
                    IPCGuiCallback.cc  \
                    CallbackInfo.cc  \
                    calibGui_rootDict.cc 

guiheaders_calibGui := CalibGui.hh ScanGui.hh PrimListGui.hh ConfigGui.hh GlobalConfigGui.hh



tgtincs_calibGui := \
		rcecalib \
		rcecalib/server 

tgtlibs_calibGui := \
    rcecalib/idl \
    rcecalib/analysis \
    rcecalib/ipcrootclient \
    rcecalib/ipcclient

tgtslib_calibGui := \
    dl\
    $(z_lib) \
    $(rootlibs) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(oh_root_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib)

ifdef SQLDB
    tgtslib_calibGui +=$(mysql_lib) 
endif
#--------------------------------------------
tgtsrcs_cosmicMonitoringGui := CosmicMonitoringGui.cc  cosmicMonitoringGui_rootDict.cc  CosmicMonitoringGuiEmbedded.cc ConfigGui.cc

guiheaders_cosmicMonitoringGui := CosmicMonitoringGui.hh CosmicMonitoringGuiEmbedded.hh ConfigGui.hh


tgtincs_cosmicMonitoringGui := \
		rcecalib \
                rcecalib/server 



tgtlibs_cosmicMonitoringGui := \
    rcecalib/idl \
    rcecalib/eudaq \
    rcecalib/rceconfig \
    rcecalib/dataproc \
    rcecalib/HW \
    rcecalib/util \
    rcecalib/ipcclient



tgtslib_cosmicMonitoringGui := \
    dl\
    $(z_lib) \
    $(rootlibs) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(omniorb_lib)\
    $(omnithread_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(oh_root_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib)

#--------------------------------------------

tgtsrcs_test_sctrl_server := test_server.cc
tgtincs_test_sctrl_server := \
		rcecalib 

tgtlibs_test_sctrl_server := \
    rcecalib/eudaq \
    $(rce_service) \
    $(rce_net) \
    rcecalib/util \
    rcecalib/HW \
    rcecalib/dataproc \
    rcecalib/scanctrl \
    rcecalib/idl \
    rcecalib/rceconfig 


tgtslib_test_sctrl_server := \
    dl \
    $(z_lib) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(tdaq_cmdline_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(boost_regex_lib)
#--------------------------------------------

tgtsrcs_rceOfflineProducer := RceOfflineProducer.cc \
                              CosmicDataReceiver.cc \
                              rceOfflineProducer_rootDict.cc \
                              CosmicMonitoringGuiEmbedded.cc \
			      ConfigGui.cc \
	                      CosmicOfflineDataProc.cc

guiheaders_rceOfflineProducer := ConfigGui.hh CosmicMonitoringGuiEmbedded.hh

tgtincs_rceOfflineProducer := \
		rcecalib \
                rcecalib/server

tgtlibs_rceOfflineProducer := \
    rcecalib/eudaq \
    rcecalib/ipcclient \
    $(rce_service) \
    $(rce_net) \
    rcecalib/util \
    rcecalib/HW \
    rcecalib/dataproc \
    rcecalib/scanctrl \
    rcecalib/idl \
    rcecalib/rceconfig 


tgtslib_rceOfflineProducer := \
    dl \
    $(z_lib) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(rootlibs) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(tdaq_cmdline_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(boost_regex_lib)
#--------------------------------------------

tgtsrcs_sendBitStream := sendBitStream.cc 
tgtincs_sendBitStream := \
		rcecalib 

tgtlibs_sendBitStream := \
    $(rce_service) \
    $(rce_net) \
    rcecalib/idl \
    rcecalib/util \
    rcecalib/rceconfig \
    rcecalib/HW \
    rcecalib/dataproc \
    rcecalib/eudaq \
    rcecalib/ipcclient


tgtslib_sendBitStream := \
    dl \
    $(zlib) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(ers_lib) \
    $(owl_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib)
#--------------------------------------------

tgtsrcs_sendRandomPattern := sendRandomPattern.cc 
tgtincs_sendRandomPattern := \
		rcecalib 

tgtlibs_sendRandomPattern := \
    $(rce_service) \
    $(rce_net) \
    rcecalib/idl \
    rcecalib/util \
    rcecalib/rceconfig \
    rcecalib/HW \
    rcecalib/dataproc \
    rcecalib/eudaq \
    rcecalib/ipcclient


tgtslib_sendRandomPattern := \
    dl \
    $(zlib) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(ers_lib) \
    $(owl_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(tdaq_cmdline_lib) \
    $(boost_regex_lib)

#--------------------------------------------
tgtsrcs_rebootHSIO := rebootHSIO.cc 
tgtincs_rebootHSIO := \
		rcecalib 

tgtlibs_rebootHSIO := \
    rcecalib/idl \
    rcecalib/ipcclient


tgtslib_rebootHSIO := \
    dl \
    $(z_lib) \
    $(boost_thread_lib) \
    $(boost_date_time_lib) \
    $(owl_lib) \
    $(ers_lib) \
    $(ipc_lib) \
    $(is_lib) \
    $(rdb_lib) \
    $(oh_lib) \
    $(tdaq_cmdline_lib) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(boost_regex_lib)

#=========================================================
tgtsrcs_host_bootloader := host_bootloader.cc 
tgtincs_host_bootloader := boost
tgtlibs_host_bootloader := $(rce_service) $(rce_net) rcecalib/rcecontrol
tgtslib_host_bootloader := /usr/lib/rt

#=========================================================
tgtsrcs_testspeed := testspeed.cc EthPrimitive.cc
tgtincs_testspeed := boost
tgtlibs_testspeed := $(rce_service) $(rce_net)
tgtslib_testspeed := /usr/lib/rt


endif

