#include "rcecalib/HW/SerialIF.hh"
#include "ers/ers.h"

SerialIF* SerialIF::m_serial=0;

SerialIF::SerialIF(){
  ERS_ASSERT_MSG(m_serial==0,"the interface exists already");
  m_serial=this;
}
void SerialIF::destroy(){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  delete m_serial;
  m_serial=0;
}

void SerialIF::send(BitStream *bs, int opt){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  m_serial->Send(bs,opt);
}
void SerialIF::setChannelInMask(unsigned linkmask){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  m_serial->SetChannelInMask(linkmask);
}
void SerialIF::setChannelOutMask(unsigned linkmask){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  m_serial->SetChannelOutMask(linkmask);
}
int SerialIF::enableTrigger(bool on){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  return m_serial->EnableTrigger(on);
} 
void SerialIF::disableOutput(){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  m_serial->DisableOutput();
} 
void SerialIF::setOutputMode(unsigned mode){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  m_serial->SetOutputMode(mode);
} 
void SerialIF::setDataRate(Rate rate){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  m_serial->SetDataRate(rate);
} 
unsigned SerialIF::sendCommand(unsigned char opcode){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  return m_serial->SendCommand(opcode);
}
unsigned SerialIF::writeRegister(unsigned addr, unsigned val){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  return m_serial->WriteRegister(addr,val);
}
unsigned SerialIF::readRegister(unsigned addr, unsigned &val){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  return m_serial->ReadRegister(addr, val);
}
unsigned SerialIF::writeBlockData(std::vector<unsigned>& data){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  return m_serial->WriteBlockData(data);
}
unsigned SerialIF::readBlockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  return m_serial->ReadBlockData(data, retvec);
}
unsigned SerialIF::readBuffers(std::vector<unsigned char>& retvec){
  ERS_ASSERT_MSG(m_serial!=0,"there is no serial interface defined");
  return m_serial->ReadBuffers(retvec);
}
