#ifndef IPCFEI4BMODULE_HH
#define IPCFEI4BMODULE_HH

#include "rcecalib/config/FEI4/FEI4BModule.hh"
#include "ipc/object.h"
#include "IPCFEI4BAdapter.hh"

class IPCPartition;
class AbsFormatter;

namespace FEI4{
template <class TP = ipc::single_thread>
class IPCFEI4BModule: public IPCNamedObject<POA_ipc::IPCFEI4BAdapter,TP>, public FEI4::FEI4BModule {
public:
  IPCFEI4BModule(IPCPartition & p, const char * name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt);
  ~IPCFEI4BModule();
  CORBA::Long IPCdownloadConfig(const ipc::PixelFEI4BConfig &config);    
  void IPCsetChipAddress(CORBA::ULong addr);
  CORBA::ULong IPCwriteHWglobalRegister(CORBA::Long reg, CORBA::UShort val);
  CORBA::ULong IPCreadHWglobalRegister(CORBA::Long reg, CORBA::UShort &val);
  CORBA::ULong IPCwriteDoubleColumnHW(CORBA::ULong bit, CORBA::ULong dcol, const ipc::uvec & data, ipc::uvec_out retv);
  CORBA::ULong IPCreadDoubleColumnHW(CORBA::ULong bit, CORBA::ULong dcol, ipc::uvec_out retv);
  CORBA::ULong IPCclearPixelLatches();
  CORBA::Long IPCverifyModuleConfigHW();
  void shutdown();
  void destroy();

};
};
  

#endif
