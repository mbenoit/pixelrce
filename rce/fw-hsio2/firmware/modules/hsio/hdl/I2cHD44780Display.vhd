library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.all;
use work.i2cpkg.all;
use work.I2cRegPCA9535DriverPkg.all;
use work.HD44780DriverPkg.all;

entity I2cHD44780Display is
  generic (
    init_reset_count0: natural := 250000000;
    init_reset_count1: natural := 1000000;
    init_reset_count2: natural := 100000;
    i2c_addr: std_logic_vector(2 downto 0) := "100";
    i2c_prescale: natural := 77;
    display_width: natural := 20
  );
  port
  (
    clk:           in    std_logic;
    rst:           in    std_logic := '0';
    displayI2cSda: inout std_logic;
    displayI2cScl: inout std_logic;
    
    DisplayBuffer: in HD44780DriverDisplayBufferType := (others => x"41")
  );
end I2cHD44780Display;

architecture I2cHD44780Display of I2cHD44780Display is
  signal displayI2cIn:  i2c_in_type;
  signal displayI2cOut: i2c_out_type;

  signal displayI2cRegMasterIn: I2cRegMasterInType;
  signal displayI2cRegMasterOut: I2cRegMasterOutType;
    
  signal I2cRegPCA9535DriverOut: I2cRegPCA9535DriverOutType;
  signal I2cRegPCA9535DriverIn:  I2cRegPCA9535DriverInType;

  signal init: std_logic := '0';

  signal HD44780GpioIn:  HD44780GpioInType := HD44780DRIVER_GPIO_IN_INIT;
  signal HD44780GpioOut: HD44780GpioOutType;
  signal HD44780En: std_logic := '0';
begin
  displayI2cSda    <= displayI2cOut.sda when displayI2cOut.sdaoen = '0' else 'Z';
  displayI2cIn.sda <= to_x01z(displayI2cSda);
  displayI2cScl    <= displayI2cOut.scl when displayI2cOut.scloen = '0' else 'Z';
  displayI2cIn.scl <= to_x01z(displayI2cScl);

  displayI2cRegMaster : entity work.I2cRegMaster
  generic map 
  (
    OUTPUT_EN_POLARITY_G => 0,
    FILTER_G             => 16,
    PRESCALE_G           => i2c_prescale
  )
  port map 
  (
    clk    => clk,
    srst   => rst,
    regIn  => displayI2cRegMasterIn,
    regOut => displayI2cRegMasterOut,
    i2ci   => displayI2cIn,
    i2co   => displayI2cOut
  );

  displayI2cRegPCA9535Driver: entity work.I2cRegPCA9535Driver
  generic map 
  (
    addr => i2c_addr
  )
  port map
  (
    clk => clk,
    rst => rst,

    I2cRegMasterIn  => displayI2cRegMasterIn,
    I2cRegMasterOut => displayI2cRegMasterOut,
  
    I2cRegPCA9535DriverOut => I2cRegPCA9535DriverOut,
    I2cRegPCA9535DriverIn  => I2cRegPCA9535DriverIn
  );

  process begin
    wait until rising_edge(clk);
    if (rst = '1') then
      init <= '0';
      HD44780En <= '0';
    else
      HD44780GpioIn.ack  <= '0';
      HD44780GpioIn.fail <= '0';
      if (init = '0') then
        if (I2cRegPCA9535DriverIn.configure = '0') then
          I2cRegPCA9535DriverIn.configure <= '1';
        else
          I2cRegPCA9535DriverIn.configure <= '0';
          init <= '1';
        end if;
      else
        HD44780GpioIn.ack <= I2cRegPCA9535DriverOut.ack;
        HD44780GpioIn.fail <= I2cRegPCA9535DriverOut.fail;
        if (I2cRegPCA9535DriverIn.write = '1') then
          I2cRegPCA9535DriverIn.write <= '0';
        elsif (I2cRegPCA9535DriverOut.busy = '1') then
          I2cRegPCA9535DriverIn.write <= '0';
        else
          HD44780En <= '1';
          if HD44780GpioOut.update = '1' then
            I2cRegPCA9535DriverIn.write <= '1';
            I2cRegPCA9535DriverIn.writeValue <= HD44780GpioOut.db & "00000" 
              & HD44780GpioOut.e & HD44780GpioOut.rw & HD44780GpioOut.rs;
          end if;
        end if;
      end if;
    end if;
  end process;

  HD44780Driver_inst: entity work.HD44780Driver
  generic map
  (
    init_reset_count0 => init_reset_count0,
    init_reset_count1 => init_reset_count1,
    init_reset_count2 => init_reset_count2,
    display_width => display_width
  )
  port map
  (
    clk => clk,
    rst => rst,
    en => HD44780En,

    GpioIn => HD44780GpioIn,
    GpioOut => HD44780GpioOut,

    DisplayBuffer => DisplayBuffer
  );

end I2cHD44780Display;
