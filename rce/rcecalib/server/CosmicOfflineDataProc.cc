#include <boost/property_tree/ptree.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include "rcecalib/server/CosmicOfflineDataProc.hh"
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/config/FormattedRecord.hh"
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/dataproc/DataProcFactory.hh"
#include "rcecalib/dataproc/CosmicEventReceiver.hh"
#include "rcecalib/dataproc/CosmicEvent.hh"
#include "rcecalib/dataproc/CosmicEventIterator.hh"
#include "rcecalib/server/IPCController.hh"
#include "rcecalib/config/AbsFormatter.hh"
#include "rcecalib/config/ModuleInfo.hh"
#include "eudaq/Event.hh"
#include "eudaq/DataSender.hh"
#include "eudaq/DataSenderIF.hh"
#include "eudaq/DetectorEvent.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/counted_ptr.hh"
#include <iomanip>
#include <iostream>
#include <stdlib.h>
#include <time.h>

namespace{
  const unsigned timeout=5;
}

void printhitbus(unsigned word){
  for (int i=0;i<3;i++){
    std::cout<<"FE "<<3-i<<": ";
    for (int j=0;j<10;j++){
      std::cout<<((word&(1<<(j*3+i)))!=0);
    }
    std::cout<<std::endl;
  }
}
int CosmicOfflineDataProc::processData(unsigned short rlink, unsigned char* data, int size){
  //std::cout<<"Event size = "<<size<<std::endl;
  assert(size!=0);
  //unsigned long a_sec, a_nsec;
  //omni_thread::get_time(&a_sec,&a_nsec);
  //if(a_sec-m_sec>timeout&&m_sec!=0xffffffff)switchfile();
  //m_sec=a_sec;
  m_nfrag++;
  int link=rlink&0x000f;
  int rceNum = (rlink&0x0FE0)>>5;
  //std::cout<<"Data from link "<<link<<std::endl;
  if (link==8){
    eudaq::DetectorEvent *dev=new eudaq::DetectorEvent(m_runNo,0,0);
    counted_ptr<eudaq::Event> ev(dev);
    eudaq::RawDataEvent* cpcp=new eudaq::RawDataEvent("TEMP",m_runNo,0);
    counted_ptr<eudaq::Event> cpc(cpcp);
    cpcp->AddBlock(0);
    cpcp->AppendBlock(0,(char*)&rceNum,sizeof(unsigned));
    cpcp->AppendBlock(0,(char*)data,8*sizeof(unsigned));
    dev->AddEvent(cpc);
    CosmicEventReceiver::receiveEvent(ev);  //this function adds event to monitoring GUI
    return 0;
  }
  bool marker=((rlink&0x10)==0x0010);
  int rceIndex = m_rceToIndex[rceNum];

  m_hits=0;
  int module = m_linkToIndex[rceNum][link];  //map link/rce onto module index
  // if synching throw away fragments until we find the marker
  if(m_modsynch[module]==true) {
    
    if (marker==false){
      if(m_print>0){
	if(link==10)std::cout<<"Ignoring Link 10: "<<std::endl;
	else {
	  std::cout<<"Ignoring Link "<<link<<std::endl;
	}
	m_print--;
      }
      m_tossed[module]++;
      return 0;
    }
    m_modsynch[module]=false;
    //if(marker)std::cout<<"Markerevent"<<std::endl;
    std::cout<<"Tossed "<<m_tossed[module]<<" fragments for link "<<link<<" , RCE "<<rceNum<<std::endl;
  }
  if (link==10){
     // unsigned trgtime1=data[3];
     // unsigned trgtime2=data[4];
     // unsigned long long trgtime=trgtime1;
     // trgtime=(trgtime<<32) | trgtime2;

     // std::cout<<"Trigger time: "<<std::hex<<trgtime<<std::dec<<std::endl;
    //std::cout<<"Link 10 L1id "<<((data[0]>>24)&0xf)<<std::endl;
    unsigned udata[32];
    for(int i=0;i<size;i+=4)
      udata[i/4]=data[i]<<24|data[i+1]<<16|data[i+2]<<8|data[i+3];
    bool success=m_iterator->setTdcData(udata,rceIndex);
    //std::cout<<"TLU trigger id:"<<(udata[7]&0xffff)<<std::endl;
//    std::cout<<"Trigger word:"<<((udata[7]&0xff0000)>>16)<<std::endl;
    //unsigned fifothresh=(udata[7]&0xf000000)>>24;
    //if(fifothresh)std::cout<<std::hex<<fifothresh<<std::dec<<std::endl;
    m_event++;
    //std::cout<<"Hitbus word:"<<hitbusword<<std::endl;
    //unsigned backpressure=(udata[7]&0x1000000);
    //unsigned fifofull=(udata[7]&0x2000000);
    //if(backpressure)std::cout<<"Backpressure"<<std::endl;
    //if(fifofull)std::cout<<"Fifo full"<<std::endl;
//     unsigned hitbus1=udata[1];
//     unsigned hitbus2=udata[2];
//     if(hitbus1!=0){
//       std::cout<<"Hitbus TA:"<<std::endl;
//       printhitbus(hitbus1);
//     }
//     if(hitbus2!=0){
//        std::cout<<"Hitbus TB:"<<std::endl;
//        printhitbus(hitbus2);
//     }
    unsigned trgtime1=udata[3];
    unsigned trgtime2=udata[4];
    unsigned long long trgtime=trgtime1;
    trgtime=(trgtime<<32) | trgtime2;
    //std::cout<<"Trigger time stamp: "<<trgtime<<std::endl;
    unsigned deadtime1=udata[5];
    unsigned deadtime2=udata[6];
    unsigned long long deadtime=deadtime1;
    deadtime=(deadtime<<32) | deadtime2;
    //std::cout<<"Deadtime: "<<deadtime<<std::endl;
 
    if(success==false){
      resynch();
      return 1;
    }
  }else{
    unsigned parsed[16384];
    int parsedsize=0;
    int nl1a;
    if(m_swap[module]==true){
      unsigned swapped[16384];
      for(int i=0;i<size;i+=4){
	swapped[i/4]=data[i]<<24|data[i+1]<<16|data[i+2]<<8|data[i+3];
      }
      m_formatter[module]->decode(swapped,size/sizeof(unsigned),parsed, parsedsize, nl1a);    
    }else{
      m_formatter[module]->decode((unsigned*)&data[0],size/sizeof(unsigned),parsed, parsedsize, nl1a);    
    }
    unsigned int l1id=99;
    int ntrg=0;
    int bxfirst=-666;
    int bxlast=-777;
    int start=0;
    do{ //possibly we must split the data because it belongs to 2 triggers. 
      //if(start!=0)std::cout<<"Newstart "<<start<<" "<<m_iterator->nEvents()+1<<std::endl;

      int newstart=processModuleData(&parsed[start],parsedsize-start,link, module, l1id, bxfirst, bxlast, ntrg);
      //if(m_hits>0) std::cout<<"Link "<<link<<" had "<<m_hits<<" hits."<<std::endl;
      // std::cout<<"Data from module "<<module<<" link="<<link<<" with l1id="<<l1id<<" bxfirst="<<bxfirst<<" ntrg="<<ntrg<<std::endl;
      //bool isdut=((link<9) || (m_file==0)); //all data is DUT when running as a producer (m_file==0)
      bool isdut=true; //not using CTEL for now.
      if(ntrg==0){
	std::cout<<"Inserting duplicate header for data-only frame on module RCE="<<rceNum<<" outlink="<<link<<std::endl;
	FormattedRecord fr(FormattedRecord::HEADER);
	fr.setL1id(l1id); 
	fr.setBxid(bxfirst);
	unsigned hd=fr.getWord();
	m_iterator->addPixelData(module,l1id, bxfirst, bxlast, ntrg, rceIndex, isdut, &hd,1);
      }
      bool success=m_iterator->addPixelData(module,l1id, bxfirst, bxlast, ntrg, rceIndex, isdut, &parsed[start],newstart);
      if(success==false){
	resynch();
	return 1;
      }
      start+=newstart;
    }while(start<parsedsize);
  }
  return 0;
}

int CosmicOfflineDataProc::processModuleData(unsigned* udata,int size, int link, int module, 
					     unsigned& l1id, int& bxfirst, int& bxlast, int &ntrg){
  FormattedRecord* data=(FormattedRecord*)udata;
  if(!data[0].isHeader()){
    std::cout<<"Module data not starting with 0x2xxxxxxx. Using old header info."<<std::endl;
    ntrg=0;
    bxfirst=m_bxfirst[module];
    bxlast=bxfirst;
    l1id=m_l1id[module];
  }else{
    ntrg=1;
    data[0].setLink(link);
    bxfirst=data[0].getBxid()&0xff;
    bxlast=bxfirst;
    l1id=data[0].getL1id()&0xf;
    m_l1id[module]=l1id;
    m_bxfirst[module]=bxfirst;
    //int bx=data[0].getBxid();
    //std::cout<<"Link "<<link<<" l1a "<<l1id<<" bx "<<bx<<std::endl;
  }
  //std::cout<<"Link "<<link<<" l1a "<<l1id<<" bxfirst "<<m_bxfirst[module]<<std::endl;
  unsigned l1a;
  for (int i=1;i<size;i++){
    if (data[i].isHeader()){
      l1a=data[i].getL1id()&0xf;
      if(l1id!=l1a)return i;
      data[i].setLink(link);
      bxlast=data[i].getBxid()&0xff;
      //std::cout<<"Link "<<link<<" l1a "<<l1a<<" bx "<<bxlast<<std::endl;
      ntrg++;
    }
    //}else if(data[i]&0x80000000){
         //       //m_hits++;
         //     int chip=(data[i]>>24)&0xf;
         //     int tot=(data[i]>>16)&0xff;
         //     int col=(data[i]>>8)&0x1f;
         //     int row=data[i]&0xff;  	   
         //     int tr=bx-bxfirst;
         //     if (tr<0)tr+=256;
      //if(row<156 && tot > 6)
         //     std::cout<<"Link "<<link<<" Trigger "<<tr<<" Chip "<<chip<<" row "<<row<<" col "<<col<<" tot "<<tot<<std::endl;
         //     }
  }
  return size;
}
      

void CosmicOfflineDataProc::resynch(){
  std::cout<<"Resynchronizing"<<std::endl;
  // set synch flags for everybody
  for (int i=0;i<64;i++)m_modsynch[i]=true;
  for (int i=0;i<64;i++)m_tossed[i]=0;
  m_print=0;
  m_iterator->resynch();
  m_controller->resynch();//pause run, reset counters, post markers
}

CosmicOfflineDataProc::CosmicOfflineDataProc(IPCController* controller,
					     std::vector<ModuleInfo*> modinfo,
					     std::vector<int> rcesAll,
					     unsigned runnum, 
					     const std::string& outputFile,
					     bool writeEudet, bool writeRaw)
  : m_runNo(runnum), m_event(0), m_hitbus(0), m_controller(controller), m_sec(0xffffffff), m_chunk(0) 
{
  
  m_nModules=modinfo.size();
  m_l1id=new int[m_nModules];
  m_bxfirst=new int[m_nModules];
  m_swap=new int[m_nModules];
  m_rceToIndex=new int[MAX_RCES];
  m_linkToIndex=new int*[MAX_RCES];
  for(int i=0;i<MAX_RCES;i++)m_linkToIndex[i]=0;
  for(int i=0;i<m_nModules;i++){
    m_l1id[i]=0;
    m_bxfirst[i]=0;
  }
  
  //make a vector only of the *unique* rce numbers
  std::vector<int> rces;  
  for(unsigned imod=0; imod<rcesAll.size(); imod++){
    int rceNum=rcesAll[imod];
    bool newRCE=true;

    for(unsigned jrce=0; jrce<rces.size(); jrce++){
      if(rces[jrce] == rceNum){
	newRCE=false;
	break;
      }
    }
    if(newRCE){
      rces.push_back(rceNum);
    }

  }
  
  unsigned nRces=rces.size();
  for(unsigned i=0; i<nRces; i++){
    m_rceToIndex[rces[i]] = i;
    m_linkToIndex[rces[i]]=new int[MAX_LINKS];
    m_linkToIndex[rces[i]][10]=m_nModules+i;
  }
  for (int i=0;i<m_nModules;i++){
    m_linkToIndex[rcesAll[i]][modinfo[i]->getOutLink()]=i;
  }

  for (int i=0;i<m_nModules;i++){
    m_formatter[i]=modinfo[i]->getFormatter();
    if(std::string(m_formatter[i]->getName())=="JJ" ||
       std::string(m_formatter[i]->getName())=="AFPHPTDC"){ // need to byte-swap
      m_swap[i]=true;
    }else{
      m_swap[i]=false;
    }
  }
  for(int i=0;i<64;i++){
    m_modsynch[i] = false;
    m_tossed[i] = 0;
  }
  m_nfrag=0;
  m_print=0;
  bool monitor=false; 
  m_pfile=0;
  m_file=0;
  if(outputFile!=""){
    monitor=true;
    if(writeEudet==true){
      m_file=new eudaq::FileSerializer(outputFile.c_str());
      eudaq::DetectorEvent dev(m_runNo,0,0);
      dev.SetFlags(eudaq::Event::FLAG_BORE);
      dev.SetTag("CONFIG", "Name = Test");
      eudaq::RawDataEvent *revc=new eudaq::RawDataEvent(eudaq::RawDataEvent::BORE("CTEL",m_runNo));
      counted_ptr<eudaq::Event> cpc(revc);
      dev.AddEvent(cpc);
      eudaq::RawDataEvent *revd=new eudaq::RawDataEvent(eudaq::RawDataEvent::BORE("APIX-CT",m_runNo));
      counted_ptr<eudaq::Event> cpd(revd);
      dev.AddEvent(cpd);
      dev.Serialize(*m_file);
    } 
    if(writeRaw==true){
      std::cout<<"Run number "<<m_runNo<<std::endl;
      std::string pfname=outputFile.substr(0,outputFile.size()-4)+"_000000.dat";
      m_filename=outputFile.substr(0,outputFile.size()-4);
      m_pfile=new std::ofstream(pfname.c_str(), std::ios::binary);
      unsigned utime=(unsigned)time(0);
      m_pfile->write((char*)&utime, 4);
      m_pfile->write((char*)&nRces, 4);
    }
  }
  // create the event iterator class which does all the work
  m_iterator=new CosmicEventIterator(monitor, m_file, m_pfile, m_runNo,m_nModules,rces);
  

}

CosmicOfflineDataProc::~CosmicOfflineDataProc(){
  m_iterator->flushEvents();
  std::cout<<"Number of fragments processed: "<<m_nfrag<<std::endl;
  //if(m_event>0)std::cout<<"Hitbus efficiency in the previous run: "<<(float)m_hitbus/(float)m_event<<std::endl;
  if(m_pfile!=0){
    m_pfile->close();
    delete m_pfile;
  }
  if(m_file!=0){
    int eventnumber=m_iterator->nEvents()+1;
    eudaq::DetectorEvent dev(m_runNo,eventnumber,0);
    dev.SetFlags(eudaq::Event::FLAG_EORE);
    eudaq::RawDataEvent *revc=new eudaq::RawDataEvent(eudaq::RawDataEvent::EORE("CTEL",m_runNo,eventnumber));
    counted_ptr<eudaq::Event> cpc(revc);
    dev.AddEvent(cpc);
    eudaq::RawDataEvent *revd=new eudaq::RawDataEvent(eudaq::RawDataEvent::EORE("APIX-CT",m_runNo,eventnumber));
    counted_ptr<eudaq::Event> cpd(revd);
    dev.AddEvent(cpd);
    dev.Serialize(*m_file);
    delete m_file;
  }
  std::cout<<"close file called"<<std::endl;
  delete m_iterator;
  delete [] m_l1id;
  delete [] m_bxfirst;
  delete [] m_swap;
  delete [] m_rceToIndex;
  for(int i=0;i<MAX_RCES;i++)delete [] m_linkToIndex[i];
  delete [] m_linkToIndex;
}

inline void CosmicOfflineDataProc::switchfile(){
  m_chunk++;
  m_pfile->close();
  char chunknum[16];
  sprintf(chunknum, "_%06d.dat", m_chunk);
  m_pfile->open((m_filename+chunknum).c_str(),std::ios::binary);
  unsigned utime=(unsigned)time(0);
  m_pfile->write((char*)&utime, 4);
}
