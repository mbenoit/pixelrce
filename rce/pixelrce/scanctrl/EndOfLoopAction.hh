#ifndef ENDOFLOOPACTION_HH
#define ENDOFLOOPACTION_HH

#include <string>

class EndOfLoopAction{
public:
  EndOfLoopAction(std::string name):
    m_name(name){}
  virtual ~EndOfLoopAction(){}
  std::string name(){return m_name;}
  virtual int execute() = 0;
protected:
  std::string m_name;
};

#endif
