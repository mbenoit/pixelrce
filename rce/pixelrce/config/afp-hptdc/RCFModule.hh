#ifndef RCFAFPHPTDCMODULE_HH
#define RCFAFPHPTDCMODULE_HH

#include "config/afp-hptdc/Module.hh"
#include "rcf/AFPHPTDCModuleConfig.hh"
#include "rcf/RCFAFPHPTDCAdapter.hh"

class AbsFormatter;

namespace afphptdc{
  class RCFModule: public RCFAFPHPTDCAdapter, public afphptdc::Module {
  public:
    RCFModule(RCF::RcfServer& server, const char * name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt);
    ~RCFModule();
    int32_t RCFdownloadConfig(ipc::AFPHPTDCModuleConfig config);    
private:
  RCF::RcfServer& m_server;
    
  };
};
  

#endif
