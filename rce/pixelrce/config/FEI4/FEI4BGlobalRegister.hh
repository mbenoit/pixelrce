#ifndef FEI4BGLOBALREGISTER_FEI4_HH
#define FEI4BGLOBALREGISTER_FEI4_HH

#include <map>
#include <string>
#include <vector>
#include "HW/SerialIF.hh"
#include "config/FEI4/GlobalRegister.hh"


namespace FEI4{

class FECommands;
  
  class FEI4BGlobalRegister: public GlobalRegister{
  public:
    FEI4BGlobalRegister(FECommands* commands);
    virtual ~FEI4BGlobalRegister();
    void printFields(std::ostream &os);
  private:
    void initialize();
    static RegisterDef m_def;
    static bool m_initialized;
    RegisterDef* getDef(){return &m_def;}

		  
  };

};

#endif
