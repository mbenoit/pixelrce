#ifndef MODULEINFO_HH
#define MODULEINFO_HH

class AbsFormatter;

class ModuleInfo{
public:
  ModuleInfo(std::string name, int id, int inLink, int outLink, 
	     int nFE, int nRow, int nCol, AbsFormatter* fmt):
    m_name(name), m_id(id),m_inLink(inLink),m_outLink(outLink), 
    m_nFE(nFE), m_nRow(nRow), m_nCol(nCol), m_formatter(fmt){}
  std::string getName(){return m_name;}
  int getId(){return m_id;}
  int getInLink(){return m_inLink;}
  int getOutLink(){return m_outLink;}
  int getNFrontends(){return m_nFE;}
  int getNRows(){return m_nRow;}
  int getNColumns(){return m_nCol;}
  AbsFormatter* getFormatter(){return m_formatter;}
  
private:
  std::string m_name;
  int m_id;
  int m_inLink;
  int m_outLink;
  int m_nFE;
  int m_nRow;
  int m_nCol;
  AbsFormatter* m_formatter;
};

#endif
