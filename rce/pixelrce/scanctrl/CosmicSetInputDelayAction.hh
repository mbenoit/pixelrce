#ifndef COSMICSETINPUTDELAYACTION_HH
#define COSMICSETINPUTDELAYACTION_HH
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include "scanctrl/LoopAction.hh"
#include "config/ConfigIF.hh"
#include <string>
#include <vector>

class CosmicSetInputDelayAction: public LoopAction{
public:
  CosmicSetInputDelayAction(std::string name, ConfigIF* cif, boost::property_tree::ptree *pt ):
    LoopAction(name),m_configIF(cif){
    try{
      int nPoints=pt->get<int>("nPoints");
      BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt->get_child("dataPoints")){
	int data = boost::lexical_cast<int>(v.second.data());
	dataPoints.push_back(data);
      }
      assert((int)dataPoints.size()==nPoints);
    }
    catch(boost::property_tree::ptree_bad_path ex){
      std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    }
  }

  int execute(int i){
    //std::cout<<"Set input delay "<<i<<std::endl;
    unsigned serstat;
    serstat=m_configIF->writeHWregister(7,0);//reset delays
    assert(serstat==0);
    int reg=5;
    if(dataPoints[i]<0)reg=6;
    for (int j=0;j<abs(dataPoints[i]);j++) {
      serstat=m_configIF->writeHWregister(reg,0); //increment delay
      assert(serstat==0);
    }
    return 0;
  }

private:
  ConfigIF* m_configIF;
  std::vector<int> dataPoints;
};

#endif
