#include "util/RceName.hh"
#include "config/FEI3/RCFModule.hh"
#include <boost/lexical_cast.hpp>
#include <iostream>
namespace FEI3{

  RCFModule::RCFModule(RCF::RcfServer &server, const char * name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt):
  Module(name, id, inlink, outlink, fmt), m_server(server){
  //  std::cout<<"RCFModule"<<std::endl;
    char binding[32];
    sprintf(binding, "I_RCFFEI3Adapter_%d", id);
    m_server.bind<I_RCFFEI3Adapter>(*this, binding);
}

RCFModule::~RCFModule(){
    char binding[32];
    sprintf(binding, "I_RCFFEI3Adapter_%d", m_id);
    m_server.unbind<I_RCFFEI3Adapter>(binding);
}

int32_t RCFModule::RCFdownloadConfig(ipc::PixelModuleConfig legacyModuleConfig){
  m_mcc.setRegister(Mcc::MCC_LV1,legacyModuleConfig.MCCRegisters.regLV1);
  m_mcc.setRegister(Mcc::MCC_CSR,legacyModuleConfig.MCCRegisters.regCSR);
  m_mcc.setRegister(Mcc::MCC_CAL,legacyModuleConfig.MCCRegisters.regCAL);
  m_mcc.setRegister(Mcc::MCC_FEEN,legacyModuleConfig.maskEnableFEConfig);

  ipc::PixelFEConfig *legacyFEConfig;
  ipc::PixelFETrims *legacyFETrims;
  ipc::PixelFEMasks *legacyFEMasks;
  for(int chip=0;chip<N_FRONTENDS;++chip) {
    m_frontend[chip]->setChipAddress(chip, legacyModuleConfig.FEConfig[chip].FEIndex);
    legacyFEConfig = &legacyModuleConfig.FEConfig[chip];
    legacyFETrims = &legacyFEConfig->FETrims;
    legacyFEMasks = &legacyFEConfig->FEMasks;

    globalLegacyToUsr(legacyFEConfig, chip);
    
    m_frontend[chip]->setVcalCoeff(0, legacyFEConfig->FECalib.vcalCoeff[0]); 
    m_frontend[chip]->setVcalCoeff(1, legacyFEConfig->FECalib.vcalCoeff[1]); 
    m_frontend[chip]->setVcalCoeff(2, legacyFEConfig->FECalib.vcalCoeff[2]); 
    m_frontend[chip]->setVcalCoeff(3, legacyFEConfig->FECalib.vcalCoeff[3]); 
    m_frontend[chip]->setCapVal(0, legacyFEConfig->FECalib.cinjLo); 
    m_frontend[chip]->setCapVal(1, legacyFEConfig->FECalib.cinjHi); 

    for(int col=0;col<Frontend::N_COLS;++col) {
      for(int row=0;row<Frontend::N_ROWS;++row) {
	unsigned char tdac = legacyFETrims->dacThresholdTrim[row][col];
	unsigned char fdac = legacyFETrims->dacFeedbackTrim[row][col];
	unsigned mask = (1 << (row & 0x1f));
	unsigned char enable = (legacyFEMasks->maskEnable[row >> 5][col] & mask) ? 1 : 0;
	unsigned char select = (legacyFEMasks->maskSelect[row >> 5][col] & mask) ? 1 : 0;
	unsigned char kill = (legacyFEMasks->maskPreamp[row >> 5][col] & mask) ? 1 : 0;
	unsigned char hitbus = (legacyFEMasks->maskHitbus[row >> 5][col] & mask) ? 1 : 0;
	PixelRegister* pixel=m_frontend[chip]->getPixelRegister(col,row);
	pixel->setField(PixelRegister::tdac, tdac);
	pixel->setField(PixelRegister::fdac, fdac);
	pixel->setField(PixelRegister::enable, enable);
	pixel->setField(PixelRegister::select, select);
	pixel->setField(PixelRegister::kill, kill);
	pixel->setField(PixelRegister::hitbus, hitbus);
      }
    }
  }
  std::cout<<"Configure done"<<std::endl;
  return 0;
}
void RCFModule::globalLegacyToUsr(ipc::PixelFEConfig* legacyFE, uint32_t chip){

  const ipc::PixelFEGlobal *feGlobal;
  feGlobal = &legacyFE->FEGlobal;

  m_frontend[chip]->setGlobalRegField("latency", feGlobal->latency);
  m_frontend[chip]->setGlobalRegField("selfTriggerDelay", feGlobal->selfLatency);
  m_frontend[chip]->setGlobalRegField("selfTriggerWidth", feGlobal->selfWidth);
  m_frontend[chip]->setGlobalRegField("enableSelfTrigger", feGlobal->enableSelfTrigger);
  m_frontend[chip]->setGlobalRegField("enableHitParity", feGlobal->enableHitParity);
  m_frontend[chip]->setGlobalRegField("doMux", feGlobal->muxDO);
  m_frontend[chip]->setGlobalRegField("selectMonHit", feGlobal->muxMonHit);
  m_frontend[chip]->setGlobalRegField("tsiTscEnable", feGlobal->enableTimestamp);
  
  m_frontend[chip]->setGlobalRegField("selectDataPhase", 0); /* previously spare */
  m_frontend[chip]->setGlobalRegField("enableEOEParity", 0); /* previously spare */
  
  m_frontend[chip]->setGlobalRegField("hitBusScaler", 0); /* read-only */
  
  m_frontend[chip]->setGlobalRegField("monLeakADCRefTest", feGlobal->monADCRef);
  m_frontend[chip]->setGlobalRegField("monLeakADCDAC", feGlobal->dacMonLeakADC);
  m_frontend[chip]->setGlobalRegField("monLeakDACTest", feGlobal->monMonLeakADC);
  m_frontend[chip]->setGlobalRegField("monLeakADCEnableComparator", feGlobal->enableMonLeak);
  m_frontend[chip]->setGlobalRegField("monLeakADCMonComp", 0); /* status */
  m_frontend[chip]->setGlobalRegField("aRegTrim", feGlobal->aregTrim);
  m_frontend[chip]->setGlobalRegField("enableARegMeas", feGlobal->enableAregMeas);
  m_frontend[chip]->setGlobalRegField("aRegMeas", feGlobal->aregMeas);
  m_frontend[chip]->setGlobalRegField("enableAReg", feGlobal->enableAreg);
  m_frontend[chip]->setGlobalRegField("enableLVDSRefMeas", feGlobal->enableLvdsRegMeas);
  m_frontend[chip]->setGlobalRegField("dRegTrim", feGlobal->dregTrim);
  m_frontend[chip]->setGlobalRegField("enableDRegMeas", feGlobal->enableDregMeas);
  m_frontend[chip]->setGlobalRegField("dRegMeas", feGlobal->dregMeas);
  m_frontend[chip]->setGlobalRegField("capMeasCircuitry", feGlobal->capMeasure);
  
  m_frontend[chip]->setGlobalRegField("enableCapTest", feGlobal->enableCapTest);
  m_frontend[chip]->setGlobalRegField("enableAnalogOut", feGlobal->enableBuffer);
  m_frontend[chip]->setGlobalRegField("enableTestPixelMux", feGlobal->muxTestPixel);
  m_frontend[chip]->setGlobalRegField("enableVCalMeas", feGlobal->enableVcalMeasure);
  m_frontend[chip]->setGlobalRegField("enableLeakMeas", feGlobal->enableLeakMeasure);
  m_frontend[chip]->setGlobalRegField("enableBufferBoost", feGlobal->enableBufferBoost);
  
  m_frontend[chip]->setGlobalRegField("enableCP8", feGlobal->enableCP8);
  m_frontend[chip]->setGlobalRegField("testDacForIVDD2Dac", feGlobal->monIVDD2);
  m_frontend[chip]->setGlobalRegField("dacIVDD2", feGlobal->dacIVDD2);
  m_frontend[chip]->setGlobalRegField("dacID", feGlobal->dacID);
  m_frontend[chip]->setGlobalRegField("testDacForIDDac", feGlobal->monID);
  
  m_frontend[chip]->setGlobalRegField("enableCP7", feGlobal->enableCP7);
  m_frontend[chip]->setGlobalRegField("testDacForIP2Dac", feGlobal->monIP2);
  m_frontend[chip]->setGlobalRegField("dacIP2", feGlobal->dacIP2);
  m_frontend[chip]->setGlobalRegField("dacIP", feGlobal->dacIP);
  m_frontend[chip]->setGlobalRegField("testDacForIPDac", feGlobal->monIP);
  
  m_frontend[chip]->setGlobalRegField("enableCP6", feGlobal->enableCP6);
  m_frontend[chip]->setGlobalRegField("testDacForITrimThDac", feGlobal->monITRIMTH);
  m_frontend[chip]->setGlobalRegField("dacITRIMTH", feGlobal->dacITRIMTH);
  m_frontend[chip]->setGlobalRegField("dacIF", feGlobal->dacIF);	
  m_frontend[chip]->setGlobalRegField("testDacForIFDac", feGlobal->monIF);
  
  m_frontend[chip]->setGlobalRegField("enableCP5", feGlobal->enableCP5);
  m_frontend[chip]->setGlobalRegField("testDacForITrimIfDac", feGlobal->monITRIMIF);
  m_frontend[chip]->setGlobalRegField("dacITRIMIF", feGlobal->dacITRIMIF);
  m_frontend[chip]->setGlobalRegField("dacVCAL", feGlobal->dacVCAL);
  m_frontend[chip]->setGlobalRegField("testDacForVCalDac", feGlobal->monVCAL);
  
  m_frontend[chip]->setGlobalRegField("enableCP4", feGlobal->enableCP4);
  m_frontend[chip]->setGlobalRegField("enableCinjHigh", feGlobal->enableCinjHigh);
  m_frontend[chip]->setGlobalRegField("enableExternalInj", feGlobal->enableExternal);
  m_frontend[chip]->setGlobalRegField("enableTestAnalogRef", feGlobal->enableTestAnalogRef);
  m_frontend[chip]->setGlobalRegField("eocMux", feGlobal->muxEOC);
  m_frontend[chip]->setGlobalRegField("CEUClockControl", feGlobal->frequencyCEU);
  
  m_frontend[chip]->setGlobalRegField("enableDigitalInject", feGlobal->enableDigital);
  m_frontend[chip]->setGlobalRegField("enableCP3", feGlobal->enableCP3);
  m_frontend[chip]->setGlobalRegField("testDacForITH1Dac", feGlobal->monITH1);
  m_frontend[chip]->setGlobalRegField("dacITH1", feGlobal->dacITH1);
  m_frontend[chip]->setGlobalRegField("dacITH2", feGlobal->dacITH2);
  m_frontend[chip]->setGlobalRegField("testDacForITH2Dac", feGlobal->monITH2);
  
  m_frontend[chip]->setGlobalRegField("enableCP2", feGlobal->enableCP2);
  m_frontend[chip]->setGlobalRegField("testDacILDac", feGlobal->monIL);
  m_frontend[chip]->setGlobalRegField("dacIL", feGlobal->dacIL);
  m_frontend[chip]->setGlobalRegField("dacIL2", feGlobal->dacIL2);
  m_frontend[chip]->setGlobalRegField("testDacIL2Dac", feGlobal->monIL2);
  
  m_frontend[chip]->setGlobalRegField("enableCP1", feGlobal->enableCP1);
  m_frontend[chip]->setGlobalRegField("threshTOTMinimum", feGlobal->threshTOTMinimum);
  m_frontend[chip]->setGlobalRegField("threshTOTDouble", feGlobal->threshTOTDouble);
  m_frontend[chip]->setGlobalRegField("modeTOTThresh", feGlobal->modeTOTThresh);
  
  m_frontend[chip]->setGlobalRegField("enableCP0", feGlobal->enableCP0);
  m_frontend[chip]->setGlobalRegField("enableHitbus", feGlobal->enableHitbus);
  m_frontend[chip]->setGlobalRegField("gTDac", feGlobal->gdac);
  m_frontend[chip]->setGlobalRegField("enableTune", feGlobal->enableTune);
  m_frontend[chip]->setGlobalRegField("enableBiasComp", feGlobal->enableBiasComp);
  m_frontend[chip]->setGlobalRegField("enableIpMonitor", feGlobal->enableIpMonitor);
}

};

