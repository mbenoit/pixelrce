#ifndef CONFIGBASE_HH
#define CONFIGBASE_HH

#include "config/PixelConfig.hh"
#include "config/PixelConfigFactory.hh"

class ConfigBase{
public:
  ConfigBase(const char* name, int inlink, int outlink, int rce, int phase, const char* filename): m_cfg(0), m_inlink(0), m_outlink(0), m_rce(0), m_phase(0), m_name(name){
    PixelConfigFactory pf;
    m_cfg=pf.createConfig(filename);
  }
  ~ConfigBase(){
    delete m_cfg;
  }
  
  PixelConfig* getModuleConfig(){
    return m_cfg;
  }
  
  int getInlink(){
    return m_inlink;
  }
  int getOutlink(){
    return m_outlink;
  }
  int getRce(){
    return m_rce;
  }
  int getPhase(){
    return m_phase;
  }

private:
  PixelConfig* m_cfg;
  int m_inlink;
  int m_outlink;
  int m_rce;
  int m_phase;
  std::string m_name;
};

#endif
