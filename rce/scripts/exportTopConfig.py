#!/daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt/bin/python

import os
import sys
import string
import shutil
import getopt

if __name__ == '__main__':
    opts, args = getopt.getopt( sys.argv[1:], "r")
    userelpath=0
    for opt, arg in opts:
        if opt == '-r':
            print "Relative path"
            userelpath=1
    if len(args) != 4:
        print "Usage: exportTopConfig.py [-r <write relative path>] oldTopfile newTopfile oldpath newpath"
        exit()
    try:
        f=open(args[0]);
    except IOError:
        print "File "+args[0]+" does not exist. Exiting."
        exit()
    try:
        g=open(args[1], "w");
    except IOError:
        print "Cannot open file "+args[1]+". Exiting."
        exit()
    oldpath=args[2]
    newpath=args[3]
    if oldpath[-1]!='/': oldpath+='/'
    if newpath[-1]!='/': newpath+='/'
    while 1:
        oldcfg=f.readline()
        if not oldcfg: break
        p1=f.readline()
        p2=f.readline()
        p3=f.readline()
        p4=f.readline()
        p5=f.readline()
        oldcfg=oldcfg.lstrip()
        oldcfg=oldcfg.rstrip()
        fullnewpath="None"
        if oldcfg!="None":
            fulloldpath=oldcfg
            isrelpath=0
            if oldcfg[0]!='/':
                fulloldpath=oldpath+oldcfg
                isrelpath=1
            if fulloldpath.find(oldpath)!=-1:
                fullnewpath=fulloldpath.replace(oldpath, newpath)
                relpathf=fulloldpath.replace(oldpath,"")
                relpath=relpathf[:relpathf.find("/configs")]
                slashpos=relpath.rfind('/')
                configdir=relpath[slashpos+1:]
                basepath=relpath[:slashpos]
            else:
	        print fulloldpath
 	        print oldpath
                print "Oldpath is not part of the filename"
                exit()
            try:
                os.makedirs(newpath+relpath+"/configs")
            except OSError:
                xx=0
                #print "Skipping creation of %s because it exists already."%(newpath+relpath+"/configs")
            shutil.copy(fulloldpath, fullnewpath)
            subfiles = []
            c=open(fullnewpath);
            for line in c:
                if line.find("#")!=-1: line=line[:line.find("#")]
                line=line.lstrip()
                line=line.rstrip()
                if line.find("enable")==0: subfiles.append(line[6:].lstrip().rstrip())
                if line.find("largeCap")==0: subfiles.append(line[8:].lstrip().rstrip())
                if line.find("smallCap")==0: subfiles.append(line[8:].lstrip().rstrip())
                if line.find("hitbus")==0: subfiles.append(line[6:].lstrip().rstrip())
                if line.find("tdac")==0: subfiles.append(line[4:].lstrip().rstrip())
                if line.find("fdac")==0: subfiles.append(line[4:].lstrip().rstrip())
            for fn in subfiles:
                if fn.isdigit()==False:
                    path, filename=os.path.split(fn)
                    try:
                        os.makedirs(os.path.join(newpath,basepath,path))
                    except OSError:
                        xxx=0
                        #print "Skipping creation of %s because it exists already."%(newpath+basepath+path)
                    shutil.copy(os.path.join(oldpath, basepath, fn), os.path.join(newpath, basepath, fn))
                    
        if userelpath==1:
            g.write(relpathf) 
        else:
            g.write(fullnewpath) 
        g.write('\n');
        g.write(p1)
        g.write(p2)
        g.write(p3)
        g.write(p4)
        g.write(p5)
    g.close()
