#ifndef ABSRECEIVER_HH
#define ABSRECEIVER_HH

#include "rcecalib/dataproc/AbsDataHandler.hh"

class AbsReceiver{
public:
  AbsReceiver(AbsDataHandler* handler):m_handler(handler){}
  virtual ~AbsReceiver(){}
  virtual void resynch(){}
protected:
  AbsDataHandler* m_handler;
  
};
#endif
