#ifndef PRIMLISTGUI_HH
#define PRIMLISTGUI_HH

#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>
#include <TGComboBox.h>
#include <TGListBox.h>
#include <TGFileDialog.h>
#include <TGListView.h>
#include <fstream>
#include <string>
#include <map>
#include <utility>


//push update after scan
namespace ipc{
  class ScanOptions;
}
namespace RCE {
  class PixScan;
  class PrimList;
}
class ConfigGui;

class PrimListGui: public TGVerticalFrame{ 
public:
  PrimListGui(const char* name, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options);
  virtual ~PrimListGui();
  void print(std::ostream &os);
  void printFromGui();
  void disableControls();
  void enableControls();
  RCE::PrimList* getPrimList();
  void load();
  int loadPrimList(const char* fPath);
  void clear();
  void setFlavor(std::string pFlavor);
  void setScanTypes (std::map<int, std::string>  pscantypes);
  bool isLoaded();
  void updateStatus(std::string str);
  int currentScan;
  std::string getCurrScanName();
  int getNumScans();
  RCE::PixScan* getCurrentScan();
  ipc::ScanOptions* getCurrentScanConfig();
  std::string getCurrentPreScript();
  std::string getCurrentPostScript();
  std::string getCurrentTopConfig();
  std::vector<std::string>& getCurrentDisabledList();
  std::vector<std::string>& getCurrentEnabledList();
  bool getCurrentUpdateConfig();
  int getCurrentPause();
  void changeIncludes(ConfigGui* cfg[]);
  void setFailed(bool on){m_failed=on;}
  bool failed(){return m_failed;}
  void setRunNumber(int runnum);
private:
  RCE::PrimList* m_pList;
  TGLabel *m_name;
  TGLabel *plStatusLabel;
  void updateText(TGLabel* label, const char* newtext);
  //both flavor passed from CalibGui
  std::string flavor; 
  //scan types copied from ScanGui
  std::map<std::string, int> m_scantypes;
  bool m_failed;
  //helper functions
  int toInt(std::string);
  int parseScan(std::string line);
  int setScanParam(std::string name, std::string value, RCE::PixScan* pScan, std::string &preScript, 
		   std::string &postScript,  std::vector<std::string> &enabled, 
		   std::vector<std::string> &disabled, bool &updateConfig, std::string &topConfig, int &pause);
  bool toBool(std::string val);
  double toDouble(std::string val);
  std::vector <std::string> toVector(std::string value);
  
  ClassDef(PrimListGui,0);
};

#endif
