#include "rcecalib/scanctrl/DelayScanSetup.hh"
#include "rcecalib/scanctrl/ScanLoop.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/scanctrl/ActionFactory.hh"
#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/scanctrl/EndOfLoopAction.hh"
#include "rcecalib/util/exceptions.hh"

int DelayScanSetup::setupLoops( NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af){
  int retval=0; 
  loop.clear();
  try{ //catch bad scan option parameters
    // This calibration has a single loop
    int nPoints = scanOptions->get<int>("scanLoop_0.nPoints");
    ScanLoop *scanloop=new ScanLoop("scanLoop_0",nPoints);
    LoopAction* disable=af->createLoopAction("DISABLE_TRIGGER");
    scanloop->addLoopAction(disable);
    LoopAction* setdelay=af->createLoopAction("COSMIC_SET_INPUT_DELAY",&scanOptions->get_child("scanLoop_0"));
    scanloop->addLoopAction(setdelay);
    LoopAction* changebin=af->createLoopAction("CHANGE_BIN");
    scanloop->addLoopAction(changebin);
    LoopAction* enabletdc=af->createLoopAction("COSMIC_ENABLE_TDC");
    scanloop->addLoopAction(enabletdc);
    LoopAction* enablesc=af->createLoopAction("COSMIC_ENABLE_SCINTILLATOR_TRIGGER");
    scanloop->addLoopAction(enablesc);
    LoopAction* enable=af->createLoopAction("ENABLE_TRIGGER");
    scanloop->addLoopAction(enable);
    // End of loop actions
    EndOfLoopAction* disabletrigger=af->createEndOfLoopAction("DISABLE_TRIGGER","");
    scanloop->addEndOfLoopAction(disabletrigger);
    EndOfLoopAction* disablechannels=af->createEndOfLoopAction("DISABLE_ALL_CHANNELS","");
    scanloop->addEndOfLoopAction(disablechannels);
    // loop is the nested loop object
    loop.addNewInnermostLoop(scanloop);
  }
  catch(boost::property_tree::ptree_bad_path ex){
    rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
    ers::error(issue);
    retval=1;
  }
  //  loop.print();
  return retval;
}
