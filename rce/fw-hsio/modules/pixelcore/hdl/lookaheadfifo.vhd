--------------------------------------------------------------
-- Look ahead fifo
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity lookaheadfifo is
        generic (
          buffersize : integer :=8192 -- FIFO size
        );
port(	wr_clk:	    in std_logic;
        rd_clk:     in std_logic;
	rst:	    in std_logic;
	din:	    in std_logic_vector(24 downto 0);
	dout:	    out std_logic_vector(24 downto 0):=(others =>'0');
	dnext:	    out std_logic_vector(24 downto 0);
        wr_en:      in std_logic;
        rd_en:      in std_logic;
        dnextvalid: out std_logic;
        empty:      out std_logic;
        full:       out std_logic;
        overflow:   out std_logic;
        prog_full:  out std_logic;
        underflow:  out std_logic;
        valid:      out std_logic := '0'
);
end lookaheadfifo;

--------------------------------------------------------------

architecture LOOKAHEADFIFO of lookaheadfifo is

COMPONENT data24bitfifo4096
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    full : OUT STD_LOGIC;
    overflow : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    almost_empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC;
    underflow : OUT STD_LOGIC;
    prog_full : OUT STD_LOGIC
  );
END COMPONENT;
COMPONENT data24bitfifo8192
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    full : OUT STD_LOGIC;
    overflow : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    almost_empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC;
    underflow : OUT STD_LOGIC;
    prog_full : OUT STD_LOGIC
  );
END COMPONENT;

signal frd_en: std_logic:='0';
signal buffervalid: std_logic:='0';
signal doutvalid: std_logic:='0';
signal fdout: slv(24 downto 0);

begin
    dnextvalid<=buffervalid;
    dnext<=fdout;
    frd_en<=buffervalid and rd_en;
    valid<=doutvalid;
    process begin
    wait until rising_edge(rd_clk);
    if(frd_en='1')then
      dout<=fdout;
      doutvalid<='1';
    elsif(rd_en='1')then
      if(doutvalid='1')then
        doutvalid<='0';
      end if;
    end if;
    end process;		

  buf_4096: if (buffersize=4096) generate
   fei4fifo : data24bitfifo4096
     port map (
       din => din,
       rd_clk => rd_clk,
       rd_en => frd_en,
       rst => rst,
       wr_clk => wr_clk,
       wr_en => wr_en,
       dout => fdout,
       empty => empty,
       full => full,
       overflow => overflow,
       prog_full => prog_full,
       valid => buffervalid,
       underflow => underflow);
  end generate;
  buf_8192: if (buffersize=8192) generate
   fei4fifo : data24bitfifo8192
     port map (
       din => din,
       rd_clk => rd_clk,
       rd_en => frd_en,
       rst => rst,
       wr_clk => wr_clk,
       wr_en => wr_en,
       dout => fdout,
       empty => empty,
       full => full,
       overflow => overflow,
       prog_full => prog_full,
       valid => buffervalid,
       underflow => underflow);
  end generate;

end LOOKAHEADFIFO;

--------------------------------------------------------------
