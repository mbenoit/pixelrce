
#include "config/FEI3/Mcc.hh"
#include "config/FEI3/FECommands.hh"
#include "HW/SerialIF.hh"
#include <assert.h>
#include <string>
#include <stdio.h>

namespace FEI3{
  Mcc::Mcc(){
    clear();
  }
  Mcc::~Mcc(){}
  void Mcc::clear(){
    for (int i=0;i<MCC_NREG;i++)m_register[i]=0;
  }
  
  void Mcc::setRegister(Mcc::Register reg, unsigned val){
    if(reg<MCC_NREG){
      m_register[reg]=val;
    }
  }
  void Mcc::setNLvl1(unsigned val){
    assert(val<=16);
    m_register[MCC_LV1]=(val-1)<<8;
  }
  void Mcc::enableDelay(bool on){
    if(on){
      m_register[MCC_CAL]|=(1<<10);
    }else{
      m_register[MCC_CAL]&=~(1<<10);
    }
  }
  void Mcc::setBandwidth(unsigned val){
    m_register[MCC_CSR]=val;
  }
  void Mcc::setChipEnables(unsigned val){
    m_register[MCC_FEEN]=val;
  }
  void Mcc::setStrobeDuration(unsigned val){
    m_register[MCC_CNT]=val;
  }
  void Mcc::setStrobeDelayRange(unsigned val){
    assert(val<16);
    m_register[MCC_CAL]&=~(0xf<<6); //clear the field
    m_register[MCC_CAL]|=(val<<6);
  }
  void Mcc::setStrobeDelay(unsigned val){
    assert(val<64);
    m_register[MCC_CAL]&=~(0x3f); //clear the field
    m_register[MCC_CAL]|=val;
    enableDelay(1);//delay gets enabled implicitely
    //    std::cout<<" MCC_CAL "<<std::hex<<m_register[MCC_CAL]<<std::dec<<std::endl;
  }
  void Mcc::writeRegister(BitStream *bs, Mcc::Register reg){
    FECommands::mccWriteRegister(bs,reg,m_register[reg]);
  }
  void Mcc::configureHW(){
    BitStream* bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    writeRegister(bs, Mcc::MCC_CSR);
    writeRegister(bs, Mcc::MCC_LV1);
    writeRegister(bs, Mcc::MCC_CAL);
    writeRegister(bs, Mcc::MCC_CNT);
    SerialIF::send(bs);
    delete bs;
  }

  int Mcc::setParameter(const char* name, int val){
    int retval=1;
    if(std::string(name)=="STROBE_DELAY"){
      setStrobeDelay(val);
    }else if(std::string(name)=="STROBE_DEL_RANGE"){
      setStrobeDelayRange(val);
    }else if (std::string(name)=="trigOpt.nL1AperEvent"){
      setNLvl1(val);
    }else if (std::string(name)=="trigOpt.strobeMCCDelay"){
      setStrobeDelay(val);    
    }else if (std::string(name)=="trigOpt.strobeMCCDelayRange"){
      setStrobeDelayRange(val);
    }else if (std::string(name)=="trigOpt.strobeDuration"){
      setStrobeDuration(val);
    }else if (std::string(name)=="bandwidth"){
      setBandwidth(val);
    }else if (std::string(name)=="enables"){
      setChipEnables(val);
    }else{
      retval=0;
    }
    return retval;
  }
};
    
    
