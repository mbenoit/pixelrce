#include "config/AbsTrigger.hh"
#include <algorithm>


bool AbsTrigger::lookupParameter(const char* name){
  std::list<std::string>::iterator result = std::find(m_parameters.begin(), m_parameters.end(), name);
    if( result==m_parameters.end())return false;
    else return true;
  }
