#ifndef FEI4BFORMATTER_HH
#define FEI4BFORMATTER_HH

#include "config/AbsFormatter.hh"
#include "util/RceHisto1d.cc"

class FEI4BFormatter:public AbsFormatter{
public:
  FEI4BFormatter(int id);
  virtual ~FEI4BFormatter();
  int decode(const unsigned* data, int size, unsigned* parsedData, int &parsedsize, int &nL1A);
  void configure(boost::property_tree::ptree* scanOptions);
  int decodeToT(int rawTot);

private:
  int m_hitDiscCnfg;
  RceHisto1d<int, int>* m_errhist;
  unsigned m_sr_bcid;
  unsigned m_sr_l1id;
  unsigned m_bxidraw;
  unsigned m_l1idraw;
};
#endif
