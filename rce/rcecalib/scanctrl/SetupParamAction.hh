#ifndef SETUPPARAMACTION_HH
#define SETUPPARAMACTION_HH

#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/config/ConfigIF.hh"
#include <string>
#include <vector>

class SetupParamAction: public LoopAction{
public:
  SetupParamAction(std::string name, ConfigIF* cif, boost::property_tree::ptree *pt ):
    LoopAction(name),m_configIF(cif){
    try{
      m_scanParameter=pt->get<std::string>("scanParameter");
      int nPoints=pt->get<int>("nPoints");
      ERS_DEBUG(2,"scanParameter: "<<m_scanParameter);
      BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt->get_child("dataPoints")){
	ERS_DEBUG(2, v.second.data());
	int data = boost::lexical_cast<int>(v.second.data());
	dataPoints.push_back(data);
      }
      ERS_ASSERT_MSG((int)dataPoints.size()==nPoints,"of an inconsistency in the number of scan points.");
    }
    catch(boost::property_tree::ptree_bad_path ex){
      rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
      ers::error(issue);
    }
  }

  int execute(int i){
    ERS_DEBUG(2,"SetupParamAction "<<i);
    //std::cout<<"Parameter is "<<dataPoints[i]<<std::endl;
    return m_configIF->setupParameter(m_scanParameter.c_str(),dataPoints[i]);
  }

private:
  ConfigIF* m_configIF;
  std::string m_scanParameter;
  std::vector<int> dataPoints;
};

#endif
