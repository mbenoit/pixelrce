
#include "server/CosmicGui.hh"
#include "server/RCFController.hh"
#include "util/HistoManager.hh"
#include "TApplication.h"
#include <TROOT.h>
#include <TStyle.h>
#include <boost/program_options.hpp>
#include <stdlib.h>

int main(int argc, char **argv){
  //
  // Initialize command line parameters with default values
  //
  bool start;
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("start,s", boost::program_options::value<bool>(&start)->default_value(false), "Start run when GUI comes up");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);
  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }
  RCF::RcfInitDeinit rcfInit;
  RCFController controller;
  AbsController &acontroller(controller);
  new HistoManager(0);
  
  gROOT->SetStyle("Plain");
  TApplication theapp("app",&argc,argv);
  new CosmicGui(acontroller, start, gClient->GetRoot(),800, 735);
  theapp.Run();
  return 0;
}


