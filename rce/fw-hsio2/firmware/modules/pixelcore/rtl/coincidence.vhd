--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;
use work.arraytype.all;

--------------------------------------------------------------


entity coincidence is

port(	clk: 	    in std_logic;
	rst:	    in std_logic;
        enabled:    in std_logic;
        fifothresh: in std_logic;
        trgin: in std_logic;
        serbusy: in std_logic;
        tdcreadoutbusy: in std_logic;
        extbusy: in std_logic;
        l1a: out std_logic;
        busy: out std_logic;
        coinc: out std_logic;
        coincd: out std_logic
);
end coincidence;

--------------------------------------------------------------

architecture COINCIDENCE of coincidence is

signal busys: std_logic;
signal rbusy: std_logic;
signal coinct: std_logic;
signal oldcoinct: std_logic;
signal oldoldcoinct: std_logic;
signal oldbusy: std_logic;
signal oldoldbusy: std_logic;
signal l1asig: std_logic;
signal go: std_logic;

begin
  l1a<=l1asig;
  busys<=fifothresh or serbusy or tdcreadoutbusy or extbusy or coinct or l1asig;
  rbusy<=serbusy or tdcreadoutbusy;
  go <= enabled and not busys;
  busy<= enabled and busys;
  coinc<=coinct;
  coincd<=trgin;
  
  process (rst,l1asig, go,trgin)
  begin
    if( rst='1' or l1asig='1')then
      coinct<='0';
    elsif(rising_edge(trgin))then
      if(go='1')then
        coinct<='1';
      end if;
    end if;
  end process;
  
  process (rst,clk)
  begin
    if(rst='1')then
      oldcoinct<='0';
      oldoldcoinct<='0';
      oldbusy<='0';
      l1asig<='0';
    elsif(rising_edge(clk))then
      if(l1asig='1')then
        l1asig<='0';
      elsif(coinct='1' and oldoldcoinct='0')then  -- keep one bin as a buffer
        l1asig<='1';
      end if;
      oldcoinct<=coinct;
      oldoldcoinct<=oldcoinct;
      oldbusy<=rbusy;
    end if;
  end process;

end COINCIDENCE;
