-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasSLinkLscGtx7.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-04-14
-- Last update: 2015-12-16
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;

entity AtlasSLinkLscGtp7 is
   generic (
      TPD_G : time := 1 ns);
   port (
      sysClk    : in  sl;
      sysRst    : in  sl;
      gtRstDone : out sl;
      -- G-Link's MGT Serial IO
      gtTxP     : out sl;
      gtTxN     : out sl;
      gtRxP     : in  sl;
      gtRxN     : in  sl;
      -- TLK2501 transmit ports
      TLK_TXD   : in  std_logic_vector(15 downto 0);
      TLK_TXEN  : in  std_logic;
      TLK_TXER  : in  std_logic;
      -- TLK2501 transmit ports
      TLK_RXD   : out std_logic_vector(15 downto 0);
      TLK_RXDV  : out std_logic;
      TLK_RXER  : out std_logic);       
end AtlasSLinkLscGtp7;

architecture mapping of AtlasSLinkLscGtp7 is
   
   signal rxCharIsKByteDly,
      gtTxResetDone,
      gtRxResetDone : sl;
   signal txCharIsK,
      rxCharIsK : slv(1 downto 0);
   signal rxDataByteDly : slv(7 downto 0);
   signal txData,
      GTX_RXDATA,
      rxData : slv(15 downto 0);

   signal QPllRefClk, QPllClk, QPllLock, QPllRefClkLost, QPllReset,
          QPllLockDetClk, PllReset, qPllRefClkIn: slv(1 downto 0);
   
   attribute dont_touch : string;
   attribute dont_touch of
      rxCharIsKByteDly,
      rxDataByteDly,
      gtTxResetDone,
      gtRxResetDone,
      txCharIsK,
      rxCharIsK,
      txData,
      rxData : signal is "true";

   -- attribute KEEP_HIERARCHY : string;
   -- attribute KEEP_HIERARCHY of
   -- tlk_gtx_interface_1 : label is "TRUE";
   
begin

   gtRstDone <= gtTxResetDone and gtRxResetDone;

   -- The GTX7 automatically aligns the commas on the even 
   -- byte (byte[0]). But the tlk_gtx_interface firmware 
   -- module expects the alignment to be on the odd byte 
   -- (byte[1]). We correct for this by swapping the 
   -- byte mapping and delaying the odd byte by 1 clock cycle.
   process(sysClk)
   begin
      if rising_edge(sysClk) then
         rxCharIsKByteDly <= rxCharIsK(1);
         rxDataByteDly    <= rxData(15 downto 8);
      end if;
   end process;

   tlk_gtx_interface_1 : entity work.tlk_gtx_interface
      port map (
         SYS_RST                 => sysRst,
         -- GTX receive ports
         GTX_RXUSRCLK2           => sysClk,
         GTX_RXCHARISK(1)        => rxCharIsK(0),
         GTX_RXCHARISK(0)        => rxCharIsKByteDly,
         GTX_RXDATA(15 downto 8) => rxData(7 downto 0),
         GTX_RXDATA(7 downto 0)  => rxDataByteDly,
         -- GTX transmit ports
         GTX_TXUSRCLK2           => sysClk,
       GTX_TXDATA              => txData,
         GTX_TXCHARISK           => txCharIsK,
         -- TLK2501 ports
         TLK_TXD                 => TLK_TXD,
         TLK_TXEN                => TLK_TXEN,
         TLK_TXER                => TLK_TXER,
         TLK_RXD                 => TLK_RXD,
         TLK_RXDV                => TLK_RXDV,
         TLK_RXER                => TLK_RXER);

   QPllLockDetClk <= sysClk & sysClk;
   PllReset(0) <= QPllReset(0) or sysRst;
   PllReset(1) <= QPllReset(1) or sysRst;
   qPllRefClkIn <= sysClk & sysClk;
   Gtp7QuadPll_Inst : entity work.Gtp7QuadPll  
      generic map (
         PLL0_REFCLK_SEL_G    => "111",
         PLL0_FBDIV_IN_G      => 4,
         PLL0_FBDIV_45_IN_G   => 5,
         PLL0_REFCLK_DIV_IN_G => 1,
         PLL1_REFCLK_SEL_G    => "111",
         PLL1_FBDIV_IN_G      => 4,
         PLL1_FBDIV_45_IN_G   => 5,
         PLL1_REFCLK_DIV_IN_G => 1)          
      port map (
         qPllRefClk     => qPllRefClkIn,
         qPllOutClk     => QPllClk,
         qPllOutRefClk  => QPllRefClk,
         qPllLock       => QPllLock,
         qPllLockDetClk => QPllLockDetClk,
         qPllRefClkLost => QPllRefClkLost,
         qPllReset      => PllReset);  
   

      Gtp7Core_Inst : entity work.Gtp7Core
         generic map (
            TPD_G                    => TPD_G,
            RXOUT_DIV_G              => 2,
            TXOUT_DIV_G              => 2,
            RX_CLK25_DIV_G           => 5,
            TX_CLK25_DIV_G           => 5,
            RX_OS_CFG_G              => "0000010000000",
            TX_PLL_G                 => "PLL0",
            RX_PLL_G                 => "PLL0",
            TX_EXT_DATA_WIDTH_G      => 16,
            TX_INT_DATA_WIDTH_G      => 20,
            TX_8B10B_EN_G            => true,
            RX_EXT_DATA_WIDTH_G      => 16,
            RX_INT_DATA_WIDTH_G      => 20,
            RX_8B10B_EN_G            => true,
            TX_BUF_EN_G              => true,
            TX_OUTCLK_SRC_G          => "OUTCLKPMA",
            TX_DLY_BYPASS_G          => '1',
            TX_PHASE_ALIGN_G         => "NONE",
            TX_BUF_ADDR_MODE_G       => "FULL",
            RX_BUF_EN_G              => true,
            RX_OUTCLK_SRC_G          => "OUTCLKPMA",
            RX_USRCLK_SRC_G          => "RXOUTCLK",
            RX_DLY_BYPASS_G          => '0',
            RX_DDIEN_G               => '0',
            RX_BUF_ADDR_MODE_G       => "FULL",
            RX_ALIGN_MODE_G          => "GT",          -- Default
            ALIGN_COMMA_DOUBLE_G     => "FALSE",       -- Default
            ALIGN_COMMA_ENABLE_G     => "1111111111",  -- Default
            ALIGN_COMMA_WORD_G       => 2,             -- Default
            ALIGN_MCOMMA_DET_G       => "TRUE",
            ALIGN_MCOMMA_VALUE_G     => "1010000011",  -- Default
            ALIGN_MCOMMA_EN_G        => '1',
            ALIGN_PCOMMA_DET_G       => "TRUE",
            ALIGN_PCOMMA_VALUE_G     => "0101111100",  -- Default
            ALIGN_PCOMMA_EN_G        => '1',
            SHOW_REALIGN_COMMA_G     => "FALSE",
            RXSLIDE_MODE_G           => "AUTO",
            RX_DISPERR_SEQ_MATCH_G   => "TRUE",        -- Default
            DEC_MCOMMA_DETECT_G      => "TRUE",        -- Default
            DEC_PCOMMA_DETECT_G      => "TRUE",        -- Default
            DEC_VALID_COMMA_ONLY_G   => "FALSE",       -- Default
            CBCC_DATA_SOURCE_SEL_G   => "DECODED")     -- Default
         port map (
            stableClkIn      => sysClk,
            qPllRefClkIn     => QPllRefClk,
            qPllClkIn        => QPllClk,
            qPllLockIn       => QPllLock,
            qPllRefClkLostIn => QPllRefClkLost,
            qPllResetOut     => QPllReset,
            gtTxP            => gtTxP,
            gtTxN            => gtTxN,
            gtRxP            => gtRxP,
            gtRxN            => gtRxN,
            rxUsrClkIn       => sysClk,
            rxUsrClk2In      => sysClk,
            rxUserResetIn    => sysRst,
            rxResetDoneOut   => gtRxResetDone,
            rxDataOut        => rxdata,
            rxCharIsKOut     => rxCharIsK,
            txUsrClkIn       => sysClk,
            txUsrClk2In      => sysClk,
            txUserResetIn    => sysRst,
            txResetDoneOut   => gtTxResetDone,
            txDataIn         => txData,
            txCharIsKIn      => txCharIsK);
end mapping;

