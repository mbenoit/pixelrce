#ifndef LOOPFACTORY_HH
#define LOOPFACTORY_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "scanctrl/LoopSetup.hh"
class NestedLoop;


class LoopFactory{
public:
  LoopFactory();
  LoopSetup* createLoopSetup(const char* type);
};
  

#endif
