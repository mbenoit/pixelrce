#ifdef RCE_V2
#include "datCode.hh"
#include "rcecalib/server/CmdDecoder.hh"
#include DAT_PUBLIC( service,       dynalink,    RunnableModule.hh)
#include DAT_PUBLIC( service,       dynalink,    LinkingError.hh)
#include DAT_PUBLIC( service,       dynalink,    Linker.hh)
#include <stdio.h>
#include <arpa/inet.h>
extern "C"{
#include <rtems/libio.h>
}
#else
#include "rcecalib/server/CmdDecoder.hh"
#include "rce/dynalink/RunnableModule.hh"
#include "rce/dynalink/LinkingError.hh"
#include "rce/dynalink/Linker.hh"
extern "C"{
  #include <librtemsNfs.h>
}
#endif
#include <stdlib.h>
#include <signal.h>
#include <iostream>
#include <sstream>
#include "namespace_aliases.hh"

void CmdDecoder::run(){
  const char* command;
  while(1){
    command=m_eth->receiveCommand();
    std::string reply=decode(command);
    m_eth->reply(reply.c_str());
  }
}

std::string CmdDecoder::decode(const char* msg){
  //split message by whitespace
  std::vector<std::string> command;
  std::istringstream iss(msg);
  do {
    std::string sub;
    iss >> sub;
    command.push_back(sub);
    iss.peek();
  } while (iss);
  if(command.size()==0)return "Bad Command";
  if(command[0]=="reboot")return cmdReboot(command);
  else if(command[0]=="setenv")return cmdSetenv(command);
  else if(command[0]=="mount")return cmdMount(command);
  else if(command[0]=="runTask")return cmdLd(command);
  else if(command[0]=="shutdown")return cmdShutdown(command);
  else if(command[0]=="download")return cmdDownload(command);
  else return "Unknown command";
  return "OK";
}
std::string CmdDecoder::cmdReboot(std::vector<std::string>& command){
  if(command.size()>1 && command[1]=="-noshutdown"){
    // do nothing
  }else if(m_running==true){
    cmdShutdown(command);
    usleep(500000);
  }
  const char *cc_argv[] = 
    {
      "reboot",              /* always the name of the program */
      "-S",
      "0"
    };
  int cc_argc = sizeof( cc_argv ) / sizeof( cc_argv[ 0 ]  ); 
  m_eth->reply("Rebooting...");
#ifdef RCE_V2   
   service::shell::Reboot_main(cc_argc, (char **)cc_argv);
#else
  main_reboot(cc_argc, (char **)cc_argv);
#endif
  return "OK";
}
std::string CmdDecoder::cmdSetenv(std::vector<std::string>& cmd){
  if(cmd.size()!=3)return "Wrong number of parameters for setenv.";
  setenv(cmd[1].c_str(),cmd[2].c_str(), 1);
  return "OK";
}
std::string CmdDecoder::cmdMount(std::vector<std::string>& cmd){
#ifdef RCE_V2
   if(cmd.size()!=3)return "Wrong number of parameters for mount.";
  //rpcUdpInit();
  //nfsInit( 0, 0 );
  printf("BOOTP server is %s. Use as NFS server.\n",inet_ntoa( rtems_bsdnet_bootp_server_address));
  mkdir(cmd[2].c_str(),S_IRWXU|S_IRWXG|S_IRWXO);
  mount(cmd[1].c_str(), cmd[2].c_str(), "nfs", RTEMS_FILESYSTEM_READ_WRITE, "");
  //  int stat=mount(inet_ntoa( rtems_bsdnet_bootp_server_address), (char*)cmd[1].c_str() ,(char*)cmd[2].c_str() );
  int stat=0;
  if(stat==0){
    return "OK";
  }else{
    return "NFS mount failed.";
  }
#else 
  if(cmd.size()!=3 && cmd.size()!=4)return "Wrong number of parameters for mount.";
  rpcUdpInit();
  nfsInit( 0, 0 );
  int stat=0;
  if(cmd.size()==3){
    printf("BOOTP server is %s. Use as NFS server.\n",inet_ntoa( rtems_bsdnet_bootp_server_address));
    stat=nfsMount(inet_ntoa( rtems_bsdnet_bootp_server_address), (char*)cmd[1].c_str() ,(char*)cmd[2].c_str() );
  }else{
    stat=nfsMount((char*)cmd[1].c_str(), (char*)cmd[2].c_str() ,(char*)cmd[3].c_str() ); 
  }
  if(stat==0){
    return "OK";
  }else{
    return "NFS mount failed.";
  }
#endif
}
std::string CmdDecoder::cmdLd(std::vector<std::string>& cmd){
  if(cmd.size()!=2)return "Wrong number of parameters for runTask.";
  if(m_running==true)return "There is already a task running. Reboot first.";
  const char *cc_argv[6]={"runTask", "-P", "100", "-N", "clb", ""};
  cc_argv[5]=cmd[1].c_str();
#ifdef RCE_V2
  int stat=service::shell::RunTask_main(6,(char**)cc_argv);
#else
  int stat=main_runTask(6,(char**)cc_argv);
#endif
  if(stat==0){
    m_running=true;
    pause();
    return "OK";
  }else{
    return "runTask failed.";
  }
}
std::string CmdDecoder::cmdShutdown(std::vector<std::string>& cmd){
  if(m_running==false)return "Calibserver is not running.";
  kill(getpid(),SIGUSR1);
  return "OK";
}

std::string CmdDecoder::cmdDownload(std::vector<std::string>& cmd){
  if(cmd.size()!=3)return "Wrong number of parameters for download.";
  if(m_running==true)return "There is already a module running.";
  const char* ior=getenv("TDAQ_IPC_INIT_REF");
  if(ior==0 || std::string(ior).substr(0,4)!="IOR:")return "No IOR defined";
  int bufsize=atoi(cmd[1].c_str());
  unsigned char* aBuffer;
  try{
    aBuffer = RCE::ELF::RunnableModule::getBuffer(bufsize);
  }catch(...){
    return "Can't allocate an ELF image buffer.";
  }
  m_eth->reply("OK");
  std::string reply=m_eth->receiveModule(aBuffer, bufsize);
  if(reply!="OK")return reply.c_str();
  RCE::ELF::RunnableModule* rmod=new(aBuffer) RCE::ELF::RunnableModule;
  try{
    RCE::Dynalink::Linker &lnk(RCE::Dynalink::Linker::instance());
    lnk.clear();
    unsigned int name=0x43414c49; //CALI
    unsigned int priority = 100;
    unsigned int stack = RTEMS_MINIMUM_STACK_SIZE;
    unsigned int mode  = RTEMS_DEFAULT_MODES;
    unsigned int attr  = RTEMS_DEFAULT_ATTRIBUTES;
    rmod->run(name, priority, stack, mode, attr);
  }catch(RCE::Dynalink::LinkingError& e) {
    char rs[256];
    sprintf(rs, "Linking error: %s", e.what());
    return rs;
  }
  std::cout<<"Loaded module at "<<std::hex<<rmod<<std::dec<<std::endl;
  m_running=true;
  pause();
  return "OK";
}
