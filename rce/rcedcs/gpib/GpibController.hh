#ifndef GPIBCONTROLLER_HH
#define GPIBCONTROLLER_HH

#include <string>

class GpibController{
public:
  GpibController(const char* hostname, int stationid);
  ~GpibController();
  int connect();
  void disconnect();
  void write(const char* command);
  const char* read(const char* command);
private:
  void setStationNumber(int n);
  void setModeAuto(bool on);
  std::string m_hostname;
  int m_stationid;
  int m_socket;
};

#endif
