#ifndef RCFFEI4BADAPTER_HH
#define RCFFEI4BADAPTER_HH

#include <stdint.h>
#include <RCF/RCF.hpp>
#include <SF/vector.hpp>
#include "rcf/PixelFEI4BConfig.hh"

RCF_BEGIN(I_RCFFEI4BAdapter, "I_RCFFEI4BAdapter")
RCF_METHOD_R1(int32_t, RCFdownloadConfig, ipc::PixelFEI4BConfig)
RCF_METHOD_V1(void, RCFsetChipAddress, uint32_t)
RCF_METHOD_R2(uint32_t, RCFwriteHWglobalRegister, int32_t, uint16_t)
RCF_METHOD_R2(uint32_t, RCFreadHWglobalRegister, int32_t, uint16_t&)
RCF_METHOD_R4(uint32_t, RCFwriteDoubleColumnHW, uint32_t, uint32_t, std::vector<uint32_t>, std::vector<uint32_t>&)
RCF_METHOD_R3(uint32_t, RCFreadDoubleColumnHW, uint32_t, uint32_t, std::vector<uint32_t>&)
RCF_END(I_RCFFEI4BAdapter)

    class RCFFEI4BAdapter 

    {
      virtual int32_t RCFdownloadConfig(ipc::PixelFEI4BConfig config)=0;
      virtual void RCFsetChipAddress(uint32_t val)=0;
      virtual uint32_t RCFwriteHWglobalRegister(int32_t reg, uint16_t val)=0;
      virtual uint32_t RCFreadHWglobalRegister(int32_t reg, uint16_t &val)=0;
      virtual uint32_t RCFwriteDoubleColumnHW(uint32_t bit, uint32_t dcol, std::vector<uint32_t> data, std::vector<uint32_t> &readback)=0;
      virtual uint32_t RCFreadDoubleColumnHW(uint32_t bit, uint32_t dcol, std::vector<uint32_t> &readback)=0;
    };

#endif
