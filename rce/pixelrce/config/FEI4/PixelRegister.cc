#include "config/FEI4/PixelRegister.hh"
#include "HW/SerialIF.hh"
#include "config/FEI4/FECommands.hh"
#include "config/FEI4/FEI4ARecord.hh"
#include "util/VerifyErrors.hh"
//#include "PixelRegister.hh"
#include <iostream>
#include <stdio.h>
#include <unistd.h> /* for usleep */

namespace FEI4{
  std::string PixelRegister::m_cachedName;
  PixelRegister::Field PixelRegister::m_cachedField(not_found);
  std::map<std::string, PixelRegister::Field> PixelRegister::m_parameters;
  bool PixelRegister::m_initialized = false;
  int PixelRegister::m_hitbusMode = 0;
  
  PixelRegister::PixelRegister(FECommands* commands):m_commands(commands){
    // initialize static field dictionay
    if(!m_initialized)initialize();
    //Clear all registers
    for (int i=0;i<N_PIXEL_REGISTER_BITS;i++){
      for(int k=0;k<N_COLS/2;k++){
	for (int j=0;j<DROW_WORDS;j++){
	  m_pixreg[i][k][j]=0;
	}
      }
    }
  }
  PixelRegister::~PixelRegister(){
  }
  void PixelRegister::initialize(){
    // Translation from scan parameter to Global register field
    m_parameters["TDACS"]=tdac;
    m_parameters["FDACS"]=fdac;
    m_initialized=true;
  }
  

  void PixelRegister::setDoubleColumn(unsigned bit, unsigned dcol, unsigned *data){
    if(bit<N_PIXEL_REGISTER_BITS && dcol<N_COLS/2){
      for(int i=0;i<DROW_WORDS;i++){
	m_pixreg[bit][dcol][i]=data[i];
      }
    }
  }
  void PixelRegister::setFieldAll(Field field, unsigned val){
    unsigned bitpos=PixelRegisterFields::fieldPos[field];
    unsigned mask=PixelRegisterFields::fieldMask[field];
    unsigned v=val;
    if(field==tdac)v=flipBits(5, val);// tdac has reverse bit order
    while(mask&1){
      setBitAll(bitpos, v&0x1);
      mask>>=1;
      v>>=1;
      bitpos++;
    }
  }

  void PixelRegister::setBitAll(int bit, int on){
    if( (unsigned)bit==PixelRegisterFields::fieldPos[hitbus] && m_hitbusMode==1)on=!on;
    unsigned setword=0;
    if(on)setword=0xffffffff;
    for (int j=0;j<N_COLS/2;j++){
      for(int k=0;k<DROW_WORDS;k++){
	m_pixreg[bit][j][k]=setword;
      }
    }
  }
  void PixelRegister::setBitDcol(unsigned dcol, int bit, int on){
    if((unsigned)bit==PixelRegisterFields::fieldPos[hitbus] && m_hitbusMode==1)on=!on;
    if(dcol>=N_COLS/2){
      std::cout<<"Error: Trying to set double column "<<dcol<<std::endl;
      return;
    }
    unsigned setword=0;
    if(on)setword=0xffffffff;
    for(int k=0;k<DROW_WORDS;k++){
      m_pixreg[bit][dcol][k]=setword;
    }
  }
  void PixelRegister::setBitCol(unsigned col, int bit, int on){
    if((unsigned)bit==PixelRegisterFields::fieldPos[hitbus] && m_hitbusMode==1)on=!on;
    if(col>N_COLS || col==0){
      std::cout<<"Error: Trying to set column "<<col<<std::endl;
      return;
    }
    unsigned dcol=(col-1)/2;
    unsigned setword=0;
    if(on)setword=0xffffffff;
    unsigned offset=0;
    if(col%2==1)offset=DROW_WORDS/2+1;
    for(unsigned k=offset;k<offset+DROW_WORDS/2;k++){
      m_pixreg[bit][dcol][k]=setword;
    }
    unsigned mask=0x0000ffff;
    if(col%2==0)mask=0xffff0000;
    if(on)m_pixreg[bit][dcol][10]|=mask;
    else m_pixreg[bit][dcol][10]&=~mask;
  }
  

  void PixelRegister::setupMaskStage(int bit, int maskStage, unsigned nMaskStages){
    setBitAll(bit, 0); //clear the bit
    int on=1;
    if((unsigned)bit==PixelRegisterFields::fieldPos[hitbus] && m_hitbusMode==1)on=0;
    for (int j=1;j<=N_COLS;j++){
      for(int k=maskStage+1;k<=N_ROWS;k+=nMaskStages){
	setBit(bit, k, j, on);
      }
    }
  }
  void PixelRegister::setupMaskStageCol(unsigned col, int bit, int maskStage, unsigned nMaskStages){
    setBitCol(col, bit, 0); //clear the bit
    int on=1;
    if((unsigned)bit==PixelRegisterFields::fieldPos[hitbus] && m_hitbusMode==1)on=0;
    for(int k=maskStage+1;k<=N_ROWS;k+=nMaskStages){
      setBit(bit, k, col, on);
    }
  }
  void PixelRegister::dumpPixelRegister(std::ostream &os){
    char line[512];
    for (int i=0;i<N_PIXEL_REGISTER_BITS;i++){
      sprintf(line, "BIT %d:\n",i);
      os<<line;
      for (int j=0;j<N_COLS/2;j++){
	sprintf(line, "Double Col: %02d: ", j);
	os<<line;
	for(int k=0;k<DROW_WORDS;k++){
	  sprintf (line, "%08x ",m_pixreg[i][j][k]);
	  os<<line;
	}
	os<<std::endl;
      }
    }
  }

  void PixelRegister::dumpDoubleColumn(unsigned bit, unsigned dcol, std::ostream &os){
    char line[512];
    sprintf(line, "BIT %d:\n",bit);
    os<<line;
    sprintf(line, "Double Col: %02d: ", dcol);
    os<<line;
    for(int k=0;k<DROW_WORDS;k++){
      sprintf (line, "%08x ",m_pixreg[bit][dcol][k]);
      os<<line;
    }
    os<<std::endl;
  }

  int PixelRegister::verifyDoubleColumnHW(int bit, unsigned dcol, std::vector<unsigned> &readback){
    readback.clear();
    BitStream *bs=new BitStream;
    m_commands->feWriteCommand(bs);
    for(int i=0;i<DROW_WORDS;i++)bs->push_back(0); // refill with 0
    bs->push_back(0);
    SerialIF::send(bs, SerialIF::WAITFORDATA);
    delete bs;
    usleep(100);
    if(readback.size()!=2*DROW_WORDS){
      std::cout<<std::dec<<"Dcol "<<dcol<<" bit "<<bit<<" Expecting 42 val records, received "<<readback.size()<<std::endl;
      return ModuleVerify::PIXEL_WRONG_N_WORDS;
    }else{
      for(int i=0;i<2*DROW_WORDS;i++){
	if(((~readback[i])&0xffff)!=((m_pixreg[bit][dcol][i/2]>>(16*((i+1)%2)))&0xffff)){
	  std::cout<<std::dec<<"Dcol "<<dcol<<" bit "<<bit<<" bad readback "<<std::endl; 
	 std::cout<<std::hex<<"Is "<<((~readback[i])&0xffff)<<" should be "<<((m_pixreg[bit][dcol][i/2]>>(16*((i+1)%2)))&0xffff)<<std::dec<<std::endl;
	 return ModuleVerify::PIXEL_READBACK_DIFFERENT;
	}
      }
    }
    return 0;
 }
  unsigned PixelRegister::writeDoubleColumnHW(unsigned bit, unsigned dcol){
    BitStream *bs=new BitStream;
    m_commands->feWriteCommand(bs);
    for(int i=0;i<DROW_WORDS;i++)bs->push_back(m_pixreg[bit][dcol][i]);
    bs->push_back(0);
    // for(size_t i=0;i<bs->size();i++)std::cout<<"input "<<std::hex<<(*bs)[i]<<std::dec<<std::endl;
    SerialIF::send(bs, SerialIF::WAITFORDATA);
    delete bs;
    return 0;
 }
  unsigned PixelRegister::writeDoubleColumnHW(const unsigned* data, std::vector<unsigned>& readback){
    BitStream *bs=new BitStream;
    m_commands->feWriteCommand(bs);
    for(int i=0;i<DROW_WORDS;i++)bs->push_back(data[i]);
    bs->push_back(0);
    //for(size_t i=0;i<bs->size();i++)std::cout<<"input "<<std::hex<<(*bs)[i]<<std::dec<<std::endl;
    std::vector<unsigned char> shiftin;
    unsigned stat=SerialIF::writeBlockData(*bs);
    usleep(1000);
    stat=SerialIF::readBuffers(shiftin);
    std::cout<<"Stat "<<stat<<" size "<<shiftin.size()<<std::endl;
    
    if (stat>0)stat=decodePixelRecord(shiftin, readback);
    /*
    if(stat>0){
      while (shiftin.size()%4!=0)shiftin.push_back(0);
      for(int i=0;i<shiftin.size();i+=4){
	readback.push_back(shiftin[i]<<24|shiftin[i+1]<<16|shiftin[i+2]<<8|shiftin[i+3]);
	std::cout<<std::hex<<readback[readback.size()-1]<<std::dec<<" "<<std::endl;
      }
      std::cout<<std::endl;
    }
    */
    delete bs;
    return stat;
  }
    
  unsigned PixelRegister::decodePixelRecord(std::vector<unsigned char>& shiftin, std::vector<unsigned>& readback){
    if(shiftin.size()==0)return FEI4ARecord::Empty;
    unsigned char* bytepointer=(unsigned char*)&shiftin[0];
    size_t size=shiftin.size();
    unsigned char* last=bytepointer+size-3;
    FEI4ARecord rec;
    bool odd=true;
    while(bytepointer<=last){
      rec.setRecord(bytepointer);
      printf("Record: %08x\n",rec.getUnsigned()&0xffffff);
      if(rec.isServiceRecord()){ // service record
	printf("Service record. Error code: %d. Count: %d \n",rec.getErrorCode(), rec.getErrorCount());
      } else if(rec.isAddressRecord()){ // address record
	if(!rec.isShift())return FEI4ARecord::NotShift; 
	std::cout<<"Read address record with address "<<rec.getAddress()<<std::endl;
      } else if(rec.isValueRecord()){ // value record
	unsigned val;
	printf("Value record: %08x\n",rec.getUnsigned()&0xffffff);
	if(odd==true)val=rec.getValue()<<16;
	else {
	 val|=rec.getValue();
	 readback.push_back(val);
	}
	odd=!odd;
      }
      bytepointer+=3;
    }
    std::cout<<"Return vector"<<std::endl;
    for(size_t i=0;i<readback.size();i++)std::cout<<std::hex<<readback[i]<<std::dec<<std::endl;
    if(!odd)return FEI4ARecord::OddNwords;
    return FEI4ARecord::OK;
  }

  PixelRegister::Field PixelRegister::lookupParameter(const char* name){
    // check if this is the last name used, return cached value
    if(std::string(name)==m_cachedName)return m_cachedField;
    // Now check if we can translate the name to a field name
    if(m_parameters.find(name)!=m_parameters.end()){
      //cache result
      m_cachedName=name;
      m_cachedField=m_parameters[name]; 
      return m_cachedField;
    }else return not_found;
  }

  void PixelRegister::setHitBusMode(int value){
    m_hitbusMode=value;
    std::cout<<"Set HitBus mode to "<<m_hitbusMode<<std::endl;
  }

}
