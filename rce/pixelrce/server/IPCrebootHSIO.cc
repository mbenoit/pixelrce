
#include <ipc/partition.h>
#include <ipc/core.h>
#include <ipc/object.h>
#include <boost/regex.hpp>
#include "server/IPCController.hh"
#include "IPCConfigIFAdapter.hh"

#include <boost/program_options.hpp>
#include <iostream>
#include <stdlib.h>

int main( int argc, char ** argv ){
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("partition,p", boost::program_options::value<std::string>(), "partition to work in.");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);
  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }

//
// Initialize command line parameters with default values
//
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }

    const char *p_name=getenv("TDAQ_PARTITION");
    if(p_name==NULL) p_name="rcetest";
    if(vm.count("partition"))p_name=vm["partition"].as<std::string>().c_str();
    IPCPartition   p(p_name );
    IPCController controller(p);
    // How many RCEs are in the partition?
    std::map< std::string, ipc::IPCConfigIFAdapter_var >	objects;
    std::vector<int> rces;
    p.getObjects<ipc::IPCConfigIFAdapter>( objects );
    std::map< std::string, ipc::IPCConfigIFAdapter_var >::iterator it;
    for ( it = objects.begin(); it != objects.end(); it++ ){
      boost::regex re("configIF_RCE(\\d+)");
      boost::cmatch matches;
      if(boost::regex_search(it->first.c_str(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	rces.push_back(strtol(match.c_str(),0,10));
      }
    }	
    if(rces.size()==0){
      std::cout<<"No RCE in your partition. Exiting."<<std::endl;
      exit(0);
    }
    int rce=rces[0];
    if(rces.size()>1){
      std::cout<<"There are multiple RCEs in your partition:"<<std::endl;
      while(1){
	for(size_t i=0;i<rces.size();i++)std::cout<<rces[i]<<std::endl;
	std::cout<<"Which one has the HSIO attached that you would like to reboot (99 = all)?"<<std::endl;
	std::cin>>rce;
	bool valid=false;
	for(size_t i=0;i<rces.size();i++)if(rce==99 || rce==rces[i])valid=true;
	if(valid)break;
	std::cout<<"This RCE is not on the list. Try again."<<std::endl;
      }
    }
    for(size_t i=0;i<rces.size();i++){
      if(rce==99||rce==rces[i]){
	controller.addRce(rces[i]);
	std::cout<<"Rebooting HSIO on RCE "<<rces[i]<<std::endl;
	controller.sendHWcommand(rces[i],8); //reboot
      }
    }
}
