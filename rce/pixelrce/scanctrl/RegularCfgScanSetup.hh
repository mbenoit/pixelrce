#ifndef REGULARCFGSCANSETUP_HH
#define REGULARCFGSCANSETUP_HH

#include "scanctrl/NestedLoop.hh"
#include "scanctrl/LoopSetup.hh"
#include "scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/ConfigIF.hh"

class RegularCfgScanSetup: public LoopSetup{
public:
  RegularCfgScanSetup():LoopSetup(){};
  virtual ~RegularCfgScanSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
