#ifndef AFPHPTDCCONFIG_HH
#define AFPHPTDCCONFIG_HH

#include "config/PixelConfig.hh"
#include  "AFPHPTDCModuleConfig.hh"
#include "config/afp-hptdc/AFPHPTDCConfigFile.hh"

class AFPHPTDCConfig: public PixelConfig{
public:
  AFPHPTDCConfig(std::string filename); 
  virtual ~AFPHPTDCConfig(){
    delete m_config;
  }
#ifdef __IPC__
  virtual int downloadConfig(IPCPartition&p, int rce, int id);
#else
  virtual int downloadConfig(int rce, int id);
#endif
  virtual void* getStruct(){return (void*)m_config;}
  virtual void writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key);
  virtual void configureFormatter(AbsFormatter*);

private:
  ipc::AFPHPTDCModuleConfig *m_config;
};

#endif
