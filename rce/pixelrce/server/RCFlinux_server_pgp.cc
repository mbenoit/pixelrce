//
//      RCFlinux_server_pgp.cc
//
//      RCF based calibration executable for Linux.
//
//      Martin Kocian
/////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include "util/RCFProvider.hh"
#include "util/HistoManager.hh"
#include "util/Monitoring.hh"
#include "scanctrl/RCFScan.hh"
#include "scanctrl/RCFScanRoot.hh"
#include "config/RCFConfigIF.hh"
#include "HW/SerialHexdump.hh"
#include "HW/SerialPgpFei4.hh"
#include "config/RCFModuleFactory.hh"
#include "util/RceName.hh"
#include "scanctrl/Scan.hh"
#include "server/PgpModL.hh"
#include "boost/thread/mutex.hpp"
#include "boost/thread/condition_variable.hpp"
boost::mutex mutex;
boost::condition_variable cond;


void sig_handler( int sig )
{    
  std::cout << " :: [IPCServer::sigint_handler] the signal " << sig << " received - exiting ... "<<std::endl;
  boost::mutex::scoped_lock pl( mutex );
  cond.notify_one();
}


//////////////////////////////////////////
//
// Main function
//
//////////////////////////////////////////


int main ( int argc, char ** argv )
{
 
    
   signal( SIGINT , sig_handler );
   signal( SIGTERM, sig_handler );
   

   //RCF
   RCF::RcfInitDeinit rcfInit;
   RCF::RcfServer server ( RCF::TcpEndpoint("0.0.0.0", RceName::MAINPORT));

   //Serial IF
   PgpModL pgp;
   pgp.open();
   //new SerialHexdump;
   new SerialPgpFei4;
   new Monitoring;

   //Module Factory

   ModuleFactory *moduleFactory=new RCFModuleFactory(server);
   
   RCFConfigIF rcfConfigIF(moduleFactory);
   RCFScan rcfScan;
   RCFScanRoot rcfScanRoot(server, &rcfConfigIF, &rcfScan);
   server.bind<I_RCFConfigIFAdapter>(rcfConfigIF);
   server.bind<I_RCFScanAdapter>(rcfScan);
   server.bind<I_RCFScanRootAdapter>(rcfScanRoot);
   Provider* ipcprov=new Provider(server);
   new HistoManager(ipcprov);
    //
   server.start();
   std::cout << "ipc_test_server has been started." << std::endl;

   boost::mutex::scoped_lock pl(mutex);
   cond.wait(pl);
   std::cout << "Shutdown." << std::endl;
  
   
   return 0;
}
