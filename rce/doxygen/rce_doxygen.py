#!/usr/bin/python

import pysvn
import os.path
import commands
workrev=-1
client=pysvn.Client()
workdir='/home/wittgen/svn_rce_trunk_doxy'
if( os.path.exists(workdir) ):
    entry=client.info(workdir)
    work_rev=entry.revision.number
else:
    client.checkout('svn+ssh://svn.cern.ch/svn/reps/RceCimDev/trunk',workdir)
    entry=client.info(workdir)
    work_rev=entry.revision.number
info=client.info2('svn+ssh://svn.cern.ch/svn/reps/RceCimDev/trunk',pysvn.Revision( pysvn.opt_revision_kind.head ),recurse=False)
latest_rev=info[0][1].rev.number
print latest_rev, work_rev
if ( latest_rev != work_rev ):
    client.update(workdir)
    print 'run doxygen'
    status,text=commands.getstatusoutput("cd /home/wittgen/svn_rce_trunk_doxy;source ./rcecalib/scripts/setup_rce.sh;make doxygen")
    print status






