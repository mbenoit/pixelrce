#ifndef FEI3__TRIGGER_HH
#define FEI3__TRIGGER_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "config/AbsTrigger.hh"
#include "HW/BitStream.hh"
#ifdef __rtems__
#include "rce/pic/Pool.hh"
#include "rce/pic/Tds.hh"
#endif


namespace FEI3{

  class Trigger: public AbsTrigger{
  public:
    Trigger();
    ~Trigger();
    int configureScan(boost::property_tree::ptree* scanOptions);
    int setupParameter(const char* name, int val);
    int sendTrigger();
    int enableTrigger(bool on);
    int resetCounters();
    void setupTriggerStream();
  private:
    BitStream m_triggerStream;
    int m_calL1ADelay;
#ifdef __rtems__
    RcePic::Tds* m_tds;
    RcePic::Pool *m_pool;
#endif
  };

};

#endif
