#ifndef DELAYSCANSETUP_HH
#define DELAYSCANSETUP_HH

#include "rcecalib/scanctrl/NestedLoop.hh"
#include "rcecalib/scanctrl/LoopSetup.hh"
#include "rcecalib/scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/ConfigIF.hh"

class DelayScanSetup: public LoopSetup{
public:
  DelayScanSetup():LoopSetup(){};
  virtual ~DelayScanSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
