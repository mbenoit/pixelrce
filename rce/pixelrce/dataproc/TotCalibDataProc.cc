#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "dataproc/TotCalibDataProc.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"
#include "dataproc/fit/CalculateMeanSigma.cc"

int TotCalibDataProc::fit(std::string fitfun) {
#ifdef RCEPIXLIB
  std::cout << "Running: " << fitfun << std::endl;
  //  if(fitfun=="CALCULATE_MEAN_SIGMA" && m_nPoints!=0) {
     // calculateMeanSigma(m_histo_occ, m_histo_tot, m_histo_tot2, m_histo_tot_mean, m_histo_tot_sigma); 
  }
#endif
  return 0;
}

int TotCalibDataProc::processData(unsigned link, unsigned *data, int size){
  //    std::cout<<"Process data"<<std::endl;
  for (int i=0;i<size;i++){
    int module=m_linkToIndex[link]; // will be different when parser is fully there
    FormattedRecord current(data[i]);
    //    if (current.isHeader()){
         // l1id = current.getL1id();
         // bcid = current.getBxid();
      //printf("bcid : %x \n", bcid);
      //printf("l1id : %x \n", l1id);
         // if (l1id != l1id_last)
	    //{
	    //  l1id_last = l1id;
	    //  bcid_ref  = bcid;
	    //}
         // bcid = bcid-bcid_ref;
      // printf("bcidafter : %x \n", bcid);
        //}
    if (current.isData()){
      unsigned int tot=current.getToT();
      unsigned int col=current.getCol();
      unsigned int row=current.getRow();
      if((row<(unsigned)m_info[module].getNRows()) && (col<(unsigned)m_info[module].getNColumns()) && tot<15 && tot!=0) {
	m_histo_tot[module][tot-1]->incrementFast(col,row);
      }
    }
  }
  return 0;
}

TotCalibDataProc::TotCalibDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif) {

  for (unsigned int module=0;module<m_configIF->getNmodules();module++){
    std::vector<RceHisto2d<char, char>* > vhcc;
    m_histo_tot.push_back(vhcc);
    char name[128];
    char title[128];
    RceHisto2d<char, char> *histoc;
    unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
    unsigned int rows=m_info[module].getNRows();
    unsigned int moduleId=m_info[module].getId();
    std::string moduleName=m_info[module].getName();
    for(int i=1;i<15;i++){ //ToT loop
      /* retrieve scan points - Vcal steps in this case */
      sprintf(title,"OCCUPANCY Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Occupancy_ToT_%02d", moduleId, i);
      histoc=new RceHisto2d<char, char>(name,title,cols,0,cols,rows,0,rows);
      histoc->setAxisTitle(0,"Column");
      histoc->setAxisTitle(1, "Row");
      m_histo_tot[module].push_back(histoc);
    }
  }
}


TotCalibDataProc::~TotCalibDataProc(){
  for (size_t module=0;module<m_histo_tot.size();module++)
    for(size_t i=0;i<m_histo_tot[module].size();i++)delete m_histo_tot[module][i];
}
  

