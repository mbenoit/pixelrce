#include "scanctrl/DelayScanSetup.hh"
#include "scanctrl/ScanLoop.hh"
#include <boost/property_tree/ptree.hpp>
#include "scanctrl/ActionFactory.hh"
#include "scanctrl/LoopAction.hh"
#include "scanctrl/EndOfLoopAction.hh"
#include <iostream>

int DelayScanSetup::setupLoops( NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af){
  int retval=0; 
  loop.clear();
  try{ //catch bad scan option parameters
    // This calibration has a single loop
    int nPoints = scanOptions->get<int>("scanLoop_0.nPoints");
    ScanLoop *scanloop=new ScanLoop("scanLoop_0",nPoints);
    LoopAction* disable=af->createLoopAction("DISABLE_TRIGGER");
    scanloop->addLoopAction(disable);
    LoopAction* setdelay=af->createLoopAction("COSMIC_SET_INPUT_DELAY",&scanOptions->get_child("scanLoop_0"));
    scanloop->addLoopAction(setdelay);
    LoopAction* changebin=af->createLoopAction("CHANGE_BIN");
    scanloop->addLoopAction(changebin);
    LoopAction* enabletdc=af->createLoopAction("COSMIC_ENABLE_TDC");
    scanloop->addLoopAction(enabletdc);
    LoopAction* enablesc=af->createLoopAction("COSMIC_ENABLE_SCINTILLATOR_TRIGGER");
    scanloop->addLoopAction(enablesc);
    LoopAction* enable=af->createLoopAction("ENABLE_TRIGGER");
    scanloop->addLoopAction(enable);
    // End of loop actions
    EndOfLoopAction* disabletrigger=af->createEndOfLoopAction("DISABLE_TRIGGER","");
    scanloop->addEndOfLoopAction(disabletrigger);
    EndOfLoopAction* disablechannels=af->createEndOfLoopAction("DISABLE_ALL_CHANNELS","");
    scanloop->addEndOfLoopAction(disablechannels);
    // loop is the nested loop object
    loop.addNewInnermostLoop(scanloop);
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    retval=1;
  }
  //  loop.print();
  return retval;
}
