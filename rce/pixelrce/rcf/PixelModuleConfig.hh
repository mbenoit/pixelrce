/**************************************************************************
 *
 * Title: Master/PIX moduleConfigStructures.h
 * Version: 11th November 2002
 *
 * Description:
 * ROD Master DSP Pixel module configuration structures
 * and constant definitions.
 **************************************************************************/
#ifndef PIXELMODULECONFIG_HH
#define PIXELMODULECONFIG_HH

#include <RCF/RCF.hpp>
#include <stdint.h>

namespace ipc{

  const long IPC_N_PIXEL_COLUMNS =18;
  const long IPC_N_PIXEL_ROWS =160;
  const long IPC_N_PIXEL_FE_CHIPS=16;
  
  const long IPC_FE_I1  =1;
  const long IPC_FE_I2  =2;
  const long IPC_MCC_I1 =1;
  const long IPC_MCC_I2 =2;

  /* MCC options */

  const long IPC_MCC_SINGLE_40 =0;
  const long IPC_MCC_DOUBLE_40 =1;
  const long IPC_MCC_SINGLE_80 =2;
  const long IPC_MCC_DOUBLE_80 =3;


struct PixelFEGlobal{ //FE global register
	//This is valid for both FE-I1 & 2, since the FE-I1 is a sub-set.
	uint8_t  latency             ;                 
	uint8_t  dacIVDD2            ;                    
	uint8_t  dacIP2              ;                      
	uint8_t  dacID               ;
	
	uint8_t  dacIP               ;                       
	uint8_t  dacITRIMTH          ;                  
	uint8_t  dacIF               ;                       
	uint8_t  dacITH1             ;
	
	uint8_t  dacITH2             ;                     
	uint8_t  dacIL               ;                       
	uint8_t  dacIL2              ;                      
	uint8_t  dacITRIMIF          ; 
	
	uint8_t  dacSpare            ;                        
	uint8_t  threshTOTMinimum    ;            
	uint8_t  threshTOTDouble     ;             
	uint8_t  hitbusScaler        ;
	
	uint8_t  capMeasure          ; 
	uint8_t  gdac                ;       
	uint8_t  selfWidth           ;
	uint8_t  selfLatency         ;
	uint8_t  muxTestPixel        ;
	uint8_t  aregTrim            ;
	uint8_t  aregMeas            ;
	uint8_t  dregTrim            ;
	uint8_t  dregMeas            ;
	uint8_t  parity              ;                
	
	uint8_t  dacMonLeakADC       ;
  /* was defined as uint8_t, but needs 10 bits */
	uint32_t dacVCAL      ; /* extended to 10 bit for FE2/3 */          
	uint8_t  widthSelfTrigger    ;        
	uint8_t  muxDO               ; /* note: dynamically defined */                      
	uint8_t  muxMonHit           ;
	uint8_t  muxEOC              ;                      
	
	uint8_t  frequencyCEU        ;                
	uint8_t  modeTOTThresh       ;               
	uint8_t  enableTimestamp     ;             
	uint8_t  enableSelfTrigger   ;        
	uint8_t  enableHitParity     ;                    
	uint8_t  monMonLeakADC       ;            
	uint8_t  monADCRef           ;                
	uint8_t  enableMonLeak       ;            
	uint8_t  statusMonLeak       ;            
	uint8_t  enableCapTest       ;                
	uint8_t  enableBuffer        ;                 
	uint8_t  enableVcalMeasure   ;            
	uint8_t  enableLeakMeasure   ;            
	uint8_t  enableBufferBoost   ;            
	uint8_t  enableCP8           ;                    
	uint8_t  monIVDD2            ;                     
	uint8_t  monID               ;                        
	uint8_t  enableCP7           ;                    
	uint8_t  monIP2              ;                       
	uint8_t  monIP               ;                        
	uint8_t  enableCP6           ;                    
	uint8_t  monITRIMTH          ;                   
	uint8_t  monIF               ;                        
	uint8_t  enableCP5           ;                    
	uint8_t  monITRIMIF          ;                   
	uint8_t  monVCAL             ;                      
	uint8_t  enableCP4           ;                    
	uint8_t  enableCinjHigh      ;               
	uint8_t  enableExternal      ;               
	uint8_t  enableTestAnalogRef ;
	
	uint8_t  enableDigital       ;                
	uint8_t  enableCP3           ;                    
	uint8_t  monITH1             ;                      
	uint8_t  monITH2             ;                      
	uint8_t  enableCP2           ;                    
	uint8_t  monIL               ;                        
	uint8_t  monIL2              ;                       
	uint8_t  enableCP1           ;                                 
	uint8_t  enableCP0           ;   
	uint8_t  enableHitbus        ;                 
	uint8_t  monSpare            ;                     
	uint8_t  enableAregMeas      ;
	uint8_t  enableAreg          ;
	uint8_t  enableLvdsRegMeas   ;
	uint8_t  enableDregMeas      ;
	uint8_t  enableTune          ;
	uint8_t  enableBiasComp      ;
	uint8_t  enableIpMonitor     ;
    void serialize(SF::Archive &ar){
	ar &  latency             ;                 
	ar &  dacIVDD2            ;                    
	ar &  dacIP2              ;                      
	ar &  dacID               ;
	ar &  dacIP               ;                       
	ar &  dacITRIMTH          ;                  
	ar &  dacIF               ;                       
	ar &  dacITH1             ;
	ar &  dacITH2             ;                     
	ar &  dacIL               ;                       
	ar &  dacIL2              ;                      
	ar &  dacITRIMIF          ; 
	ar &  dacSpare            ;                        
	ar &  threshTOTMinimum    ;            
	ar &  threshTOTDouble     ;             
	ar &  hitbusScaler        ;
	ar &  capMeasure          ; 
	ar &  gdac                ;       
	ar &  selfWidth           ;
	ar &  selfLatency         ;
	ar &  muxTestPixel        ;
	ar &  aregTrim            ;
	ar &  aregMeas            ;
	ar &  dregTrim            ;
	ar &  dregMeas            ;
	ar &  parity              ;                
	ar &  dacMonLeakADC       ;
	ar & dacVCAL      ; 
	ar &  widthSelfTrigger    ;        
	ar &  muxDO               ;
	ar &  muxMonHit           ;
	ar &  muxEOC              ;                      
	ar &  frequencyCEU        ;                
	ar &  modeTOTThresh       ;               
	ar &  enableTimestamp     ;             
	ar &  enableSelfTrigger   ;        
	ar &  enableHitParity     ;                    
	ar &  monMonLeakADC       ;            
	ar &  monADCRef           ;                
	ar &  enableMonLeak       ;            
	ar &  statusMonLeak       ;            
	ar &  enableCapTest       ;                
	ar &  enableBuffer        ;                 
	ar &  enableVcalMeasure   ;            
	ar &  enableLeakMeasure   ;            
	ar &  enableBufferBoost   ;            
	ar &  enableCP8           ;                    
	ar &  monIVDD2            ;                     
	ar &  monID               ;                        
	ar &  enableCP7           ;                    
	ar &  monIP2              ;                       
	ar &  monIP               ;                        
	ar &  enableCP6           ;                    
	ar &  monITRIMTH          ;                   
	ar &  monIF               ;                        
	ar &  enableCP5           ;                    
	ar &  monITRIMIF          ;                   
	ar &  monVCAL             ;                      
	ar &  enableCP4           ;                    
	ar &  enableCinjHigh      ;               
	ar &  enableExternal      ;               
	ar &  enableTestAnalogRef ;
	ar &  enableDigital       ;                
	ar &  enableCP3           ;                    
	ar &  monITH1             ;                      
	ar &  monITH2             ;                      
	ar &  enableCP2           ;                    
	ar &  monIL               ;                        
	ar &  monIL2              ;                       
	ar &  enableCP1           ;                                 
	ar &  enableCP0           ;   
	ar &  enableHitbus        ;                 
	ar &  monSpare            ;                     
	ar &  enableAregMeas      ;
	ar &  enableAreg          ;
	ar &  enableLvdsRegMeas   ;
	ar &  enableDregMeas      ;
	ar &  enableTune          ;
	ar &  enableBiasComp      ;
	ar &  enableIpMonitor     ;
    }
} ;				     
							    

struct PixelFEMasks{ //Pixel-level control bits
	uint32_t maskEnable[5][IPC_N_PIXEL_COLUMNS]; /* 32 bits, one bit per pixel thus 5 words per column */
	uint32_t maskSelect[5][IPC_N_PIXEL_COLUMNS];
	uint32_t maskPreamp[5][IPC_N_PIXEL_COLUMNS];
	uint32_t maskHitbus[5][IPC_N_PIXEL_COLUMNS];
    void serialize(SF::Archive &ar){
      for(int i=0;i<5;i++)
	for(int j=0;j<IPC_N_PIXEL_COLUMNS;j++)
	  ar & maskEnable[i][j];
      for(int i=0;i<5;i++)
	for(int j=0;j<IPC_N_PIXEL_COLUMNS;j++)
	  ar & maskSelect[i][j];
      for(int i=0;i<5;i++)
	for(int j=0;j<IPC_N_PIXEL_COLUMNS;j++)
	  ar & maskPreamp[i][j];
      for(int i=0;i<5;i++)
	for(int j=0;j<IPC_N_PIXEL_COLUMNS;j++)
	  ar & maskHitbus[i][j];
    }
} ;  
   

struct PixelFETrims{ //Trim DACs:
	uint8_t dacThresholdTrim[IPC_N_PIXEL_ROWS][IPC_N_PIXEL_COLUMNS]; 
	uint8_t  dacFeedbackTrim[IPC_N_PIXEL_ROWS][IPC_N_PIXEL_COLUMNS];
    void serialize(SF::Archive &ar){
      for(int i=0;i<IPC_N_PIXEL_ROWS;i++)
	for(int j=0;j<IPC_N_PIXEL_COLUMNS;j++)
	  ar & dacThresholdTrim[i][j];
      for(int i=0;i<IPC_N_PIXEL_ROWS;i++)
	for(int j=0;j<IPC_N_PIXEL_COLUMNS;j++)
	  ar & dacFeedbackTrim[i][j];
    }
} ; 


struct PixelFECalib{ 
/* Sub-structure for calibration of injection-capacitors, VCAL-DAC and
   leakage current measurement */
	float cinjLo;
	float cinjHi;
	float vcalCoeff[4];
	float chargeCoeffClo;  
	float chargeCoeffChi;
	float chargeOffsetClo;
	float chargeOffsetChi;
	float monleakCoeff;
    void serialize(SF::Archive &ar){
      ar & cinjLo & cinjHi;
      for(int i=0;i<4;i++)ar & vcalCoeff[i];
      ar & chargeCoeffClo & chargeCoeffChi & chargeOffsetClo & chargeOffsetChi & monleakCoeff;
    }
} ; 


struct PixelMCCRegisters{ //MCC registers
   uint16_t regCSR;                  
   uint16_t regLV1;                  
   uint16_t regFEEN;				 
   uint16_t regWFE;					 

   uint16_t regWMCC; 				
   uint16_t regCNT;					 
   uint16_t regCAL;					 
   uint16_t regPEF;		

   uint16_t regWBITD;
   uint16_t regWRECD;
   uint16_t regSBSR;

   /* Strobe duration is a virtual register (shared with CNT); when preparing the
      modules for data this value is substituted for the count. */
   uint16_t regSTR;
  void serialize(SF::Archive &ar){
    ar & regCSR & regLV1 & regFEEN & regWFE & regWMCC & regCNT & regCAL & regPEF 
       & regWBITD & regWRECD & regSBSR;
  }
} ;

struct PixelFEConfig{ //FE level parameters
	uint32_t FEIndex; 	
	PixelFEGlobal FEGlobal;
	PixelFEMasks FEMasks;
	PixelFETrims FETrims;
	PixelFECalib FECalib;
  void serialize(SF::Archive &ar){
    ar & FEIndex & FEGlobal & FEMasks & FETrims & FECalib;
  }
} ;


struct PixelModuleConfig{
	/* FE module-level options: */
	uint16_t maskEnableFEConfig; /* 16 bits, one per FE */
	uint16_t maskEnableFEScan;
	uint16_t maskEnableFEDacs;
	uint8_t feFlavour;
	uint8_t mccFlavour;
	
	/* FE configurations: */
	PixelFEConfig FEConfig[IPC_N_PIXEL_FE_CHIPS];
	
	/* MCC configuration */
	PixelMCCRegisters MCCRegisters;
	
	char  idStr[256]; /* Module identification string */
	uint8_t present;    /* Module is physically present. Does not need setting
	                     externally; handled by the Master DSP. */
	
	uint8_t active;     /* 1 -> participates in scans */
	                  /* 0 -> registers unchanged during scanning */
	uint8_t moduleIdx;  /* Copy of the module's index for access from a pointer */
	uint8_t groupId;    /* The ID of the module's group. This is used to indicate
	                     which slave DSP will receive the module's data (if group
	                     based distribution is set), and also to allow different
	                     module groups to be triggered independently (for
	                     cross-talk studies). valid range: [0,7] */
	uint8_t pTTC;       /* primary TX channel  */
	uint8_t rx;         /* data link used by module */
  void serialize(SF::Archive &ar){
    ar & maskEnableFEConfig & maskEnableFEScan & maskEnableFEDacs & feFlavour & mccFlavour;
    for (int i=0;i<IPC_N_PIXEL_FE_CHIPS;i++) ar & FEConfig[i];
    ar & MCCRegisters;
    for (int i=0;i<256;i++) ar & idStr[i];
    ar & present & active & moduleIdx & groupId & pTTC & rx;
  }
	
} ; /* declare N_PIXEL_CONFIG_SETS structure for each of N_PIXEL_MODULES */
};

#endif   /* multiple inclusion protection */


