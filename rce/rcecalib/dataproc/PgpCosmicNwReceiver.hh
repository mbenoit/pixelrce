#ifndef PGPCOSMICNWRECEIVER_HH
#define PGPCOSMICNWRECEIVER_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/dataproc/AbsReceiver.hh"
#include "rcecalib/HW/Receiver.hh"
#include "rcecalib/profiler/Profiler.hh"
#include <iostream>
#include "namespace_aliases.hh"


namespace eudaq{
  class DataSender;
}

class PgpCosmicNwReceiver: public AbsReceiver, public PgpTrans::Receiver{
public:
  PgpCosmicNwReceiver(AbsDataHandler* handler, boost::property_tree::ptree* scanOptions);
  virtual ~PgpCosmicNwReceiver();
  void receive(PgpTrans::PgpData *pgpdata);
  void resynch();
private:
  unsigned* m_buffer;
  unsigned m_counter;
  bool m_linksynch[16];
  int m_tossed[16];
  int m_print;
  Profiler::Timer m_timer;
  eudaq::DataSender *m_ds;
  unsigned char *m_sendbuffer;
  int m_bufferpointer;
  unsigned m_abs_sec;
  unsigned m_bufferssend, m_senddiff;
  unsigned m_oldl1, m_oldbx, m_firstbx, m_bad;
};
#endif
