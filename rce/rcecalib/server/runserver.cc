/// @file core.cc
/// @brief As the last part of the initialization of the system after
/// a reboot, load from flash, relocate and run the application module
/// selected by the RCE front panel.

#ifdef RCE_V2
#include "datCode.hh"
#include DAT_PUBLIC( service, fci,       Exception.hh)
#else
#include "rce/debug/Debug.hh"
#include "rce/debug/Print.hh"
#include "rce/shell/ShellCommon.hh"
#include "rce/gdbstub/rtems-gdb-stub.h"


#include "rce/service/Thread.hh"
#include "rceusr/init/NetworkConfig.hh"
#include "rceusr/tool/DebugHandler.hh"

#include "rce/pgp/DriverList.hh"

extern "C"{
  #include <librtemsNfs.h>
}
#endif

#include <omnithread.h>
#include <pthread.h>
#include "calibserver.hh"

#include <exception>
#include <stdio.h>
#include <string>

#include <iostream>

// We need to include <iostream> so that symbolic references to
// std::cout et al. are made.  The weak definitions will therefore be
// picked up from libstdc++.  Modules are not linked against libstdc++
// so they rely on the definitions in the core.
#include <iostream>

using std::exception;
using std::string;

using RCE::Shell::startShell;

using RceDebug::printv;

using RceInit::configure_network_from_dhcp;

using RceSvc::Exception;

// use global IP address of bootp server in RTEMS
extern struct in_addr rtems_bsdnet_bootp_server_address;

extern "C" {

  // This symbol is set by the static linker (ld) to the start if the
  // "dynamic section" which the linker uses to find the system symbol
  // table.
  extern const char _DYNAMIC;
  
  void *posix_init( void *argument );
#if RCE_INTERFACE == 1
#warning RCE_INTERFACE=1
#elif RCE_INTERFACE == 0
#warning RCE_INTERFACE=0
#else
#error invalid RCE_INTERFACE
#endif


  unsigned interface = RCE_INTERFACE; 
  void init_executive()
  {
    try {
         RceInit::configure_network_from_dhcp(interface);
	 RcePgp::init_pgp();
         printf("starting shell\n");	
	 startShell();
	 // Start a debugger daemon
	 // First arg = 0 -->  use socket IO over TCP.
	 // Second arg = 0 --> Priority is chosen by the debugger daemon 
	 rtems_gdb_start(0, 0);

	 rpcUdpInit();
	 nfsInit( 0, 0 );
	 printf("BOOTP server is %s. Use as NFS server.\n",inet_ntoa( rtems_bsdnet_bootp_server_address));
	 nfsMount(inet_ntoa( rtems_bsdnet_bootp_server_address),(char*) NFSDIR ,(char*)"/nfs"); //NFSDIR comes from constituents.mk
         pthread_t mthread;
         pthread_attr_t attr;
         int stacksize;
         int ret;
         // setting a new size
         stacksize = (PTHREAD_STACK_MIN + 0x20000);
        // void* stackbase = (void *) malloc(size);
         ret = pthread_attr_init(&attr);
         ret = pthread_attr_setstacksize(&attr, stacksize);
	 struct sched_param sparam;
	 sparam.sched_priority = 5;
	 pthread_attr_setschedparam(&attr, &sparam);
	 pthread_create( &mthread, &attr , posix_init, NULL);

	   
    } catch (Exception& e) {
      printv("*** RCE exception %s", e.what());
    } catch (exception& e) {
      printv("*** C++ exception %s", e.what());
    }
    printf("Done with init_executive");
  }
void *posix_init( void *argument ){
  printf("posixinit\n");	
  std::cout<<"Init Exec called"<<std::endl;	
  omni_thread::init_t omni_thread_init;
  pthread_attr_t attr;
  size_t st = 0;
  pthread_attr_init(&attr);
  pthread_attr_getstacksize( &attr, &st );
  st = _Thread_Executing->Start.Initial_stack.size;
  printf( "Init Task Stack Size is: %d\n", st );
  calibserver::run(false); 
  printf("posixinit done\n");	
 return 0;
}
}
