#include "analysis/Fei3CfgFileWriter.hh"

#include <iostream>
#include <fstream>
#include <boost/regex.hpp>
#include <TFile.h>
#include <TH2.h>


void Fei3CfgFileWriter::writeDacFile(const char* filename, TH2* tdac){
  int ncol=18;
  int nrow=160;
  std::cout<<std::dec;
  for (int i=0;i<16;i++){
    std::string name(filename);
    name+=Form("_FE_%d.dat",i);
    std::ofstream maskfile(name.c_str());
    for (int col=1;col<=ncol;col++){
      for (int row=1;row<=nrow;row++){
	maskfile<<int(tdac->GetBinContent(i*ncol+col, row)+0.1)<<" ";
      }
      maskfile<<std::endl;
    }
    maskfile.close();
  }
}


void Fei3CfgFileWriter::writeMaskFile(const char* filename, TH2* histo){
  int ncol=18;
  std::cout<<std::dec;
  for (int i=0;i<16;i++){
    std::string name(filename);
    name+=Form("_FE_%d.dat",i);
    std::ofstream maskfile(name.c_str());
    for (int l=0;l<ncol;l++){
      std::cout<<Form("%-6d", l);
      for(int j=4;j>=0;j--){
	unsigned mask=0;
	std::cout<<" ";
	for(int k=31;k>=0;k--){
	  if(histo->GetBinContent(i*ncol+l+1, j*32+k+1)!=0)mask|=1;
	  mask<<=1;
	}
	std::cout<<std::hex<<mask<<std::dec;
      }
      std::cout<<std::endl;
    }
    maskfile<<std::endl;
    maskfile.close();
  }
}
