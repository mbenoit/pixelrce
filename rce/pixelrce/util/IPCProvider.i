#ifndef PROVIDER_CC
#define PROVIDER_CC


template<class TC,class TE>
void 
Provider::publish(const std::string & name,
	const std::string & title,
	RceHistoAxis & xaxis,
	const TC * bins,
	const TE * errors,
	bool hasOverflowAndUnderflow,
	int tag ,
	const std::vector< std::pair<std::string,std::string> > & annotations){ 
  OHAxis  xaxisoh=OHMakeAxis<double>(xaxis.label().c_str(), xaxis.bincount(), dsw(xaxis.low()), dsw(xaxis.width()) );
  // Publish in OH
  try {
    m_provider->publish(name, title, xaxisoh, bins, errors, hasOverflowAndUnderflow, tag, annotations);
  } catch (daq::oh::RepositoryNotFound) {
    std::string mess = "The repository is not found ";
    std::cout<<mess<<std::endl;
  } catch (daq::oh::ObjectTypeMismatch) {
    std::string mess = "Arguments invalid ";
    std::cout<<mess<<std::endl;
  } catch (...) {
    std::string mess = "Unknown exception thrown ";
    std::cout<<mess<<std::endl;
  }
}

template<class TC,class TE>
void
Provider::publish(const std::string & name,
		     const std::string & title,
		     RceHistoAxis & xaxis,
		     RceHistoAxis & yaxis,
		     const TC * bins,
		     const TE * errors,
		     bool hasOverflowAndUnderflow,
		     int tag,
		     const std::vector< std::pair<std::string,std::string> > & annotations ) {
  OHAxis  xaxisoh=OHMakeAxis<double>(xaxis.label().c_str(), xaxis.bincount(), dsw(xaxis.low()), dsw(xaxis.width()) );
  OHAxis  yaxisoh=OHMakeAxis<double>(yaxis.label().c_str(), yaxis.bincount(), dsw(yaxis.low()), dsw(yaxis.width()) );
  try {
    m_provider->publish(name, title, xaxisoh, yaxisoh, bins, errors, hasOverflowAndUnderflow, tag, annotations);
  } catch (daq::oh::RepositoryNotFound) {
    std::string mess = "The repository is not found ";
    std::cout<<mess<<std::endl;
  } catch (daq::oh::ObjectTypeMismatch) {
    std::string mess = "Arguments invalid ";
    std::cout<<mess<<std::endl;
  } catch (...) {
    std::string mess = "Unknown exception thrown ";
    std::cout<<mess<<std::endl;
  }
}

#endif
