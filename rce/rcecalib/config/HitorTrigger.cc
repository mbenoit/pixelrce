
#include "rcecalib/config/HitorTrigger.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/HW/BitStream.hh"
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/config/FEI4/FECommands.hh"
#include "rcecalib/util/exceptions.hh"
#include <iostream>
#include <fstream>
#include <stdlib.h>


HitorTrigger::HitorTrigger(ConfigIF* cif):AbsTrigger(), m_configIF(cif){
    //std::cout<<"Hitor trigger"<<std::endl;
  }
  
int HitorTrigger::configureScan(boost::property_tree::ptree* scanOptions){
  return 0;
}
  
int HitorTrigger::sendTrigger(){

  // read errors
  m_configIF->resetErrorCountersHW();
  m_i++;
  return 0;
}


