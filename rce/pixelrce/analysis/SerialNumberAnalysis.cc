#include "analysis/SerialNumberAnalysis.hh"
#include "server/PixScan.hh"
#include "config/FEI4/FEI4BConfig.hh"

#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>

using namespace RCE;

void SerialNumberAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  std::cout<<"Serial number analysis"<<std::endl;
  TIter nextkey(file->GetListOfKeys());
  TKey *key;
  boost::regex re("_(\\d+)_SN");
  boost::regex re2("at ([AC][1-8]-[12]) ");
  int sn=0;
  int allmatch=true;
  while ((key=(TKey*)nextkey())) {
    std::string name(key->GetName());
    std::string title(key->GetTitle());
    boost::cmatch matches;
    if(boost::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      boost::regex_search(title.c_str(), matches, re2);
      assert(matches.size()>1);
      std::string match2(matches[1].first, matches[1].second);
      TH1* histo = (TH1*)key->ReadObj();
      int sn_read=histo->GetBinContent(1);
      ipc::PixelFEI4BConfig* conf=(ipc::PixelFEI4BConfig*)findConfig(cfg, id)->getStruct();
      if(conf){
	sn=conf->FEGlobal.Chip_SN;
	if(sn!=sn_read){
	  std::cout<<"FE at "<<match2<<": Readback of serial number ("<<sn_read<<") does not match serial number in config file ("<<sn<<")."<<std::endl;
	  allmatch=false;
	}else{
	  std::cout<<"FE at "<<match2<<": Readback of serial number ("<<sn_read<<") identical as in config file."<<std::endl;
	}
        conf->FEGlobal.Chip_SN=sn_read;
	writeConfig(anfile, runno);
      }
      delete histo;
    }
  }  
  if(allmatch)std::cout<<"The configurations for all frontends match between EFUSE and config file."<<std::endl;
  else std::cout<<"!!!!!!! There are mismatches between read back values and config files."<<std::endl;
  if(configUpdate())writeTopFile(cfg, anfile, runno);
}

