#ifndef SERIALNUMBERANALYSIS_HH
#define SERIALNUMBERANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"

class ConfigGui;
class TFile;
class TH2D;
namespace RCE{
  class PixScan;
}

class SerialNumberAnalysis: public CalibAnalysis{
public:
  SerialNumberAnalysis(): CalibAnalysis(){}
  ~SerialNumberAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
