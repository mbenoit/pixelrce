#ifndef IPCCALLBACK_HH
#define IPCCALLBACK_HH

#include "ipc/object.h"
#include "Callback.hh"
#include <ipc/server.h>

class IPCCallback : public IPCServer,
		    public IPCObject<POA_ipc::Callback>
{
public:
  IPCCallback(): m_rce(0){}
  void notify( const ipc::CallbackParams &msg )=0;
  void stopServer()=0;
  void addRce(){m_rce++;}
protected:
  int m_rce;
};

#endif
