
#include "rcecalib/server/IPCController.hh"
#include "rcecalib/server/IPCRegularCallback.hh"
#include "rcecalib/server/PixScan.hh"
#include "IPCFEI3Adapter.hh"
#include "IPCFEI4AAdapter.hh"
#include "IPCFEI4BAdapter.hh"
#include "IPCHitbusAdapter.hh"
#include "IPCAFPHPTDCAdapter.hh"
#include "IPCScanAdapter.hh"
#include "IPCConfigIFAdapter.hh"
#include "IPCScanRootAdapter.hh"
#include "ScanOptions.hh"
#include "ers/ers.h"
#include <stdio.h>
#include <boost/lexical_cast.hpp>
#include <string>

IPCController::IPCController(IPCPartition &p):m_partition(p){}

void IPCController::addRce(int rce){
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)return; //RCE has already been added.
  m_rces.push_back(rce);
}
void IPCController::removeAllRces(){
  m_rces.clear();
}
void IPCController::downloadModuleConfig(int rce, int id, PixelModuleConfig& config){
  ipc::IPCFEI3Adapter_var modhandle;
  std::string names=boost::lexical_cast<std::string>(id)+"_RCE"+boost::lexical_cast<std::string>(rce);
  const char* name=names.c_str();
  try {
    bool v=m_partition.isObjectValid<ipc::IPCFEI3Adapter>(name);
    if(!v) {
      std::cout<<"Not valid"<<std::endl;
      assert(0);
    }
    modhandle = m_partition.lookup<ipc::IPCFEI3Adapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    modhandle -> IPCdownloadConfig( config );
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
}

void IPCController::downloadModuleConfig(int rce, int id, PixelFEI4AConfig& config){
  ipc::IPCFEI4AAdapter_var modhandle;
  std::string names=boost::lexical_cast<std::string>(id)+"_RCE"+boost::lexical_cast<std::string>(rce);
  const char* name=names.c_str();
  try {
    bool v=m_partition.isObjectValid<ipc::IPCFEI4AAdapter>(name);
    if(!v) {
      std::cout<<name<<" not valid"<<std::endl;
      assert(0);
    }
    modhandle = m_partition.lookup<ipc::IPCFEI4AAdapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    modhandle -> IPCdownloadConfig( config );
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
}

void IPCController::downloadModuleConfig(int rce, int id, PixelFEI4BConfig& config){
  ipc::IPCFEI4BAdapter_var modhandle;
  std::string names=boost::lexical_cast<std::string>(id)+"_RCE"+boost::lexical_cast<std::string>(rce);
  const char* name=names.c_str();
  try {
    bool v=m_partition.isObjectValid<ipc::IPCFEI4BAdapter>(name);
    if(!v) {
      std::cout<<"Not valid"<<std::endl;
      assert(0);
    }
    modhandle = m_partition.lookup<ipc::IPCFEI4BAdapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    modhandle -> IPCdownloadConfig( config );
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
}

void IPCController::downloadModuleConfig(int rce, int id, HitbusModuleConfig& config){
  ipc::IPCHitbusAdapter_var modhandle;
  std::string names=boost::lexical_cast<std::string>(id)+"_RCE"+boost::lexical_cast<std::string>(rce);
  const char* name=names.c_str();
  try {
    bool v=m_partition.isObjectValid<ipc::IPCHitbusAdapter>(name);
    if(!v) {
      std::cout<<"Not valid"<<std::endl;
      assert(0);
    }
    modhandle = m_partition.lookup<ipc::IPCHitbusAdapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    modhandle -> IPCdownloadConfig( config );
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
}
void IPCController::downloadModuleConfig(int rce, int id, AFPHPTDCModuleConfig& config){
  ipc::IPCAFPHPTDCAdapter_var modhandle;
  std::string names=boost::lexical_cast<std::string>(id)+"_RCE"+boost::lexical_cast<std::string>(rce);
  const char* name=names.c_str();
  try {
    bool v=m_partition.isObjectValid<ipc::IPCAFPHPTDCAdapter>(name);
    if(!v) {
      std::cout<<"Not valid"<<std::endl;
      assert(0);
    }
    modhandle = m_partition.lookup<ipc::IPCAFPHPTDCAdapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    modhandle -> IPCdownloadConfig( config );
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
}

void IPCController::addModule(const char* name, const char* type, int id, int inLink, int outLink, int rce, const char* formatter){
  ipc::IPCConfigIFAdapter_var handle;
  char cfa[128];
  sprintf(cfa, "configIF_RCE%d",rce);
  try {
    handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    ipc::ModSetup s;
    s.id=id;
    s.inLink=inLink;
    s.outLink=outLink;
    std::string typestring;
    if(std::string(type)=="FEI3")typestring="IPC_FEI3_multiThread";
    else if(std::string(type)=="FEI4A")typestring="IPC_FEI4A_multiThread";
    else if(std::string(type)=="FEI4B")typestring="IPC_FEI4B_multiThread";
    else if(std::string(type)=="Hitbus")typestring="IPC_Hitbus_multiThread";
    else if(std::string(type)=="HPTDC")typestring="IPC_AFPHPTDC_multiThread";
    else(assert(0));
    handle -> IPCsetupModule( name,typestring.c_str(),s , formatter);
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
}

int IPCController::setupTrigger(const char* type){
  ipc::IPCConfigIFAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "configIF_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      return m_rces[i]+1000;
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle->IPCsetupTriggerIF(type);
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
  return 0;
}

void IPCController::removeAllModules(){
  ipc::IPCConfigIFAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "configIF_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle->IPCdeleteModules();
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
}
void IPCController::startScan(){
  ipc::IPCScanAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "scanCtrl_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCScanAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle -> IPCstartScan( );
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
}

void IPCController::abortScan(){
  ipc::IPCScanAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "scanCtrl_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCScanAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle -> IPCabort( );
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
}

void IPCController::stopWaitingForData(){
  ipc::IPCScanAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "scanCtrl_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCScanAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
    handle -> IPCstopWaiting( );
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  } 
}

void IPCController::getEventInfo(unsigned* nevent) {
  ipc::IPCConfigIFAdapter_var handle;
  char cfa[128];
  assert(m_rces.size()>0);
  sprintf(cfa, "configIF_RCE%d", m_rces[0]);
  try {
    handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    *nevent= handle -> IPCnTrigger();
      
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
}
int IPCController::verifyModuleConfigHW(int rce, int id) {
  ipc::IPCConfigIFAdapter_var handle;
  char cfa[128];
  int retval=0;
  sprintf(cfa, "configIF_RCE%d",rce);
  try {
    handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    retval|=handle -> IPCverifyModuleConfigHW(id);
    
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return retval;
}

void IPCController::setupParameter(const char* par, int val) {
  ipc::IPCConfigIFAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "configIF_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle -> IPCsetupParameter(par, val);
      
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
}

void IPCController::setupMaskStage(int stage) {
  ipc::IPCConfigIFAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "configIF_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle -> IPCsetupMaskStage(stage);
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
}

  
void IPCController::downloadScanConfig(ipc::ScanOptions& options){
  ipc::IPCScanRootAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "scanRoot_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCScanRootAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle -> IPCconfigureScan(options);
      
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }  
}

void IPCController::waitForScanCompletion(ipc::Priority pr, IPCCallback* callb){
  IPCCallback *cb;
  if(callb==0) cb=new IPCRegularCallback;
  else cb=callb;
  ipc::IPCScanRootAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "scanRoot_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCScanRootAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle -> IPCconfigureCallback(cb->_this(), pr );
      cb->addRce();
      
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
  cb->run();
  cb->_destroy();
}
void IPCController::runScan(ipc::Priority pr, IPCCallback* callb){
  IPCCallback *cb;
  if(callb==0) cb=new IPCRegularCallback;
  else cb=callb;
  ipc::IPCScanRootAdapter_var rhandle;
  ipc::IPCScanAdapter_var shandle;
  char cfa[128];
  char cfb[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "scanRoot_RCE%d", m_rces[i]);
    sprintf(cfb, "scanCtrl_RCE%d", m_rces[i]);
    try {
      shandle = m_partition.lookup<ipc::IPCScanAdapter>( cfb );
      rhandle = m_partition.lookup<ipc::IPCScanRootAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      rhandle -> IPCconfigureCallback(cb->_this(), pr );
      cb->addRce();
      shandle->IPCstartScan();
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
  cb->run();
  cb->_destroy();
}  

unsigned IPCController::getNEventsProcessed(){
  ipc::IPCScanRootAdapter_var handle;
  char cfa[128];
  assert(m_rces.size()>0);
  //maybe have to change if have multiple RCE's?
  sprintf(cfa, "scanRoot_RCE%d", m_rces[0]);
  try {
    handle = m_partition.lookup<ipc::IPCScanRootAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    return handle -> IPCnEvents( );
      
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 0;
}

void IPCController::resynch(){
  ipc::IPCScanRootAdapter_var handle;
  char cfa[128];
  assert(m_rces.size()>0);

  for(size_t i=0;i<m_rces.size();i++){
    std::cout<<"IPCController sending resynch command to RCE "<<m_rces[i]<<std::endl;

    sprintf(cfa, "scanRoot_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCScanRootAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle -> IPCresynch( );
      
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }

  }

}

void IPCController::resetFE(){
  ipc::IPCConfigIFAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "configIF_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle->IPCresetFE();
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
}

void IPCController::configureModulesHW(){
  ipc::IPCConfigIFAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "configIF_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      handle->IPCconfigureModulesHW();
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
}

unsigned IPCController::writeHWglobalRegister(const char* name, int reg, unsigned short val){
  ipc::IPCFEI4AAdapter_var modhandle;
  try {
    bool v=m_partition.isObjectValid<ipc::IPCFEI4AAdapter>(name);
    if(!v) {
      std::cout<<"Not valid"<<std::endl;
      assert(0);
    }
    modhandle = m_partition.lookup<ipc::IPCFEI4AAdapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    return modhandle->IPCwriteHWglobalRegister(reg,val);
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 1000;
}

unsigned IPCController::readHWglobalRegister(const char* name, int reg, unsigned short &val){
  ipc::IPCFEI4AAdapter_var modhandle;
  try {
    bool v=m_partition.isObjectValid<ipc::IPCFEI4AAdapter>(name);
    if(!v) {
      std::cout<<"Not valid"<<std::endl;
      assert(0);
    }
    modhandle = m_partition.lookup<ipc::IPCFEI4AAdapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    return modhandle->IPCreadHWglobalRegister(reg,val);
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 1000;
}
unsigned IPCController::writeHWdoubleColumn(const char* name, unsigned bit, unsigned dcol, std::vector<unsigned> data, std::vector<unsigned> &retvec){
  ipc::IPCFEI4AAdapter_var modhandle;
  try {
    bool v=m_partition.isObjectValid<ipc::IPCFEI4AAdapter>(name);
    if(!v) {
      std::cout<<"Not valid"<<std::endl;
      assert(0);
    }
    modhandle = m_partition.lookup<ipc::IPCFEI4AAdapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    ipc::uvec ipcdata;
    ipc::uvec_var retv;
    ipcdata.length(data.size());
    for(size_t i=0;i<data.size();i++)ipcdata[i]=data[i];
    unsigned stat= modhandle->IPCwriteDoubleColumnHW(bit, dcol, ipcdata, retv);
    for(unsigned i=0;i<retv->length();i++)retvec.push_back(retv[i]);
    return  stat;
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 1000;
}
unsigned IPCController::readHWdoubleColumn(const char* name, unsigned bit, unsigned dcol, std::vector<unsigned> &retvec){
  ipc::IPCFEI4AAdapter_var modhandle;
  try {
    bool v=m_partition.isObjectValid<ipc::IPCFEI4AAdapter>(name);
    if(!v) {
      std::cout<<"Not valid"<<std::endl;
      assert(0);
    }
    modhandle = m_partition.lookup<ipc::IPCFEI4AAdapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    ipc::uvec_var retv;
    unsigned stat= modhandle->IPCreadDoubleColumnHW(bit, dcol, retv);
    for(unsigned i=0;i<retv->length();i++)retvec.push_back(retv[i]);
    return  stat;
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 1000;
}

unsigned IPCController::sendHWcommand(int rce, unsigned char opcode){
  ipc::IPCConfigIFAdapter_var handle;
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char cfa[128];
  sprintf(cfa, "configIF_RCE%d", rce);
  try {
    handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    return handle->IPCsendHWcommand(opcode);
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 1000;
}

unsigned IPCController::writeHWregister(int rce, unsigned addr, unsigned val){
  ipc::IPCConfigIFAdapter_var handle;
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char cfa[128];
  sprintf(cfa, "configIF_RCE%d", rce);
  try {
    handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    return handle->IPCwriteHWregister(addr,val);
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 1000;
}

unsigned IPCController::readHWregister(int rce, unsigned addr, unsigned &val){
  ipc::IPCConfigIFAdapter_var handle;
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char cfa[128];
  sprintf(cfa, "configIF_RCE%d", rce);
  try {
    handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    return handle->IPCreadHWregister((CORBA::ULong)addr,(CORBA::ULong&) val);
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 1000;
}
unsigned IPCController::writeHWblockData(int rce, std::vector<unsigned>& data){
  ipc::IPCConfigIFAdapter_var handle;
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char cfa[128];
  sprintf(cfa, "configIF_RCE%d", rce);
  try {
    handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    ipc::blockdata ipcdata;
    ipcdata.length(data.size());
    for(size_t i=0;i<data.size();i++)ipcdata[i]=data[i];
    return handle->IPCwriteHWblockData(ipcdata);
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 1000;
}
unsigned IPCController::readHWblockData(int rce, std::vector<unsigned>& data,std::vector<unsigned>& retvec ){
  ipc::IPCConfigIFAdapter_var handle;
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char cfa[128];
  sprintf(cfa, "configIF_RCE%d", rce);
  try {
    handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    ipc::blockdata ipcdata;
    ipc::blockdata_var retv;
    ipcdata.length(data.size());
    for(size_t i=0;i<data.size();i++)ipcdata[i]=data[i];
    unsigned stat= handle->IPCreadHWblockData(ipcdata, retv);
    for(unsigned i=0;i<retv->length();i++)retvec.push_back(retv[i]);
    return stat;
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return 1000;
}

unsigned IPCController::readHWbuffers(int rce, std::vector<unsigned char>& retvec ){
  ipc::IPCConfigIFAdapter_var handle;
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char cfa[128];
  sprintf(cfa, "configIF_RCE%d", rce);
  try {
    handle = m_partition.lookup<ipc::IPCConfigIFAdapter>( cfa );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    ipc::chardata_var retv;
    unsigned nbuf= handle->IPCreadHWbuffers(retv);
    for(unsigned i=0;i<retv->length();i++)retvec.push_back(retv[i]);
    return nbuf;
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
  return (unsigned)-1;
}

int IPCController::getScanStatus(){
  ipc::IPCScanAdapter_var handle;
  int status=0;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "scanCtrl_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCScanAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      status|= handle->IPCgetStatus();
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
  }
  return status;
}
