
#include <boost/regex.hpp>
#include "server/RCFController.hh"
#include "RCFConfigIFAdapter.hh"

#include <boost/program_options.hpp>
#include <iostream>
#include <stdlib.h>

int main( int argc, char ** argv ){
  int rce;
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("rce,r", boost::program_options::value<int>(&rce)->required(), "RCE to connect to");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);
  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }

  RCF::RcfInitDeinit rcfInit;
  RCFController controller;
  // How many RCEs are in the partition?
  std::cout<<"Rebooting HSIO on RCE "<<rce<<std::endl;
  controller.addRce(rce);
  controller.sendHWcommand(rce,8); //reboot
}
