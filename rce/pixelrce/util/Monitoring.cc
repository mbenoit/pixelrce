#include "util/Monitoring.hh"
#include <assert.h>

Monitoring* Monitoring::m_instance=0;

Monitoring::Monitoring(){
  assert(m_instance==0);
  m_instance=this;
}
void Monitoring::destroy(){
  assert(m_instance!=0);
  delete m_instance;
  m_instance=0;
}
void Monitoring::publish(const char* name, float val){
  assert(m_instance!=0);
  m_instance->Publish(name, val);
}
void Monitoring::publish(const char* name, unsigned val){
  assert(m_instance!=0);
  m_instance->Publish(name, val);
}
