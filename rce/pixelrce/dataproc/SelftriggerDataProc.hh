#ifndef SELFTRIGGERDATAPROC_HH
#define SELFTRIGGERDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include "dataproc/fit/AbsFit.hh"
#include "dataproc/ClusterProc.hh"
#include <vector>
#include "util/RceHisto2d.cc"
#include "util/RceHisto1d.cc"
#include <fstream>

class SelftriggerDataProc: public AbsDataProc{
public:
  SelftriggerDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~SelftriggerDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);

protected:

  std::vector<RceHisto2d<int, int>*> m_histo_occ;
  std::vector<RceHisto2d<int, int>*> m_histo_tot2d;
  std::vector<RceHisto1d<int, int>*> m_histo_tot;
  std::vector<RceHisto1d<int, int>*> m_histo_nhit;
  std::vector<RceHisto1d<int, int>*> m_histo_totev;
  std::vector<RceHisto1d<int, int>*> m_histo_timing;
  std::vector<RceHisto1d<int, int>*> m_histo_nhits_cluster;
  std::vector<RceHisto1d<int, int>*> m_histo_tot_cluster;
  std::vector<RceHisto2d<int, int>*> m_histo_tot_size;
  std::vector<RceHisto1d<int, int>*> m_histo_bcidDiff;
  std::vector<RceHisto1d<int, int>*> m_histo_l1idDiff;


  //unsigned int m_bcid_ref;
  //unsigned int m_bcid;
  unsigned int* m_bcid_ref;
  unsigned int* m_bcid;
  unsigned int* m_l1id;
  unsigned int* m_l1id_last;
  int *m_totev, *m_nhit;
  std::ofstream* m_file;
  //bool m_occ, m_tot, m_bcid;
  unsigned short m_counter;

  //BCID skipping stuff
  bool* m_bcid_initial_set;
  unsigned int* m_bcid_initial;
  unsigned int* m_hiteventid;
  unsigned int* m_l1idchanged;

  unsigned long long m_trgtime; //BCID coming from link-10 (tig info link)
  bool m_trgtime_initial_set;
  unsigned long long m_trgtime_initial; 

  unsigned int **  m_bcidDiff_index_l1id_vs_module;
  bool ** m_bcidDiff_index_l1id_vs_module_isSet;

  unsigned long long * m_trigtimeDiff_index_l1id;
  unsigned long long * m_trigtime_index_l1id;
  
  unsigned int nmod;

  bool m_link10DataOn;

  // Clustering
  ClusterProc *m_cluster_proc;
};

#endif
