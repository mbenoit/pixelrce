#include <stdio.h>
#include <unistd.h>

#include <ers/ers.h>

#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <ipc/server.h>

#include <owl/timer.h>
#include <owl/semaphore.h>

#include <boost/regex.hpp>

#include "rcecalib/scanctrl/IPCScan.cc"
#include "rcecalib/scanctrl/IPCScanRoot.cc"
#include "rcecalib/config/IPCConfigIF.cc"
#include "rcecalib/HW/SerialHexdump.hh"
#include "rcecalib/HW/SerialPgp.hh"
#include "rcecalib/HW/SerialPgpFei4.hh"
#include "rcecalib/config/IPCModuleFactory.hh"
#include "rcecalib/util/RceName.hh"

#include "rcecalib/server/eudaqserver.hh"
#include "rcecalib/server/RceProducer.hh"

#include <is/infoT.h>
#include <is/infodictionary.h>
#include <ipc/core.h>

static IPCServer ipcServer;

//////////////////////////////////////////
//
// Main function
//
//////////////////////////////////////////


void eudaqserver::run(bool ismodule, char* partition){

const char *cc_argv[] = 
{
	"eudaqserver",              /* always the name of the program */
	"-p",
	"rcetest",
	//       "-ORBtraceLevel",       /* trace level */
		//"1",
	"-ORBgiopMaxMsgSize", 
	"33554422",       /* max message size 32 MB */
        "-ORBmaxServerThreadPerConnection", "1"
//   "-ORBendPoint",
// " giop:tcp:192.168.1.35:"
 //"-ORBtraceInvocations",
 //"1"
};
 if(partition)cc_argv[2]=partition;
int cc_argc = sizeof( cc_argv ) / sizeof( cc_argv[ 0 ]  ); 
    try {
      IPCCore::init( cc_argc, (char**)cc_argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return;
    }
   
       // Declare command object and its argument-iterator
   
   IPCPartition p((const char*)cc_argv[2]);
   printf("Partition is %s\n",cc_argv[2]);
   char name[128];
   sprintf(name, "configIF_RCE%d", RceName::getRceNumber());
   p.isObjectValid<ipc::IPCConfigIFAdapter>(name);

   //Serial IF
   new SerialPgpFei4;

   //Module Factory

   ModuleFactory *moduleFactory=new IPCModuleFactory(p);
   
   // Config IF
   
   IPCConfigIF<ipc::single_thread> *cif;
   IPCScan<ipc::multi_thread> *scan;   
   IPCScanRoot<ipc::single_thread> * scanroot;   

   try{
     cif=new IPCConfigIF<ipc::single_thread>(p, name, moduleFactory);
     sprintf(name, "scanCtrl_RCE%d", RceName::getRceNumber());
     scan = new IPCScan<ipc::multi_thread>( p, name);   
     sprintf(name, "scanRoot_RCE%d", RceName::getRceNumber());
     scanroot = new IPCScanRoot<ipc::single_thread>( p, name, (ConfigIF*)cif, (Scan*)scan);   
   }catch(...){
     std::cout<<"Could not add IPC objects (ipc_server not running? Several servers in the same partition?)."<<std::endl;
     assert(0);
   }

   IPCHistoManager *hm;
   try{
     sprintf(name, "RCE%d",RceName::getRceNumber());
     hm=new IPCHistoManager(p,"RceIsServer", name);
   }catch(...){
     std::cout<<"Could not start histogram manager (is_server not running? Another server running on the same partition?)."<<std::endl;
     assert(0);
   }
   printf("ipc_test_server has been started.\n");
   
   
   RceProducer producer("RceProducer", "tcp://172.21.7.146:44000", p);
   //RceProducer producer("RceProducer", "tcp://rdcds105:44000", p);
   if(ismodule==true){
     kill(getpid(),SIGUSR1);
     pause();
   }else{
     ipcServer.run();
   }

   scan->_destroy();
   scanroot->_destroy();
   
   std::cout << "Test successfully completed." << std::endl;
}
