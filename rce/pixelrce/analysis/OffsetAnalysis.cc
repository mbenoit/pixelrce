#include "analysis/OffsetAnalysis.hh"
#include "server/PixScan.hh"

#include <TFile.h>
#include <TStyle.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TF1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>

namespace{
  const double threshold=0;
}
using namespace RCE;

void OffsetAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  unsigned int numval=scan->getLoopVarValues(0).size();
  gStyle->SetOptFit(10);
  gStyle->SetOptStat(0);
  std::cout<<"Offset analysis"<<std::endl;
  file->cd("loop1_0");
  TIter nextkey(gDirectory->GetListOfKeys());
  TKey *key;
  boost::regex re("_(\\d+)_Mean");
  boost::regex re2("Mean");
  while ((key=(TKey*)nextkey())) {
    file->cd("loop1_0");
    std::string name(key->GetName());
    boost::cmatch matches;
    if(boost::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      std::string chi2HistoName = boost::regex_replace (name, re2, "ChiSquare");
      TH2* histo = (TH2*)key->ReadObj();
      TH2* chi2Histo=(TH2*)gDirectory->Get(chi2HistoName.c_str());
      assert(chi2Histo);
      TH1D* hits1d[2];
      for (int i=0;i<2;i++){
	hits1d[i]=new TH1D(Form("HitsPerBin_%d__Mod_%d", i, id), 
			   Form("Hits per bin %d Mod %d at %s", i, id, findFieldName(cfg, id)), 
			   numval, -.5, (float)numval-.5) ;
	hits1d[i]->GetXaxis()->SetTitle("Scan Point");
      }
      fillHit1d(name, hits1d[0], numval);
      file->cd("loop1_1");
      fillHit1d(name, hits1d[1], numval);
      TH2* histo2 = (TH2*)gDirectory->Get(name.c_str());
      TH2* chi2Histo2 = (TH2*)gDirectory->Get(chi2HistoName.c_str());
      TH1D* offset=new TH1D(Form("Offset_Mod_%d",id), 
			    Form("Offset Mod %d at %s", id, findFieldName(cfg, id)), 50, 0, 0.05);
      offset->GetXaxis()->SetTitle("Offset (mV)");
      float clo=1.9;
      float chi=3.9;
      float m=1.4e-3;
      PixelConfig* confb=findConfig(cfg, id);
      if(confb){
	FECalib fecalib=confb->getFECalib(0);
	clo=fecalib.cinjLo;
	chi=fecalib.cinjHi;
	m=fecalib.vcalCoeff[1];
	if(clo==0){
	  std::cout<<"Cannot determine alpha because clo is 0 for module "<<id<<std::endl;
	  continue;
	}
      }else{
	std::cout<<"No configuration available. Using default values."<<std::endl;
      }
      float alpha=(clo+chi)/clo;
      if(alpha==1){
	std::cout<<"Cannot determine offset because alpha is 1 for module "<<id<<std::endl;
	continue;
      }
      for (int i=1;i<=histo->GetNbinsX();i++){
	for(int j=1;j<=histo->GetNbinsY();j++){
	  if(chi2Histo->GetBinContent(i, j)!=0 && chi2Histo2->GetBinContent(i, j)!=0) {
	    float v1=histo->GetBinContent(i, j);
	    float v2=histo2->GetBinContent(i, j);
	    float n=m*(v1-alpha*v2)/(alpha-1);
	    offset->Fill(n);
	  }
	}
      }
      offset->Fit("gaus");
      TF1* gaus=offset->GetFunction("gaus");
      double off=0;
      if(gaus)off=gaus->GetParameter(1);
      else std::cout<<"Warning: Fit for offset failed. Using default value of 0"<<std::endl;
      std::cout<<"Offset for module "<<id<<" is "<<off<<std::endl;
      if(off<0 || off>0.06){
	std::cout<<"Offset out of range (0..50). Setting to 0"<<std::endl;
	off=0;
      }
      if(confb){
	FECalib fecalib=confb->getFECalib(0);
	fecalib.vcalCoeff[0]=off;
	confb->setFECalib(0, fecalib);
	writeConfig(anfile, runno);
      }

      delete histo;
      anfile->cd();
      offset->Write();
      offset->SetDirectory(gDirectory);
      hits1d[0]->Write();
      hits1d[0]->SetDirectory(gDirectory);
      hits1d[1]->Write();
      hits1d[1]->SetDirectory(gDirectory);
    }
  }  
  if(configUpdate())writeTopFile(cfg, anfile, runno);
}

void OffsetAnalysis::fillHit1d(std::string &name, TH1* hits1d, unsigned numval){
for(unsigned int k=0;k<numval;k++){
  boost::regex re2("Mean");
  std::string histoName = boost::regex_replace (name, re2, Form("Occupancy_Point_%03d", k));
  TH2* occHisto=(TH2*)gDirectory->Get(histoName.c_str());
  if(occHisto==0){
    std::cout<<"No Occupancy histograms found. Won't fill 1-d hit histo."<<std::endl;
    break;
  }
  hits1d->SetBinContent(k+1, occHisto->GetSumOfWeights());
 }
}
