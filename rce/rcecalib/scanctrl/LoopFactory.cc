#include "rcecalib/util/exceptions.hh"
#include "rcecalib/scanctrl/LoopFactory.hh"
#include "rcecalib/scanctrl/RegularScanSetup.hh"
#include "rcecalib/scanctrl/RegularCfgScanSetup.hh"
#include "rcecalib/scanctrl/IfScanSetup.hh"
#include "rcecalib/scanctrl/SelftriggerSetup.hh"
#include "rcecalib/scanctrl/DelayScanSetup.hh"
#include "rcecalib/scanctrl/CosmicDataSetup.hh"
#include "rcecalib/scanctrl/NoiseScanSetup.hh"
#include <stdio.h> /* for sprintf */

LoopFactory::LoopFactory(){}


LoopSetup* LoopFactory::createLoopSetup(const char* type){
  
  if(std::string(type)=="Regular")
    return new RegularScanSetup;
  else if(std::string(type)=="RegularCfg")
    return new RegularCfgScanSetup;
  else if(std::string(type)=="IfScan")
    return new IfScanSetup;
  else if(std::string(type)=="Delay")
    return new DelayScanSetup;
  else if(std::string(type)=="CosmicData")
    return new CosmicDataSetup;
  else if(std::string(type)=="Selftrigger")
    return new SelftriggerSetup;
  else if(std::string(type)=="Noisescan")
    return new NoiseScanSetup;
  else {
    char message[128];
    sprintf(message, "Scan loop setup for %s is not defined.", type);
    ERS_ASSERT_MSG(0, message);
  }
  return 0;
}

