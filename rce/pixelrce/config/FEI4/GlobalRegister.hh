#ifndef GLOBALREGISTER_FEI4_HH
#define GLOBALREGISTER_FEI4_HH

#include <map>
#include <string>
#include <vector>
#include "HW/SerialIF.hh"
#include "config/FEI4/RegisterDef.hh"

namespace FEI4{

class FECommands;
  
  class GlobalRegister{
  public:
    enum PulseReg{Efuse_sense=0x1, Stop_Clk=0x2, ReadErrorReq=0x4, ReadSkipped=0x8,
		  CalEn=0x20, SR_clr=0x40, Latch_en=0x80, SR_Clock=0x100, GADC_Start=0x200, SR_Read=0x400};
    enum mode{SW, HW};
    GlobalRegister(FECommands* commands);
    virtual ~GlobalRegister();
    void setField(const char* name, unsigned val, mode t);
    void setFieldFun(unsigned short* reg, int bitpos, int width, unsigned val);
    unsigned getField(const char* name);
    int getRegisterNumber(const char* name){
      if(m_rd->m_fields.find("name")==m_rd->m_fields.end())return -1;
      return m_rd->m_fields[name].reg;
    }
    void writeHW();
    void writeRegisterHW(int i);
    unsigned writeRegisterHW(int i, unsigned short val);
    void disableThresholdHW(bool dis);
    unsigned readRegisterHW(int i, unsigned short &val);
    void readRegisterHW(int i);
    int verifyConfigHW(std::vector<unsigned> &inpvec);
    unsigned decodeGlobalRecord(std::vector<unsigned> inpvec, int i, unsigned short& val);
    virtual void printFields(std::ostream &os)=0;
    void printRegisters(std::ostream &os);
    void printShadowRegisterAB(std::ostream &os);
    void printShadowRegisterC(std::ostream &os);
    void addField(const char* name, int reg, int bitpos, int srab, int src, int width, bool reverse);
    void addParameter(const char* name, const char* field);
    void enableSrHW(bool on);
    std::string lookupParameter(const char* name);
    // accessor function for daughter class's static members
    virtual RegisterDef* getDef()=0;

  protected:
    unsigned m_chipId;
    bool m_clkOutMode;
    FECommands* m_commands;
    RegisterDef *m_rd;
    unsigned short *m_regw;// write registers
    unsigned short *m_regr;// readback
    unsigned short *m_srab;//shadow register AB
    unsigned short *m_src;// shadow register C
		  
  };

};

#endif
