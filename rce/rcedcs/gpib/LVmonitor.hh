#ifndef COSMICGUI_HH
#define COSMICGUI_HH

#include "TGMdiMainFrame.h"
#include <TTimer.h>
#include <TTimeStamp.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <string>

class LVchan;
class GpibController;

class LVmonitor: public TGMainFrame {
public:
  LVmonitor(GpibController &c, const TGWindow *p,UInt_t w,UInt_t h);
  virtual ~LVmonitor();
  void toggle();
  void timeouts();
  void quit();
private:

  GpibController& m_controller;
  int m_noconn;
  LVchan* m_lvchan[2];
  TGTextButton* m_start;
  TTimer *m_timers;
  TTimeStamp m_starttime;
  bool m_ison;

ClassDef (LVmonitor,0)
};
#endif
