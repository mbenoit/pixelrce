
ifdef SWAP_DATA
CPPFLAGS += '-DSWAP_DATA'
endif
ifeq ($(profiler),y)
CPPFLAGS+='-D__PROFILER_ENABLED__'
endif

ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)
modlibnames := dataproc 
libsrcs_dataproc := AbsDataProc.cc \
                    RegularScanDataHandler.cc \
                    RegularScanRawDataHandler.cc \
                    ModuleCrosstalkRawDataHandler.cc \
                    DelayScanDataHandler.cc \
                    CosmicDataHandler.cc \
                    DataProcFactory.cc \
                    OccupancyDataProc.cc \
                    RawFei4OccupancyDataProc.cc \
                    NoiseOccupancyDataProc.cc \
                    TotDataProc.cc \
                    TotCalibDataProc.cc \
                    BcidDataProc.cc \
                    SelftriggerDataProc.cc \
                    HitorDataProc.cc \
                    CosmicDataProc.cc \
                    CosmicEvent.cc \
                    CosmicEventIterator.cc \
                    DelayScanDataProc.cc \
                    SimpleReceiver.cc \
                    MeasurementReceiver.cc \
                    MeasurementDataProc.cc \
	            PgpReceiver.cc \
	            PgpCosmicReceiver.cc \
	            PgpCosmicNwReceiver.cc \
	            fit/FitData.cc \
		    MultiTrigOccupancyDataProc.cc \
                    MultiTrigNoiseDataProc.cc \
                    TemperatureDataProc.cc \
                    MonleakDataProc.cc \
                    SNDataProc.cc\
                    Fei4RegisterTestDataProc.cc\
                    ClusterProc.cc

libincs_dataproc := rcecalib \
                    $(ers_include_path) \
                    $(owl_include_path) \
                    $(ipc_include_path) \
                    $(is_include_path) \
                    $(oh_include_path) \
                    $(boost_include_path) \
                    $(omniorb_include_path)


endif

ifneq ($(findstring linux,$(tgt_os)),)
libnames := dataproc 
libsrcs_dataproc := AbsDataProc.cc \
                    RegularScanDataHandler.cc \
                    RegularScanRawDataHandler.cc \
                    ModuleCrosstalkRawDataHandler.cc \
                    DelayScanDataHandler.cc \
                    CosmicDataHandler.cc \
                    DataProcFactory.cc \
                    NoiseOccupancyDataProc.cc \
                    OccupancyDataProc.cc \
                    RawFei4OccupancyDataProc.cc \
                    TotDataProc.cc \
                    TotCalibDataProc.cc \
                    BcidDataProc.cc \
                    SelftriggerDataProc.cc \
                    HitorDataProc.cc \
                    CosmicDataProc.cc \
                    CosmicEvent.cc \
                    CosmicEventIterator.cc \
                    DelayScanDataProc.cc \
                    SimpleReceiver.cc \
                    MeasurementDataProc.cc \
                    fit/FitData.cc \
		    MultiTrigOccupancyDataProc.cc \
                    MultiTrigNoiseDataProc.cc \
	            TemperatureDataProc.cc \
	            MonleakDataProc.cc \
                    SNDataProc.cc\
                    Fei4RegisterTestDataProc.cc\
		    ClusterProc.cc
 
libincs_dataproc := rcecalib \
                    $(ers_include_path) \
                    $(owl_include_path) \
                    $(ipc_include_path) \
                    $(is_include_path) \
                    $(oh_include_path) \
                    $(boost_include_path) \
                    $(omniorb_include_path)


endif
