#ifndef RCENAME_HH
#define RCENAME_HH

#include <string>

class RceName{
public:
  static int getRceNumber();
private:
  static int m_number;
  static bool m_initialized;
};

#endif
