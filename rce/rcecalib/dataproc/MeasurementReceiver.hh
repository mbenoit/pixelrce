#ifndef MEASUREMENTRECEIVER_HH
#define MEASUREMENTRECEIVER_HH

#include "rcecalib/dataproc/PgpReceiver.hh"
#include "rcecalib/config/TriggerReceiverIF.hh"
#ifdef RCE_V2
#include "datCode.hh"
#include DAT_PUBLIC( oldPpi, net,       IpAddress.hh)
#else
#include "rce/net/IpAddress.hh"
#endif
#include <boost/property_tree/ptree_fwd.hpp>
#include "namespace_aliases.hh"
class MeasurementReceiver: public PgpReceiver, public TriggerReceiverIF{
public:
  MeasurementReceiver(AbsDataHandler* handler, boost::property_tree::ptree* scanOptions);
  void Receive(unsigned *data, int size);
  void receive(PgpTrans::PgpData* pgpdata){}; //pgp dummy
  const char* sendCommand(std::string inpline, RceNet::IpAddress address, const char* rce);
private:
  RceNet::IpAddress m_addr;
  std::string m_ipname;
};
#endif
