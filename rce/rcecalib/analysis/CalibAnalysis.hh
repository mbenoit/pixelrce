#ifndef CALIBANALYSIS_HH
#define CALIBANALYSIS_HH

#include <string>
#include "PixelFEI4AConfig.hh"
#include "PixelFEI4BConfig.hh"
#include "PixelModuleConfig.hh"

class TFile;
class ConfigGui;
namespace RCE{
  class PixScan;
}

class CalibAnalysis{
public:
  CalibAnalysis(): m_cfg(0), m_update(false){}
  virtual ~CalibAnalysis(){}
  virtual void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[])=0;
  std::string getPath(TFile* file);
  ipc::PixelFEI4AConfig* findFEI4AConfig(ConfigGui* cfg[], int id);
  ipc::PixelFEI4BConfig* findFEI4BConfig(ConfigGui* cfg[], int id);
  ipc::PixelModuleConfig* findFEI3Config(ConfigGui* cfg[], int id);
  void writeFEI4Config(TFile *anfile, int runno);
  void writeFEI3Config(TFile *anfile, int runno);
  bool configUpdate(){return m_update;}
  void writeTopFile(ConfigGui* cfg[], TFile* anfile, int runno);
  const char* findFieldName(ConfigGui* cfg[], int id);
  const std::string findFEType(ConfigGui* cfg[], int id);
  void clearFEI4Masks(unsigned char (*masks)[ipc::IPC_N_I4_PIXEL_ROWS]);
  static std::string addPosition(const char* hname, ConfigGui* cfg[]);
private:
  ConfigGui* m_cfg;
  int m_channel;
  bool m_update;
};

#endif
