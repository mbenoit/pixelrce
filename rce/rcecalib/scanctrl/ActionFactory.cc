
#include "rcecalib/scanctrl/ActionFactory.hh"

#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/dataproc/AbsDataProc.hh"
#include "rcecalib/scanctrl/Scan.hh"


// Loop actions
#include "rcecalib/scanctrl/SetupMaskStageAction.hh"
#include "rcecalib/scanctrl/SetupParamAction.hh"
#include "rcecalib/scanctrl/ChangeBinAction.hh"
#include "rcecalib/scanctrl/SendTriggerAction.hh"
#include "rcecalib/scanctrl/EnableTriggerAction.hh"
#include "rcecalib/scanctrl/DisableTriggerAction.hh"
#include "rcecalib/scanctrl/DisableAllChannelsAction.hh"
#include "rcecalib/scanctrl/ConfigureModulesAction.hh"
#include "rcecalib/scanctrl/ConfigureModulesNEAction.hh"
#include "rcecalib/scanctrl/CosmicSetInputDelayAction.hh"
#include "rcecalib/scanctrl/CosmicEnableTDCAction.hh"
#include "rcecalib/scanctrl/SetupTriggerAction.hh"
#include "rcecalib/scanctrl/CosmicEnableScintillatorTriggerAction.hh"
#include "rcecalib/scanctrl/PauseAction.hh"
#include "rcecalib/scanctrl/IfWaitAction.hh"
#include "rcecalib/scanctrl/SetChannelMaskAction.hh"

// End of loop actions 
#include "rcecalib/scanctrl/FitEoLAction.hh"
#include "rcecalib/scanctrl/CloseFileEoLAction.hh"
#include "rcecalib/scanctrl/DisableTriggerEoLAction.hh"
#include "rcecalib/scanctrl/DisableAllChannelsEoLAction.hh"
#include "rcecalib/scanctrl/ResetFEEoLAction.hh"

LoopAction* ActionFactory::createLoopAction(std::string action, boost::property_tree::ptree* pt){
  // create various loop actions based on action string
  if(action=="SETUP_MASK")
    return new SetupMaskStageAction(action,m_configIF,m_dataProc, pt);
  else if (action=="SETUP_PARAM")
    return new SetupParamAction(action,m_configIF, pt);
  else if (action=="CHANGE_BIN")
    return new ChangeBinAction(action,m_dataProc);
  else if (action == "SEND_TRIGGER")
    return new SendTriggerAction(action,m_configIF);
  else if (action == "SETUP_CHANNELMASK")
    return new SetChannelMaskAction(action,m_configIF);
  else if (action == "DISABLE_ALL_CHANNELS")
    return new DisableAllChannelsAction(action,m_configIF);
  else if (action == "CONFIGURE_MODULES")
    return new ConfigureModulesAction(action,m_configIF);
  else if (action == "CONFIGURE_MODULES_NO_ENABLE")
    return new ConfigureModulesNEAction(action,m_configIF);
  else if (action == "ENABLE_TRIGGER")
    return new EnableTriggerAction(action,m_configIF);
  else if (action == "DISABLE_TRIGGER")
    return new DisableTriggerAction(action,m_configIF);
  else if (action == "PAUSE")
    return new PauseAction(action,m_scan);
  else if (action == "COSMIC_SET_INPUT_DELAY")
    return new CosmicSetInputDelayAction(action,m_configIF,pt);
  else if (action == "COSMIC_ENABLE_TDC")
    return new CosmicEnableTDCAction(action,m_configIF);
  else if (action == "COSMIC_ENABLE_SCINTILLATOR_TRIGGER")
    return new CosmicEnableScintillatorTriggerAction(action,m_configIF);
  else if (action == "SETUP_TRIGGER")
    return new SetupTriggerAction(action,m_configIF, pt);
  else if (action == "IF_WAIT")
    return new IfWaitAction(action);
  else{
    std::cerr<<"Unknown Action"<<std::endl;
    return 0;
  }
}

EndOfLoopAction* ActionFactory::createEndOfLoopAction(std::string action, std::string fitfunc){
  if(action=="FIT")
    return new FitEoLAction(action,m_dataProc,fitfunc);
  if(action=="CALCULATE_MEAN_SIGMA")
    return new FitEoLAction(action,m_dataProc,fitfunc);
  if(action=="CLOSE_FILE")
    return new CloseFileEoLAction(action,m_dataProc);
  if(action=="DISABLE_TRIGGER")
    return new DisableTriggerEoLAction(action,m_configIF);
  if(action=="DISABLE_ALL_CHANNELS")
    return new DisableAllChannelsEoLAction(action,m_configIF);
  if(action=="RESET_FE")
    return new ResetFEEoLAction(action,m_configIF);
  else if(action=="NO_ACTION")
    return 0;
  else return 0;
}
