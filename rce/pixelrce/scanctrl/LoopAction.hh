#ifndef LOOPACTION_HH
#define LOOPACTION_HH

#include <string>


class LoopAction{
public:
  LoopAction(std::string name):
    m_name(name){}
  std::string name(){return m_name;}
  virtual ~LoopAction(){}
  virtual int execute(int i) = 0;
  
protected:
  std::string m_name;
};

#endif
