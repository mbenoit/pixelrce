#ifndef BCIDDATAPROC_HH
#define BCIDDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include <vector>
#include "util/RceHisto2d.cc"

class BcidDataProc: public AbsDataProc{
public:
  BcidDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~BcidDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);

protected:

  std::vector<std::vector<RceHisto2d<short, short>*> > m_histo_bcid;
  std::vector<std::vector<RceHisto2d<short, short>*> > m_histo_bcid2;
  std::vector<std::vector<RceHisto2d<short, short>*> > m_histo_bcid_sigma;
  std::vector<std::vector<RceHisto2d<short, short>*> > m_histo_bcid_mean;
  std::vector<std::vector<RceHisto2d<char, char>*> > m_histo_occ;
  std::vector<int> m_vcal;
  int m_nTrigger;
  int m_nLoops;
  int m_nPoints;
  unsigned int bcid_ref;
  unsigned int l1id_last;
  unsigned int bcid;
  unsigned int l1id;
};

#endif
