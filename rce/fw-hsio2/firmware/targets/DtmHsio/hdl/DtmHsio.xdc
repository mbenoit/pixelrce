
# IO Types
#set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsP]
#set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsM]
#set_property IOSTANDARD LVCMOS25 [get_ports plSpareP]
#set_property IOSTANDARD LVCMOS25 [get_ports plSpareM]

# PGP Clocks
create_clock -name locRefClk -period 4.0 [get_ports locRefClkP]

create_generated_clock -name pgpClk250 -source [get_ports locRefClkP] \
    -multiply_by 4 -divide_by 6.4 [get_pins U_HsioPgpLane/U_PgpClkGen/CLKOUT0]

set_clock_groups -asynchronous \
      -group [get_clocks -include_generated_clocks fclk0] \
      -group [get_clocks -include_generated_clocks locRefClk]

U_DtmCore/U_HsioEnGen.U_GmiiToRgmii/U_CoreGen[0].GmiiToRgmiiCore_Inst
