#include <boost/property_tree/ptree.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include "dataproc/AtlasDataProc.hh"
#include "dataproc/Channeldefs.hh"
#include "config/ConfigIF.hh"
#include "config/RceFWRegisters.hh"
#include "util/RceName.hh"
#include "util/Monitoring.hh"
#include <iomanip>
#include <iostream>
#include <stdlib.h>

void* AtlasDataProc::monitoring( void *ptr ){
  AtlasDataProc* dataproc=(AtlasDataProc*)ptr;
  dataproc->monitoring();
  return 0;
}
void AtlasDataProc::monitoring(){
  while(1){
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);
    sleep(5);
    pthread_testcancel();
    unsigned nhits=0;
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, 0);
    for(int i=0;i<m_nModules;i++){
      unsigned hitsmod=m_fwregs->getEfbCounter(0,m_outlink[i], FWRegisters::OCCUPANCY); 
      nhits+=hitsmod;
      Monitoring::publish(m_modlabels[i], (float)hitsmod/m_normalization);
      //std::cout<<m_outlink[i]<<": "<<hitsmod<<std::endl;
    }
    Monitoring::publish("Occ_RCE%02d", (float)nhits/m_normalization);
    //std::cout<<"All: "<<nhits<<std::endl;
    Monitoring::publish("Nevt_RCE%02d", m_fwregs->getNumberOfEvents(0));
    Monitoring::publish("NMonMissed_RCE%02d", m_fwregs->getNumberOfMonMissed(0));
  }
}

int AtlasDataProc::fit(std::string command){
  if(command=="CloseFile"){
    std::cout<<"Stopping monitoring thread. "<<m_nfrag<<std::endl;
    pthread_cancel(m_thread);
  }
  return 0;
}

int AtlasDataProc::processData(unsigned rlink, unsigned *data, int size){
  const char* errors[8]={"Too many headers ", "Timeout ", "Bad header ", "Skipped trigger ", 
		   "Data no header ", "Missing trigger ", "Desynched ", "Disabled "};
  unsigned short *datas=(unsigned short*)data;
  int status=2*size-14;
  m_nfrag++;
  //std::cout<<"Fragment of size "<<size<<std::endl;
  for(int i=0; i<=7;i++){
    if(datas[status+i]!=0){
      std::cout<<errors[i]<<datas[status+i]<<std::endl;
      dumpEvent(data, size);
    }
  }
  return 0;
}

void AtlasDataProc::dumpEvent(unsigned* data, int size){
  std::cout<<"Event Fragment:"<<std::endl;
  for(int i=0;i<size;i++){
    std::cout<<i<<": "<<std::hex<<data[i]<<std::dec<<std::endl;
  }
}


AtlasDataProc::AtlasDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif) {
  std::cout<<"Start AtlasDataProc Constructor"<<std::endl;
  m_fwregs=new RceFWRegisters(cif);
  m_normalization=1000;
  try{ //catch bad scan option parameters
    m_nModules=m_configIF->getNmodules();
    //m_nL1AperEv=scanOptions->get<int>("trigOpt.nL1AperEvent");
    std::string name=scanOptions->get<std::string>("Name");
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
  m_rceNumber=RceName::getRceNumber();
  m_modlabels=new char*[m_nModules];
  m_outlink=new int[m_nModules];
  for(int i=0;i<m_nModules;i++){
    m_modlabels[i]=new char[32];
    sprintf(m_modlabels[i], "Occ_RCE%02d_FE%02d", m_rceNumber, m_configIF->getModuleInfo(i).getOutLink());
    m_outlink[i]=m_configIF->getModuleInfo(i).getOutLink();
  }
  pthread_create( &m_thread, 0, AtlasDataProc::monitoring, (void*)this);
}

AtlasDataProc::~AtlasDataProc(){
  void *status;
  pthread_cancel(m_thread);
  pthread_join(m_thread, &status);
  if(status != PTHREAD_CANCELED){
    std::cout<<"Thread was not canceled properly!"<<std::endl;
  }else{
    std::cout<<"Thread exited normally"<<std::endl;
  }
  delete m_fwregs;
  for(int i=0;i<m_nModules;i++)delete[] m_modlabels[i];
  delete [] m_modlabels;
  delete [] m_outlink;
}

unsigned AtlasDataProc::nEvents(){
  return m_nfrag;
}
