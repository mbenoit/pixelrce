#ifndef REGULARSCANSETUP_HH
#define REGULARSCANSETUP_HH

#include "rcecalib/scanctrl/NestedLoop.hh"
#include "rcecalib/scanctrl/LoopSetup.hh"
#include "rcecalib/scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/ConfigIF.hh"

class RegularScanSetup: public LoopSetup{
public:
  RegularScanSetup():LoopSetup(){};
  virtual ~RegularScanSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
