// Reads in module config from Turbo DAQ file. Modified version of PixLib's TurboDaqDB.h
#ifndef TURBODAQFILE_HH
#define TURBODAQFILE_HH

#include <map>
#include <string>
#include <fstream>
#include <stdint.h>
#include "PixelModuleConfig.hh"

#ifdef __RCF__
typedef uint32_t corbaulong;
#else
typedef CORBA::ULong corbaulong;
#endif

  class TurboDaqFile { 
  protected:
    void openTurboDaqFiles(std::string filename);
    int columnPairMaskDecode(int mask, int chipNumber);
    void boolMaskFileDecode(corbaulong arr[][ipc::IPC_N_PIXEL_COLUMNS], int valflag, std::ifstream* file);
    void intMaskFileDecode(unsigned char arr[][ipc::IPC_N_PIXEL_COLUMNS], int valflag, std::ifstream* file);
    void getLineDos(std::ifstream& is, std::string& str);
    void writeMaskFile(corbaulong arr[][ipc::IPC_N_PIXEL_COLUMNS],  std::string path);
    void writeMaskFileFT(unsigned char arr[][ipc::IPC_N_PIXEL_COLUMNS],  std::string path);
    std::string getFullPath(std::string relPath);
    
    std::string m_moduleCfgFilePath;
    std::ifstream *m_moduleCfgFile;
    
  public:
    ~TurboDaqFile(){}
    TurboDaqFile();
    void readModuleConfig(ipc::PixelModuleConfig*, std::string filename);
    void writeModuleConfig(ipc::PixelModuleConfig* cfg, const std::string &base, const std::string &confdir, 
const std::string &configname, const std::string &key);
    void dump(const ipc::PixelModuleConfig&);
  };

#endif
