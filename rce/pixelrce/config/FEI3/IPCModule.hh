#ifndef IPCFEI3MODULE_HH
#define IPCFEI3MODULE_HH

#include "config/FEI3/Module.hh"
#include "ipc/object.h"
#include "PixelModuleConfig.hh"
#include "IPCFEI3Adapter.hh"

class IPCPartition;
class AbsFormatter;

namespace FEI3{
template <class TP = ipc::single_thread>
class IPCModule: public IPCNamedObject<POA_ipc::IPCFEI3Adapter,TP>, public FEI3::Module {
public:
  IPCModule(IPCPartition & p, const char * name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt);
  ~IPCModule();
  CORBA::Long IPCdownloadConfig(const ipc::PixelModuleConfig &config);    
  void globalLegacyToUsr(const ipc::PixelFEConfig* legacy, unsigned chip);    
  void shutdown();
  void destroy();

};
};
  

#endif
