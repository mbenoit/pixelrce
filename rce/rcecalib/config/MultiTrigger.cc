
#include "rcecalib/config/MultiTrigger.hh"
#include "rcecalib/config/FEI3/FECommands.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/HW/SerialIF.hh"
#include "rcecalib/profiler/Profiler.hh"
#ifdef __rtems__
#include "rcecalib/HW/Headers.hh"
#include "rcecalib/HW/RCDImaster.hh"
#ifdef RCE_V2
#include "datCode.hh"
#include DAT_PUBLIC( oldPpi, pic,       Tds.hh)
#include DAT_PUBLIC( oldPpi, pic,       Pool.hh)
#include DAT_PUBLIC( oldPpi, pgp,       Driver.hh)
#include DAT_PUBLIC( oldPpi, pgp,       DriverList.hh)
#else
#include "rce/pic/Pool.hh"
#include "rce/pgp/Driver.hh"
#include "rce/pgp/DriverList.hh"
#include "rce/pic/Tds.hh"
#endif
#include "namespace_aliases.hh"
using namespace PgpTrans;
#endif


  MultiTrigger::MultiTrigger():AbsTrigger(),
			       m_calL1ADelay(-1),m_repetitions(0),m_interval(0),m_nIntervals(0),m_injectForTrigger(1){
    m_parameters.push_back("TRIGGER_DELAY");
    m_parameters.push_back("MULTITRIG_INTERVAL");
#ifdef __rtems__
    const RcePic::Params Tx = {
      RcePic::NonContiguous,
      16,     // Header
      64*132, // Payload
      1     // Number of buffers
    };

  m_pool = new RcePic::Pool::Pool(Tx);    
  m_tds=m_pool->allocate();
  assert (m_tds!=0); 
  char* header=(char*)m_tds->header();
  int headersize=m_tds->headersize();
  for (int i=0;i<headersize;i++)header[i]=0;
  new(m_tds->header())BlockWriteTxHeader(0);
#endif
  }
  MultiTrigger::~MultiTrigger(){
#ifdef __rtems__
    m_pool->deallocate(m_tds);
    delete m_pool;
#endif
  }
  int MultiTrigger::configureScan(boost::property_tree::ptree* scanOptions){
    int retval=0;
    m_i=0; //reset the number of triggers
    try{
      m_calL1ADelay = scanOptions->get<int>("trigOpt.CalL1ADelay");
      m_repetitions = scanOptions->get<int>("trigOpt.nTriggersPerGroup");
      m_injectForTrigger = scanOptions->get<int>("trigOpt.injectForTrigger");
    
      std::cout<<"I'm here right?"<<std::endl;
      m_nIntervals = scanOptions->get<int>("scanLoop_0.nPoints");
      std::cout<<"intervals = "<<m_nIntervals<<std::endl;
      m_intervals.clear();
      
      /*
      char pointname[100];
      for(int ii=0; ii<m_nIntervals; ii++){
	sprintf(pointname,"scanLoop_1.dataPoints.P_%d",ii);
	int interval = scanOptions->get<int>(pointname);
	m_intervals.push_back(interval);
	std::cout<<"interval_"<<ii<<" = "<<interval<<std::endl;
      }
      */

      m_interval = 241;  //  m_intervals[0];
      std::cout<<"interval = "<<m_interval<<std::endl;
      
      setupTriggerStream();
    }
     catch(boost::property_tree::ptree_bad_path ex){
       rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
       ers::error(issue);
       retval=1;
     }
    return retval;
  }

  int MultiTrigger::setupParameter(const char* name, int val){
    
    if(std::string(name)=="MULTITRIG_INTERVAL"){
      if(val!=m_interval){ // have to redo trigger stream
	std::cout<<"Remaking trigger stream with interval "<<val<<std::endl;
        m_interval=val;
        setupTriggerStream();
      }
    }
    else if(std::string(name)=="TRIGGER_DELAY"){
      if(val!=m_calL1ADelay){ // have to redo trigger stream
	//	std::cout<<"Setting up trigger stream. Delay is "<<val<<std::endl;
	m_calL1ADelay=val;
	setupTriggerStream();
      }
    }
    return 0;
  }

  void MultiTrigger::setupTriggerStream(){
#ifdef __rtems__
    unsigned* payload=(unsigned*)m_tds->payload();
#endif
    m_triggerStream.clear();
    //prepend 4 zeroes a la NewDsp
    BitStreamUtils::prependZeros(&m_triggerStream);
    std::vector<char> bytestream;
    
    /*Create trigger stream */
    /*m_repetitions number of CAL commands, each separated by m_interval zeros. */
    /*Then there is a delay, followed by m_repetitions number of L1A commands. */
    /*The L1A commands are separated by (m_interval+4) zeros, since the L1A commands */
    /*are shorter than CAL commands. */
    /*The delay between the CAL commands and the L1A commands is set by m_calL1ADelay, */
    /*which is the delay between end of the first CAL and end of the first L1A command.  */
    /* CAL is a slow command with 9 bits 101100100, L1A is 5 bits 11101*/
    
    /*This code only works for m_repetitions=2. */

    /*WARNING: If m_interval has the wrong value, then */
    /*trigger stream won't make sense, because the CAL command will overlap */
    /*with the L1A's command */
    /*For two-trigger case, this means*/
    /*m_interval <= (m_calL1ADelay-14) or m_interval >= m_calL1ADelay. */

    std::cout<<"MultiTrigger setup:  calL1ADelay = "<<m_calL1ADelay<<" ; interval = "<<m_interval<<" ; repetitions = "<<m_repetitions<<std::endl;

    if(m_repetitions!=2){
      std::cout<<"MultiTrigger currently only implemented for two-trigger case.  Ignoring m_repetitions = "<<m_repetitions<<std::endl;
    }
    
    if(m_interval>m_calL1ADelay-14 && m_interval < m_calL1ADelay){
      std::cout<<"ERROR in MultiTrigger.cc:  m_interval = "<<m_interval<<" has a forbidden value!"<<std::endl; 
      std::cout<<"Cannot allow CAL commands and L1A commands to overlap!"<<std::endl;
    }


    int totalLen = 16 + m_interval + 9 + m_calL1ADelay;
    int numChars = totalLen/8;
    if(totalLen%8 !=0){
      numChars++;
    }

    std::cout<<"Making bytestream "<<numChars<<" bytes long."<<std::endl;

    std::vector<char> bytestream1(numChars, 0);
    std::vector<char> bytestream2(numChars, 0);
    std::vector<char> bytestream3(numChars, 0);
    std::vector<char> bytestream4(numChars, 0);

    //m_injectForTrigger is a bit mask.  if bit 0 is enabled, then inject on first trigger.
    //if bit 1 is enabled, then inject on second trigger.  
    
    if( m_injectForTrigger&1 ){
      //do a pulse injection for the first trigger
      createByteStream(bytestream1,0x0164,9,7);     /* 101100100 (9bits) = CAL */
    }
    //  else{
    //   //don't do a pulse injection for the first trigger
    //  createByteStream(bytestream1,0x0000,9,7);
    // }
    
    if( m_injectForTrigger&2 ){
      //do a pulse injection for the second trigger
      createByteStream(bytestream2,0x0164,9,16+m_interval);   //2nd CAL command
    }
    
    createByteStream(bytestream3,0x1d,5,16+m_calL1ADelay-5);   /* 11101 (5bits) = L1A */
    createByteStream(bytestream4,0x1d,5,16+m_interval+9+m_calL1ADelay-5);  //2nd L1A command

    
    //The overall bytestream is the logical OR of each of the individual bytestreams
    for(int ibyte=0; ibyte<numChars; ibyte++){
      
      char finalByte = bytestream1[ibyte];
      finalByte = finalByte | bytestream2[ibyte];
      finalByte = finalByte | bytestream3[ibyte];
      finalByte = finalByte | bytestream4[ibyte];
     
      bytestream.push_back(finalByte);
    }
    
    while(bytestream.size()%4!=0)bytestream.push_back(0);
    
    
    //    for (size_t i=0;i<bytestream.size();i++)std::cout<<std::hex<<(unsigned)bytestream[i]<<std::dec<<std::endl;
    //    std::cout<<"byte stream size is "<<bytestream.size()<<std::endl;
    
    for(unsigned int i=0;i<bytestream.size()/4;i++){
      unsigned word=0;
      for (int j=0;j<4;j++){
	word<<=8;
	word|=bytestream[i*4+j];
      }
      m_triggerStream.push_back(word);
    }
    
    
    //std::cout<<"Length of trigger stream is "<<m_triggerStream.size()<<std::endl;
    //for(size_t i=0; i<m_triggerStream.size(); i++){
    //  std::cout<<std::hex<<(unsigned)m_triggerStream[i]<<std::dec<<std::endl;
    //}
    
    
    //SerialIF::writeRegister(19,0);
      //for (size_t i=0;i<m_triggerStream.size();i++)SerialIF::writeRegister(18,m_triggerStream[i]);
#ifdef __rtems__
      for (size_t i=0;i<m_triggerStream.size();i++)payload[i]=m_triggerStream[i];
      m_tds->payloadsize(m_triggerStream.size()*sizeof(unsigned));
      m_tds->flush(true);
#endif
  }
  
  int MultiTrigger::sendTrigger(){
#ifdef __rtems__
    RCDImaster::instance()->blockWrite(m_tds);
    //SerialIF::sendCommand(0xa);
#else
    SerialIF::send(&m_triggerStream,SerialIF::DONT_CLEAR|SerialIF::WAITFORDATA);
#endif
    m_i++;
    return 0;
  }

  int MultiTrigger::enableTrigger(bool on){
    return SerialIF::enableTrigger(on);
    return 0;
  }
  int MultiTrigger::resetCounters(){
    BitStream *bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    FEI3::FECommands::sendECR(bs);
    FEI3::FECommands::sendBCR(bs);
    SerialIF::send(bs,SerialIF::WAITFORDATA);
    delete bs;
    return 0;
  }

void MultiTrigger::createByteStream(std::vector<char>& stream, unsigned int command, unsigned int len, unsigned int offset){

  //  std::cout<<"byte stream "<<command<<" length = "<<len<<" offset = "<<offset<<std::endl;

  assert(len<=9);  //if len>9, some parts of function would have to change

  int shift = 16 - len;
  command = (command<<shift);
 
  unsigned int numChars = offset/8;
  offset = offset%8;

  //if len>9, then the following command could potentially erase bits, which we don't want
  command = (command>>offset);
  char byte1 = (command >> 8 ) & 0xff;
  char byte2 = command & 0xff;

  stream[numChars] = byte1;
  if( (offset+len)>8 ){
    stream[numChars+1]  = byte2; 
  }
  
 
  // for(size_t i=0; i<stream.size(); i++){
  //  std::cout<<std::hex<<(unsigned)stream[i]<<std::dec<<std::endl;
  // }
  // std::cout<<std::endl<<std::endl;
  
}
