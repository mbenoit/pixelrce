#include "rcedcs/gpib/LVchan.hh"
#include "rcedcs/gpib/GpibController.hh"
#include <iostream>
#include <stdlib.h>

LVchan::LVchan(const char* name, unsigned channel, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options):
  TGVerticalFrame(p,w,h,options), m_channel(channel){

  ChangeBackground(GetWhitePixel());
  
  TGLabel *m_name=new TGLabel(this,name);
  m_name->ChangeBackground(GetWhitePixel());
  AddFrame(m_name,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 2, 2, 10, 0));
  FontStruct_t labelfont;
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  m_name->SetTextFont(labelfont);
  
  TGLabel *voltagelabel=new TGLabel(this,"Voltage:");
  voltagelabel->ChangeBackground(GetWhitePixel());
  AddFrame(voltagelabel,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 2, 2, 10, 0));

  m_voltage=new TGLabel(this,"     0.00    ");
  m_voltage->ChangeBackground(GetWhitePixel());
  AddFrame(m_voltage,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 2, 2, 10, 0));
  m_voltage->SetTextFont(labelfont);

  TGLabel *currentlabel=new TGLabel(this,"Current:");
  currentlabel->ChangeBackground(GetWhitePixel());
  AddFrame(currentlabel,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 2, 2, 10, 0));

  m_current=new TGLabel(this,"     0.00    ");
  m_current->ChangeBackground(GetWhitePixel());
  AddFrame(m_current,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 2, 2, 10, 0));
  m_current->SetTextFont(labelfont);

}

LVchan::~LVchan(){
  Cleanup();
}

void LVchan::update(GpibController* controller){
  char cmd[128];
  sprintf(cmd,"INST:SELECT OUTPUT%d", m_channel);
  controller->write(cmd);
  char formatted[20];
  sprintf(formatted,"%.02f V",atof(controller->read("MEASURE:VOLTAGE?")));
  updateText(m_voltage,formatted);
  sprintf(formatted,"%.03f A",atof(controller->read("MEASURE:CURRENT?")));
  updateText(m_current,formatted);
}  

void LVchan::updateText(TGLabel* label, const char* newtext){
  unsigned len=strlen(label->GetText()->GetString());
  label->SetText(newtext);
  if (strlen(newtext)>len)Layout();
}
