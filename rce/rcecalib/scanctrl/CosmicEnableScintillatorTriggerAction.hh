#ifndef COSMICENABLESCINTILLATORTRIGGER_HH
#define COSMICENABLESCINTILLATORTRIGGER_HH

#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/config/ConfigIF.hh"

class CosmicEnableScintillatorTriggerAction: public LoopAction{
public:
  CosmicEnableScintillatorTriggerAction(std::string name, ConfigIF* cif):
    LoopAction(name),m_configIF(cif){}
  int execute(int i){
    unsigned serstat= m_configIF->writeHWregister(11,1);
    assert(serstat==0);
    //    std::cout<<"Enabled scintillator trigger"<<std::endl;
    return 0;
  }
private:
  ConfigIF* m_configIF;
};

#endif
