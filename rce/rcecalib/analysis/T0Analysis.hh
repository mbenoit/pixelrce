#ifndef T0ANALYSIS_HH
#define T0ANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"

class ConfigGui;
class TFile;
class TH2D;
class TH1;
namespace RCE{
  class PixScan;
}

class T0Analysis: public CalibAnalysis{
public:
  T0Analysis(): CalibAnalysis(){}
  ~T0Analysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
  void fillHit1d(std::string &name, TH1* hits1d, unsigned numval);
};


#endif
