library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.stdrtlpkg.all;

entity LedTestPatternGenerator is
  generic
  (
    divider: integer
  );
  port 
  (
    clk:    in  sl;
    led:    out slv(7 downto 0) := (others => '0');
    dip_sw: in  slv(7 downto 0)
  );
end LedTestPatternGenerator;

architecture LedTestPatternGenerator of LedTestPatternGenerator is
  subtype counter_t is natural range 0 to divider - 1;
  signal counter: counter_t := counter_t'high;
  signal ledv: std_logic_vector(7 downto 0) := (others => '0');
  signal next_ledv: std_logic := '0';
begin
  process begin
    wait until rising_edge(clk);
    if (counter = counter_t'low) then
      counter <= counter_t'high;
      next_ledv <= '1';
    else
      counter <= counter - 1;
      next_ledv <= '0';
    end if;
  end process;

  process begin
    wait until rising_edge(clk) and next_ledv = '1';
    case ledv is
      when x"00" => ledv <= x"ff";
      when x"ff" => ledv <= x"0f";
      when x"0f" => ledv <= x"f0";
      when x"f0" => ledv <= x"33";
      when x"33" => ledv <= x"cc";
      when x"cc" => ledv <= x"55";
      when x"55" => ledv <= x"aa";
      when x"aa" => ledv <= x"01";
      when others => 
        ledv <= ledv(6 downto 0) & '0';
    end case;
  end process;

  process begin
    wait until rising_edge(clk);
    case dip_sw is
      when x"00" => 
        led <= ledv;
      when others =>
        led <= dip_sw;
    end case;
  end process;
end LedTestPatternGenerator;
