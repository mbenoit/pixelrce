#ifndef IPCAFPHPTDCMODULE_HH
#define IPCAFPHPTDCMODULE_HH

#include "rcecalib/config/afp-hptdc/Module.hh"
#include "ipc/object.h"
#include "AFPHPTDCModuleConfig.hh"
#include "IPCAFPHPTDCAdapter.hh"

class IPCPartition;
class AbsFormatter;

namespace afphptdc{
template <class TP = ipc::single_thread>
class IPCModule: public IPCNamedObject<POA_ipc::IPCAFPHPTDCAdapter,TP>, public afphptdc::Module {
public:
  IPCModule(IPCPartition & p, const char * name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt);
  ~IPCModule();
  CORBA::Long IPCdownloadConfig(const ipc::AFPHPTDCModuleConfig &config);    
  void shutdown();
  void destroy();

};
};
  

#endif
