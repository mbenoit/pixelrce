--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;
use work.arraytype.all;
use work.AxiStreamPkg.all;
use work.SsiPkg.all;
use work.StdRtlPkg.all;
--------------------------------------------------------------


entity eventfragmentbuilder is
generic(
        AXI_CONFIG_C : AxiStreamConfigType := ssiAxiStreamConfig(4));
port(	clk: 	            in std_logic;
        regClk:             in std_logic;
	rst:	            in std_logic;
        enabled:            in std_logic;
        mAxisMaster:        out AxiStreamMasterType;
        mAxisSlave:         in AxiStreamSlaveType;
        mAxisMasterMon:     out AxiStreamMasterType;
        mAxisSlaveMon:      in AxiStreamSlaveType;
        channelmask:        in std_logic_vector(15 downto 0);
        masked:             out std_logic_vector(15 downto 0);
        counters:           out CounterType;
        resetRun:           in sl;
        nExp:               in slv(4 downto 0);
        timeout:            in slv(9 downto 0);
        timeoutfirst:       in slv(9 downto 0);
        missing_header_timeout: in slv(7 downto 0);
        runnumber:          in slv(31 downto 0);
        nummon:             in slv(31 downto 0);
        monenabled:         in sl:='0';
        nEvt:               out slv(31 downto 0);
        nEvtNonMon:         out slv(31 downto 0);
        datavalid:          in std_logic_vector(15 downto 0):=(others => '0');
        nextdatavalid:      in std_logic_vector(15 downto 0):=(others => '0');
        datain:             in Slv33Array(15 downto 0):=(others => (others => '0'));
        nextdatain:         in Slv33Array(15 downto 0):=(others => (others => '0'));
        ldfei4:             out std_logic_vector(15 downto 0);
        ttcvalid :          in sl;
        ttcdata:            in slv(51 downto 0);
        ttcld:              out sl
);
end eventfragmentbuilder;

--------------------------------------------------------------

architecture EVENTFRAGMENTBUILDER of eventfragmentbuilder is

   function l1idcomp(l1id: slv; ttcid: slv) return integer is
   begin
     return (conv_integer(unsigned(l1id))-conv_integer(unsigned(ttcid))) mod 32; 
   end function l1idcomp;

  type state_type is (idle, writeHeader, feWait, readFeHeader, startData, readData, missingHeaderTimeout,
                      doneTimeout, nextFe, writeTrailer);
   constant COUNTER_INIT_C: CounterType :=(
    timeoutcounter =>      (others => (others => '0')),
    toomanyheadercounter => (others => (others => '0')),
    skippedtriggercounter => (others => (others => '0')),
    badheadercounter =>    (others => (others => '0')),
    missingtriggercounter => (others => (others => '0')),
    datanoheadercounter => (others => (others => '0')),
    desynchcounter => (others => (others => '0')),
    occounter => (others => (others => '0'))
    );
  type RegType is record
    txMaster   : AxiStreamMasterType;
    state      : state_type;
    reqdataold : slv(15 downto 0);
    reqdatattc : sl;
    el1id      : slv(31 downto 0);
    bcid       : slv(11 downto 0);
    headercounter:  integer range 0 to 15;
    trailercounter: integer range 0 to 15;
    numrec     : integer range 0 to 32767;
    timeout    : integer range 0 to 1023;
    nTrig      : slv(4 downto 0);
    comeback   : sl;
    timeoutstatus: slv(15 downto 0);
    toomanyheaders: slv(15 downto 0);
    skippedtriggers: slv(15 downto 0);
    badheader: slv(15 downto 0);
    missingtrigger: slv(15 downto 0);
    datanoheader: slv(15 downto 0);
    desynched: slv(15 downto 0);
    enabledMask: slv(15 downto 0);
    nSkipped: Slv10Array(15 downto 0);
    skippedLast: Slv10Array(15 downto 0);
    hmtimeout: integer range 0 to 1023;
    writeData: sl;
    forcereq: slv(15 downto 0);
    counters   : countertype;
    occl: Slv32Array(15 downto 0);
    occevcount: slv(31 downto 0);
    mongood: sl;
    nEvt: slv(31 downto 0);
    nEvtNonMon: slv(31 downto 0);
    link       : integer range 0 to 15;
    bcidset    : sl;
    bciddiff   : slv(9 downto 0);
   end record RegType;

   constant REG_INIT_C : RegType :=(
     txMaster => AXI_STREAM_MASTER_INIT_C,
     state => idle,
     reqdataold => (others => '0'),
     reqdatattc => '0',
     el1id => (others => '0'),
     bcid => (others => '0'),
     headercounter => 0,
     trailercounter => 0,
     numrec => 0,
     timeout => 0,
     nTrig => (others =>'0'),
     comeback => '0',
     timeoutstatus => (others => '0'),
     toomanyheaders => (others => '0'),
     skippedtriggers => (others => '0'),
     badheader => (others => '0'),
     missingtrigger => (others => '0'),
     datanoheader => (others => '0'),
     desynched => (others => '0'),
     enabledMask => (others => '0'),
     nSkipped => (others => (others => '0')),
     skippedLast => (others => (others => '0')),
     hmtimeout => 0,
     writeData => '0',
     forcereq => (others => '0'),
     counters => COUNTER_INIT_C,
     occl => (others => (others => '0')),
     occevcount => (others => '0'),
     mongood => '0',
     nEvt => (others => '0'),
     nEvtNonMon => (others => '0'),
     link => 0,
     bcidset => '0',
     bciddiff => (others => '0')
     );


  constant header: Slv32Array(3 downto 0):=(0=>x"EE1234EE",
                                            1=>x"00000009",
                                            2=>x"03010000",
                                            3=>x"00850001");
    
  signal slinkready: std_logic:='0';
  signal reqdata: slv(15 downto 0);
  signal resetrun_s: sl; 
  signal enabled_s: sl; 
  signal channelmask_s: slv(15 downto 0); 
  signal masked_s: slv(15 downto 0); 
  signal nExp_s: slv(4 downto 0); 
  signal timeout_s: slv(9 downto 0); 
  signal timeoutfirst_s: slv(9 downto 0); 
  signal missing_header_timeout_s: slv(7 downto 0); 
  signal runnumber_s: slv(31 downto 0); 
  signal nummon_s: slv(31 downto 0); 
  signal monthresh: sl;
  signal monenabled_s: sl;
  
  signal r   : RegType := REG_INIT_C;
  signal rin : RegType;

begin


  oneshot_resetrun: entity work.oneshot
    port map(
      clk => clk,
      rst => rst,
      datain => resetrun,
      dataout => resetrun_s);
  synch_enabled: entity work.Synchronizer
   port map(
     clk => clk,
     dataIn => enabled,
     dataOut => enabled_s);
  synch_monenabled: entity work.Synchronizer
   port map(
     clk => clk,
     dataIn => monenabled,
     dataOut => monenabled_s);
  synch_channelmask : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 16)    
    port map (
      clk        => clk,
      dataIn => channelmask,
      dataOut => channelmask_s);  
  synch_nExp : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 5)    
    port map (
      clk        => clk,
      dataIn => nExp,
      dataOut => nExp_s);  
  synch_timeout : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 10)    
    port map (
      clk        => clk,
      dataIn => timeout,
      dataOut => timeout_s);  
  synch_timeoutfirst : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 10)    
    port map (
      clk        => clk,
      dataIn => timeoutfirst,
      dataOut => timeoutfirst_s);  
  synch_missing_header_timeout : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 8)    
    port map (
      clk        => clk,
      dataIn => missing_header_timeout,
      dataOut => missing_header_timeout_s);  
  synch_runnumber : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 32)    
    port map (
      clk        => clk,
      dataIn => runnumber,
      dataOut => runnumber_s);  
  synch_nummon : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 32)    
    port map (
      clk        => clk,
      dataIn => nummon,
      dataOut => nummon_s);  
  synchout_nEvt : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 32)    
    port map (
      clk        => regclk,
      dataIn => r.nEvt,
      dataOut => nEvt);  
  synchout_nEvtNonMon : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 32)    
    port map (
      clk        => regclk,
      dataIn => r.nEvtNonMon,
      dataOut => nEvtNonMon);  
  synchout_masked : entity work.SynchronizerVector
    generic map (
      WIDTH_G => 16)    
    port map (
      clk        => regClk,
      dataIn => masked_s,
      dataOut => masked);  
  countersynch: for I in 0 to 15 generate
    synchout_counter : entity work.SynchronizerVector
      generic map (
        WIDTH_G => 88)    
      port map (
        clk        => regClk,
        dataIn(7 downto 0) => r.counters.timeoutcounter(I),
        dataIn(15 downto 8) => r.counters.toomanyheadercounter(I),
        dataIn(23 downto 16) => r.counters.skippedtriggercounter(I),
        dataIn(31 downto 24) => r.counters.badheadercounter(I),
        dataIn(39 downto 32) => r.counters.missingtriggercounter(I),
        dataIn(47 downto 40) => r.counters.datanoheadercounter(I),
        dataIn(55 downto 48) => r.counters.desynchcounter(I),
        dataIn(87 downto 56) => r.counters.occounter(I),
        dataOut(7 downto 0) => counters.timeoutcounter(I),
        dataOut(15 downto 8) => counters.toomanyheadercounter(I),
        dataOut(23 downto 16) => counters.skippedtriggercounter(I),
        dataOut(31 downto 24) => counters.badheadercounter(I),
        dataOut(39 downto 32) => counters.missingtriggercounter(I),
        dataOut(47 downto 40) => counters.datanoheadercounter(I),
        dataOut(55 downto 48) => counters.desynchcounter(I),
        dataOut(87 downto 56) => counters.occounter(I));
  end generate countersynch;
  
  mAxisMaster<=r.txMaster;

  mAxisMasterMon.tValid<=r.txMaster.tValid and monenabled_s and r.mongood;
  -- for monitroring copy everything except for tValid
  mAxisMasterMon.tData<=r.txMaster.tData;
  mAxisMasterMon.tStrb<=r.txMaster.tStrb;
  mAxisMasterMon.tKeep<=r.txMaster.tKeep;
  mAxisMasterMon.tLast<=r.txMaster.tLast;
  mAxisMasterMon.tDest<=r.txMaster.tDest;
  mAxisMasterMon.tId<=r.txMaster.tId;
  mAxisMasterMon.tUser<=r.txMaster.tUser;
  monthresh<=mAxisSlaveMon.tReady;
  slinkready<=mAxisSlave.tReady;
  REQ: for I in 0 to 15 generate
    reqdata(I)<=(nextdatavalid(I) and not datain(I)(32) and datavalid(I) and slinkready) or r.forcereq(I) ;
  end generate REQ;
  ldfei4<=reqdata;
  ttcld<=r.reqdatattc;
  masked_s<=not r.enabledMask;

  comb: process (r, enabled_s, nExp_s, nextdatavalid, channelmask_s, datain, nextdatain, ttcvalid, ttcdata, rst, resetrun_s, reqdata, monthresh, monenabled_s) is
    variable v: RegType;
  begin
    v := r;
    v.txMaster.tUser := (others => '0');
    v.reqdatattc := '0';
    v.txMaster.tValid := '0';
    v.txMaster.tLast := '0';
    v.reqdataold := reqdata;
    case v.state is
      when idle =>
        if(resetRun_s='1')then
          v.counters:=COUNTER_INIT_C;
          v.timeoutstatus:=(others=>'0');
          v.badheader:=(others=>'0');
          v.enabledmask:= channelmask_s;
          v.nSkipped:=(others => (others => '0'));
          v.skippedLast:=(others => (others => '0'));
          v.nTrig := (others =>'0');
          v.bcidset := '0';
          v.occl := (others => (others => '0'));
          v.occevcount := (others =>'0');
          v.nEvt := (others =>'0');
          v.nEvtNonMon := (others =>'0');
        elsif(enabled_s='1' and ttcvalid='1' )then
          v.reqdatattc:='1';
          v.el1id:=ttcdata(31 downto 0);
          v.bcid:= ttcdata(43 downto 32);
          v.headercounter:= 0;
          v.trailercounter:= 0;
          v.numRec:=0;
          v.toomanyheaders:=(others=>'0');
          v.skippedtriggers:=(others=>'0');
          v.datanoheader:=(others=>'0');
          v.desynched:=(others=>'0');
          v.occevcount:=unsigned(r.occevcount)+1;
          v.nEvt:=unsigned(r.nEvt)+1;
          if(monenabled_s='1' and monthresh='0')then
            v.nEvtNonMon:=unsigned(r.nEvtNonMon)+1;
          end if;
          v.mongood:= monthresh;
          v.state:=writeHeader;
        end if;
      when writeHeader =>
        if(slinkready='1')then
          v.txMaster.tValid:='1';
          v.headercounter:=r.headercounter+1;
          if(r.headercounter=0)then
            ssiSetUserSof(AXI_CONFIG_C, v.txMaster, '1');
          end if;
          if(r.headercounter<4) then
            v.txMaster.tData(31 downto 0):= header(r.headercounter);
          elsif(r.headercounter=4)then
            v.txMaster.tData(31 downto 0):= runnumber_s;
          elsif(r.headercounter=5)then
            v.txMaster.tData(31 downto 0):= r.el1id;
          elsif(r.headercounter=6)then
            v.txMaster.tData(31 downto 0):= x"00000" & r.bcid;
          elsif(r.headercounter=7)then
            v.txMaster.tData(31 downto 0):= (others => '0');
          elsif(r.headercounter=8)then
            v.txMaster.tData(31 downto 0):= (others => '0');
            v.state:=feWait;
            v.link:=0;
            v.timeout:=conv_integer(unsigned(timeout_s))+conv_integer(unsigned(timeoutfirst_s));
          end if; 
        end if;
      when feWait =>
        if(r.enabledMask=x"0")then
          v.state:=writeTrailer;
        end if;
        if(r.enabledMask(r.link)='0')then
          if(r.link=15)then
            v.state:= nextFe;
          else
            v.link:=r.link+1;
          end if;
        else
          if(nextdatavalid(r.link)='1')then
            v.state := readFeHeader;
            v.timeoutstatus(r.link):='0';
          else
            if(r.timeout=0)then
              if(r.nSkipped(r.link) /= "0000000000" )then
                v.nSkipped(r.link):=unsigned(r.nSkipped(r.link))-1;
                v.skippedtriggers(r.link):='1';
                if(r.counters.skippedtriggercounter(r.link)/=x"ff")then
                  v.counters.skippedtriggercounter(r.link):=unsigned(r.counters.skippedtriggercounter(r.link))+1;
                end if;
              else
                v.timeoutstatus(r.link):='1';
                if(r.counters.timeoutcounter(r.link)/=x"ff")then
                  v.counters.timeoutcounter(r.link):=unsigned(r.counters.timeoutcounter(r.link))+1;
                end if;
                if(r.timeoutstatus(r.link)='1')then
                  v.enabledMask(r.link):='0';
                end if;
              end if;
              v.state:= nextFe;
            else
              v.timeout:=r.timeout-1;
            end if;
          end if;
        end if;
      when readFeHeader =>
        v.writeData:='0';
        v.comeback:='0';
        v.badheader(r.link):='0';
        if(nextdatain(r.link)(31 downto 28)="0011")then -- header
          if(nextdatain(r.link)(14 downto 10)=r.el1id(4 downto 0))then --good l1id
            v.state:=startData;
            v.writeData:='1';
            if(r.bcidset='0')then -- set BCID difference
              v.bcidset:='1';
              v.bciddiff:=unsigned(nextdatain(r.link)(9 downto 0))-unsigned(r.bcid(9 downto 0));
            elsif(unsigned(r.bcid(9 downto 0))+unsigned(r.bciddiff)+unsigned(r.nTrig)
                  /=unsigned(nextdatain(r.link)(9 downto 0))) then
              v.desynched(r.link):='1';
              if(r.counters.desynchcounter(r.link)/=x"ff")then
                v.counters.desynchcounter(r.link):=unsigned(r.counters.desynchcounter(r.link))+1;
              end if;
            end if;
          elsif (l1idcomp(nextdatain(r.link)(14 downto 10), r.el1id(4 downto 0)) = 31)then
              v.toomanyheaders(r.link):='1';
              if(r.counters.toomanyheadercounter(r.link)/=x"ff")then
                v.counters.toomanyheadercounter(r.link):=unsigned(r.counters.toomanyheadercounter(r.link))+1;
              end if;
              v.comeback:='1';
              v.state:=startData;
          elsif (l1idcomp(nextdatain(r.link)(14 downto 10), r.el1id(4 downto 0)) <= unsigned(r.nSkipped(r.link)))then
            v.skippedtriggers(r.link):='1';
            if(r.counters.skippedtriggercounter(r.link)/=x"ff")then
              v.counters.skippedtriggercounter(r.link):=unsigned(r.counters.skippedtriggercounter(r.link))+1;
            end if;
            v.nSkipped(r.link):=unsigned(r.nSkipped(r.link))-1;
            v.state:=nextFe;
          elsif (l1idcomp(nextdatain(r.link)(14 downto 10), r.el1id(4 downto 0)) = 1)then --SEU
            v.missingtrigger(r.link):='1';
            if(r.counters.missingtriggercounter(r.link)/=x"ff")then
              v.counters.missingtriggercounter(r.link):=unsigned(r.counters.missingtriggercounter(r.link))+1;
            end if;
            v.state:=nextFe;
          else --just bad
            v.badheader(r.link):='1';
            if(r.counters.badheadercounter(r.link)/=x"ff")then
              v.counters.badheadercounter(r.link):=unsigned(r.counters.badheadercounter(r.link))+1;
            end if;
            if(unsigned(r.bcid(9 downto 0))+unsigned(r.bciddiff)+unsigned(r.nTrig)=unsigned(nextdatain(r.link)(9 downto 0))) then
              v.writeData:='1'; -- BCID correct => SEU
            end if;
            if(r.badheader(r.link)='1')then --2 bad ids in a row => desynch
              v.enabledMask(r.link):='0';
            end if;
            v.state:=startData;
          end if;
        else --not a header.
          v.state:=startData;
        end if;
      when startData =>
        v.forcereq(r.link):='1';
        v.state:=readData;
      when readData =>
        v.forcereq(r.link):='0';
        if(r.reqdataold(r.link)='1')then
          if(r.writeData='1')then
            v.txMaster.tData(31 downto 0):= datain(r.link)(31 downto 0);
            v.txMaster.tValid:='1';
            v.numRec:=r.numRec+1;
          end if;
          if(datain(r.link)(31 downto 28)="0000" and datain(r.link)(15 downto 10)="001111")then -- skipped
            v.skippedlast(r.link):=datain(r.link)(9 downto 0);
            v.nSkipped(r.link):=unsigned(r.nSkipped(r.link))+unsigned(datain(r.link)(9 downto 0))-unsigned(r.skippedLast(r.link));
          elsif(datain(r.link)(31 downto 30)="11")then --hit
            if(datain(r.link)(3 downto 0)=x"f")then -- just one hit
              v.occl(r.link):=unsigned(r.occl(r.link))+1;
            else
              v.occl(r.link):=unsigned(r.occl(r.link))+2;
            end if;
          end if;
          if(datain(r.link)(32)='1')then -- eof
            v.hmtimeout:=conv_integer(unsigned(missing_header_timeout_s));
            v.state:=missingHeaderTimeout;
          end if;
        end if;
      when missingHeaderTimeout =>
        if(nextdatavalid(r.link)='1')then
          if(nextdatain(r.link)(31 downto 28)="0011")then -- header
            v.state:=doneTimeout;
          else
            v.state:= startData; -- data without header
            v.dataNoHeader(r.link):='1';
            if(r.counters.datanoheadercounter(r.link)/=x"ff")then
              v.counters.datanoheadercounter(r.link):=unsigned(r.counters.datanoheadercounter(r.link))+1;
            end if;
          end if;
        elsif(r.hmtimeout=0)then
          v.state:=doneTimeout;
        else
          v.hmtimeout:=r.hmtimeout-1;
        end if;
      when doneTimeout =>
        if(r.comeback='1')then
          v.state:=feWait;
        else
          v.state:=nextFe;
        end if;
      when nextFe =>
        v.timeout:=conv_integer(unsigned(timeout_s));
        if(r.link=15)then
          if (r.nTrig=unsigned(nExp_s)-1)then
            v.nTrig:=(others =>'0');
            if(r.occevcount=nummon_s)then
              v.counters.occounter:=r.occl;
              v.occl:= (others => (others => '0'));
              v.occevcount:=(others => '0');
            end if;
            v.state:=writeTrailer;
          else
            v.link:=0;
            v.state:=feWait; 
            v.nTrig:=unsigned(r.nTrig)+1;
          end if;
        else
          v.link:=r.link+1;
          v.state:= feWait;
        end if;
      when writeTrailer =>
        if(slinkready='1')then
          v.txMaster.tValid:='1';
          v.trailercounter:=r.trailercounter+1;
          if(r.trailercounter=0)then
            v.txMaster.tData(31 downto 0):= r.timeoutstatus & r.toomanyheaders;
          elsif(r.trailercounter=1)then
            v.txMaster.tData(31 downto 0):= r.skippedtriggers & r.badheader;
          elsif(r.trailercounter=2)then
            v.txMaster.tData(31 downto 0):= r.missingtrigger & r.datanoheader;
          elsif(r.trailercounter=3)then
            v.txMaster.tData(31 downto 0):= (r.enabledMask xor channelmask_s) & r.desynched;
          elsif(r.trailercounter=4)then
            v.txMaster.tData(31 downto 0):= x"00000004";
          elsif(r.trailercounter=5)then
            v.txMaster.tData(31 downto 0):= toSlv(r.numRec, 32);
          elsif(r.trailercounter=6)then
            v.txMaster.tData(31 downto 0):= x"00000001";
            v.txMaster.tLast:= '1';
            v.state:=idle;
          end if;
        end if;
      end case;

      if (rst='1')then
        v:= REG_INIT_C;
      end if;
        
      rin <= v;

  end process comb;

   seq : process (clk) is
   begin
      if rising_edge(clk) then
         r <= rin;
      end if;
   end process seq;
  --ila: entity work.ila_0
  --  port map(
  --    clk => clk,
  --    probe0 => toSlv(r.index, 4),
  --    probe1=> (0=>r.txMaster.tValid),
  --    probe2=> (0=>r.txMaster.tLast),
  --    probe3=> (0=>r.parity),
  --    probe4=> (0=>r.txMaster.tUser(0)),
  --    probe5=> r.txMaster.tData(15 downto 0),
  --    probe6=> (0=>enabled),
  --    probe7=> (0=>indatavalid(r.index)),
  --    probe8=> datain(r.index)(15 downto 0),
  --    probe9=> (0=>datain(r.index)(16)),
  --    probe10=> (0=>datain(r.index)(17)));

end EVENTFRAGMENTBUILDER;
