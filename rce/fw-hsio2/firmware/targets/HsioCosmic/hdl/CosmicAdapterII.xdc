
#output clocks
set_property PACKAGE_PIN AA2 [get_ports clkA_n]
set_property PACKAGE_PIN AB1 [get_ports clkB_n]

set_property IOSTANDARD LVDS_25 [get_ports clkA_p]
set_property IOSTANDARD LVDS_25 [get_ports clkA_n]
set_property IOSTANDARD LVDS_25 [get_ports clkB_p]
set_property IOSTANDARD LVDS_25 [get_ports clkB_n]

# Downgrade UCIO-1 and NSTD-1 (FIXME)
set_property SEVERITY {WARNING} [get_drc_checks UCIO-1]
set_property SEVERITY {Warning} [get_drc_checks NSTD-1]

# Eudet TLU
set_property PACKAGE_PIN Y3  [get_ports {oExttrgclkP}]
set_property PACKAGE_PIN Y2  [get_ports {oExttrgclkN}]
set_property PACKAGE_PIN W1  [get_ports {oExtbusyP}]
set_property PACKAGE_PIN Y1  [get_ports {oExtbusyN}]
set_property PACKAGE_PIN R8  [get_ports {iExttriggerP}]
set_property PACKAGE_PIN R7  [get_ports {iExttriggerN}]
set_property PACKAGE_PIN T3  [get_ports {iExtrstP}]
set_property PACKAGE_PIN T2  [get_ports {iExtrstN}]

set_property IOSTANDARD LVDS_25 [get_ports oExttrgclkP]
set_property IOSTANDARD LVDS_25 [get_ports oExttrgclkN]
set_property IOSTANDARD LVDS_25  [get_ports {oExtbusyP}]
set_property IOSTANDARD LVDS_25  [get_ports {oExtbusyN}]
set_property IOSTANDARD LVDS_25 [get_ports iExttriggerP]
set_property IOSTANDARD LVDS_25 [get_ports iExttriggerN]
set_property IOSTANDARD LVDS_25 [get_ports iExtrstP]
set_property IOSTANDARD LVDS_25 [get_ports iExtrstN]

#Discriminators
set_property PACKAGE_PIN AD26 [get_ports {discinP[0]}]
set_property PACKAGE_PIN AE26 [get_ports {discinP[1]}]
set_property PACKAGE_PIN AC26 [get_ports {discinP[2]}]
set_property PACKAGE_PIN AC27 [get_ports {discinP[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {discinP[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {discinP[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {discinP[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {discinP[3]}]

# ADC I2C
set_property PACKAGE_PIN AH28 [get_ports {adcI2cSclA}]
set_property PACKAGE_PIN AH29 [get_ports {adcI2cSdaA}]
set_property PACKAGE_PIN AG30 [get_ports {adcAsA}]

set_property IOSTANDARD LVCMOS33 [get_ports {adcI2cSclA}]
set_property IOSTANDARD LVCMOS33 [get_ports {adcI2cSdaA}]
set_property IOSTANDARD LVCMOS33 [get_ports {adcAsA}]

set_property PULLUP TRUE [get_ports {adcI2cSclA}]
set_property PULLUP TRUE [get_ports {adcI2cSdaA}]

# ADC I2C B There is no ADC B on this board.
set_property PACKAGE_PIN AE27 [get_ports {adcI2cSclB}]
set_property PACKAGE_PIN AF27 [get_ports {adcI2cSdaB}]
set_property PACKAGE_PIN AH27 [get_ports {adcAsB}]

set_property IOSTANDARD LVCMOS33 [get_ports {adcI2cSclB}]
set_property IOSTANDARD LVCMOS33 [get_ports {adcI2cSdaB}]
set_property IOSTANDARD LVCMOS33 [get_ports {adcAsB}]

set_property PULLUP TRUE [get_ports {adcI2cSclB}]
set_property PULLUP TRUE [get_ports {adcI2cSdaB}]

#Output to Frontends
set_property PACKAGE_PIN AF8 [get_ports {serialout_n[0]}]
set_property PACKAGE_PIN AE7 [get_ports {serialout_n[1]}]
set_property PACKAGE_PIN AG7 [get_ports {serialout_n[2]}]
set_property PACKAGE_PIN AD8 [get_ports {serialout_n[3]}]
set_property PACKAGE_PIN AE6 [get_ports {serialout_n[4]}]
set_property PACKAGE_PIN AF5 [get_ports {serialout_n[5]}]
set_property PACKAGE_PIN AG5 [get_ports {serialout_n[6]}]
set_property PACKAGE_PIN AE3 [get_ports {serialout_n[7]}]
set_property PACKAGE_PIN W3 [get_ports {serialout_n[11]}]
set_property PACKAGE_PIN V1 [get_ports {serialout_n[12]}]
set_property PACKAGE_PIN V6 [get_ports {serialout_n[13]}]
set_property PACKAGE_PIN Y6 [get_ports {serialout_n[14]}]

set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[14]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[14]}]

#Input from Frontends
set_property PACKAGE_PIN AJ4 [get_ports {serialin_n[0]}]
set_property PACKAGE_PIN AD4 [get_ports {serialin_n[1]}]
set_property PACKAGE_PIN AJ3 [get_ports {serialin_n[2]}]
set_property PACKAGE_PIN AG4 [get_ports {serialin_n[3]}]
set_property PACKAGE_PIN AG2 [get_ports {serialin_n[4]}]
set_property PACKAGE_PIN AF2 [get_ports {serialin_n[5]}]
set_property PACKAGE_PIN AJ1 [get_ports {serialin_n[6]}]
set_property PACKAGE_PIN AE1 [get_ports {serialin_n[7]}]
set_property PACKAGE_PIN P10 [get_ports {serialin_n[11]}]
set_property PACKAGE_PIN U4 [get_ports {serialin_n[12]}]
set_property PACKAGE_PIN J3 [get_ports {serialin_n[13]}]
set_property PACKAGE_PIN G2 [get_ports {serialin_n[14]}]

set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[14]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[14]}]

