#!/bin/bash -xv 

#--> These aliases are used to manipulate environmental variables used to hold
#--> directory paths, e.g. PATH, MANPATH.  They allow one to append, prepend
#--> or remove specific directories from such variables.

if [ -n "${BASH_VERSION:-""}" ]; then

  addpath_clean()
  {
    delpath $* 
    eval "$1=\$$1:$2"
  }

  addpath2_clean()
  {
    delpath $* 
    eval "$1=$2:\$$1"
  }

  # add to end of path
  addpath()
  {
    if eval test -z \${$1}; then
      eval "$1=$2"
    elif ! eval test -z \"\${$1##$2}\" -o -z \"\${$1##*:$2:*}\" -o -z \"\${$1%%*:$2}\" -o -z \"\${$1##$2:*}\" ; then
      eval "$1=\$$1:$2"
    fi
  }

  # add to front of path
  addpath2()
  {
    if eval test -z \${$1}; then
      eval "$1=$2"
    elif ! eval test -z \"\${$1##$2}\" -o -z \"\${$1##*:$2:*}\" -o -z \"\${$1%%*:$2}\" -o -z \"\${$1##$2:*}\" ; then
      eval "$1=$2:\$$1"
    fi
  }

  # delete from path
  delpath()
  {
    eval "$1=\$(echo \$$1 | sed -e s%^$2\$%% -e s%^$2\:%% -e s%:$2\:%:%g -e s%:$2\\\$%%)"
  }

  modpath()
  {
    eval "$1=\$(echo \$$1 | sed -e s%^$2\$%$3% -e s%^$2\:%$3\:% -e s%:$2\:%:$3\:%g -e s%:$2\\\$%:$3%)"
  }
fi

if [ ! -d /home/$USER ] ; then 
  exit 0
fi



if [  -n $LD_LIBRARY_PATH ] ;  then
  export LD_LIBRARY_PATH=''
fi

export ORBHOST=''
export ORBHOST=`/sbin/ifconfig  | grep "inet addr:1[79]2" | head -1 | gawk -F: '{print $2}' | awk '{print $1}'`
if [ -n $ORBHOST  ] ; then
  export ORBendPoint="giop:tcp:${ORBHOST}:0"
else
  unset ORBHOST
fi

export TDAQ_IPC_INIT_REF="file:${HOME}/ipc_root.ref"
if [ `uname -m` = 'x86_64' ] ;  then
  export HOST_ARCH=x86_64-slc5-gcc43-opt
  export TDAQ_HOST_ARCH=x86_64-slc5-gcc43-opt
fi
if [ `uname -m` = 'i386' ] ; then
  export HOST_ARCH=i386-linux 
  export TDAQ_HOST_ARCH=i686-slc5-gcc43-opt
fi

#override for now - x86_64 build is broken
export HOST_ARCH=i686-slc5-gcc43-opt
export TDAQ_HOST_ARCH=i686-slc5-gcc43-opt

export RCE_ARCH=ppc-rtems-rce405-opt
export RCE=${HOME}/rce

#ambush setup
if [ -e /reg/g/atlas/ambush ] ; then
  export AMBUSH=/reg/g/atlas/ambush
  addpath2 LD_LIBRARY_PATH ${AMBUSH}/lib
  addpath2 PATH ${AMBUSH}/bin
fi

#setup python
if [ -e  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt ]; then
addpath2 LD_LIBRARY_PATH  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt/lib
addpath2 PATH  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt/bin
else
  echo Error: Python is not installed
  exit 1
fi

if [ -e /daq/slc5/opt/rtems-4.9.2 ]; then
  export RTEMS=/daq/slc5/opt/rtems-4.9.2
else
  echo Error: RTEMS is not installed
fi

#setup rtems cross compilers
RTEMS_GCC=''
if [ -e  /daq/slc5/opt/powerpc-rtems49-gcc432 ] ; then
  RTEMS_GCC=/daq/slc5/opt/powerpc-rtems49-gcc432
else 
  echo "rtems gcc not found"
  exit 1
fi
if [ -n $RTEMS_GCC ] ; then
addpath2 LD_LIBRARY_PATH $RTEMS_GCC/lib
addpath2 PATH $RTEMS_GCC/bin
fi

#setup ROOT
export ROOTSYS=""
if [ -e /daq/slc5/sw/lcg/app/releases/ROOT/5.26.00d_python2.6/${TDAQ_HOST_ARCH} ]; then
  export ROOTSYS=/daq/slc5/sw/lcg/app/releases/ROOT/5.26.00d_python2.6/${TDAQ_HOST_ARCH}/root
  # this needs the TDAQ gcc
  if [ `uname -m` = 'x86_64' ] ; then
    .  /daq/slc5/sw/lcg/contrib/gcc/4.3/x86_64-slc5-gcc34-opt/setup.sh
    addpath2 LD_LIBRARY_PATH /daq/slc5/sw/lcg/contrib/gcc/4.3/x86_64-slc5-gcc34-opt/lib
   else
     .  /daq/slc5/sw/lcg/contrib/gcc/4.3/slc4_ia32_gcc34/setup.sh
     addpath2  LD_LIBRARY_PATH /daq/slc5/sw/lcg/contrib/gcc/4.3/slc4_ia32_gcc34-opt/lib
   fi
else
  echo "TDAQ gcc not installed"
  exit 1
fi
if [ -n $ROOTSYS ] ; then
  addpath2 PATH $ROOTSYS/bin
  addpath2 LD_LIBRARY_PATH $ROOTSYS/lib
fi

#setup RCE client software 
addpath2 PATH  ${RCE}/build/rceis/bin/${HOST_ARCH}:${RCE}/build/rceipc/bin/${HOST_ARCH}:${RCE}/build/rcecalib/bin/${HOST_ARCH}:${RCE}/rcecalib/scripts
addpath2 LD_LIBRARY_PATH  ${RCE}/build/rcecalib/lib/${HOST_ARCH}:${RCE}/build/rceers/lib/${HOST_ARCH}:${RCE}/build/rceowl/lib/${HOST_ARCH}:${RCE}/build/rceipc/lib/${HOST_ARCH}:${RCE}/build/rceowl/lib/${HOST_ARCH}:${RCE}/build/rceoh/lib/${HOST_ARCH}:${RCE}/build/rceis/lib/${HOST_ARCH}

export RELEASE=${RCE}
export RCE_BIN=${RCE}/build/rcecalib/bin/${RCE_ARCH}
export RCE_MOD=${RCE}/build/rcecalib/mod/${RCE_ARCH}

export XMD_INI=${RCE}/rcecalib/xmd.ini
export TDAQ_IPC_INIT_REF=file:/${HOME}/ipc_root.ref
export SVNROOT=svn+ssh://svn.cern.ch/reps/RceCimDev
export TDAQ_PARTITION=rcetest_${USER}

alias rce_ipc_server='ipc_server -p $TDAQ_PARTITION'
alias rce_is_server='is_server -p  $TDAQ_PARTITION -n RceIsServer'
alias rce_ipc_ls='ipc_ls ; ipc_ls -p $TDAQ_PARTITION'
function rce_load () { echo -e "reboot\nsetenv TDAQ_PARTITION $TDAQ_PARTITION\nsetenv TDAQ_IS_COMPRESSION_THRESHOLD 100000000\n" | host_bootloader -r $1 -l $RCE_MOD/calibservermod.1.0.prod.so ; } 
alias rce_killall='pkill -u $USER ipc_server; pkill -u $USER is_server;'
#fix HOSTTYPE on bash
export HOSTTYPE=`uname -m`-linux


if [ ! -d ~/calibData ] ; then
mkdir ~/calibData 
fi
#tdaq setup
export TDAQ_INST_PATH=/daq/slc5/tdaq/tdaq-03-00-01/installed
export TDAQC_INST_PATH=/daq/slc5/tdaq-common/tdaq-common-01-16-02/installed
export TDAQC_EXT_PATH=/daq/slc5/tdaq-common/tdaq-common-01-16-02/external
export TDAQ_BOOST=/daq/slc5/sw/lcg/external/Boost/1.42.0_python2.6
addpath2 PATH             $TDAQ_INST_PATH/$HOST_ARCH/bin
addpath2 LD_LIBRARY_PATH  $TDAQ_INST_PATH/$HOST_ARCH/lib
addpath2 PATH             $TDAQC_INST_PATH/$HOST_ARCH/bin
addpath2 LD_LIBRARY_PATH  $TDAQC_INST_PATH/$HOST_ARCH/lib
addpath2 PATH             $TDAQC_EXT_PATH/$HOST_ARCH/bin
addpath2 LD_LIBRARY_PATH  $TDAQC_EXT_PATH/$HOST_ARCH/lib
addpath2 PATH             $TDAQ_INST_PATH/share/bin
addpath2 LD_LIBRARY_PATH  $TDAQ_INST_PATH/share/lib
export PYTHONPATH=$TDAQ_INST_PATH/$HOST_ARCH/lib

