# List of projects (low level first)
projects := rtems 
projects+=  root 
ifeq ($(RCE_CORE_VERSION),2.2)
projects+=tool
projects+=configuration
projects+=service
projects+=oldPpi
projects+=platform
else
projects+=  rce 
projects+=  rceusr 
projects+=  rceapp
endif
projects+=  rcecalib
projects+=  rcedcs
rtems_use := $(RTEMS)
ifeq ($(RCE_CORE_VERSION),2.2)
tool_use:=$(RCE_CORE)/build/tool
configuration_use:=$(RCE_CORE)/build/configuration
service_use:=$(RCE_CORE)/build/service
oldPpi_use:=$(RCE_CORE)/build/oldPpi
platform_use+=$(RCE_CORE)/build/platform
else
rce_use    := $(RCE_CORE)/build/rce
rceusr_use := $(RCE_CORE)/build/rceusr
rceapp_use := $(RCE_CORE)/build/rceapp
endif
rcecalib_use  := release
rcedcs_use  := release
root_use :=  $(ROOTSYS)



