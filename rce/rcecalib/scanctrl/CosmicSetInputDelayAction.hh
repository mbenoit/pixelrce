#ifndef COSMICSETINPUTDELAYACTION_HH
#define COSMICSETINPUTDELAYACTION_HH

#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/config/ConfigIF.hh"
#include <string>
#include <vector>

class CosmicSetInputDelayAction: public LoopAction{
public:
  CosmicSetInputDelayAction(std::string name, ConfigIF* cif, boost::property_tree::ptree *pt ):
    LoopAction(name),m_configIF(cif){
    try{
      int nPoints=pt->get<int>("nPoints");
      BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt->get_child("dataPoints")){
	ERS_DEBUG(2, v.second.data());
	int data = boost::lexical_cast<int>(v.second.data());
	dataPoints.push_back(data);
      }
      ERS_ASSERT_MSG((int)dataPoints.size()==nPoints,"of an inconsistency in the number of scan points.");
    }
    catch(boost::property_tree::ptree_bad_path ex){
      rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
      ers::error(issue);
    }
  }

  int execute(int i){
    ERS_DEBUG(2,"CosmicSetInputDelayAction "<<i);
    //std::cout<<"Set input delay "<<i<<std::endl;
    unsigned serstat;
    serstat=m_configIF->writeHWregister(7,0);//reset delays
    assert(serstat==0);
    int reg=5;
    if(dataPoints[i]<0)reg=6;
    for (int j=0;j<abs(dataPoints[i]);j++) {
      serstat=m_configIF->writeHWregister(reg,0); //increment delay
      assert(serstat==0);
    }
    return 0;
  }

private:
  ConfigIF* m_configIF;
  std::vector<int> dataPoints;
};

#endif
