--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.StdRtlPkg.all;
use work.AtlasTtcRxPkg.all;
use work.all;

--------------------------------------------------------------

entity ttcreadoutefb is
port(	clk: 	    in std_logic;
        clken:      in std_logic;
        ttc_in:     in AtlasTTCRxOutType;
        enabled:    in std_logic;
	d_out:	    out std_logic_vector(51 downto 0):=(others => '0');
        ld:         out std_logic:='0'
);
end ttcreadoutefb;

--------------------------------------------------------------

architecture TTCREADOUTEFB of ttcreadoutefb is

  begin

    process
    begin
        wait until rising_edge(clk);
        if(enabled='1' and clken='1' and ttc_in.trigL1='1') then -- L1A
          d_out(31 downto 24)<=ttc_in.eventRstCnt;
          d_out(23 downto 0)<=ttc_in.eventCnt;
          d_out(43 downto 32)<=ttc_in.bunchCnt;
          d_out(51 downto 44)<=ttc_in.bunchRstCnt;
          ld<='1';
        else
          ld<='0';
        end if;
    end process;		

end TTCREADOUTEFB;

--------------------------------------------------------------
