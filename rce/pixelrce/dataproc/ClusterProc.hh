#ifndef CLUSTERPROC_HH
#define CLUSTERPROC_HH

#include <stdio.h>
#include <vector>
#include "util/RceHisto1d.cc"
#include "util/RceHisto2d.cc" 
#include "util/RceMath.hh"

class ClusterProc {
public:
	ClusterProc();
	ClusterProc(RceHisto1d<int, int> *histo_hits, RceHisto1d<int, int> *histo_tot, RceHisto2d<int, int> *histo_tot_size);
	~ClusterProc();

	void assignHitHisto(RceHisto1d<int, int> *histo_hits);
        void assignToTHisto(RceHisto1d<int, int> *histo_tot); 
	void assignToTClusterSizeHisto(RceHisto2d<int, int> *histo_tot_size);

	void addHit(unsigned int col, unsigned int row, unsigned int tot, unsigned int bcid);

	void clusterize();
	void recursiveSeek(unsigned int this_cluster, unsigned int col, unsigned int row, unsigned int bcid);
	void iterativeSeek(unsigned int this_cluster);

	void reset();
	void fillHistos();
private:
	// Histograms
	RceHisto1d<int, int> *m_histo_hits;
	RceHisto1d<int, int> *m_histo_tot;
	RceHisto2d<int, int> *m_histo_tot_size;
	RceHisto1d<int, int> *m_histo_col_rms;
	RceHisto1d<int, int> *m_histo_row_rms;
	RceHisto2d<int, int> *m_histo_col_row_rms;

	// Cluster counter
	unsigned int cluster_id;

	// To prevent calling the function too often
	unsigned int max_hits_in_cluster;
	unsigned int max_hits;
	
        // Cluster cuts
	int bcidCut;
        int colCut;
        int rowCut;

	// Raw data
	std::vector<unsigned int> colVec;
	std::vector<unsigned int> rowVec;
	std::vector<unsigned int> totVec;
	std::vector<unsigned int> bcidVec;

	// Clustered data
	std::vector<unsigned int> tot_in_cluster;
	std::vector<unsigned int> hits_in_cluster;
	std::vector<bool> cluster_with_overflow;
};

#endif
