#ifndef MODULEFACTORY_HH
#define MODULEFACTORY_HH

#include "config/AbsModule.hh"
#include "config/AbsModuleGroup.hh"
#include "config/AbsTrigger.hh"

class ConfigIF;

class ModuleFactory{
public:
  ModuleFactory(){};
  virtual ~ModuleFactory(){};
  virtual AbsModule* createModule(const char* name, const char* type, unsigned id, unsigned inlink, unsigned outlink, const char* formatter)=0;
  virtual AbsTrigger* createTriggerIF(const char* type, ConfigIF* cif)=0;
  std::vector<AbsModuleGroup*> &getModuleGroups(){return m_modulegroups;}
protected:
  std::vector<AbsModuleGroup*> m_modulegroups;
};
  

#endif
