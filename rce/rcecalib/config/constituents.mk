
ifneq ($(findstring linux,$(tgt_os)),)
libnames := rceconfig 
endif
ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)
 modlibnames := rceconfig 
endif
ifeq ($(profiler),y)
CPPFLAGS+='-D__PROFILER_ENABLED__'
endif

libsrcs_rceconfig := AbsTrigger.cc \
                    AbsModuleGroup.cc \
                    ConfigIF.cc \
                    EventFromFileTrigger.cc \
	            TriggerReceiverIF.cc \
                    MultiShotTrigger.cc \
                    HitorTrigger.cc \
                    IPCModuleFactory.cc \
                    DummyFormatter.cc \
                    Trigger.cc \
                    MultiTrigger.cc \
                    FEI3/MaskStageFactory.cc \
                    FEI3/MaskStaging.cc \
                    FEI3/RegularMaskStaging.cc \
                    FEI3/CrosstalkMaskStaging.cc \
                    FEI3/FECommands.cc \
                    FEI3/Frontend.cc \
                    FEI3/GlobalRegister.cc \
                    FEI3/PixelRegister.cc \
                    FEI3/Mcc.cc \
                    FEI3/ModuleGroup.cc \
                    FEI3/Module.cc \
                    FEI3/JJFormatter.cc \
                    FEI4/FECommands.cc \
                    FEI4/PixelRegister.cc \
                    FEI4/GlobalRegister.cc \
                    FEI4/FEI4AGlobalRegister.cc \
                    FEI4/FEI4BGlobalRegister.cc \
                    FEI4/FEI4AFormatter.cc \
                    FEI4/FEI4BFormatter.cc \
                    FEI4/FEI4HitorFormatter.cc \
                    FEI4/FEI4OccFormatter.cc \
                    FEI4/MaskStageFactory.cc \
                    FEI4/MaskStaging.cc \
                    FEI4/AnalogMaskStaging.cc \
                    FEI4/FastAnalogMaskStaging.cc \
                    FEI4/AnalogSingleMaskStaging.cc \
                    FEI4/AnalogColprMaskStaging.cc \
                    FEI4/AnalogSimpleColprMaskStaging.cc \
                    FEI4/AnalogColpr2MaskStagingFei4a.cc \
                    FEI4/DigitalMaskStaging.cc \
                    FEI4/DigStepMaskStaging.cc \
                    FEI4/MaskStaging26880.cc \
                    FEI4/AnalogMaskStaging.cc \
                    FEI4/CrosstalkMaskStaging.cc \
                    FEI4/CrosstalkFastMaskStaging.cc \
                    FEI4/DiffusionMaskStaging.cc \
                    FEI4/NoiseMaskStaging.cc \
                    FEI4/ModuleCrosstalkMaskStaging.cc \
                    FEI4/PatternMaskStaging.cc \
                    FEI4/FEI4AModule.cc \
                    FEI4/FEI4BModule.cc \
                    FEI4/ModuleGroup.cc  \
                    FEI4/Module.cc  \
                    FEI4/TemperatureTrigger.cc  \
                    FEI4/MonleakTrigger.cc  \
                    FEI4/SNTrigger.cc  \
                    FEI4/Fei4RegisterTestTrigger.cc \
                    hitbus/ModuleGroup.cc  \
                    hitbus/Module.cc  \
                    afp-hptdc/AFPHPTDCFormatter.cc \
                    afp-hptdc/ModuleGroup.cc  \
                    afp-hptdc/Module.cc 

ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)
  libsrcs_rceconfig += MeasurementTrigger.cc
endif


libincs_rceconfig := rcecalib \
                  $(ers_include_path) \
                  $(owl_include_path) \
                  $(ipc_include_path) \
                  $(is_include_path) \
                  $(oh_include_path) \
                  $(boost_include_path) \
                  $(omniorb_include_path)


