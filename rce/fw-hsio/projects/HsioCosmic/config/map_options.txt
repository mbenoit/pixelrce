##-----------------------------------------------------------------------------
## Title         : Xilinx map options file
##-----------------------------------------------------------------------------

#-intstyle silent
-ol high
-xe n
-t 1
-register_duplication on
#-global_opt off
#-equivalent_register_removal on
#-u
#-cm Area
-detail
#-ir off
#-pr off
#-lc off
#-mt 2



