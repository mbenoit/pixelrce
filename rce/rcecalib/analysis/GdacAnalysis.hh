#ifndef GDACANALYSIS_HH
#define GDACANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"
#include <map>
#include <vector>

class ConfigGui;
class TFile;
class TH2D;
class TH1D;

namespace RCE{
  class PixScan;
}
namespace{
  struct fitval{
    double val;
    double err;
  };
}

class GdacAnalysis: public CalibAnalysis{
public:
  GdacAnalysis(): CalibAnalysis(){}
  ~GdacAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
  void findThreshold(TFile *anfile, int gdac, std::map<int, std::vector<fitval> > & ret, ConfigGui* cfg[]);
  unsigned short interpolate(int id, int target, TH1D* histo);
};


#endif
