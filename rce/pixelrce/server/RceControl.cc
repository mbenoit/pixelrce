#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include "server/BootLoaderPort.hh"
#include <boost/algorithm/string.hpp>
#include <termios.h>
#include "RceControl.hh"

#include <arpa/inet.h> // for htonl
namespace RCE {
  RceControl::RceControl(const char *rce,int timeout):m_rce(rce),m_timeout(timeout)  {    
    m_dst=RceNet::IpAddress(RceNet::getaddr(rce),BootloaderPort);}

  int RceControl::setEnvVar(const char *var,const char *val) {
    std::string inpline;
    inpline="setenv "+std::string(var)+std::string(" ")+std::string(val);
    sendCommand(inpline);
    return 0;
  }
  


  int RceControl::setIorFromFile(const char *iorfile) {
    std::string inpline;
    //std::istream *inp;
    if(!iorfile) iorfile=getenv("TDAQ_IPC_INIT_REF"); 
    if(strstr(iorfile,"file:/") )iorfile+=6;
    if(iorfile){ 
      std::ifstream g(iorfile);
      if (!g.good()) {
	std::cout<<"Error: Could not find file "<<iorfile<<std::endl;
	return -1;
      }
      inpline="";
      getline (g,inpline);
      g.close();
      if(inpline.substr(0,4)!="IOR:"){
	std::cout<<"Error: "<<iorfile<<" does not contain an IOR string"<<std::endl;
      return -1;
      }
      inpline="setenv TDAQ_IPC_INIT_REF "+inpline;
      sendCommand(inpline);
    }
    return 0;
  }

int RceControl::loadModule(const char *filename) {
  if(!filename) return -1;
    // Read module into buffer
    std::ifstream f(filename);
    if (!f.good()) {
      std::cout<<"Error: Could not find file "<<filename<<std::endl;
      return -1;
    }
    f.seekg(0, std::ios::end);
    unsigned length = f.tellg();
    f.seekg(0, std::ios::beg);
    char *buffer=new char[length];
    f.read(buffer,length);
    char msg[128];
    // send download command
    sprintf(msg,"download %d bytes",length);
    std::string rep=sendCommand(msg);
    if(rep!="OK") return -1; 
    std::cout<<"HOST"<<" => "<<m_rce<<": Uploading module."<<std::endl;
    // download module
    RceNet::SocketTcp socket2;
    socket2.connect(m_dst);
    socket2.send(buffer, length);
    delete [] buffer;
    // receive confirmation
    socket2.setrcvtmo(m_timeout);
    int bytes=socket2.recv(msg,128);
    socket2.close();
    if(bytes<0){
      std::cout<<"Receive error."<<std::endl;
    }
    msg[bytes]=0;
    std::cout<<"HOST"<<" <= "<<m_rce<<": "<<msg<<std::endl;
    return 0;
}

const char* RceControl::sendCommand(std::string inpline){
  std::cout<<"HOST"<<" => "<<m_rce<<": "<<inpline<<std::endl;
  RceNet::SocketTcp *socket=0;
  int nretries=10;
  while(nretries>0){
    try{
      socket=new RceNet::SocketTcp;
      socket->connect(m_dst);
      socket->send(inpline.c_str(), inpline.size());
      break;
    } catch (...){
      //std::cout<<"Connect failed. Trying again"<<std::endl;
      nretries--;
      delete socket;
      sleep(1);
    }
  }
  if(nretries==0){
    std::cout<<"Network error. Exiting."<<std::endl;
    exit(0);
  }
  static char line[128];
  try{
    socket->setrcvtmo(m_timeout);
    int bytes=socket->recv(line,128);
    line[bytes]=0;
    std::cout<<"HOST"<<" <= "<<m_rce<<": "<<line<<std::endl;
    //if(std::string(line)=="Rebooting...")sleep(1); // wait for reboot to finish
  }
  catch (...){
    std::cout<<"Network error. Exiting."<<std::endl;
    delete socket;
    return 0;
  }
  socket->close();
  delete socket;
  return line;
}

}
