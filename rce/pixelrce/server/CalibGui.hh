#ifndef CALIBGUI_HH
#define CALIBGUI_HH

#include "TGMdiMainFrame.h"
#include <TTimer.h>
#include <TTimeStamp.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include "TFile.h"
#include <string>
#include "ConfigGui.hh"
#include "GlobalConfigGui.hh"

namespace RCE {
class PixScan; }
namespace ipc{
  class ScanOptions;
}
class PrimListGui;
class ScanGui;
class AbsController;
class AbsHistoController;
class TGListTree;
class TGListTreeItem;
class TH1;
class TRootEmbeddedCanvas;
class DataExporter;
class ScanLog;
class CallbackInfo;

class CalibGui: public TGMainFrame {
public:
  CalibGui(AbsController *c, AbsHistoController *hc,const TGWindow *p,UInt_t w,UInt_t h);
  virtual ~CalibGui();
  int startRun();
  int setupScan();
  void enableControls(bool on);
  void handleFileMenu(Int_t);
  void handlePlotMenu(Int_t);
  void timeouts();
  void toggle();
  void quit();
  void stopRun();
  void analyze();
  void saveHistos();
  int openFile();
  void clearTree();
  void loopAction(int i, int j);
  void setupTdacTuning(int j);
  void setupTdacFastTuning(int j);
  void setupGdacTuningOuterLoop();
  void setupGdacTuning(int j);
  void setupGdacFastTuning(int j);
  void setupGdacCoarseFastTuning(int j);
  void setupOffsetScan(int j);
  void setupMask(int loop_i, int j);
  void updateTree();
  void runloop();
  void runScan(int pr);
  void verifyConfiguration();
  void displayHisto(TGListTreeItem* item, int b);
  TGLabel* getEventLabel(){return m_nEvents;}
  void synchSave1();
  void synchSave2();
  std::string getScanName();
  bool saveChecked(){return m_save->IsDisabledAndSelected();}
  bool exportChecked(){return m_export->IsDisabledAndSelected();}
  bool debugChecked(){return m_debug->IsDisabledAndSelected();}
  ConfigGui* getConfig(int i){return m_config[i];}
  TGTextEntry *getComment(){return m_comment;}
  enum Status{OK, FAILED, ABORTED};
  Status getStatus(){return m_status;}
  bool isRunning(){return m_isrunning;}
  void allOffA(){switchOn(0,0);}
  void allOffC(){switchOn(1,0);}
  void allOffA2(){switchOn(2,0);}
  void allOffC2(){switchOn(3,0);}
  void allOnA(){switchOn(0,1);}
  void allOnC(){switchOn(1,1);}
  void allOnA2(){switchOn(2,1);}
  void allOnC2(){switchOn(3,1);}
  void switchOn(int panel, bool on);
  void getFirm();
  void finishLogFile();
  void logScan();
  void writeGuiConfig(const char* filename);
  void readGuiConfig(const char* filename);
  void printFromGui();

  static void *runloop( void *ptr );

private:
  RCE::PixScan* currentScan();
  ipc::ScanOptions* currentScanConfig();
  void EndOfPrimListScan();
  void setRun(Bool_t on);
  bool running(){return m_isrunning;}
  void fillHistoTree(TGListTreeItem* branch);
  enum Filemenu {LOAD, SAVE, QUIT};
  enum Plotmenu {GIF, PDF};
  PrimListGui * m_primList;
  AbsController& m_controller;
  AbsHistoController& m_hcontroller;
  ConfigGui* m_config[ConfigGui::MAX_MODULES];
  ScanGui * m_scan;
  TGTextButton *m_start, *m_quit, *m_print;
  TGCheckButton *m_save, *m_export, *m_debug;
  TGLabel *m_nEvents, *m_time, *m_rate, *m_irate;
  TGLabel *m_nLoop1, *m_nLoop2;
  TGTextEntry *m_comment;
  TGHorizontalFrame *m_datapaneldd;
  TTimer *m_timers;
  TTimeStamp m_starttime;
  unsigned m_nevt;
  unsigned m_nevtMin;
  double m_oneminago;
  bool m_isrunning;
  Status m_status;
  std::string m_oldRunName;
  TGListTree *m_tree;
  TH1 *m_histo;
  TRootEmbeddedCanvas *m_canvas;
  TFile *m_file, *m_anfile;
  std::string m_exportdir;
  std::string m_rcdir;
  std::string m_dir;
  CallbackInfo *m_cbinfo;
  TString m_homedir;
  TString m_logfile;
  bool m_delhisto;
  TGCanvas *m_tgc;
  GlobalConfigGui* m_globalconf;
  DataExporter *m_dataExporter;
  ScanLog *m_scanLog;
  const TGPicture *m_thp;
 
  
ClassDef (CalibGui,0)
};
#endif
