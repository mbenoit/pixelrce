#ifndef __RCE_CONTROL_HH__
#define __RCE_CONTROL_HH__
#ifdef RCE_V2
#include "datCode.hh"
#include DAT_PUBLIC( oldPpi, net,       IpAddress.hh)
#include DAT_PUBLIC( oldPpi, net,       Error.hh)
#include DAT_PUBLIC( oldPpi, net,       Getaddr.hh)
#include DAT_PUBLIC( oldPpi, net,       SocketTcp.hh)

#else
#include "rce/net/IpAddress.hh"
#include "rce/net/Getaddr.hh"
#include "rce/net/IpAddress.hh"
#include "rce/net/SocketTcp.hh"
#include "rce/net/Error.hh"
#endif
#include "namespace_aliases.hh"
namespace RCE {
  class RceControl {
  public:
    RceControl(){};
    RceControl(const char *rce,int timeout=10000);
    int setIorFromFile(const char* iorfile=NULL);
    int setEnvVar(const char* var,const char *val);
    int loadModule(const char *name);    
    int runScript(const char *name);
    const char* sendCommand(std::string inpline);
  private:
    std::string m_rce;
    int m_timeout;
    RceNet::IpAddress m_dst;
  };
}
#endif
