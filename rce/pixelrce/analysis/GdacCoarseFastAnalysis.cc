#include "analysis/GdacCoarseFastAnalysis.hh"
#include "server/PixScan.hh"
#include "server/ConfigGui.hh"
#include "config/FEI3/Module.hh"
#include "config/FEI4/Module.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>
#include "TH1D.h"
#include "TStyle.h"

void GdacCoarseFastAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  int repetitions = scan->getRepetitions();
  double targetEff = 0.5;
  TKey* key;
  char subdir[128];
  char name[128];
  char title[128];
  int binsx=0;
  size_t numvals=scan->getLoopVarValues(1).size();
  std::map<int, hdatagdaccoarsefast> *histomap=new std::map<int, hdatagdaccoarsefast>[numvals];
  double *val=new double[numvals];
  for (size_t i=0;i<numvals;i++){
    sprintf(subdir, "/loop2_0/loop1_%d", i);
    file->cd(subdir); // GDAC steps
    TIter nextkey(gDirectory->GetListOfKeys()); // histos
    while ((key=(TKey*)nextkey())) {
      boost::cmatch matches;
      boost::regex re("_(\\d+)_Occupancy_Point_000");
      if(boost::regex_search(key->GetName(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	int id=strtol(match.c_str(),0,10);
	TH2* histo = (TH2*)key->ReadObj();
	histo->SetMinimum(0); histo->SetMaximum(repetitions);
	TH1* gdachisto=(TH1*)gDirectory->Get(Form("GDACCoarse_settings_Mod_%d_it_%d", id, i));
	assert(gdachisto);
	histomap[i][id].occupancy=histo;
	histomap[i][id].gdac=gdachisto;
	binsx=gdachisto->GetNbinsX();
      }
    }
  }
  for(std::map<int, hdatagdaccoarsefast>::iterator it=histomap[0].begin();it!=histomap[0].end();it++){
    int id=it->first;
    float pct = 100;
    sprintf(name, "BestOcc_Mod_%d", id);
    sprintf(title, "Best Occupancy Module %d at %s", id, findFieldName(cfg, id));
    TH1F* occBest=new TH1F(name, title, binsx, 0, binsx);
    occBest->SetMinimum(0); occBest->SetMaximum(pct);
    occBest->GetXaxis()->SetTitle("Chip");
    occBest->GetYaxis()->SetTitle("Occupancy[%]");
   
    sprintf(name, "BestGdacCoarse_Mod_%d", id);
    sprintf(title, "Best GdacCoarse Module %d at %s", id, findFieldName(cfg, id));
    TH1F* gdac=new TH1F(name, title, binsx, 0, binsx);
    gdac->GetXaxis()->SetTitle("Chip");
    gdac->GetYaxis()->SetTitle("GdacCoarse");

    PixelConfig* confb=findConfig(cfg, id);

    int ncol, nrow, nchip, maxval, maxstage;
    ncol=80;
    nrow=336;
    nchip=1;
    maxval=256;
    if (confb->getType()=="FEI4A") {
      maxstage=120;
    } else {
      maxstage=24;
    }

    int nstages = scan->getMaskStageSteps();
    int npixtotal = nchip*ncol*nrow;
    float npixused=float(nstages)/float(maxstage)*npixtotal;

    int nfe = binsx;
    for (int i = 0; i < nfe; i++){
      int index = 0;
      for (size_t k = 0; k < numvals; k++) {
	int histfebins = (histomap[k][id].occupancy->GetNbinsX())/nfe;
	int nbinsy = histomap[k][id].occupancy->GetNbinsY();
	float meanEff = 0.0;
	for (int binx = histfebins*i+1; binx <= histfebins*(i+1); binx++) {
	  for (int biny = 1; biny <= nbinsy; biny++) {
	    meanEff += histomap[k][id].occupancy->GetBinContent(binx, biny) / repetitions;
	  }
	}
	val[k] = meanEff/npixused;
	if (val[k] < targetEff) {
	  index = k;
	  break;
	}
      }
      occBest->SetBinContent(i+1, pct*val[index]);
      gdac->SetBinContent(i+1, histomap[index][id].gdac->GetBinContent(i+1));
      std::cout<<"Best GDACCoarse for module " << id << " is " << histomap[index][id].gdac->GetBinContent(i+1) << std::endl;
      if(confb->getType()!="FEI3")confb->setGDacCoarse(0, histomap[index][id].gdac->GetBinContent(i+1));
    }

    anfile->cd();

    occBest->Write();
    occBest->SetDirectory(gDirectory);
   
    gdac->Write();
    gdac->SetDirectory(gDirectory);

    if(confb->getType()!="FEI3") writeConfig(anfile, runno);
  }
  if (configUpdate()) writeTopFile(cfg, anfile, runno);
  delete [] histomap;
  delete [] val;
}
