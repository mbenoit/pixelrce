
#include "config/Trigger.hh"
#include "config/FEI3/FECommands.hh"
#include <boost/property_tree/ptree.hpp>
#include "HW/SerialIF.hh"
#include <iostream>
#ifdef __rtems__
#include "HW/Headers.hh"
#include "HW/RCDImaster.hh"
#include "rce/pgp/Driver.hh"
#include "rce/pgp/DriverList.hh"
using namespace PgpTrans;
#endif

namespace FEI3{

  Trigger::Trigger():AbsTrigger(),m_calL1ADelay(-1){
    m_parameters.push_back("TRIGGER_DELAY");
#ifdef __rtems__
    const RcePic::Params Tx = {
      RcePic::NonContiguous,
      16,     // Header
      64*132, // Payload
      1     // Number of buffers
    };

  m_pool = new RcePic::Pool::Pool(Tx);    
  m_tds=m_pool->allocate();
  assert (m_tds!=0); 
  char* header=(char*)m_tds->header();
  int headersize=m_tds->headersize();
  for (int i=0;i<headersize;i++)header[i]=0;
  new(m_tds->header())BlockWriteTxHeader(0);
#endif
  }
  Trigger::~Trigger(){
#ifdef __rtems__
    m_pool->deallocate(m_tds);
    delete m_pool;
#endif
  }
  int Trigger::configureScan(boost::property_tree::ptree* scanOptions){
    int retval=0;
    m_i=0; //reset the number of triggers
    try{
      int calL1ADelay = scanOptions->get<int>("trigOpt.CalL1ADelay");
      if(calL1ADelay==-1 || calL1ADelay!=m_calL1ADelay){ // have to redo trigger stream
	m_calL1ADelay=calL1ADelay;
	setupTriggerStream();
      }
    }
     catch(boost::property_tree::ptree_bad_path ex){
       std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
       retval=1;
     }
    return retval;
  }
  int Trigger::setupParameter(const char* name, int val){
    if(std::string(name)=="TRIGGER_DELAY"){
      if(val!=m_calL1ADelay){ // have to redo trigger stream
	//	std::cout<<"Setting up trigger stream. Delay is "<<val<<std::endl;
	m_calL1ADelay=val;
	setupTriggerStream();
      }
    }
    return 0;
  }

  void Trigger::setupTriggerStream(){
#ifdef __rtems__
    unsigned* payload=(unsigned*)m_tds->payload();
#endif
    m_triggerStream.clear();
    //prepend 4 zeroes a la NewDsp
    BitStreamUtils::prependZeros(&m_triggerStream);
    std::vector<char> bytestream;
    /* calculate the size required for this stream */
    /*trigger delay is between end of CAL and L1A command */
    /* CAL is a slow command with 9 bits 101100100, L1A is 5 bits 11101*/
    /* form the trigger stream */	
    bytestream.push_back(0x01);
    bytestream.push_back(0x64); /* 101100100 (9bits) = CAL */
    /* at this point, still have to provide (delay - 5) zero bits */
    if(m_calL1ADelay!=0xffff){
      int nBits = m_calL1ADelay - 5;
      int nBytes = nBits >> 3;
      for(int k=0;k<nBytes;++k) bytestream.push_back(0);
      unsigned short t = (0xE800 >> (nBits & 0x7));
      bytestream.push_back((t >> 8) & 0xff);
      bytestream.push_back(t & 0xff);
    }else{
      std::cout<<"CAL strobe only."<<std::endl;
    }
    while(bytestream.size()%4!=0)bytestream.push_back(0);
    //for (size_t i=0;i<bytestream.size();i++)std::cout<<std::hex<<(unsigned)bytestream[i]<<std::dec<<std::endl;
    //std::cout<<"Size is "<<bytestream.size()<<std::endl;
    for(unsigned int i=0;i<bytestream.size()/4;i++){
      unsigned word=0;
      for (int j=0;j<4;j++){
	word<<=8;
	word|=bytestream[i*4+j];
      }
      m_triggerStream.push_back(word);
    }
    m_triggerStream.push_back(0);
    std::cout<<"Size of trigger stream is "<<m_triggerStream.size()*4<<" bytes"<<std::endl;
    SerialIF::writeRegister(19,0);
    //for (size_t i=0;i<m_triggerStream.size();i++)SerialIF::writeRegister(18,m_triggerStream[i]);
#ifdef __rtems__
    for (size_t i=0;i<m_triggerStream.size();i++)payload[i]=m_triggerStream[i];
    m_tds->payloadsize(m_triggerStream.size()*sizeof(unsigned));
    m_tds->flush(true);
#endif
  }
  
  int Trigger::sendTrigger(){
    //std::cout<<"Trigger "<<m_i<<std::endl;
#ifdef __rtems__
    RCDImaster::instance()->blockWrite(m_tds);
    //SerialIF::sendCommand(0xa);
#else
    SerialIF::send(&m_triggerStream,SerialIF::DONT_CLEAR|SerialIF::WAITFORDATA);
#endif
    m_i++;
    return 0;
  }

  int Trigger::enableTrigger(bool on){
    return SerialIF::enableTrigger(on);
    return 0;
  }
  int Trigger::resetCounters(){
    BitStream *bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    FECommands::sendECR(bs);
    FECommands::sendBCR(bs);
    SerialIF::send(bs,SerialIF::WAITFORDATA);
    delete bs;
    return 0;
  }
};
