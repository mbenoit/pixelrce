#include "config/FWRegisters.hh"

void FWRegisters::setChannelmask(int rce, unsigned mask){
  writeRegister(rce, CHANNELMASK, mask);
}
void FWRegisters::setMode(int rce, opmode md){
  writeRegister(rce, MODE, md);
}
void FWRegisters::setChannelOutmask(int rce, unsigned mask){
  writeRegister(rce, CHANNELOUTMASK, mask);
}
void FWRegisters::incrementDiscDelay(int rce, int channel){
  writeRegister(rce, INCDISCDELAY, 1<<channel);
}
void FWRegisters::setDiscOpMode(int rce, unsigned val){
  writeRegister(rce, DISCOP, val);
}
void FWRegisters::resetDelays(int rce){
  writeRegister(rce, RESETDELAYS, 1);
}
void FWRegisters::setTriggermask(int rce, unsigned mask){
  writeRegister(rce, TRIGGERMASK, mask);
}
void FWRegisters::setDeadtime(int rce, unsigned deadtime){
  writeRegister(rce, DEADTIME, deadtime);
}
void FWRegisters::setEncoding(int rce, streamencoding enc){
  writeRegister(rce, ENCODING, enc);
}
void FWRegisters::setHitbusOp(int rce, unsigned val){
  writeRegister(rce, HITBUSOP, val);
}
void FWRegisters::setL1Type(int rce, routing val){
  writeRegister(rce, L1ROUTE, val);
}
void FWRegisters::setHitbusDataDelay(int rce, unsigned delay){
  writeRegister(rce, HBDELAY, delay);
}
void FWRegisters::setHitbusDataNegativeDelay(int rce, unsigned delay){
  writeRegister(rce, HBDELAYNEG, delay);
}
void FWRegisters::setTelescopeOp(int rce, unsigned op){
  writeRegister(rce, TELESCOPEOP, op);
}
void FWRegisters::setTemperatureReadoutFrequency(int rce, unsigned ticks){
  writeRegister(rce, TEMPFREQ, ticks);
}
void FWRegisters::setTemperatureReadoutEnable(int rce, bool on){
  writeRegister(rce, TEMPENABLE, on);
}
void FWRegisters::setNumberofFeFramesPgp(int rce, unsigned num){
  writeRegister(rce, MAXBUFLENGTH, num);
}
void FWRegisters::setOutputDelay(int rce, unsigned inlink, unsigned val){
  writeRegister(rce, OUTPUTDELAYS+inlink, val);
}
void FWRegisters::setRcePresent(int rce){
  sendCommand(rce, CMD_PRESENT);
} 
void FWRegisters::setHitDiscConfig(int rce, int chan, unsigned val){
  writeRegister(rce, HITDISCCONFIG+chan, val);
}
void FWRegisters::setNExp(int rce, unsigned val){
  unsigned rval= val==0? 16 : val;
  writeRegister(rce, NEXP, rval);
}
void FWRegisters::setEfbTimeout(int rce, unsigned val){
  writeRegister(rce, EFBTIMEOUT, val);
}
void FWRegisters::setEfbTimeoutFirst(int rce, unsigned val){
  writeRegister(rce, EFBTIMEOUTFIRST, val);
}
void FWRegisters::setEfbMissingHeaderTimeout(int rce, unsigned val){
  writeRegister(rce, EFBMISSINGHEADERTIMEOUT, val);
}
void FWRegisters::setRunNumber(int rce, unsigned val){
  writeRegister(rce, RUNNUMBER, val);
}
void FWRegisters::setOccNormalization(int rce, unsigned val){
  writeRegister(rce, NUMMON, val);
}
void FWRegisters::enableMonitoring(int rce, unsigned val){
  writeRegister(rce, MONENABLED, val);
}
void FWRegisters::enableSLinkBlowoff(int rce, bool on){
  writeRegister(rce, BLOWOFF, on);
}
unsigned FWRegisters::getNumberOfEvents(int rce){
  return readRegister(rce, NEVT);
}
unsigned FWRegisters::getNumberOfMonMissed(int rce){
  return readRegister(rce, NEVTNOMON);
}
unsigned FWRegisters::getEfbCounter(int rce, int chan, EFBCOUNTER cnt){
  return readRegister(rce, EFBCOUNTERS+8*chan+(int)cnt);
}
