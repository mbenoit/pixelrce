#ifndef RCFCONFIGIF_CC
#define RCFCONFIGIF_CC

#include <stdint.h>
#include <vector>
#include <RCF/RCF.hpp>
#include <SF/string.hpp>
#include <SF/vector.hpp>

struct ModSetup{
  int32_t id;
  int32_t inLink;
  int32_t outLink;
  void serialize(SF::Archive &ar){
    ar & id & inLink & outLink;
  }
};
  
RCF_BEGIN(I_RCFConfigIFAdapter, "I_RCFConfigIFAdapter")
RCF_METHOD_V0(void, RCFsendTrigger)
RCF_METHOD_R2(uint32_t, RCFsetupParameter, std::string, int32_t)
RCF_METHOD_V1(void, RCFsetupMaskStage, int32_t)
RCF_METHOD_V0(void, RCFenableTrigger)
RCF_METHOD_V0(void, RCFdisableTrigger)
RCF_METHOD_V0(void, RCFresetFE)
RCF_METHOD_V0(void, RCFconfigureModulesHW)
RCF_METHOD_R2(uint32_t, RCFwriteHWregister, uint32_t, uint32_t)
RCF_METHOD_R2(uint32_t, RCFreadHWregister, uint32_t, uint32_t&)
RCF_METHOD_R1(uint32_t, RCFsendHWcommand, uint8_t)
RCF_METHOD_R1(uint32_t, RCFwriteHWblockData, std::vector<uint32_t>)
RCF_METHOD_R2(uint32_t, RCFreadHWblockData, std::vector<uint32_t>, std::vector<uint32_t>&)
RCF_METHOD_R1(uint32_t, RCFreadHWbuffers, std::vector<uint8_t>&)
RCF_METHOD_R1(int32_t, RCFverifyModuleConfigHW, int32_t)
RCF_METHOD_R0(int32_t, RCFdeleteModules)
RCF_METHOD_R0(int32_t, RCFnTrigger)
RCF_METHOD_R1(int32_t, RCFsetupTriggerIF, std::string)
RCF_METHOD_R4(int32_t, RCFsetupModule, std::string, std::string, ModSetup, std::string)
RCF_END(I_RCFConfigIFAdapter)

  class RCFConfigIFAdapter 
  {
    virtual void RCFsendTrigger()=0;
    virtual uint32_t RCFsetupParameter(std::string name, int32_t val)=0;
    virtual void RCFsetupMaskStage(int32_t stage)=0;
    virtual void RCFenableTrigger()=0;
    virtual void RCFdisableTrigger()=0;
    virtual void RCFresetFE()=0;
    virtual void RCFconfigureModulesHW()=0;
    virtual uint32_t RCFwriteHWregister(uint32_t addr, uint32_t val)=0;
    virtual uint32_t RCFreadHWregister(uint32_t addr, uint32_t& val)=0;
    virtual uint32_t RCFsendHWcommand(uint8_t opcode)=0;
    virtual uint32_t RCFwriteHWblockData(std::vector<uint32_t> data)=0;
    virtual uint32_t RCFreadHWblockData(std::vector<uint32_t> data,  std::vector<uint32_t> &retv)=0;
    virtual uint32_t RCFreadHWbuffers(std::vector<uint8_t> &retv)=0;
    virtual int32_t RCFverifyModuleConfigHW(int32_t id)=0;
    virtual int32_t RCFdeleteModules()=0;
    virtual int32_t RCFnTrigger()=0;
    virtual int32_t RCFsetupTriggerIF(std::string type)=0;
    virtual int32_t RCFsetupModule(std::string name, std::string type, ModSetup par, std::string formatter)=0;
    
  };

#endif
