#ifndef FEI3__FRONTEND_HH
#define FEI3__FRONTEND_HH

#include "rcecalib/HW/BitStream.hh"
#include "rcecalib/config/FEI3/GlobalRegister.hh"
#include "rcecalib/config/FEI3/PixelRegister.hh"

namespace FEI3{

  class Frontend{
  public:
    Frontend(unsigned chip);
    ~Frontend();
    enum{N_COLS=18, N_ROWS=160 ,N_PIXEL_REGISTER_BITS=14};
    void setChipAddress(unsigned i, unsigned addr);
    void setVcalCoeff(unsigned i, float val);
    void setCapVal(unsigned i, float val);
    float getVcalCoeff(unsigned i);
    float getCapVal(unsigned i);
    void writePixelRegisterHW(unsigned bit);
    void writeGlobalRegisterHW();
    void setGlobalRegField(const char *name, int val);
    void setPixelParameter(PixelRegister::Field field, int val);
    void configureHW();
    void shiftEnablesHW(unsigned short bitmask);
    PixelRegister* getPixelRegister(unsigned col, unsigned row);
    GlobalRegister* getGlobalRegister(){return m_global;}
    const float dacToElectrons(int dac);
    const int electronsToDac(float ne);
    
  private:
    void pixelUsrToRaw(BitStream *bs, unsigned bit);
    unsigned m_chip;
    GlobalRegister *m_global;
    PixelRegister m_pixel[N_COLS][N_ROWS];
    float m_vcalCoeff[4];
    float m_capVal[2];
  };

};

#endif
