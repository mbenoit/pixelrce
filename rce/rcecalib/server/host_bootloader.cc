#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include "rcecalib/server/BootLoaderPort.hh"
#include <boost/algorithm/string.hpp>
#include "RceControl.hh"
#include <termios.h>

#include <arpa/inet.h> // for htonl


int main(int argc, char** argv)
{
  extern char* optarg;
  int c;
  const char* scriptname=0;
  const char* filename=0;

  const char* rce=0;
  const char *iorfile=0;
  while ( (c=getopt( argc, argv, "r:s:l:i:")) != EOF ) {
    switch(c) {
    case 'r': 
      rce=optarg;
      break;
    case 's':
      scriptname = optarg;
      break;
    case 'l':
      filename = optarg;
      break;
    case 'i':
      iorfile = optarg;
      break;
    }
  }
  RCE::RceControl rcecontrol;
  try {
    rcecontrol =RCE::RceControl(rce);
  } catch(...) {
    std::cout<<"Usage: host_bootloader -r IP_address [-s config_file] [-l sharedlib.so] [-i ior_file]"<<std::endl;
    return 0;
  }
  std::string inpline;
  std::istream *inp;
  if(scriptname){
    std::ifstream* inpf= new std::ifstream (scriptname);
    if (!inpf->is_open()){
      std::cout<<"File "<<scriptname<<" not found. Exiting."<<std::endl;
      exit(1);
    }
    inp=(std::istream*)inpf;
  }else{
    inp = &std::cin;
    struct termios tios;
    // Make sure backspace works in the terminal window.
    tcgetattr(STDIN_FILENO, &tios);
    if(tios.c_cc[VERASE]!=0x7f){
      tios.c_cc[VERASE]=0x7f; // set backspace as erase
      tcsetattr(STDIN_FILENO,TCSANOW, &tios); 
    }
  }
  while (true){ //main loop
    getline (*inp,inpline);
    if(inp->eof())break;
    boost::trim(inpline);
    if(inpline.size()!=0 && inpline[0]!='#'){
      rcecontrol.sendCommand(inpline);
    }
  }
  // option -i: read iorfile and set TDAQ_IPC_INIT_REF 
  rcecontrol.setIorFromFile(iorfile);

    
  //option -l: download shared library.
  if(filename){ 
    if(rcecontrol.loadModule(filename)) exit(-1);
  }
}

