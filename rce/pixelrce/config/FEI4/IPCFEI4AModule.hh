#ifndef IPCFEI4AMODULE_HH
#define IPCFEI4AMODULE_HH

#include "config/FEI4/FEI4AModule.hh"
#include "ipc/object.h"
#include "IPCFEI4AAdapter.hh"

class IPCPartition;
class AbsFormatter;

namespace FEI4{
template <class TP = ipc::single_thread>
class IPCFEI4AModule: public IPCNamedObject<POA_ipc::IPCFEI4AAdapter,TP>, public FEI4::FEI4AModule {
public:
  IPCFEI4AModule(IPCPartition & p, const char * name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt);
  ~IPCFEI4AModule();
  CORBA::Long IPCdownloadConfig(const ipc::PixelFEI4AConfig &config);    
  void IPCsetChipAddress(CORBA::ULong addr);
  CORBA::ULong IPCwriteHWglobalRegister(CORBA::Long reg, CORBA::UShort val);
  CORBA::ULong IPCreadHWglobalRegister(CORBA::Long reg, CORBA::UShort &val);
  CORBA::ULong IPCwriteDoubleColumnHW(CORBA::ULong bit, CORBA::ULong dcol, const ipc::uvec & data, ipc::uvec_out retv);
  CORBA::ULong IPCreadDoubleColumnHW(CORBA::ULong bit, CORBA::ULong dcol, ipc::uvec_out retv);
  CORBA::ULong IPCclearPixelLatches();
  CORBA::Long IPCverifyModuleConfigHW();
  void shutdown();
  void destroy();

};
};
  

#endif
