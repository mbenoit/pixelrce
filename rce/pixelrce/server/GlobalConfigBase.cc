#include "server/GlobalConfigBase.hh"
#include "util/exceptions.hh"
#include <fstream>
#include <iostream>
#include <sys/stat.h> 

GlobalConfigBase::GlobalConfigBase(const char* confdir, const char* filename){
  std::string fn=std::string(confdir)+"/"+filename;
  struct stat stFileInfo;
  int intStat;
  intStat = stat(fn.c_str(), &stFileInfo);
  if(fn=="None" || intStat != 0) { //File does not exist
    std::cout<<"File does not exist."<<std::endl;
    rcecalib::Config_File_Error err;
    throw err;
  }else{
    std::ifstream ifile(fn.c_str());
    std::string mfn;
    int inlink, outlink, rce, phase;
    bool included;
    char modname[128];
    for(int i=0;i<MAX_MODULES;i++){
      sprintf(modname, "Module_%d", i);
      ifile>>mfn;
      ifile>>included;
      ifile>>inlink;
      ifile>>outlink;
      ifile>>rce;
      ifile>>phase;
      if(included==true){
	m_configs.push_back(new ConfigBase(modname, inlink, outlink, rce, phase, mfn.c_str()));
      }
    }
  }
}
GlobalConfigBase::~GlobalConfigBase(){
  for(configIterator it=begin(); it!=end(); it++){
    delete *it;
  }
}
