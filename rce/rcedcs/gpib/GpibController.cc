#include "GpibController.hh"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

GpibController::GpibController(const char* hostname, int stationid)
  :m_hostname(hostname), m_stationid(stationid){
  connect();
}

void GpibController::disconnect(){
  shutdown(m_socket,2);
  close(m_socket);
}
int GpibController::connect(){
  struct sockaddr_in server;
  struct hostent *hent; 
  
  m_socket=socket(AF_INET,SOCK_STREAM,0);
  if (m_socket<0) {
    printf("Error opening socket\n");
    return 1;
  } 
  server.sin_family=AF_INET;
  hent=gethostbyname(m_hostname.c_str());
  if (hent==0) {
    printf("Unknown host %s\n",m_hostname.c_str());
    return 1;
  }  
  memcpy((char *)&server.sin_addr,(char*)hent->h_addr,hent->h_length);
  server.sin_port=htons(1234); 
  
  if (::connect(m_socket,(struct sockaddr *)&server, sizeof server)<0) {
    printf("Error connecting socket\n");
    return 1;
  }
  setStationNumber(m_stationid);
  return 0;
}
GpibController::~GpibController(){
  disconnect();
}

void GpibController::write(const char* cmd){
  struct timeval timeout;
  fd_set wr;
  FD_ZERO(&wr); 
  FD_SET(m_socket,&wr);
  memset((char *)&timeout,0,sizeof(timeout));
  timeout.tv_sec=1; // 1 second timeout            
  int rv=select(m_socket+1, (fd_set *)0, &wr, (fd_set *)0, &timeout);
  if(rv<0){
    printf("Cannot write to socket\n");
    exit(1);
  }
  std::string command=std::string(cmd)+"\n";
  rv=send(m_socket, command.c_str(),command.size(),0);
  if(rv<0){
    printf("Send failed\n");
    exit(1);
  }
}

const char* GpibController::read(const char* command){
  
  write(command);
  write("++read 10"); // read until \n occurs

  static char buf[1024];
  int nbytes=0;
  struct timeval timeout;
  fd_set rd;
  FD_ZERO(&rd); 
  FD_SET(m_socket,&rd);
  memset((char *)&timeout,0,sizeof(timeout));
  timeout.tv_sec=1; // 1 second timeout            
  int rv=select(m_socket+1,&rd, (fd_set *)0, (fd_set *)0, &timeout);
  if(rv<0){
    printf("Cannot read from socket\n");
    exit(1);
  }
  if (rv> 0) {
    nbytes=recv(m_socket,buf,1023,0);
    if(nbytes<0){
      printf ("Read error\n");
      exit(1);
    }
  }
  buf[nbytes]=0;
  return buf;
}
 
void GpibController::setStationNumber(int n){
  char cmd[128];
  sprintf(cmd,"++addr %d",n);
  write(cmd);
}
void GpibController::setModeAuto(bool on){
  char cmd[128];
  sprintf(cmd,"++auto %d",(on>0));
  write(cmd);
}
  
