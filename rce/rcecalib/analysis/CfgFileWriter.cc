#include "rcecalib/analysis/CfgFileWriter.hh"
#include <TFile.h>
#include <boost/regex.hpp>
#include <string>
#include <iostream>

std::string CfgFileWriter::getPath(TFile* file){
  std::string fn(file->GetName());
  std::string name="";
  boost::regex re("[^/]*\\.root");
  std::string maskfilename = boost::regex_replace (fn, re, name);
  return maskfilename;
}
