#include <boost/property_tree/ptree.hpp>
#include "rcecalib/dataproc/DelayScanDataProc.hh"
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/dataproc/DataProcFactory.hh"
#include <assert.h>
#include <stdio.h>


int DelayScanDataProc::processData(unsigned link, unsigned *data, int size){
  printf("Coincidence\n");
  m_histo[0][0]->increment(m_currentBin);
  return 0;
}

DelayScanDataProc::DelayScanDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif) {
  try{ //catch bad scan option parameters
    int nLoops = scanOptions->get<int>("nLoops");
    assert (nLoops>0);
    int nPoints=1;
    nPoints=scanOptions->get<int>("scanLoop_0.nPoints");
    assert(nPoints>1);
    int p0=scanOptions->get<int>("scanLoop_0.dataPoints.P_0");
    char pointname[10];
    sprintf(pointname,"P_%d",nPoints-1);
    int pn=scanOptions->get<int>(std::string("scanLoop_0.dataPoints.")+pointname);
    std::vector<RceHisto1d<short, short>*> vh;
    std::cout<<"DelayScanDataProc"<<std::endl;
    m_histo.push_back(vh);
    int overlap = (pn-p0)/(nPoints-1)/2;
    m_histo[0].push_back(new RceHisto1d<short, short>("delhisto","DISC 1 - DISC 0",nPoints, p0-overlap, pn+overlap));
  }
  catch(boost::property_tree::ptree_bad_path ex){
    rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
    ers::error(issue);
    ERS_ASSERT(0);
  }
}

DelayScanDataProc::~DelayScanDataProc(){
  delete m_histo[0][0];
}
