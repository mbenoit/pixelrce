#include "HW/SerialIF.hh"
#include <assert.h>

SerialIF* SerialIF::m_serial=0;

SerialIF::SerialIF(){
  assert(m_serial==0);
  m_serial=this;
}
void SerialIF::destroy(){
  assert(m_serial!=0);
  delete m_serial;
  m_serial=0;
}

void SerialIF::send(BitStream *bs, int opt){
  assert(m_serial!=0);
  m_serial->Send(bs,opt);
}
void SerialIF::setChannelInMask(unsigned linkmask){
  assert(m_serial!=0);
  m_serial->SetChannelInMask(linkmask);
}
void SerialIF::setChannelOutMask(unsigned linkmask){
  assert(m_serial!=0);
  m_serial->SetChannelOutMask(linkmask);
}
int SerialIF::enableTrigger(bool on){
  assert(m_serial!=0);
  return m_serial->EnableTrigger(on);
} 
void SerialIF::disableOutput(){
  assert(m_serial!=0);
  m_serial->DisableOutput();
} 
void SerialIF::setOutputMode(unsigned mode){
  assert(m_serial!=0);
  m_serial->SetOutputMode(mode);
} 
void SerialIF::setDataRate(Rate rate){
  assert(m_serial!=0);
  m_serial->SetDataRate(rate);
} 
unsigned SerialIF::sendCommand(unsigned char opcode){
  assert(m_serial!=0);
  return m_serial->SendCommand(opcode);
}
unsigned SerialIF::writeRegister(unsigned addr, unsigned val){
  assert(m_serial!=0);
  return m_serial->WriteRegister(addr,val);
}
unsigned SerialIF::readRegister(unsigned addr, unsigned &val){
  assert(m_serial!=0);
  return m_serial->ReadRegister(addr, val);
}
unsigned SerialIF::writeBlockData(std::vector<unsigned>& data){
  assert(m_serial!=0);
  return m_serial->WriteBlockData(data);
}
unsigned SerialIF::readBlockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec){
  assert(m_serial!=0);
  return m_serial->ReadBlockData(data, retvec);
}
unsigned SerialIF::readBuffers(std::vector<unsigned char>& retvec){
  assert(m_serial!=0);
  return m_serial->ReadBuffers(retvec);
}
