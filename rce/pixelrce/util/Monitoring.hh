// 
// IS monitoring
// 
// Martin Kocian, SLAC, 2/11/2016
//

#ifndef RCEMONITORING_HH
#define RCEMONITORING_HH


  class Monitoring{ 
  public:
    static void destroy();
    static void publish(const char* name, float val);
    static void publish(const char* name, unsigned val);
    Monitoring();
    virtual ~Monitoring(){}
  protected:
    virtual void Publish(const char* name, float val){}
    virtual void Publish(const char* name, unsigned val){}
    static Monitoring* m_instance;
  };
    
#endif
