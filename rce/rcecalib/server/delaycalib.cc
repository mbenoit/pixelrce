#include <stdio.h>

#include <ers/ers.h>

#include <ipc/partition.h>
#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include <owl/timer.h>
#include <cmdl/cmdargs.h>
#include <unistd.h>

#include "rcecalib/server/TurboDaqFile.hh"
#include "rcecalib/server/IPCController.hh"
#include "rcecalib/server/IPCHistoController.hh"
#include "rcecalib/server/PixScan.hh"

#include "IPCScanAdapter.hh"
#include "IPCConfigIFAdapter.hh"
#include "IPCFEI3Adapter.hh"
#include "ScanOptions.hh"
#include "PixelModuleConfig.hh"

#include <TApplication.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH2F.h>
#include <TF1.h>
#include <TFile.h>
#include "rcecalib/server/Mean.hh"

int acquireDataPoint(IPCController& controller);

using namespace RCE;

int main( int argc, char ** argv )
{	
 
  TApplication *tapp;
    CmdArgStr	partition_name ('p', "partition", "partition-name", "partition to work in.");

//
// Initialize command line parameters with default values
//
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmd(*argv, &partition_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmd.parse(arg_iter);
  
    IPCPartition   p( (const char*)partition_name );
    IPCController controller(p);
    IPCHistoController hcontroller(p);
    unsigned serstat;
    int rce=0;
    serstat=controller.writeHWregister(rce, 7,0);
    assert(serstat==0);
    //for(int i=0;i<60;i++)controller.writeHWregister(6,0);
    //for (int i=0;i<60;i++)controller.writeHWregister(6,0);
    tapp=new TApplication("bla", &argc, argv);

    gROOT->SetStyle("Plain");
    gStyle->SetOptStat(0);
    gStyle->SetPalette(1);
    gStyle->SetOptFit(111);
    gStyle->SetErrorX(0);
    gStyle->SetMarkerStyle(20);
    
    TH1F* del1=new TH1F("delay1","Delay Disc 1 wrt Disc 0",13,-65.,65 );
    del1->GetXaxis()->SetTitle("Delay (IDELAY counts)");
    TCanvas *c1=new TCanvas("c1","Canvas",600,600);
    del1->SetMinimum(0);
    c1->Draw();
    del1->Draw("ep");
    sleep(1);
    
    PixScan* scn = new PixScan(PixScan::SCINTDELAY_SCAN, PixLib::EnumFEflavour::PM_FE_I2);
    //scn->resetScan();
    //scn->setLoopVarValues(0, 0, 200, 101);
    //scn->setRepetitions(50); 
    ipc::ScanOptions options;
    scn->convertScanConfig(options);
    controller.setupTrigger();
    controller.downloadScanConfig(options);
    serstat=controller.writeHWregister(rce,8,2);
    assert(serstat==0);
    controller.startScan();
    int status=-1;
    do{
      sleep(5);
      status = controller.getScanStatus();
      std::vector<TH1*> his=hcontroller.getHistosFromIS("delhisto");
      TH1* histo=his[0];
      for(int i=0;i<histo->GetNbinsX();i++){
	del1->SetBinContent(i+1,histo->GetBinContent(i+1));
	del1->SetBinError(i+1,sqrt((float)histo->GetBinContent(i+1)));
      }
      del1->Draw("ep");
      c1->Update();
      delete histo;
    }while (status !=0);
    std::cout<<"Loop done"<<std::endl;
    if(del1->GetEntries()>10){
      del1->Fit("gaus");
      float fitpar=del1->GetFunction("gaus")->GetParameter(1);
      std::cout<<"Fit result: "<<fitpar<<"IDELAY counts delay."<<std::endl;
    }
    std::cout<<"Fit done"<<std::endl;
    serstat=controller.writeHWregister(rce, 3,0); //Return to normal mode
    assert(serstat==0);
    TFile *file=new TFile("histos.root","recreate");
    del1->Write();
    file->Close();

    tapp->Run();
}
