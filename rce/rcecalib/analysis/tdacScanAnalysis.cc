#include "rcecalib/analysis/tdacScanAnalysis.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

void tdacScanAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  printf("Begin TDAC Analysis\n");
  float target = scan->getThresholdTargetValue();
  printf("Target Threshold Value: %2.1f\n",target);
  for(int rce = 0; rce < 8; rce++){
    for(int module = 0; module < 16; module++){
      if(file->Get(Form("loop1_0/RCE%i_Mod_%i_Mean",rce,module)) == 0) continue;
      TH2D* tdachis = new TH2D(Form("RCE%i_Mod_%i_TDAC",rce,module),"Best TDAC Settings",80,0,80,336,0,336);
      tdachis->GetXaxis()->SetTitle("Column");
      tdachis->GetYaxis()->SetTitle("Row");
      TH2D* ahis = new TH2D(Form("RCE%i_Mod_%i_Threshold_MinDiff",rce,module),"min(Threshold - Target Threshold)",80,0,80,336,0,336);
      ahis->GetXaxis()->SetTitle("Column");
      ahis->GetYaxis()->SetTitle("Row");
      for(int c = 1; c < 81; c++){
	for(int r = 1; r < 337; r++){
	  ahis->SetBinContent(c,r,-1.0);
	}
      }
      for(int i = 0; i < 32; i++){
	//printf("Point %3i\n",i);
	TH2* mean_histo;
	TH2* occ_histo;
	mean_histo = (TH2*)file->Get(Form("loop1_%i/RCE%i_Mod_%i_Mean",i,rce,module));
	occ_histo = (TH2*)file->Get(Form("loop1_%i/RCE%i_Mod_%i_Occupancy_Point_100",i,rce,module));
	double mean = 0.0;
	for(int j = 1; j <= mean_histo->GetNbinsX(); j++){
	  for(int k = 1; k <= mean_histo->GetNbinsY(); k++){
	    float nhits = occ_histo->GetBinContent(j,k);
	    if(nhits != 0){
	      mean = mean_histo->GetBinContent(j,k);
	      if(std::fabs(mean - target) < ahis->GetBinContent(j,k) || ahis->GetBinContent(j,k) < 0){
		ahis->SetBinContent(j,k,std::fabs(mean - target));
		tdachis->SetBinContent(j,k,i);
	      }
	    }
	  }
	}
	delete mean_histo;
	delete occ_histo;
      }
      std::string fn(anfile->GetName());
      int pos = fn.find("analysis.root");
      std::string outname = Form("%stdacs%i.dat",fn.substr(0,pos).c_str(),(rce*16+module));
      ofstream outfile(outname.c_str());
      outfile << "### ";
      for (int i=1; i<41;i++) {
	outfile << std::setw(4) << i;
      }
      outfile << std::endl;
      outfile << "### ";
      for (int i=41; i<81;i++) {
	outfile << std::setw(4) << i;
      }
      outfile << std::endl;
      for (int row=1; row<337; row++) {
	outfile << std::setw(3) << row << "a";
	for (int col=1; col<41; col++) {
	  outfile << std::setw(4) << int(tdachis->GetBinContent(col,row));
	}
	outfile << std::endl;
	outfile << std::setw(3) << row << "b";
	for (int col=41; col<81; col++) {
	  outfile << std::setw(4) << int(tdachis->GetBinContent(col,row));
	}
	outfile << std::endl;
	
      }
      anfile->cd();
      tdachis->Write();
      ahis->Write();
      delete ahis;
      delete tdachis;
    }
  }
  printf("Done Analysis\n");
}

