#include "dataproc/AtlasDataHandler.hh"
#include "dataproc/AbsDataProc.hh"
#include "dataproc/Channeldefs.hh"
#include "config/AbsFormatter.hh"
#include "config/DummyFormatter.hh"
#include "config/ConfigIF.hh"
#include "util/DataCond.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>

AtlasDataHandler::AtlasDataHandler(AbsDataProc* dataproc, 
				     DataCond& datacond,
				     ConfigIF* cif, 
				     boost::property_tree::ptree* scanOptions)
  :AbsDataHandler(dataproc, datacond, cif){
  std::cout<<"Atlas data handler"<<std::endl;
}

AtlasDataHandler::~AtlasDataHandler(){
}

void AtlasDataHandler::handle(unsigned desc, unsigned *data, int size){
  // nL1A contains the number of L1A in the data chunk
  unsigned link=desc&LINKMASK;
  if((link)==TDCREADOUT) m_dataProc->processData(desc, data, size);
  else {
    std::cout<<"Data for link "<<link<<std::endl;
  }
}
