
LIBRARY ieee;
use work.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity oneshot is 
   generic (
      TPD_G           : time     := 1 ns   -- Simulation FF output delay
      );
   port (
     clk        : in std_logic;
     rst        : in std_logic;
     datain     : in std_logic;
     dataout    : out std_logic
     );
end oneshot;

architecture ONESHOT of oneshot is

  signal clear : std_logic := '0';
  signal clearold : std_logic :='0';
  signal ffout: std_logic :='0';

begin

  process (clear, datain)
  begin
    if(clear='1')then
      ffout<='0' after TPD_G;
    elsif rising_edge(datain) then
      ffout<='1' after TPD_G;
    end if;
  end process;

  sync_inst: entity work.Synchronizer
    generic map ( TPD_G => TPD_G)
    port map(
      clk => clk,
      rst => rst,
      dataIn => ffout,
      dataOut => clear);
  process (rst, clk) begin
    if(rst='1')then
      dataout<='0' after TPD_G;
      clearold<='0' after TPD_G;
    elsif(rising_edge(clk))then
      if(clear='1' and clearold='0')then
        dataout<='1' after TPD_G;
      else
        dataout<='0' after TPD_G;
      end if;
      clearold<=clear after TPD_G;
    end if;
  end process;

end ONESHOT;
