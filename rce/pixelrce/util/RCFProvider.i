#ifndef PROVIDER_CC
#define PROVIDER_CC


template<class TC,class TE>
void 
Provider::publish(const std::string & name,
	const std::string & title,
	RceHistoAxis & xaxis,
	const TC * bins,
	const TE * errors,
	bool hasOverflowAndUnderflow,
	int tag ,
	const std::vector< std::pair<std::string,std::string> > & annotations){ 
  RCFRceHistoAxis xaxisoh(xaxis.label(), xaxis.bincount(), xaxis.low(), xaxis.width() );
  std::string namel=name;
  std::string titlel=title;
  RCF::ByteBuffer binb((char*)bins, xaxis.bincount()*sizeof(TC));
  RCF::ByteBuffer errorb;
  TC tc=1;
  TE te=1;
  m_provider->publish().publishHisto(namel, titlel, xaxisoh, binb, errorb, tc, te);
}

template<class TC,class TE>
void
Provider::publish(const std::string & name,
		     const std::string & title,
		     RceHistoAxis & xaxis,
		     RceHistoAxis & yaxis,
		     const TC * bins,
		     const TE * errors,
		     bool hasOverflowAndUnderflow,
		     int tag,
		     const std::vector< std::pair<std::string,std::string> > & annotations ) {
  RCFRceHistoAxis xaxisoh(xaxis.label(), xaxis.bincount(), xaxis.low(), xaxis.width()) ;
  RCFRceHistoAxis yaxisoh(yaxis.label(), yaxis.bincount(), yaxis.low(), yaxis.width() );
  std::string namel=name;
  std::string titlel=title;
  RCF::ByteBuffer binb((char*)bins, xaxis.bincount()*yaxis.bincount()*sizeof(TC));
  RCF::ByteBuffer errorb;
  TC tc=1;
  TE te=1;
  m_provider->publish().publishHisto(namel, titlel, xaxisoh, yaxisoh, binb, errorb, tc, te);
}

#endif
