#ifndef COSMICDATAPROC_HH
#define COSMICDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include "eudaq/FileSerializer.hh"
#include <list>
#include <fstream>
#include <fstream>

class CosmicEvent;
class CosmicEventIterator;

class CosmicDataProc: public AbsDataProc{
public:
  CosmicDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~CosmicDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string);
  unsigned nEvents();
protected:
  void resynch();
  inline int processModuleData(unsigned* data,int size, int link, unsigned& l1id, int& bxfirst, int& bxlast, int &ntrg);

  int m_nL1AperEv;
  int m_nModules;
  bool m_linksynch[16];
  int m_tossed[16];
  unsigned m_runNo;
  eudaq::FileSerializer *m_file;
  //std::ofstream m_file;
  int m_nfrag;
  CosmicEventIterator* m_iterator;
  int m_print;
  int m_hits;
  
};

#endif
