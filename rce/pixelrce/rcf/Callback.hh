#ifndef CALLBACK_HH
#define CALLBACK_HH

#include <RCF/RCF.hpp>
#include <stdint.h>

namespace ipc
{
  enum Priority{LOW, MEDIUM, HIGH, NONE};
  enum ScanStatus{IDLE, CONFIGURED, SCANNING, FITTING, STOPPED, DOWNLOADING, FAILED};

  struct CallbackParams{
    int32_t rce;
    ScanStatus status;
    int32_t maskStage;
    int32_t loop0;
    int32_t loop1;
    void serialize(SF::Archive &ar){
      ar & rce & status & maskStage & loop0 & loop1;
    }
  };
};

RCF_BEGIN(I_RCFCallback, "I_RCFCallback")
RCF_METHOD_V0(void , stopServer)
RCF_METHOD_V1(void , notify, ipc::CallbackParams )
RCF_END(I_RCFCallback)

  class Callback
  {
    virtual void notify ( ipc::CallbackParams msg )=0;
    virtual void stopServer()=0;
  };

#endif
