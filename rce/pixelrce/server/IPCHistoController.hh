#ifndef IPCHISTOCONTROLLER_HH
#define IPCHISTOCONTROLLER_HH

#include "ipc/partition.h"
#include "IPCScanRootAdapter.hh"
#include "server/AbsHistoController.hh"
#include <vector>

#include <TH1.h>


class IPCHistoController: public AbsHistoController{
public:
  IPCHistoController() {};
  IPCHistoController(IPCPartition &p);
  
  void addRce(int rce);
  void removeAllRces();
  std::vector<TH1*> getHistos(const char* reg); 
  std::vector<std::string> getHistoNames(const char* reg);
  std::vector<std::string> getPublishedHistoNames();

  std::string getIPCPartitionName() { return m_partition.name(); }

  bool clear();

  IPCPartition m_partition;
};

#endif
