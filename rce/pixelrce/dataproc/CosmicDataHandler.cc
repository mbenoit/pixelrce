#include "dataproc/CosmicDataHandler.hh"
#include "dataproc/AbsDataProc.hh"
#include "dataproc/Channeldefs.hh"
#include "config/AbsFormatter.hh"
#include "config/DummyFormatter.hh"
#include "config/ConfigIF.hh"
#include "util/DataCond.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>

CosmicDataHandler::CosmicDataHandler(AbsDataProc* dataproc, 
				     DataCond& datacond,
				     ConfigIF* cif, 
				     boost::property_tree::ptree* scanOptions)
  :AbsDataHandler(dataproc, datacond, cif){
  std::cout<<"Cosmic data handler"<<std::endl;
  m_nModules=m_configIF->getNmodules();
  for (int i=0;i<m_nModules;i++){
    m_formatter.push_back(m_configIF->getModuleInfo(i).getFormatter());
  }
  m_dummy=new DummyFormatter(0);
  m_formatter.push_back(m_dummy);
  m_linkToIndex=new int[MAXMODULES];
  for(int i=0;i<MAXMODULES;i++)m_linkToIndex[i]=m_nModules; //point to dummy formatter
  //now configure existing module formatters
  for (int i=0;i<m_nModules;i++){
    m_linkToIndex[m_configIF->getModuleInfo(i).getOutLink()]=i;
  }
  m_parsedData=new unsigned[16384];
}

CosmicDataHandler::~CosmicDataHandler(){
  delete [] m_parsedData;
  delete [] m_linkToIndex;
  delete m_dummy;
}

void CosmicDataHandler::handle(unsigned desc, unsigned *data, int size){
  // nL1A contains the number of L1A in the data chunk
  unsigned link=desc&LINKMASK;
  int parsedsize=0;
  //std::cout<<"Data for link "<<link<<std::endl;
  unsigned *thedata;
  if((link)==TDCREADOUT){
    thedata=data;
    parsedsize=size;
  } else {
    thedata=m_parsedData;
    int nL1A=0;
    int retval=m_formatter[m_linkToIndex[link]]->decode(data,size,m_parsedData, parsedsize, nL1A);
    //std::cout<<"parser"<<retval<<" "<<size<<std::endl;
    if(retval!=0){
      std::cout<<"Parser error for link "<<link<<": Data size "<<size<<" number of triggers "<<nL1A<<std::endl;
      for (int i=0;i<(size<100?size:100);i++)std::cout<<std::hex<<data[i]<<std::endl;
    }
  }
  if(parsedsize>0){
    m_dataProc->processData(desc, thedata, parsedsize);
  }
}
