#ifndef REGISTERTESTANALYSIS_HH
#define REGISTERTESTANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"

class ConfigGui;
class TFile;
class TH2D;
class TH1;
namespace RCE{
  class PixScan;
}

class RegisterTestAnalysis: public CalibAnalysis{
public:
  RegisterTestAnalysis(): CalibAnalysis(){}
  ~RegisterTestAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
