#ifndef TIMEWALKANALYSIS_HH
#define TIMEWALKANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"

class ConfigGui;
class TFile;
class TH2D;
class TH1;
namespace RCE{
  class PixScan;
}

class TimeWalkAnalysis: public CalibAnalysis{
public:
  TimeWalkAnalysis(): CalibAnalysis(){}
  ~TimeWalkAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
  void fillHit1d(std::string &name, TH1* hits1d, unsigned numval);
};


#endif
