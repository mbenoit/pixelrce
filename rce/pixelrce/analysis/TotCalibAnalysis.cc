#include "analysis/TotCalibAnalysis.hh"
#include "server/PixScan.hh"
#include "server/ConfigGui.hh"
#include "config/FEI3/Module.hh"
#include "config/FEI4/Module.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TKey.h>
#include <TMath.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>
#include "TH1D.h"
#include "TStyle.h"

void TotCalibAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  std::cout<<"TOT Calibration analysis running..."<<std::endl;
  int repetitions = scan->getRepetitions();
  double targetEff = 0.5;
  TKey* key;
  char subdir[128];
  char name[128];
  char title[128];
  int binsx=0;
  size_t numvals=scan->getLoopVarValues(1).size();
  std::map<int, int> idmap;
  std::vector<int> modmap;
  int nmod=0;
  int nrows=336;
  int ncols=80;
  std::vector<float> varValues=scan->getLoopVarValues(1);
  std::vector<std::vector<double**> > n, x, x2;
  for (size_t i=0;i<numvals;i++){
    sprintf(subdir, "loop1_%d", i);
    file->cd(subdir); // GDAC steps
    TIter nextkey(gDirectory->GetListOfKeys()); // histos
    while ((key=(TKey*)nextkey())) {
      boost::cmatch matches;
      boost::regex re("_(\\d+)_Occupancy_ToT_(\\d+)");
      if(boost::regex_search(key->GetName(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	int id=strtol(match.c_str(),0,10);
	std::string match2(matches[2].first, matches[2].second);
	int tot=strtol(match2.c_str(),0,10);
	TH2* histo = (TH2*)key->ReadObj();
	if(idmap.find(id)==idmap.end()){
	  std::vector<double** > nvec;
	  std::vector<double** > xvec;
	  std::vector<double** > x2vec;
	  n.push_back(nvec);
	  x.push_back(xvec);
	  x2.push_back(x2vec);
	  for(int l=0;l<16;l++){
	    n[nmod].push_back(new double*[ncols]);
	    x[nmod].push_back(new double*[ncols]);
	    x2[nmod].push_back(new double*[ncols]);
	    for(int j=0;j<ncols;j++){
	      n[nmod][l][j]=new double[nrows];
	      x[nmod][l][j]=new double[nrows];
	      x2[nmod][l][j]=new double[nrows];
	      for(int k=0;k<nrows;k++){
		n[nmod][l][j][k]=0;
		x[nmod][l][j][k]=0;
		x2[nmod][l][j][k]=0;
	      }
		
	    }
	  }
	  modmap.push_back(id);
	  idmap[id]=nmod++;
	}
	int mod=idmap[id];
	PixelConfig* confb=findConfig(cfg, id);
	FECalib fecalib=confb->getFECalib(0);
	float clo=fecalib.cinjLo;
	float chi=fecalib.cinjHi;
	float vcal1=fecalib.vcalCoeff[1];
	float vcal0=fecalib.vcalCoeff[0];
	float capacitance=0;
	if(scan->getMaskStageMode()==PixLib::EnumMaskStageMode::FEI4_ENA_SCAP)capacitance=clo;
	else if(scan->getMaskStageMode()==PixLib::EnumMaskStageMode::FEI4_ENA_LCAP)capacitance=chi;
	else if(scan->getMaskStageMode()==PixLib::EnumMaskStageMode::FEI4_ENA_BCAP)capacitance=chi+clo;
	capacitance=capacitance*6241;
	float fldac=(float)varValues[i];
	float el= capacitance*(vcal0 + vcal1*fldac);
	
	for(int row=0;row<nrows;row++){
	  for(int col=0;col<ncols;col++){
	    double nentries=histo->GetBinContent(col+1, row+1);
	    n[mod][tot][col][row]+=nentries;
	    x[mod][tot][col][row]+=el*nentries;
	    x2[mod][tot][col][row]+=el*el*nentries;
	  }
	}
      }
    }
  }
  std::cout<<"Part 1 done"<<std::endl;
  anfile->cd();
  TH1D cc("calibration curve", "calibration curve", 16, 0, 16);
  for (int mod=0;mod<nmod;mod++){
    int id=modmap[mod];
    TH2D* p0=new TH2D(Form("P0_Mod_%i",id),Form("P0 Mod %d at %s", id, findFieldName(cfg, id)),ncols, 0, ncols, nrows, 0, nrows);
    TH2D* p1=new TH2D(Form("P1_Mod_%i",id),Form("P1 Mod %d at %s", id, findFieldName(cfg, id)),ncols, 0, ncols, nrows, 0, nrows);
    TH2D* p2=new TH2D(Form("P2_Mod_%i",id),Form("P2 Mod %d at %s", id, findFieldName(cfg, id)),ncols, 0, ncols, nrows, 0, nrows);
    for(int col=0;col<ncols;col++){
      for(int row=0;row<nrows;row++){
	cc.Reset();
	for(int tot=0;tot<16;tot++){
	  double nent=n[mod][tot][col][row];
	  if(nent!=0){
	    cc.SetBinContent(tot+1, x[mod][tot][col][row]/nent);
	    cc.SetBinError(tot+1, TMath::Sqrt(TMath::Abs(x2[mod][tot][col][row]-x[mod][tot][col][row]*x[mod][tot][col][row]/nent))/nent);
	  }
	}
	cc.Fit("pol2", "", "", 1, 13);
	TF1* fitfun=cc.GetFunction("pol2");
	if(fitfun && fitfun->GetNpar()==3){
	  p0->SetBinContent(col+1,row+1, fitfun->GetParameter(0));
	  p1->SetBinContent(col+1,row+1, fitfun->GetParameter(1));
	  p2->SetBinContent(col+1,row+1, fitfun->GetParameter(2));
	}
      }
    }
    p0->Write();
    p1->Write();
    p2->Write();
  }
  for(size_t i=0;i<n.size();i++){
    for(int j=0;j<16;j++){
      for(int k=0;k<ncols;k++){
	delete [] n[i][j][k];
      }
      delete [] n[i][j];
    }
  }
  for(size_t i=0;i<n.size();i++){
    for(int j=0;j<16;j++){
      for(int k=0;k<ncols;k++){
	delete [] x[i][j][k];
      }
      delete [] x[i][j];
    }
  }
  for(size_t i=0;i<n.size();i++){
    for(int j=0;j<16;j++){
      for(int k=0;k<ncols;k++){
	delete [] x2[i][j][k];
      }
      delete [] x2[i][j];
    }
  }
}
