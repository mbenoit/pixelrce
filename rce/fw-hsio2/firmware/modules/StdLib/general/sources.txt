vhdl work rtl/StdRtlPkg.vhd
vhdl work rtl/PrbsPkg.vhd
vhdl work rtl/ArbiterPkg.vhd
vhdl work rtl/Code8b10bPkg.vhd

vhdl work rtl/Arbiter.vhd
vhdl work rtl/ClkOutBufDiff.vhd
vhdl work rtl/ClkOutBufSingle.vhd
vhdl work rtl/CRC32Rtl.vhd
vhdl work rtl/Crc32.vhd
vhdl work rtl/Crc32Parallel.vhd
vhdl work rtl/CrcPkg.vhd
vhdl work rtl/Debouncer.vhd
vhdl work rtl/Decoder8b10b.vhd
vhdl work rtl/DeviceDna.vhd
vhdl work rtl/DS2411Core.vhd
vhdl work rtl/DspAddSub.vhd
vhdl work rtl/DspCounter.vhd
vhdl work rtl/Encoder8b10b.vhd
vhdl work rtl/Heartbeat.vhd
vhdl work rtl/Iprog.vhd
vhdl work rtl/PwrUpRst.vhd

vhdl work rtl/CrcPkg.vhd
vhdl work rtl/Crc32.vhd
