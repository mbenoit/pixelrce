#ifndef GLOBAL_CONFIG_BASE_HH
#define GLOBAL_CONFIG_BASE_HH

#include "config/ConfigBase.hh"
#include <vector>

typedef std::vector<ConfigBase*>::iterator configIterator;

class GlobalConfigBase{
public:
  enum constants{MAX_MODULES=64};
  GlobalConfigBase(const char* confdir, const char* filename);
  ~GlobalConfigBase();
  configIterator begin(){return m_configs.begin();}
  configIterator end(){return m_configs.end();}
  
private: 
  std::vector<ConfigBase*> m_configs;
};

#endif
