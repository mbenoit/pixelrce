
#include "rcecalib/server/GlobalConfigGui.hh"
#include "rcecalib/server/FEI4AConfigFile.hh"
#include "rcecalib/server/FEI4BConfigFile.hh"
#include "rcecalib/server/TurboDaqFile.hh"
#include "PixelFEI4AConfig.hh"
#include "PixelFEI4BConfig.hh"
#include "PixelModuleConfig.hh"
#include "rcecalib/server/CalibGui.hh"
#include <iostream>
#include <boost/regex.hpp>
#include <sys/stat.h> 
#include <string>

int main(int argc,char *argv[])
{


ipc::PixelFEI4AConfig confa;
ipc::PixelFEI4BConfig confb;

FEI4AConfigFile fei4afile;
FEI4BConfigFile fei4bfile;
 int flavour=-1;
 std::string filename(argv[1]);
 try {
  fei4afile.readModuleConfig(&confa,filename);
  flavour=0;
 } catch (...) { flavour=-1;}
 if(flavour==-1) {
   try {
  fei4bfile.readModuleConfig(&confb,filename);
  flavour=1;
   } catch(...) {flavour=-1;}

 }
 if(flavour==0) fei4afile.dump(confa);
 if(flavour==1) fei4bfile.dump(confb);




  return 0;


}
