#ifndef FEI4AFORMATTER_HH
#define FEI4AFORMATTER_HH

#include "config/AbsFormatter.hh"
#include "util/RceHisto1d.cc"

class FEI4AFormatter:public AbsFormatter{
public:
  FEI4AFormatter(int id);
  virtual ~FEI4AFormatter();
  int decode(const unsigned* data, int size, unsigned* parsedData, int &parsedsize, int &nL1A);
  void configure(boost::property_tree::ptree* scanOptions);
  int decodeToT(int rawTot);

private:
  int m_hitDiscCnfg;
  RceHisto1d<int, int>* m_errhist;
};
#endif
