#ifndef IPCHITBUSMODULE_HH
#define IPCHITBUSMODULE_HH

#include "config/hitbus/Module.hh"
#include "ipc/object.h"
#include "HitbusModuleConfig.hh"
#include "IPCHitbusAdapter.hh"

class IPCPartition;
class AbsFormatter;

namespace Hitbus{
template <class TP = ipc::single_thread>
class IPCModule: public IPCNamedObject<POA_ipc::IPCHitbusAdapter,TP>, public Hitbus::Module {
public:
  IPCModule(IPCPartition & p, const char * name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt);
  ~IPCModule();
  CORBA::Long IPCdownloadConfig(const ipc::HitbusModuleConfig &config);    
  void shutdown();
  void destroy();

};
};
  

#endif
