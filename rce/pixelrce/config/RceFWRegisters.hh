#ifndef RCEFWREGISTERS_HH
#define RCEFWREGISTERS_HH

#include "config/FWRegisters.hh"

class ConfigIF;

class RceFWRegisters: public FWRegisters{
public:
  RceFWRegisters(ConfigIF* cif): m_configIF(cif){}
  virtual void writeRegister(int rce, unsigned reg, unsigned val);
  virtual unsigned readRegister(int rce, unsigned reg);
  virtual void sendCommand(int rce, unsigned opcode);
private:
  ConfigIF* m_configIF;
};
#endif
