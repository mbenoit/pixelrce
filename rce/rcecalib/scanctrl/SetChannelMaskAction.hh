#ifndef SETCHANNELMASKACTION_HH 
#define SETCHANNELMASKACTION_HH 

#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/config/ConfigIF.hh"

class SetChannelMaskAction: public LoopAction{
public:
  SetChannelMaskAction(std::string name, ConfigIF* cif):
    LoopAction(name),m_configIF(cif){}
  int execute(int i){
    return m_configIF->setChannelMask();
  }
private:
  ConfigIF* m_configIF;
};

#endif
