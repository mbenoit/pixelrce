#include "rcecalib/analysis/RegisterTestAnalysis.hh"
#include "rcecalib/server/PixScan.hh"
#include "rcecalib/config/FEI4/Utils.hh"

#include <TFile.h>
#include <TStyle.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TF1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>

namespace{
  const double threshold=0;
}
using namespace RCE;

void RegisterTestAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  gStyle->SetOptStat(0);
  std::cout<<"Register Test Analysis"<<std::endl;
  file->cd();
  TIter nextkey(gDirectory->GetListOfKeys());
  TKey *key;
  boost::regex re("_(\\d+)_Pixel_Reg");
  boost::regex re2("Pixel");
  while ((key=(TKey*)nextkey())) {
    file->cd();
    std::string hname(key->GetName());
    boost::cmatch matches;
    if(boost::regex_search(hname.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      std::string globHistoName = boost::regex_replace (hname, re2, "Global");
      TH2* histo = (TH2*)key->ReadObj();
      TH1* globHisto=(TH2*)gDirectory->Get(globHistoName.c_str());
      assert(globHisto);
      int binsx=histo->GetNbinsX();
      int binsy=histo->GetNbinsY();
      char name[128];
      char title[128];
      sprintf(name, "Mod_%d_Enable", id);
      sprintf(title, "Enable Bit Module %d at %s", id, findFieldName(cfg, id));
      TH2F* enable=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      enable->GetXaxis()->SetTitle("Column");
      enable->GetYaxis()->SetTitle("Row");
      sprintf(name, "Mod_%d_TDAC", id);
      sprintf(title, "TDAC Bits Module %d at %s", id, findFieldName(cfg, id));
      TH2F* tdac=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      tdac->GetXaxis()->SetTitle("Column");
      tdac->GetYaxis()->SetTitle("Row");
      sprintf(name, "Mod_%d_LargeCap", id);
      sprintf(title, "Large Cap Bit Module %d at %s", id, findFieldName(cfg, id));
      TH2F* largeCap=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      largeCap->GetXaxis()->SetTitle("Column");
      largeCap->GetYaxis()->SetTitle("Row");
      sprintf(name, "Mod_%d_SmallCap", id);
      sprintf(title, "Small Cap Bit Module %d at %s", id, findFieldName(cfg, id));
      TH2F* smallCap=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      smallCap->GetXaxis()->SetTitle("Column");
      smallCap->GetYaxis()->SetTitle("Row");
      sprintf(name, "Mod_%d_Hitbus", id);
      sprintf(title, "Hitbus Bit Module %d at %s", id, findFieldName(cfg, id));
      TH2F* hitbus=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      hitbus->GetXaxis()->SetTitle("Column");
      hitbus->GetYaxis()->SetTitle("Row");
      sprintf(name, "Mod_%d_FDAC", id);
      sprintf(title, "FDAC Bits Module %d at %s", id, findFieldName(cfg, id));
      TH2F* fdac=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      fdac->GetXaxis()->SetTitle("Column");
      fdac->GetYaxis()->SetTitle("Row");

      for (int i=0;i<binsx;i++){
	for (int j=0;j<binsy;j++){
	  int val=histo->GetBinContent(i+1, j+1);
	  for (int k=0;k<13;k++){
	    int bit=val&(1<<k);
	    if(bit){
	      std::cout<<"FE "<<id<<": Pixel Register Problem."<<std::endl;
	      if(k==0)enable->Fill(i,j);
	      else if(k>=1 && k<=5)tdac->Fill(i,j);
	      else if(k==6)largeCap->Fill(i,j);
	      else if(k==7)smallCap->Fill(i,j);
	      else if(k==8)hitbus->Fill(i,j);
	      else if(k>=9)fdac->Fill(i,j);
	    }
	  }
	}
      }
      delete histo;
      anfile->cd();
      enable->Write();
      enable->SetDirectory(gDirectory);
      tdac->Write();
      tdac->SetDirectory(gDirectory);
      largeCap->Write();
      largeCap->SetDirectory(gDirectory);
      smallCap->Write();
      smallCap->SetDirectory(gDirectory);
      hitbus->Write();
      hitbus->SetDirectory(gDirectory);
      fdac->Write();
      fdac->SetDirectory(gDirectory);
      ipc::PixelFEI4BConfig* confb=findFEI4BConfig(cfg, id);
      if(confb){
	sprintf(name, "Mod_%d_Global_Register_Diff", id);
	sprintf(title, "Diff of Global Reg Module %d at %s", id, findFieldName(cfg, id));
	TH1F* glob=new TH1F(name, title, 36, -0.5, 35.5);
	glob->GetXaxis()->SetTitle("Register");
	unsigned short reg[36];
	reg[0]=0;
	reg[1]=(confb->FEGlobal.SmallHitErase<<8) | flipBits(8,confb->FEGlobal.Eventlimit); 
	reg[2]=0xffff & ((42<<12) | (1<<11)); //ConfAddrEnable was explicitely turned on, TrigCnt was 42
	reg[3]=confb->FEGlobal.ErrMask0;
	reg[4]=confb->FEGlobal.ErrMask1;
	reg[5]=(flipBits(8, confb->FEGlobal.PrmpVbpRight)<<8)| flipBits(8, confb->FEGlobal.BufVgOpAmp);
	reg[6]=flipBits(8, confb->FEGlobal.PrmpVbp);
	reg[7]=(flipBits(8, confb->FEGlobal.TdacVbp)<<8)| flipBits(8, confb->FEGlobal.DisVbn);
	reg[8]=(flipBits(8, confb->FEGlobal.Amp2Vbn)<<8)| flipBits(8, confb->FEGlobal.Amp2VbpFol);
	reg[9]=flipBits(8, confb->FEGlobal.Amp2Vbp);
	reg[10]=(flipBits(8, confb->FEGlobal.FdacVbn)<<8)| flipBits(8, confb->FEGlobal.Amp2Vbpf);
	reg[11]=(flipBits(8, confb->FEGlobal.PrmpVbnFol)<<8)| flipBits(8, confb->FEGlobal.PrmpVbpLeft);
	reg[12]=(flipBits(8, confb->FEGlobal.PrmpVbpf)<<8)| flipBits(8, confb->FEGlobal.PrmpVbnLcc);
	reg[13]=0;
	reg[14]=(flipBits(8, confb->FEGlobal.LVDSDrvIref)<<8)| flipBits(8, confb->FEGlobal.GADCOpAmp);
	reg[15]=(flipBits(8, confb->FEGlobal.PllIbias)<<8)| flipBits(8, confb->FEGlobal.LVDSDrvVos);
	reg[16]=(flipBits(8, confb->FEGlobal.TempSensBias)<<8)| flipBits(8, confb->FEGlobal.PllIcp);
	reg[17]=flipBits(8, confb->FEGlobal.PlsrIdacRamp);
	reg[18]=(flipBits(8, confb->FEGlobal.VrefDigTune)<<8)| flipBits(8, confb->FEGlobal.PlsrVgOPamp);
	reg[19]=(flipBits(8, confb->FEGlobal.PlsrDacBias)<<8)| flipBits(8, confb->FEGlobal.VrefAnTune);
	reg[20]=(flipBits(8, confb->FEGlobal.Vthin_AltCoarse)<<8)| flipBits(8, confb->FEGlobal.Vthin_AltFine);
	reg[21]=(confb->FEGlobal.HITLD_In<<12)| (confb->FEGlobal.DINJ_Override<<11) | (confb->FEGlobal.DIGHITIN_Sel<<10) | flipBits(10, 341);
	reg[22]=768;
	reg[23]=confb->FEGlobal.DisableColumnCnfg0;
	reg[24]=confb->FEGlobal.DisableColumnCnfg1;
	reg[25]=(100<<8)| confb->FEGlobal.DisableColumnCnfg2;
	reg[26]=(12<<3)| (confb->FEGlobal.StopModeCnfg<<2) | (confb->FEGlobal.HitDiscCnfg);
	reg[27]=(confb->FEGlobal.EN_PLL<<15);
	reg[28]=(confb->FEGlobal.LVDSDrvSet06<<15)| (confb->FEGlobal.EN40M<<9) | (confb->FEGlobal.EN80M<<8)
	  |(confb->FEGlobal.CLK1<<5)|(confb->FEGlobal.CLK0<<2) | (confb->FEGlobal.EN160M<<1) | confb->FEGlobal.EN320M;
	reg[29]=(confb->FEGlobal.no8b10b<<13) |(flipBits(8, confb->FEGlobal.EmptyRecord)<<4)
	  | (confb->FEGlobal.LVDSDrvEn<<2) |(confb->FEGlobal.LVDSDrvSet30<<1) | confb->FEGlobal.LVDSDrvSet12;
	reg[30]=(flipBits(2, confb->FEGlobal.TempSensDiodeSel)<<14) | (confb->FEGlobal.TempSensDisable<<13) | (confb->FEGlobal.IleakRange<<12);
	reg[31]=(confb->FEGlobal.PlsrRiseUpTau<<13)| (confb->FEGlobal.PlsrPwr<<12) |(flipBits(6, confb->FEGlobal.PlsrDelay)<<6)| (confb->FEGlobal.EN80M<<8)
	  | (confb->FEGlobal.ExtDigCalSW<<5) | (confb->FEGlobal.ExtAnaCalSW<<4) | confb->FEGlobal.GADCSel;
	reg[32]=confb->FEGlobal.SELB0;
	reg[33]=confb->FEGlobal.SELB1;
	reg[34]=(confb->FEGlobal.SELB2<<8)|(confb->FEGlobal.PrmpVbpMsnEn<<4);
	reg[35]=confb->FEGlobal.Chip_SN;
	for(int i=0;i<36;i++){
	  if(globHisto->GetBinContent(i+1)!=reg[i]){
	    std::cout<<"Frontend "<<id<<" global register "<<i<<" content differs: ";
	    std::cout<<"Setting= 0x"<<std::hex<<reg[i]<<" readback= 0x"<<(unsigned)globHisto->GetBinContent(i+1)<<std::dec<<std::endl;
	    glob->SetBinContent(i+1, unsigned(globHisto->GetBinContent(i+1))^reg[i]);
	  }
	}
	glob->Write();
	glob->SetDirectory(gDirectory);
      }else{
	std::cout<<"Not an FEI4B chip. Not checking global register"<<std::endl;
      }
    }
  }  
}

