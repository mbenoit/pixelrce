#include "analysis/AnalysisFactory.hh"
#include "analysis/NoiseAnalysis.hh"
#include "analysis/Iff_Analysis.hh"
#include "analysis/Fdac_Analysis.hh"
#include "analysis/StuckPixelAnalysis.hh"
#include "analysis/GdacAnalysis.hh"
#include "analysis/GdacFastAnalysis.hh"
#include "analysis/GdacCoarseFastAnalysis.hh"
#include "analysis/TdacAnalysis.hh"
#include "analysis/TdacFastAnalysis.hh"
#include "analysis/ThresholdAnalysis.hh"
#include "analysis/TotAnalysis.hh"
#include "analysis/TotCalibAnalysis.hh"
#include "analysis/DigitalTestAnalysis.hh"
#include "analysis/ModuleCrosstalkAnalysis.hh"
#include "analysis/OffsetAnalysis.hh"
#include "analysis/CrosstalkAnalysis.hh"
#include "analysis/T0Analysis.hh"
#include "analysis/TimeWalkAnalysis.hh"
#include "analysis/MultiTrigAnalysis.hh"
#include "analysis/MultiTrigNoiseAnalysis.hh"
#include "analysis/SerialNumberAnalysis.hh"
#include "analysis/TemperatureAnalysis.hh"
#include "analysis/RegisterTestAnalysis.hh"
#include "analysis/Fei3CfgFileWriter.hh"
#include "analysis/Fei4CfgFileWriter.hh"
#include <iostream>

CalibAnalysis* AnalysisFactory::getAnalysis(std::string &type){
  CalibAnalysis* ana=0;
  if(type=="Fei3Noise")
    ana=new NoiseAnalysis(new Fei3CfgFileWriter);
  if(type=="Fei4Noise")
    ana=new NoiseAnalysis(new Fei4CfgFileWriter);
  else if(type=="Fei3StuckPixel")
    ana=new StuckPixelAnalysis(new Fei3CfgFileWriter);
  else if(type=="Fei4StuckPixel")
    ana=new StuckPixelAnalysis(new Fei4CfgFileWriter);
  else if(type=="Gdac")
    ana=new GdacAnalysis;
  else if(type=="GdacFast")
    ana=new GdacFastAnalysis;
  else if(type=="GdacCoarseFast")
    ana=new GdacCoarseFastAnalysis;
  else if(type=="Fei3Tdac_tune")
    ana=new TdacAnalysis(new Fei3CfgFileWriter);
  else if(type=="Fei4Tdac_tune")
    ana=new TdacAnalysis(new Fei4CfgFileWriter);
  else if(type=="Fei4TdacFast_tune")
    ana=new TdacFastAnalysis(new Fei4CfgFileWriter);
  else if(type=="Threshold")
    ana=new ThresholdAnalysis;
  else if(type=="Iffanalysis")
    ana=new IffAnalysis;
  else if(type=="Fei3Fdacanalysis")
    ana=new FdacAnalysis(new Fei3CfgFileWriter);
  else if(type=="Fei4Fdacanalysis")
    ana=new FdacAnalysis(new Fei4CfgFileWriter);
  else if(type=="Fei3DigitalTest")
    ana=new DigitalTestAnalysis(new Fei3CfgFileWriter);
  else if(type=="Fei4DigitalTest")
    ana=new DigitalTestAnalysis(new Fei4CfgFileWriter);
  else if(type=="TOT")
    ana=new TotAnalysis;
  else if(type=="TOTCALIB")
    ana=new TotCalibAnalysis;
  else if(type=="Offset")
    ana=new OffsetAnalysis;
  else if(type=="ModuleCrosstalk")
    ana=new ModuleCrosstalkAnalysis;
  else if(type=="Crosstalk")
    ana=new CrosstalkAnalysis;
  else if(type=="T0")
    ana=new T0Analysis;
  else if(type=="TimeWalk")
    ana=new TimeWalkAnalysis;
  else if(type=="MultiTrig")
    ana=new MultiTrigAnalysis;
  else if(type=="MultiTrigNoise")
    ana=new MultiTrigNoiseAnalysis;
  else if(type=="SerialNumber")
    ana=new SerialNumberAnalysis;
  else if(type=="Temperature")
    ana=new TemperatureAnalysis;
  else if(type=="RegisterTest")
    ana=new RegisterTestAnalysis;
  else
    std::cout<<"Analysis type "<<type<<" is not defined. "<<std::endl;
  return ana;
}
