#ifndef FEI3__MODULE_HH
#define FEI3__MODULE_HH

#include "rcecalib/config/FEI3/Frontend.hh"
#include "rcecalib/config/FEI3/Mcc.hh"
#include "rcecalib/config/AbsModule.hh"
#include "rcecalib/config/MaskStaging.hh"
#include <boost/property_tree/ptree_fwd.hpp>

class AbsFormatter;

namespace FEI3{

  class Module: public AbsModule{
  public:
    Module(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt);
    virtual ~Module();
    enum {N_FRONTENDS=16};
    enum PAR{NOT_FOUND, GLOBAL, MCC, SPECIAL, PIXEL};
    Mcc* mcc(){return &m_mcc;}
    Frontend* frontend(int i){return m_frontend[i];}
    void configureHW();
    void resetHW(bool resetfe=true);
    void resetFE();
    void setupMaskStageHW(int stage);
    void enableDataTakingHW();
    int setupParameterHW(const char* name, int val); //HW setup
    PAR setParameter(const char* name, int val); // internal setup
    int configureScan(boost::property_tree::ptree* scanOptions);
    ModuleInfo getModuleInfo();
    const float dacToElectrons(int fe, int dac);
    virtual void destroy();
    
protected:
  Frontend *m_frontend[N_FRONTENDS];
  Mcc m_mcc;
  MaskStaging<FEI3::Module>* m_maskStaging;
};

};
#endif
