#ifndef RCFFEI3ADAPTER_HH
#define RCFFEI3ADAPTER_HH

#include <stdint.h>
#include <RCF/RCF.hpp>
#include "rcf/PixelModuleConfig.hh"

RCF_BEGIN(I_RCFFEI3Adapter, "I_RCFFEI3Adapter")
RCF_METHOD_R1(int32_t, RCFdownloadConfig, ipc::PixelModuleConfig)
RCF_END(I_RCFFEI3Adapter)

    class RCFFEI3Adapter 
    {
      virtual int32_t RCFdownloadConfig(ipc::PixelModuleConfig config)=0;

    };

#endif
