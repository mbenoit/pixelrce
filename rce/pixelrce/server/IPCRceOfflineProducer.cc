#include <ipc/core.h>
#include "server/RceOfflineProducer.hh"
#include "server/IPCController.hh"
#include "util/HistoManager.hh"
#include <boost/program_options.hpp>
#include <netdb.h>


int main ( int argc, char ** argv )
{
  int rce;
  std::string hostname;
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("rce,r", boost::program_options::value<int>(&rce)->required(), "RCE to connect to")
    ("runcontrol,d", boost::program_options::value<std::string>(&hostname)->required(), "Runcontrol hostname")
    ("partition,p", boost::program_options::value<std::string>(), "partition to work in.");
  boost::program_options::variables_map vm;
  try{
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
  }catch(boost::program_options::error& e){
    std::cout<<std::endl<<"ERROR: "<<e.what()<<std::endl<<std::endl;
    std::cout<<desc<<std::endl;
    exit(0);
  }

  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }
 
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
        return 1;
    }
   
   std::string rchost;
   hostent * ipAddrContainer = gethostbyname(hostname.c_str());
   if (ipAddrContainer != 0) {
     int nBytes;
     if (ipAddrContainer->h_addrtype == AF_INET) nBytes = 4;
     else if (ipAddrContainer->h_addrtype == AF_INET6) nBytes = 6;
     else {
       std::cout << "Unrecognized IP address type. Run not started." 
		 << std::endl;
       exit(0);
     }
     std::stringstream ss("tcp://");
     ss << "tcp://"; 
     for (int i = 0, curVal; i < nBytes; i++)
       {
	 curVal = static_cast<int>(ipAddrContainer->h_addr[i]);
	 if (curVal < 0) curVal += 256;
	 ss << curVal;
	 if (i != nBytes - 1) ss << ".";
       }
     ss<<":44000";
     rchost=ss.str();
   }else{
     std::cout<<"Bad IP address. Exiting."<<std::endl;
     exit(0);
   }
   const char *p_name=getenv("TDAQ_PARTITION");
   if(p_name==NULL) p_name="rcetest";
   if(vm.count("partition"))p_name=vm["partition"].as<std::string>().c_str();
   IPCPartition   p(p_name );
   IPCController controller(p);
   new HistoManager(0);
   RceOfflineProducer producer("RceOfflineProducer", rchost.c_str(), &controller, rce);
   sleep(100000000);
}
