#ifndef RCENAME_HH
#define RCENAME_HH

#include <string>

class RceName{
public:
  enum port{MAINPORT=50000, CBPORT=50001, SUBPORT=50002};
  static int getRceNumber();
private:
  static int m_number;
  static bool m_initialized;
};

#endif
