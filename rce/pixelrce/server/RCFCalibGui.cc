
#include "server/CalibGui.hh"
#include "server/RCFController.hh"
#include "server/RCFHistoController.hh"
#include <boost/program_options.hpp>
#include <TROOT.h>
#include <TStyle.h>
#include "TApplication.h"
#include <iostream>
int main(int argc, char **argv){
//
// Parse arguments
//
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);
  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }
  RCF::RcfInitDeinit rcfInit;
  AbsController* controller=new RCFController;
  AbsHistoController* hcontroller=new RCFHistoController;
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);

  TApplication theapp("app",&argc,argv);
  new CalibGui(controller, hcontroller, gClient->GetRoot(),800,735);
  theapp.Run();
  return 0;
}

