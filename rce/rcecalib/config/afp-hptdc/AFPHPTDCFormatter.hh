#ifndef AFPHPTDCFORMATTER_HH
#define AFPHPTDCFORMATTER_HH

#include "rcecalib/config/AbsFormatter.hh"

class AFPHPTDCFormatter:public AbsFormatter{
public:
  AFPHPTDCFormatter(int id);
  virtual ~AFPHPTDCFormatter();
  int decode(const unsigned* data, int size, unsigned* parsedData, int &parsedsize, int &nL1A);
  void configure(boost::property_tree::ptree* scanOptions);

private:
  float m_calib[2][12][1024];
};
#endif
