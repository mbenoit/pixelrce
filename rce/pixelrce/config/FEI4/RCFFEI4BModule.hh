#ifndef RCFFEI4BMODULE_HH
#define RCFFEI4BMODULE_HH

#include "config/FEI4/FEI4BModule.hh"
#include "rcf/RCFFEI4BAdapter.hh"

class AbsFormatter;

namespace FEI4{
class RCFFEI4BModule: public RCFFEI4BAdapter, public FEI4::FEI4BModule {
public:
  RCFFEI4BModule(RCF::RcfServer &server, const char * name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt);
  ~RCFFEI4BModule();
  int32_t RCFdownloadConfig(ipc::PixelFEI4BConfig config);    
  void RCFsetChipAddress(uint32_t addr);
  uint32_t RCFwriteHWglobalRegister(int32_t reg, uint16_t val);
  uint32_t RCFreadHWglobalRegister(int32_t reg, uint16_t &val);
  uint32_t RCFwriteDoubleColumnHW(uint32_t bit, uint32_t dcol, std::vector<uint32_t>  data, std::vector<uint32_t>& retv);
  uint32_t RCFreadDoubleColumnHW(uint32_t bit, uint32_t dcol, std::vector<uint32_t> &retv);
  uint32_t RCFclearPixelLatches();
  int32_t RCFverifyModuleConfigHW();
private:
  RCF::RcfServer& m_server;

};
};
  

#endif
