#ifndef TOTCALIBDATAPROC_HH
#define TOTCALIBDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include <vector>
#include "util/RceHisto2d.cc"

class TotCalibDataProc: public AbsDataProc{
public:
  TotCalibDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );

  virtual ~TotCalibDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);
protected:

  std::vector<std::vector<RceHisto2d<char, char>*> > m_histo_tot;

  std::vector<int> m_vcal;
  int m_nTrigger;
  int m_nLoops;
  int m_nPoints;
};

#endif
