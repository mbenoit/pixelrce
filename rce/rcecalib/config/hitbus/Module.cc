#include "rcecalib/HW/BitStream.hh"
#include "rcecalib/config/hitbus/Module.hh"
#include "rcecalib/config/FEI4/FECommands.hh"
#include "rcecalib/HW/SerialIF.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/util/exceptions.hh"
#include <iostream>
#include "ers/ers.h"
#include <stdio.h>

namespace Hitbus{

  Module::Module(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt)
    :AbsModule(name, id,inLink, outLink, fmt){
    //    std::cout<<"Module"<<std::endl;
    if(!m_initialized)initialize();
    m_regw=new unsigned short[m_registers.size()];
    for(size_t i=0;i<m_registers.size();i++)m_regw[i]=0;
    m_commands=new FEI4::FECommands;
  }
  Module::~Module(){
    delete [] m_regw;
    delete m_commands;
  }
  void Module::writeHW(){ 
    for(size_t i=0;i<m_registers.size();i++){
      assert(writeRegisterHW(i, m_regw[i])==0);
    }
  }
  unsigned Module::writeRegisterHW(int i, unsigned short val){
    // i is position in vector, not actual address 
    m_regw[i]=val; // set register content
    int addr=m_registers[i].reg;
    int rval=val&((1<<m_registers[i].width)-1);
    writeRegisterRawHW(addr, rval);
    return 0;
  }
  void Module::writeRegisterRawHW(int i, unsigned short val){
    BitStream *bs=new BitStream;
    m_commands->writeGlobalRegister(bs, 47, (i<<8) | val);
    SerialIF::send(bs, SerialIF::WAITFORDATA); //Write register
    delete bs;
  }

  void Module::configureHW(){
    std::cout<<"Configuring hitbus"<<std::endl;
    printRegisters(std::cout); 
    resetFE();
    writeHW();
  }

  void Module::resetFE(){
    writeRegisterRawHW(0x94, 0xcd); //pulse clear 
    writeRegisterRawHW(1, 1); // core reset on
    writeRegisterRawHW(1, 0); // core reset off
  }

  // set a parameter (global or MCC) in the frontend
  int Module::setupParameterHW(const char* name, int val){
    int retval=1;
    if(m_parameters.find(name)!=m_parameters.end()){
      retval=0;
      writeRegisterHW(m_parameters[name], val);
    }
    return retval;
  }

  int Module::configureScan(boost::property_tree::ptree *scanOptions){
    int retval=0;
    // do nothing for now
    return retval;
  }
  void Module::destroy(){
    delete this;
  }
  ModuleInfo Module::getModuleInfo(){
    return ModuleInfo(m_name, m_id,m_inLink, m_outLink, N_FRONTENDS, N_ROWS, N_COLS, m_formatter);
  }

  void Module::printRegisters(std::ostream &os){
    for(size_t i=0;i<m_registers.size();i++){
      os<<m_registers[i].name<<": Address="<<m_registers[i].reg<<" Value="<<m_regw[i]<<std::endl;
    }
  }
  void Module::setRegister(const char* name, unsigned short val){
    int reg=findRegister(name);
    if(reg!=-1)m_regw[reg]=val;
  }

  //static functions 
  void Module::addParameter(const char* name, const char* field){
    int regnum=findRegister(field);
    assert(regnum>-1);
    m_parameters[name]=regnum;
  }
  int Module::findRegister(const char* name){
    for(size_t i=0;i<m_registers.size();i++){
      if(m_registers[i].name==name)return i;
    }
    return -1;
  }
  void Module::addRegister(const char* name, int reg, unsigned width){
    RegParams temp;
    temp.reg=reg;
    temp.width=width;
    temp.name=name;
    m_registers.push_back(temp);
  }
  std::vector<RegParams> Module::m_registers;
  std::map<std::string, int> Module::m_parameters;
  bool Module::m_initialized = false;

  void Module::initialize(){
    addRegister("bpm", 2, 1);
    addRegister("delay_tam1", 5, 7);
    addRegister("delay_tam2", 6, 7);
    addRegister("delay_tam3", 7, 7);
    addRegister("delay_tbm1", 8, 7);
    addRegister("delay_tbm2", 9, 7);
    addRegister("delay_tbm3", 10, 7);
    addRegister("bypass_delay", 17, 1);
    addRegister("clock", 21, 8);
    addRegister("function_A", 18, 8);
    addRegister("function_B", 19, 8);

    m_initialized=true;
  }
};

