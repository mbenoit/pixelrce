
#include "rcecalib/server/IPCHistoController.hh"
#include "IPCScanRootAdapter.hh"
#include "ers/ers.h"
#include <oh/OHIterator.h>
#include <oh/OHServerIterator.h>
#include <oh/OHProviderIterator.h>
#include <oh/OHRootReceiver.h>
#include "TH1D.h"
#include "TH2D.h"
#include "TH2C.h"
#include "TH2S.h"

class HistoReceiver : public OHRootReceiver {
public:
  void clearList(){
    m_list.clear();
  }
  void receive( OHRootHistogram & h )
  {
    m_list.push_back(h.histogram.get());
    h.histogram.release(); // we ask the histogram auto_ptr to drop ownership
	// for the histogram since it will be kept by ROOT 
  }
  std::vector<TH1*>& getHistos(){return m_list;}
private:
  std::vector<TH1*> m_list;
};

IPCHistoController::IPCHistoController(IPCPartition &p):m_partition(p){}

void IPCHistoController::addRce(int rce){
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)return; //RCE has already been added.
  m_rces.push_back(rce);
}

void IPCHistoController::removeAllRces(){
  m_rces.clear();
}

// std::vector<TH1*> IPCHistoController::getHistosFromIS(const char* reg){
//   HistoReceiver hr;
//   try{
//     OHProviderIterator pii( m_partition, "RceIsServer"); 
//     while(pii++){
//       OHIterator it( m_partition, "RceIsServer", pii.name(), reg);
//       while ( it++ ){
// 	it.retrieve( hr );
//       }
//     }
//   }catch(daq::is::InvalidCriteria){
//     std::cout<<"Invalid object"<<std::endl;
//   }catch(daq::is::RepositoryNotFound){
//     std::cout<<"Object not found"<<std::endl;
//   }catch ( daq::ipc::InvalidPartition & ex ){
//     std::cout<<"partition does not exist"<<std::endl;
//   }catch ( daq::is::Exception & ex ){
//     std::cout<<"IS exception"<<std::endl;
//   }catch(...){
//     std::cout<<"Something else went wrong"<<std::endl;
//   }
//   return hr.getHistos();
// }

std::vector<TH1*> IPCHistoController::getHistosFromIS(const char* reg){
  HistoReceiver hr;
  try{
    OHHistogramIterator it( m_partition, "RceIsServer", ".*", reg);
    while ( it++ ){
      it.retrieve( hr );
      if(hr.getHistos().back()!=0)
	hr.getHistos().back()->SetName(it.name().c_str());
    }
  }catch(daq::is::InvalidCriteria){
    std::cout<<"Invalid object"<<std::endl;
  }catch(daq::is::RepositoryNotFound){
    std::cout<<"Object not found"<<std::endl;
  }catch ( daq::ipc::InvalidPartition & ex ){
    std::cout<<"partition does not exist"<<std::endl;
  }catch ( daq::is::Exception & ex ){
    std::cout<<"IS exception"<<std::endl;
  }catch(...){
    std::cout<<"Something else went wrong"<<std::endl;
  }
  return hr.getHistos();
}

std::vector<std::string> IPCHistoController::getHistoNames(const char* reg){
  std::vector<std::string> retvect;
  ipc::StringVect_var stringvect;
  ipc::IPCScanRootAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "scanRoot_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCScanRootAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      stringvect=handle -> IPCgetHistoNames(reg);
      
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
    
    
    for(unsigned int i=0;i<stringvect->length();i++){
      retvect.push_back(std::string(stringvect[i]));
    }
  }
  return retvect;
}

std::vector<std::string> IPCHistoController::getPublishedHistoNames(){
  std::vector<std::string> retvect;
  ipc::StringVect_var stringvect;
  ipc::IPCScanRootAdapter_var handle;
  char cfa[128];
  for(size_t i=0;i<m_rces.size();i++){
    sprintf(cfa, "scanRoot_RCE%d", m_rces[i]);
    try {
      handle = m_partition.lookup<ipc::IPCScanRootAdapter>( cfa );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
    try {
      stringvect=handle -> IPCgetPublishedHistoNames();
      
    }
    catch(CORBA::Exception & ex) {
      std::cerr<<"Corba exception "<<ex._name()<<std::endl;
    }
    
    
    for(unsigned int i=0;i<stringvect->length();i++){
      retvect.push_back(std::string(stringvect[i]));
    }
  }
  return retvect;
  }



bool IPCHistoController::clearISServer(std::string regex, bool verbose)
{
  try
    {
      OHServerIterator sit( m_partition, "RceIsServer" );
      if ( verbose )
        {
	  std::cout  << "Partition \"" << m_partition.name()
		     << "\" contains " << sit.entries()
		     << " IS server(s):" << std::endl;
        }
      
      while ( sit++ )
        {
	  try {
	    OHHistogramIterator hit(m_partition, sit.name(), ".*", regex);
	    
	    if ( hit.entries() )
	      {
		if ( verbose ){
		  std::cout << "removing " << hit.entries()
			    << " histogram(s) from the '" << sit.name() << "' server ... ";
		}
		
		hit.removeAll();
		
		if ( verbose ){
		  std::cout << "done" << std::endl;
		}
	      }
	  }
	  catch( ers::Issue & ex )
	    {
	      ers::error( ex );
	      return false;
	    }
	}
    }
  catch ( ers::Issue & ex )
    {
      ers::error( ex );
      return false;
    }
  
  return true;
  
}

