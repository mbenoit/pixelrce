#ifndef MODULEGROUPFEI3_HH
#define MODULEGROUPFEI3_HH
#include "config/AbsModuleGroup.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include <vector>

namespace FEI3{

  class Module;

class ModuleGroup: public AbsModuleGroup{
public:
  ModuleGroup():AbsModuleGroup(){};
  virtual ~ModuleGroup(){}
  void addModule(Module* module);
  void deleteModules();
  int setupParameterHW(const char* name, int val, bool bcok);
  int setupMaskStageHW(int stage);
  void configureModulesHW();
  int verifyModuleConfigHW();
  void resetErrorCountersHW();
  int configureScan(boost::property_tree::ptree *scanOptions);
  void resetFE();
  void enableDataTakingHW();
  unsigned getNmodules(){return m_modules.size();}
private:
  void switchToConfigModeHW();
  std::vector<Module*> m_modules;

};
}

#endif
