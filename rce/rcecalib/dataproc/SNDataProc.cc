#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/dataproc/SNDataProc.hh"
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/config/FormattedRecord.hh"
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/profiler/Profiler.hh"
#include "rcecalib/config/FEI4/FEI4BRecord.hh"

int SNDataProc::fit(std::string fitfun) {
  return 0;
}

int SNDataProc::processData(unsigned link, unsigned *buffer, int buflen){
  //m_timer.Start();
  if(buflen==0)return 0;
  int nL1A=0;
  int module=m_linkToIndex[link];
  unsigned char* bytepointer=(unsigned char*)buffer;
  unsigned char* last=bytepointer+buflen*sizeof(unsigned)-3;
  FEI4::FEI4BRecord rec;
  while(bytepointer<=last){
    rec.setRecord(bytepointer); 
    if(rec.isData()){ //includes empty record type
      //should not happen
    } else if(rec.isDataHeader()){
      //should not happen, either
    }else if(rec.isServiceRecord()){
      //no service records
    }else if(rec.isValueRecord()){ //val rec without addr rec
      int value=rec.getValue();
      std::cout<<"Value record! Value="<<rec.getValue()<<std::endl;
      m_histo_occ[module]->set(0, value);
      nL1A++;
    }else if(rec.isAddressRecord()){ // address record
      // Ignore address records
      //std::cout<<"FE "<<m_info[module].getId()<<": Address record for ";
      //if(rec.isGlobal())std::cout<<" global register ";
      //else std::cout<<" shift register ";
      //std::cout<<rec.getAddress()<<std::endl;
      //std::cout<<"This should not happen."<<std::endl;
    }else{
      std::cout<<"FE "<<m_info[module].getId()<<": Unexpected record type: "<<std::hex<<rec.getUnsigned()<<std::dec<<std::endl;
      //return FEI4::FEI4ARecord::BadRecord;
      return 0;
    }
    bytepointer+=3;
  }
  //m_timer.Stop();
  return nL1A;
}

SNDataProc::SNDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif) {
  std::cout<<"SN Data Proc"<<std::endl;
  try{ //catch bad scan option parameters
    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      char name[128];
      char title[128];
      unsigned int moduleId=m_info[module].getId();
      std::string moduleName=m_info[module].getName();
      /* retrieve scan points - Vcal steps in this case */
      sprintf(name, "Mod_%d_SN", moduleId);
      sprintf(title, "Module %d at %s Serial Number", moduleId, moduleName.c_str());
      m_histo_occ.push_back(new RceHisto1d<int, int>(name, title, 1, 0, 1));
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
    ers::error(issue);
    ERS_ASSERT(0);
  }
}

SNDataProc::~SNDataProc(){
  for (size_t module=0;module<m_histo_occ.size();module++){
    delete m_histo_occ[module];
  }

}
  

