
#include "scanctrl/ScanRoot.hh"
#include "scanctrl/ActionFactory.hh"
#include "dataproc/AbsDataProc.hh"
#include "dataproc/AbsDataHandler.hh"
#include "dataproc/DataProcFactory.hh"
#include "scanctrl/Scan.hh"
#include "config/ConfigIF.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "util/DataCond.hh"

#include <string>
#include <iostream>


ScanRoot::ScanRoot(ConfigIF* confif, Scan *scan):
  m_configIF(confif),m_dataProc(0),m_scan(scan),m_formatter(0), m_handler(0),m_receiver(0){
}
ScanRoot::~ScanRoot(){
}

int ScanRoot::configureScan(boost::property_tree::ptree *scanOptions){
  //std::cout<<"Start ScanRoot configureScan"<<std::endl;
  int retval=0;
  //print out configuration
  //write_info(std::cout,*scanOptions);

  // set up pixel modules
  retval+=m_configIF->configureScan(scanOptions);
  // set up data processor
  printf("configIF configured\n");
  DataProcFactory dfac;
  try{ //catch bad scan option parameters
    delete m_dataProc;
    printf("Deleted old dataproc\n");
    m_dataProc=0;
    std::string dataProc = scanOptions->get<std::string>("DataProc");
    m_dataProc=dfac.createDataProcessor(dataProc.c_str(),m_configIF, scanOptions);
    printf("Created dataproc of type %s\n", dataProc.c_str());
    std::string ptype = scanOptions->get<std::string>("DataHandler");
    delete m_handler;
    m_handler=0;
    m_handler=dfac.createDataHandler(ptype.c_str(),m_dataProc, m_scan->getDataCond(), m_configIF, scanOptions);
    printf("Created data handler of type %s\n", ptype.c_str());
    std::string rtype = scanOptions->get<std::string>("Receiver");
    delete m_receiver;
    m_receiver=0;
    m_receiver=dfac.createReceiver(rtype.c_str(),m_handler, scanOptions);
    printf("Created receiver of type %s\n", rtype.c_str());
    assert(m_handler!=0);

  // set up Scan Loops
    // The action factory contains all available loop and end-of-loop actions.
    // These can depend on m_config, m_dataProc, m_scan. Therefore created here.
    ActionFactory af(m_configIF, m_dataProc, m_scan);
    m_scan->configureScan(scanOptions, af, m_handler);

    std::cout<<"Setup done"<<std::endl;
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    retval=1;
  }
  assert(m_dataProc!=0);
  return retval;
}

