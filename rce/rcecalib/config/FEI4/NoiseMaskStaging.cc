#include "rcecalib/config/FEI4/NoiseMaskStaging.hh"
#include "rcecalib/config/FEI4/Module.hh"

namespace FEI4{
  
  NoiseMaskStaging::NoiseMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
  }
  
  void NoiseMaskStaging::setupMaskStageHW(int maskStage){
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
        
    /*
      this is the code to reset bits.  Don't understand everything it's doing
      m_module->global()->setField("Colpr_Mode", 3, GlobalRegister::SW);
      for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
        if(m_masks.oMask&(1<<i)){
          m_module->pixel()->setBitAll(i,0);
          m_module->writeDoubleColumnHW(i,i,0,0);
        }
      }
    */
    
    
    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
      if(m_masks.iMask&(1<<i)){

	//turn on this bit for every pixel on the FE
	for(int dcolStage=0; dcolStage<40; dcolStage++){

	  pixel->setupMaskStageCol(dcolStage*2+1, i, 0, 1);
	  if(dcolStage==39)pixel->setupMaskStageCol(80, i, 0, 1);
	
	  m_module->writeDoubleColumnHW(i, i, dcolStage, dcolStage);
 
	  if(dcolStage!=0){
	    pixel->setupMaskStageCol(dcolStage*2, i, 0, 1);
	    m_module->writeDoubleColumnHW(i, i, dcolStage-1, dcolStage-1); 
	    global->setField("Colpr_Addr", dcolStage, GlobalRegister::HW); //this line not necessary, since not injecting
	  }
	}
	
      } //end if(m_masks.iMask&(1<<i))
    } //end for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++)
  
  }

}
