#ifndef ACTIONFACTORY_HH
#define ACTIONFACTORY_HH

#include <string>
#include <boost/property_tree/ptree_fwd.hpp>

#include "scanctrl/LoopAction.hh"
#include "scanctrl/EndOfLoopAction.hh"

class Scan;
class ConfigIF;
class AbsDataProc;

class ActionFactory{
public:
  ActionFactory(ConfigIF* cif, AbsDataProc* proc,Scan* scan):
    m_configIF(cif),m_dataProc(proc),m_scan(scan){}
  ~ActionFactory(){};
  LoopAction* createLoopAction(std::string action,boost::property_tree::ptree* pt=0);
  EndOfLoopAction* createEndOfLoopAction(std::string action, std::string fitfunc);
private:
  ConfigIF *m_configIF;
  AbsDataProc *m_dataProc;
  Scan* m_scan;
};
#endif
