#ifndef RCEMSG_HH
#define RCEMSG_HH

#include "Callback.hh"
#include <omnithread.h>

class RceCallback{
public:
  enum Priority {LOW, MEDIUM, HIGH};
  static RceCallback* instance();
  void sendMsg(ipc::Priority pr, const ipc::CallbackParams *msg);
  void shutdown();
  void configure( ipc::Callback_ptr cb, ipc::Priority pr);
private:
  RceCallback();
  ipc::Callback_var m_cb;
  ipc::Priority m_pr;
  bool m_configured;
  static omni_mutex m_guard;
  static RceCallback* m_instance;
  
};

#endif
