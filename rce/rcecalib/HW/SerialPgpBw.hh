#ifndef SERIALPGPBW_HH
#define SERIALPGPBW_HH

#include "rcecalib/HW/SerialIF.hh"
#include <vector>

class SerialPgpBw: public SerialIF {
public:
  SerialPgpBw();
protected:
  void Send(BitStream* bs, int opt);
  void SetChannelMask(unsigned linkmask);
  void SetChannelInMask(unsigned linkmask);
  void SetChannelOutMask(unsigned linkmask);
  int EnableTrigger(bool on);
  unsigned SendCommand(unsigned char opcode);
  unsigned WriteRegister(unsigned addr, unsigned val);
  unsigned ReadRegister(unsigned addr, unsigned &val);
  unsigned WriteBlockData(std::vector<unsigned>& data);
  unsigned ReadBlockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec);
  unsigned ReadBuffers(std::vector<unsigned char>& retvec);
};


#endif
