#ifndef CLOSEFILEEOLACTION_HH
#define CLOSEFILEEOLACTION_HH

#include "rcecalib/scanctrl/EndOfLoopAction.hh"
#include "rcecalib/dataproc/AbsDataProc.hh"

class CloseFileEoLAction: public EndOfLoopAction{
public:
  CloseFileEoLAction(std::string name,  AbsDataProc* proc):
    EndOfLoopAction(name),m_dataProc(proc){}
  int execute(){
    std::cout<<"Closing file"<<std::endl;
    return m_dataProc->fit("CloseFile");
  }
private:
  AbsDataProc* m_dataProc;
};

#endif
