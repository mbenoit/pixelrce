//
//      test-server.cc
//
//      test application for IPC library
//
//      Sergei Kolos January 2000
//
//      description:
//              Implements test.accessible interface
//        Creates several implementation objects
//        Performs timing for the publish and withdraw operations
/////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include <ers/ers.h>

#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include <owl/timer.h>
#include <owl/semaphore.h>
#include <cmdl/cmdargs.h>

#include "rcecalib/scanctrl/IPCScan.cc"
#include "rcecalib/scanctrl/IPCScanRoot.cc"
#include "rcecalib/config/IPCConfigIF.cc"
#include "rcecalib/HW/SerialHexdump.hh"
#include "rcecalib/config/IPCModuleFactory.hh"
#include "rcecalib/util/RceName.hh"
#include "rcecalib/scanctrl/Scan.hh"
OWLSemaphore semaphore;

CmdArgBool echo ('e', "echo", "output results to stdout.");

void sig_handler( int sig )
{    
  std::cout << " :: [IPCServer::sigint_handler] the signal " << sig << " received - exiting ... "<<std::endl;
  semaphore.post();
}


//////////////////////////////////////////
//
// Main function
//
//////////////////////////////////////////


int main ( int argc, char ** argv )
{
   CmdArgStr	partition_name	('p', "partition", "partition-name", "partition to work.");
 
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
        return 1;
    }
   
       // Declare command object and its argument-iterator
   CmdLine  cmd(*argv, &partition_name, &echo,  NULL);
   CmdArgvIter  arg_iter(--argc, ++argv);
  
       // Parse arguments

   cmd.parse(arg_iter);
    
   signal( SIGINT , sig_handler );
   signal( SIGTERM, sig_handler );
   

   IPCPartition p((const char*)partition_name);

   //Serial IF
   new SerialHexdump;

   //Module Factory

   ModuleFactory *moduleFactory=new IPCModuleFactory(p);
   
   char name[128];
   sprintf(name, "configIF_RCE%d", RceName::getRceNumber());
   // Config IF
   IPCConfigIF<ipc::single_thread> *cif=new IPCConfigIF<ipc::single_thread>(p, name, moduleFactory);
        
   sprintf(name, "scanCtrl_RCE%d", RceName::getRceNumber());
   IPCScan<ipc::multi_thread> *scan = new IPCScan<ipc::multi_thread>( p, name);   
   sprintf(name, "scanRoot_RCE%d", RceName::getRceNumber());
   IPCScanRoot<ipc::single_thread> * scanroot = new IPCScanRoot<ipc::single_thread>( p, name, (ConfigIF*)cif, (Scan*)scan);   

   sprintf(name, "RCE%d", RceName::getRceNumber());
   new IPCHistoManager(p,"RceIsServer", name);
    //
   std::cout << "ipc_test_server has been started." << std::endl;

   semaphore.wait();
   std::cout << "Shutdown." << std::endl;
  
   cif->_destroy();
   scanroot->_destroy();
   scan->_destroy();
   
   if ( echo )
   {
	std::cout << "Test successfully completed." << std::endl;
   }
      
   return 0;
}
