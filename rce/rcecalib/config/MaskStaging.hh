#ifndef MASKSTAGING_HH
#define MASKSTAGING_HH

#include "rcecalib/config/Masks.hh"
#include <string>

template <typename T>
class MaskStaging{
public:
  MaskStaging(T *module, Masks masks, std::string stagingType):
    m_masks(masks), m_module(module), m_type(stagingType), m_initialized(false){}
  virtual void setupMaskStageHW(int maskStage)=0;
  virtual void clearBitsHW();
  virtual bool cLow();
  virtual bool cHigh();
  void setClow(bool i){m_clow=i;}
  void setChigh(bool i){m_chigh=i;}
  bool initialized(){return m_initialized;}
protected:
  Masks m_masks;
  T* m_module;
  std::string m_type;
  bool m_clow;
  bool m_chigh;
  bool m_initialized;
};
  

#endif
