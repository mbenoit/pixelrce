#include "util/DataCond.hh"
#include "dataproc/DataProcFactory.hh"
#include "dataproc/OccupancyDataProc.hh"
#include "dataproc/RawFei4OccupancyDataProc.hh"
#include "dataproc/NoiseOccupancyDataProc.hh"
#include "dataproc/TotDataProc.hh"
#include "dataproc/TotCalibDataProc.hh"
#include "dataproc/BcidDataProc.hh"
#include "dataproc/SelftriggerDataProc.hh"
#include "dataproc/MultiTrigOccupancyDataProc.hh"
#include "dataproc/MultiTrigNoiseDataProc.hh"
#include "dataproc/MeasurementDataProc.hh"
#include "dataproc/HitorDataProc.hh"
#include "dataproc/CosmicDataProc.hh"
#include "dataproc/DelayScanDataProc.hh"
#include "dataproc/DelayScanDataHandler.hh"
#include "dataproc/CosmicDataHandler.hh"
#include "dataproc/SimpleDataHandler.hh"
#include "dataproc/RegularScanDataHandler.hh"
#include "dataproc/RegularScanRawDataHandler.hh"
#include "dataproc/ModuleCrosstalkRawDataHandler.hh"
#include "dataproc/SimpleReceiver.hh"
#include "dataproc/MeasurementReceiver.hh"
#include "dataproc/PgpReceiver.hh"
#include "dataproc/PgpCosmicReceiver.hh"
#include "dataproc/PgpCosmicNwReceiver.hh"
#include "dataproc/TemperatureDataProc.hh"
#include "dataproc/MonleakDataProc.hh"
#include "dataproc/SNDataProc.hh"
#include "dataproc/Fei4RegisterTestDataProc.hh"

#ifdef __rtems__
#include <boost/property_tree/ptree.hpp>
#endif
#include <iostream>


AbsDataProc* DataProcFactory::createDataProcessor(const char* type, ConfigIF* cif,
						  boost::property_tree::ptree* scanOptions){
  if(std::string(type)=="OCCUPANCY")
    return new OccupancyDataProc(cif, scanOptions);
  if(std::string(type)=="RawFei4Occupancy")
    return new RawFei4OccupancyDataProc(cif, scanOptions);
  if(std::string(type)=="Noisescan")
    return new NoiseOccupancyDataProc(cif, scanOptions);
  if(std::string(type)=="TOT")
    return new TotDataProc(cif, scanOptions);
  if(std::string(type)=="TOTCALIB")
    return new TotCalibDataProc(cif, scanOptions);
  if(std::string(type)=="BCID")
    return new BcidDataProc(cif, scanOptions);
  if(std::string(type)=="Delay")
    return new DelayScanDataProc(cif, scanOptions);
  if(std::string(type)=="CosmicData")
    return new CosmicDataProc(cif, scanOptions);
  if(std::string(type)=="Measurement")
    return new MeasurementDataProc(cif, scanOptions);
  if(std::string(type)=="Selftrigger")
    return new SelftriggerDataProc(cif, scanOptions);
  if(std::string(type)=="MultiTrigOccupancy")
    return new MultiTrigOccupancyDataProc(cif, scanOptions);
  if(std::string(type)=="MultiTrigNoise")
    return new MultiTrigNoiseDataProc(cif, scanOptions);
  if(std::string(type)=="Hitor")
    return new HitorDataProc(cif, scanOptions);
  if(std::string(type)=="Temperature")
    return new TemperatureDataProc(cif, scanOptions);
  if(std::string(type)=="Monleak")
    return new MonleakDataProc(cif, scanOptions);
  if(std::string(type)=="SerialNumber")
    return new SNDataProc(cif, scanOptions);
  if(std::string(type)=="Fei4RegisterTest")
    return new Fei4RegisterTestDataProc(cif, scanOptions);
  else 
    return 0;
}


AbsDataHandler* DataProcFactory::createDataHandler(const char* type, AbsDataProc *dataproc, DataCond& datacond, 
						   ConfigIF* cif, boost::property_tree::ptree* scanOptions){
  if(std::string(type)=="RegularScan")
    return new RegularScanDataHandler(dataproc, datacond, cif, scanOptions);
  else if(std::string(type)=="RegularScanRaw") 
    return new RegularScanRawDataHandler(dataproc, datacond, cif, scanOptions);
  else if(std::string(type)=="ModuleCrosstalkRaw") 
    return new ModuleCrosstalkRawDataHandler(dataproc, datacond, cif, scanOptions);
  else if(std::string(type)=="Simple") 
    return new SimpleDataHandler(dataproc, datacond);
  else if(std::string(type)=="DelayScan") 
    return new DelayScanDataHandler(dataproc, datacond);
  else if(std::string(type)=="CosmicData") 
    return new CosmicDataHandler(dataproc, datacond, cif, scanOptions);
  else
    return 0;
}

AbsReceiver* DataProcFactory::createReceiver(const char* type, AbsDataHandler* handler, boost::property_tree::ptree* scanOptions) {
  std::cout<<"Creating receiver of type "<<type<<std::endl;
  if(std::string(type)=="Simple")
    return new SimpleReceiver(handler);
#ifdef __rtems__
  else if(std::string(type)=="Pgp")
    return new PgpReceiver(handler);
  else if(std::string(type)=="PgpCosmic")
    return new PgpCosmicReceiver(handler);
  else if(std::string(type)=="PgpCosmicNw")
    return new PgpCosmicNwReceiver(handler, scanOptions);
  else if(std::string(type)=="Measurement")
    return new MeasurementReceiver(handler, scanOptions);
#endif
  else
    return 0;
}
     
