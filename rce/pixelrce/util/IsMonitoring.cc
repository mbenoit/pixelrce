#include "util/IsMonitoring.hh"
#include <ipc/partition.h>
#include <is/infoT.h>
#include <is/infodictionary.h>

IsMonitoring::IsMonitoring(IPCPartition &p): Monitoring(), m_dict(new ISInfoDictionary(p)){}
IsMonitoring::~IsMonitoring(){
  delete m_dict;
}

void IsMonitoring::Publish(const char* name, float val){
  char fullname[128];
  sprintf(fullname, "RceIsServer.%s", name);
  ISInfoFloat infofloat(val);
  m_dict->checkin(fullname, infofloat);
}
void IsMonitoring::Publish(const char* name, unsigned val){
  char fullname[128];
  sprintf(fullname, "RceIsServer.%s", name);
  ISInfoUnsignedInt infounsigned(val);
  m_dict->checkin(fullname, infounsigned);
}
