#include "CallbackInfo.hh"
#include "TGLabel.h"

CallbackInfo::CallbackInfo(TGLabel* label): m_label(label), m_stage(0), m_mask(0){}
  
void CallbackInfo::updateGui(){
  if(m_mask==0)return;
  char a[128];
  for(int i=0;i<NBITS;i++){
    if((1<<i)&m_mask){
      m_mask=0;
      switch(i){
      case FAILED:
	m_label->SetText("Failed");
	break;
      case DONE:
	m_label->SetText("Done");
	break;
      case VERIFY:
	m_label->SetText("Verifying");
	break;
      case WAIT:
	m_label->SetText("Waiting");
	break;
      case ANALYZE:
	m_label->SetText("Analyzing");
	break;
      case SAVE:
	m_label->SetText("Saving Histos");
	break;
      case STOP:
	m_label->SetText("RCEs done");
	break;
      case DOWNLOAD:
	m_label->SetText("Downloading");
	break;
      case FIT:
	m_label->SetText("Fit");
	break;
      case NEWSTAGE:
	sprintf(a, "%d", m_stage);
	m_label->SetText(a);
	break;
      case RUNNING:
	m_label->SetText("Running");
	break;
      }
      break;
    }
  }
}
