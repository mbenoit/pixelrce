#include "server/HistoViewerGui.hh"
#include "server/PixScan.hh"
#include "TApplication.h"
#include "TGMsgBox.h"
#include "TH1.h"
#include "TKey.h"
#include "TGIcon.h"
#include "TGMenu.h"
#include "TCanvas.h"
#include "TGCanvas.h"
#include "TGListTree.h"
#include "TRootEmbeddedCanvas.h"
#include <TGFileDialog.h>
#include <TROOT.h>
#include <TStyle.h>
#include <boost/regex.hpp>
#include <assert.h>
#include <iostream>
#include <time.h>
#include <sys/stat.h> 
#include <fstream>
#include <list>
#include <pthread.h>
#include <vector>
#include <stdlib.h> 
  
using namespace RCE;

HistoViewerGui::~HistoViewerGui(){
  Cleanup();
}

HistoViewerGui::HistoViewerGui(const char* filename, const TGWindow *p,UInt_t w,UInt_t h)
  : TGMainFrame(p,w,h) {
  
  // connect x icon on window manager
  Connect("CloseWindow()","HistoViewerGui",this,"quit()");

  TGMenuBar *menubar=new TGMenuBar(this,1,1,kHorizontalFrame | kRaisedFrame);
  TGLayoutHints *menubarlayout=new TGLayoutHints(kLHintsTop|kLHintsLeft,0,4,0,0);
  // menu "File"
  TGPopupMenu* filepopup=new TGPopupMenu(gClient->GetRoot());
  //filepopup->AddEntry("&Load Config",LOAD);
  //filepopup->AddEntry("&Save Config",SAVE);
  filepopup->AddSeparator();
  filepopup->AddEntry("&Quit",QUIT);
  menubar->AddPopup("&File",filepopup,menubarlayout);
  TGPopupMenu* plotpopup=new TGPopupMenu(gClient->GetRoot());
  plotpopup->AddEntry("&Save as png",PNG);
  plotpopup->AddEntry("&Savefd as pdf",PDF);
  menubar->AddPopup("&Plot",plotpopup,menubarlayout);
  plotpopup->Connect("Activated(Int_t)","HistoViewerGui",this,"handlePlotMenu(Int_t)");

  AddFrame(menubar, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 0,0,0,2));
  
  filepopup->Connect("Activated(Int_t)","HistoViewerGui",this,"handleFileMenu(Int_t)");

  TGVerticalFrame* datapanel=new TGVerticalFrame(this,1,1, kSunkenFrame);
  // scan panel
  TGHorizontalFrame *datapanel5 = new TGHorizontalFrame(datapanel, 2, 2, kSunkenFrame);
  datapanel->AddFrame(datapanel5,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  AddFrame(datapanel,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  TGVerticalFrame *treepanel = new TGVerticalFrame(datapanel5, 150, 2, kSunkenFrame);
  datapanel5->AddFrame(treepanel,new TGLayoutHints(kLHintsExpandY));
  TGVerticalFrame *plotpanel = new TGVerticalFrame(datapanel5, 2, 2, kSunkenFrame);
  datapanel5->AddFrame(plotpanel,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  m_tgc=new TGCanvas(treepanel, 300,100);
  //TGViewPort* vp=tgc->GetViewPort();
  m_tree=new TGListTree(m_tgc, kHorizontalFrame);
  m_tree->Connect("Clicked(TGListTreeItem*, Int_t)","HistoViewerGui", this, "displayHisto(TGListTreeItem*, Int_t)");
  //tree->AddItem(0,"/");
  treepanel->AddFrame(m_tgc,new TGLayoutHints(kLHintsExpandY));
  m_canvas=new TRootEmbeddedCanvas("Canvas",plotpanel,100,100);
  m_canvas->GetCanvas()->GetPad(0)->SetRightMargin(0.15);
  plotpanel->AddFrame(m_canvas,new TGLayoutHints(kLHintsExpandY|kLHintsExpandX));
  
  SetWindowName("HistoViewer GUI");
  Resize(w,h);
  Layout();
  MapSubwindows();
  MapWindow();
  m_file=new TFile(filename,"");
  TGListTreeItem* root=m_tree->AddItem(0,"Histos");
  m_file->cd();
  fillHistoTree(root);
  updateTree();
}

void HistoViewerGui::quit(){
  m_file->Close();
  gApplication->Terminate(0);
}

void HistoViewerGui::handlePlotMenu(int item){
  if(item==PNG){
    m_canvas->GetCanvas()->SaveAs("c1.png");
  }else if(item==PDF){
    m_canvas->GetCanvas()->SaveAs("c1.pdf");
  }
}

void HistoViewerGui::handleFileMenu(int item){
  if(item==QUIT)quit();
}
void HistoViewerGui::clearTree(){
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  if(root) m_tree->DeleteChildren(root);
  root->SetOpen(false);
  updateTree();
}
void HistoViewerGui::updateTree(){
  int x=m_tgc->GetViewPort()->GetX();
  int y=m_tgc->GetViewPort()->GetY();
  int w=m_tgc->GetViewPort()->GetWidth();
  int h=m_tgc->GetViewPort()->GetHeight();
  m_tree->DrawRegion(x,y,w,h);
}
  
void HistoViewerGui::fillHistoTree(TGListTreeItem* branch){
  const TGPicture *thp = gClient->GetPicture("h1_t.xpm");
  TIter nextkey(gDirectory->GetListOfKeys());
   TKey *key;
   while ((key=(TKey*)nextkey())) {
     TObject *obj = key->ReadObj();
     if(std::string(obj->ClassName())=="TDirectoryFile"){
       gDirectory->cd(key->GetName());
       TGListTreeItem* subdir=m_tree->AddItem(branch,key->GetName());
       fillHistoTree(subdir);
     }else{
       std::string hisname(key->GetName());
       boost::regex re("at ([AC]\\d+-\\d+)");
       boost::cmatch matches;
       if(boost::regex_search(key->GetTitle(), matches, re)){
	 assert(matches.size()>1);
	 std::string match(matches[1].first, matches[1].second);
	 hisname=match+":"+hisname;
       }
       TGListTreeItem* his=m_tree->AddItem(branch,hisname.c_str());
       his->SetPictures(thp, thp);
     }
     delete obj;
   }
   gDirectory->cd("..");
}
void HistoViewerGui::displayHisto(TGListTreeItem* item, int b){
  TGListTreeItem *parent=item->GetParent();
  if(parent==0)return;
  std::string histonames=item->GetText();
  if(histonames[4]==':')histonames=histonames.substr(5);
  const char* histoname=histonames.c_str(); 
  if(std::string(histoname).substr(0,4)=="loop")return;
  if(std::string(histoname).substr(0,8)=="Analysis")return;
  TGListTreeItem *parent1=parent->GetParent();
  if(parent1==0)m_file->cd();
  else{
    TGListTreeItem *parent2=parent1->GetParent();
    if(parent2==0)m_file->cd(parent->GetText());
    else{
      char dir[128];
      sprintf(dir, "%s/%s",parent1->GetText(), parent->GetText());
      m_file->cd(dir);
    }
  }
  m_histo=(TH1*)gDirectory->Get(histoname);
  assert(m_histo!=0);
  m_histo->SetMinimum(0);
  if(m_histo->GetDimension()==1)m_histo->Draw();
  else {
    m_histo->Draw("colz");
  }
  m_canvas->GetCanvas()->Update();
}

  
//====================================================================
int main(int argc, char **argv){
  if(argc!=2){
    std::cout<<"Usage: histoViewer rootfile_name"<<std::endl;
    exit(0);
  }
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);
  const char *filename=argv[1];
  std::cout<<"Filename "<<filename<<std::endl;
  TApplication theapp("app",&argc,argv);
  new HistoViewerGui(filename, gClient->GetRoot(),800,600);
  theapp.Run();
  return 0;
}


