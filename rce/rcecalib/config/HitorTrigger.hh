#ifndef HITORTRIGGER_HH
#define HITORTRIGGER_HH

#include "rcecalib/config/AbsTrigger.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include <string>

class ConfigIF;

  class HitorTrigger: public AbsTrigger{
  public:
    HitorTrigger(ConfigIF* cif);
    ~HitorTrigger(){};
    int sendTrigger();
    int enableTrigger(bool on){return 0;}
    int configureScan(boost::property_tree::ptree* scanOptions);
  private:
    ConfigIF* m_configIF;
  };


#endif
