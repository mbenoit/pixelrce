#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/dataproc/MeasurementDataProc.hh"
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/util/exceptions.hh"
#include "math.h"

int MeasurementDataProc::fit(std::string fitfun) {
  std::cout << "Running: " << fitfun << std::endl;
  if(fitfun=="NORMALIZE" && m_nTrigger>0) {
    float n=(float)m_nTrigger;
    for(int j=0;j<m_nPoints;j++){
      float mean=(*m_histo)(j)/n;
      m_histo->set(j, mean);
      float err=(m_err[j]/n-mean*mean)/n;
      if(err<0)err=0; //prevent rounding errors
      m_histo->setBinError(j, err);
      //	  std::cout<<"Bin content is "<<(*m_histo[module][i])(j)<<std::endl;
      //std::cout<<"Bin error is "<<m_histo[module][i]->getBinError(j)<<std::endl;
    }
  }
  return 0;
}

int MeasurementDataProc::processData(unsigned link, unsigned *data, int size){
  //    std::cout<<"Process data"<<std::endl;
  float val=*((float*)data);
  //std::cout<<"Measured value "<<val<<std::endl;
  m_histo->fill(m_currentBin, val);
  m_err[m_currentBin]+= val*val;
  return 0;
}

MeasurementDataProc::MeasurementDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif){
  try{ //catch bad scan option parameters
    m_nLoops = scanOptions->get<int>("nLoops");
    /* there is at least one parameter loop */
    /* TODO: fix in scan control */
    m_nPoints=1;
    if(m_nLoops>0){         
      m_nPoints=scanOptions->get<int>("scanLoop_0.nPoints");
      for(int i=0;i<m_nPoints;i++) {
	char pointname[10];
	sprintf(pointname,"P_%d",i);
	int vcal=scanOptions->get<int>(std::string("scanLoop_0.dataPoints.")+pointname);
	//std::cout << "point vcal " << vcal << std::endl;
	m_vcal.push_back(vcal);
      }
    }
    m_nTrigger=scanOptions->get<int>("trigOpt.nEvents");

    double width=1;
    if(m_nPoints>1)width=double(m_vcal[m_nPoints-1]-m_vcal[0])/(double)(m_nPoints-1)*0.5;
    m_histo=new RceHisto1d<float, float>("Measurement","Measurement",m_nPoints,m_vcal[0]-width,m_vcal[m_nPoints-1]+width);
    m_histo->setAxisTitle(0,"DAC");
    
    //error array
    m_err=new float[m_nPoints];
    for(int i=0;i<m_nPoints;i++)m_err[i]=0;
  }
  catch(boost::property_tree::ptree_bad_path ex){
    rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
    ers::error(issue);
    ERS_ASSERT(0);
  }
}

MeasurementDataProc::~MeasurementDataProc(){
  delete m_histo;
  delete [] m_err;
}
  

