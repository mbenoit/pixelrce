
#include "config/HitorTrigger.hh"
#include <boost/property_tree/ptree.hpp>
#include "HW/BitStream.hh"
#include "config/ConfigIF.hh"
#include "config/FEI4/FECommands.hh"
#include <iostream>
#include <fstream>
#include <stdlib.h>


HitorTrigger::HitorTrigger(ConfigIF* cif):AbsTrigger(), m_configIF(cif){
    //std::cout<<"Hitor trigger"<<std::endl;
  }
  
int HitorTrigger::configureScan(boost::property_tree::ptree* scanOptions){
  return 0;
}
  
int HitorTrigger::sendTrigger(){

  // read errors
  m_configIF->resetErrorCountersHW();
  m_i++;
  return 0;
}


