#include "config/DummyFormatter.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <stdio.h>

DummyFormatter::DummyFormatter(int id):
    AbsFormatter(id){
}
DummyFormatter::~DummyFormatter(){
}
void DummyFormatter::configure(boost::property_tree::ptree* config){
}

int DummyFormatter::decode (const unsigned int *buffer, int buflen, unsigned* parsedData, int &parsedsize, int &nL1A){
  std::cout<<"Dummy formatter called!"<<std::endl;
  return 0;
}

