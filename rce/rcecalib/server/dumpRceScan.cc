#include <iostream>
#include <boost/regex.hpp>
#include <sys/stat.h> 
#include <string>
#include "rcecalib/server/PixScan.hh"
#include "ScanOptions.hh"
#include <cmdl/cmdargs.h>
#include "PixScanBase.h"
#include "PixEnumBase.h"
int main(int argc,char *argv[])
{
 
 
  CmdArgBool list ('l', "list", "list all scans");
  CmdArgStr flavor ('f',"flavor", "module-flavor","specify module flavor");
  CmdArgStr name("[name]","name of scan");
  CmdLine  cmd(*argv, &list,&flavor,&name, NULL);
  

  CmdArgvIter  arg_iter(--argc, ++argv);
  int scanindex=-1;
  cmd.parse(arg_iter);
  PixLib::EnumFEflavour::FEflavour f; 
  f  =  PixLib::EnumFEflavour::PM_FE_I4A;
  if(!list) {
    if(!strcmp(flavor,"PM_FE_I4A"))  f =  PixLib::EnumFEflavour::PM_FE_I4A; 
    else if(!strcmp(flavor,"PM_FE_I4B"))  f  =  PixLib::EnumFEflavour::PM_FE_I4B  ;
    else if(!strcmp(flavor,"PM_FE_I2"))  f  =  PixLib::EnumFEflavour::PM_FE_I2  ;
    else if(!list) {std::cout << "unkown flavor " << flavor << std::endl;return 1;}
  }
  PixLib::EnumScanType scantype;
  std::map<std::string, int> scannames=scantype.EnumScanTypeMap();
  for (std::map <std::string, int>::const_iterator it = scannames.begin(); it != scannames.end(); ++it){
    std::string scanname=it->first;
    scanindex=it->second;
    if(!name.isNULL()) if(std::string((const char*)name)==scanname) break;
    if(list)  std::cout << scanname << std::endl;
  }
  if(list) return 0;
  std::cout <<"Flavor: " <<flavor << std::endl;
  std::cout <<"ScanName: " <<name << std::endl; 
  std::cout <<"ScanIndex: "<< scanindex<< std::endl; 
 ipc::ScanOptions scanopts;
 RCE::PixScan scan((RCE::PixScan::ScanType) scanindex, f);
 if(scan.presetRCE((RCE::PixScan::ScanType) scanindex, f)==true) {
   
   scan.convertScanConfig(scanopts);
   scan.dump(std::cout,scanopts);	      
 }
  return 0;


}
