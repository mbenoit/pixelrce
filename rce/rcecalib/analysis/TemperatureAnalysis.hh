#ifndef TEMPERATUREANALYSIS_HH
#define TEMPERATUREANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"

class ConfigGui;
class TFile;
class TH2D;
namespace RCE{
  class PixScan;
}

class TemperatureAnalysis: public CalibAnalysis{
public:
  TemperatureAnalysis(): CalibAnalysis(){}
  ~TemperatureAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
