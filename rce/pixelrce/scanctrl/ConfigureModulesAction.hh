#ifndef CONFIGUREMODULESACTION_HH 
#define CONFIGUREMODULESACTION_HH 

#include "scanctrl/LoopAction.hh"
#include "config/ConfigIF.hh"

class ConfigureModulesAction: public LoopAction{
public:
  ConfigureModulesAction(std::string name, ConfigIF* cif):
    LoopAction(name),m_configIF(cif){}
  int execute(int i){
    m_configIF->configureModulesHW();
    return 0;
  }
private:
  ConfigIF* m_configIF;
};

#endif
