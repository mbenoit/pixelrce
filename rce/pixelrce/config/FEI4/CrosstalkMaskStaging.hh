#ifndef CROSSTALKMASKSTAGING_HH
#define CROSSTALKMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{
  class Module;

  class CrosstalkMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    CrosstalkMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
};
  
}
#endif
