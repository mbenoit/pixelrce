#ifndef DATACOND_HH
#define DATACOND_HH

#include <omnithread.h>

class DataCond{
public:
  DataCond(): cond(&mutex), waitingForData(false){}
  omni_mutex mutex ;
  omni_condition cond  ;
  bool waitingForData;
};

#endif
