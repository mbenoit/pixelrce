
ifneq ($(findstring linux,$(tgt_os)),)
libnames := scanctrl 
endif
ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)
 modlibnames := scanctrl 
 endif
ifeq ($(profiler),y)
CPPFLAGS+='-D__PROFILER_ENABLED__'
endif


libsrcs_scanctrl := ActionFactory.cc \
                    LoopFactory.cc \
                    NestedLoop.cc \
                    RegularScanSetup.cc \
                    RegularCfgScanSetup.cc \
                    IfScanSetup.cc \
                    DelayScanSetup.cc \
                    CosmicDataSetup.cc \
                    SelftriggerSetup.cc \
                    NoiseScanSetup.cc \
                    Scan.cc \
                    ScanLoop.cc \
                    ScanRoot.cc \
                    RceCallback.cc 


libincs_scanctrl := $(ers_include_path) \
                    $(owl_include_path) \
                    $(ipc_include_path) \
                    $(is_include_path) \
                    $(oh_include_path) \
                    $(boost_include_path) \
                    $(omniorb_include_path)


                    
                    
                    
                    
                    
                    
            

