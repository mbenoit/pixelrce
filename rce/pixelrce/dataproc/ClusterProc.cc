#include "dataproc/ClusterProc.hh"
#include <stdio.h>

ClusterProc::ClusterProc() {
        cluster_id = 0;
        // Safety cuts
        max_hits_in_cluster = 20;
        max_hits = 3000;
        // Cluster cuts
        bcidCut = 3;
        colCut = 1;
        rowCut = 1;
}

ClusterProc::ClusterProc(RceHisto1d<int, int> *histo_hits, RceHisto1d<int, int> *histo_tot, RceHisto2d<int, int> *histo_tot_size) {
	cluster_id = 0;
        // Safety cuts
        max_hits_in_cluster = 20;
        max_hits = 3000;
        // Cluster cuts
        bcidCut = 3;
        colCut = 1;
        rowCut = 1; 
	m_histo_hits = histo_hits;
	m_histo_tot = histo_tot;
	m_histo_tot_size = histo_tot_size;
}

ClusterProc::~ClusterProc() {
	colVec.clear();
	rowVec.clear();
	totVec.clear();
	bcidVec.clear();
	
	tot_in_cluster.clear();
	hits_in_cluster.clear();
	cluster_with_overflow.clear();
}

void ClusterProc::assignHitHisto(RceHisto1d<int, int> *histo_hits) {
        m_histo_hits = histo_hits;
}

void ClusterProc::assignToTHisto(RceHisto1d<int, int> *histo_tot) {
        m_histo_tot = histo_tot;
}

void ClusterProc::assignToTClusterSizeHisto(RceHisto2d<int, int> *histo_tot_size) {
	m_histo_tot_size = histo_tot_size;
}

void ClusterProc::addHit(unsigned int col, unsigned int row, unsigned int tot, unsigned int bcid) {
	colVec.push_back(col);
	rowVec.push_back(row);
	totVec.push_back(tot);
	bcidVec.push_back(bcid);
}

void ClusterProc::iterativeSeek(unsigned int this_cluster) {
	// not yet implemented
}

void ClusterProc::recursiveSeek(unsigned int this_cluster, unsigned int col, unsigned int row, unsigned int bcid) {
	for(unsigned int i=0; i<colVec.size(); i++) {
		// Safety if for too big clusters
		if (hits_in_cluster[this_cluster] > max_hits_in_cluster) return;

		// Check if hit is in the cluster range and in time
                if ((abs((int)(colVec[i]-col)) <= colCut) &&
                    (abs((int)(rowVec[i]-row)) <= rowCut) && 
                    (abs((int)(bcidVec[i]-bcid)) <= bcidCut)) {
	                // Add it
        	        unsigned int new_col = colVec[i];
	                unsigned int new_row = rowVec[i];
			unsigned int new_bcid = bcidVec[i];
			hits_in_cluster[this_cluster]++;
        	        tot_in_cluster[this_cluster] += totVec[i];
			if (totVec[i]>13) cluster_with_overflow[this_cluster] = true;

			// Erase hit
	                colVec.erase(colVec.begin()+i);
	                rowVec.erase(rowVec.begin()+i);
        	        totVec.erase(totVec.begin()+i);
			bcidVec.erase(bcidVec.begin()+i); 

			// Search from here (questionable which bcid to pick)
			this->recursiveSeek(this_cluster, new_col, new_row, new_bcid);
		}
	}
}

void ClusterProc::clusterize() {
	unsigned int safety_cnt = 0;
	while((!colVec.empty()) && (safety_cnt < max_hits)) {
		// New cluster
		unsigned int this_cluster = cluster_id;
                hits_in_cluster.push_back(0);
                tot_in_cluster.push_back(0);
		cluster_with_overflow.push_back(false);

                // Insert first hit
                unsigned int col = colVec[0];
                unsigned int row = rowVec[0];
		unsigned int bcid = bcidVec[0];
                hits_in_cluster[this_cluster]++;
                tot_in_cluster[this_cluster] += totVec[0];
	
		// Dismiss clusters with overflow ToT
		if(totVec[0] > 13) cluster_with_overflow[this_cluster] = true;

		// Erase hit
		colVec.erase(colVec.begin());
		rowVec.erase(rowVec.begin());
		totVec.erase(totVec.begin());
		bcidVec.erase(bcidVec.begin());

		// Search for more hits
		this->recursiveSeek(this_cluster, col, row, bcid);
		//printf("New cluster with #%d hits and %d total ToT\n", hits_in_cluster[this_cluster], tot_in_cluster[this_cluster]);
		// Next cluster
		cluster_id++;
		safety_cnt++;
	}
	// Fill the histograms
	this->fillHistos();

	// Reset all vectors for the next event
	this->reset();
}

void ClusterProc::fillHistos() {
	if (m_histo_hits != NULL && m_histo_tot != NULL) {
		for (unsigned int i=0; i<hits_in_cluster.size(); i++) {
			if (!cluster_with_overflow[i]) {
				if (m_histo_hits) m_histo_hits->increment(hits_in_cluster[i]);
				if (m_histo_tot) m_histo_tot->increment(tot_in_cluster[i]);
				if (m_histo_tot_size) m_histo_tot_size->increment(tot_in_cluster[i], hits_in_cluster[i]);
			}
		}
	}
}

void ClusterProc::reset() {
	cluster_id = 0;

        colVec.clear();
        rowVec.clear();
        totVec.clear();

        tot_in_cluster.clear();
        hits_in_cluster.clear();
	cluster_with_overflow.clear();
}

