#ifndef FEI4ANALOGMASKSTAGING_HH
#define FEI4ANALOGMASKSTAGING_HH

#include "rcecalib/config/Masks.hh"
#include "rcecalib/config/MaskStaging.hh"

namespace FEI4{

  class Module;
  
  class AnalogMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    AnalogMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_oldDcolStage;
    int m_nStages;
  };
  
}
#endif
