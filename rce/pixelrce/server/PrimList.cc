/////////////////////////////////////////////////////////////////////
// PixScan.cxx
/////////////////////////////////////////////////////////////////////
//
// 14/06/12  Version 1.0 (Author:Jackie Brosamer)
//
//



#include "server/PrimList.hh"

#include <sstream>
#include <iostream>

namespace RCE { 

  //! Constructors
   //Load saved primlist from file 
   /*PrimList::PrimList() {
	m_scans = new std::vector <PixScan*> ();
    }*/



  //! Destructor
  PrimList::~PrimList(){
    for(size_t i=0;i<m_scans.size();i++){
      delete m_scans[i];
      delete m_options[i];
    }
  }

  PixScan* PrimList::getScan(int index) {
    if((unsigned)index<m_scans.size())return m_scans.at(index);
    else return 0;
  }
  std::string PrimList::getScanName(int index){
    if((unsigned)index<m_scannames.size())return m_scannames.at(index);
    else return "";
  }
  std::string PrimList::getPreScript(int index){
    if((unsigned)index<m_preScripts.size())return m_preScripts.at(index);
    else return "";
  }
 std::string PrimList::getPostScript(int index){
    if((unsigned)index<m_postScripts.size())return m_postScripts.at(index);
    else return "";
  }
 std::string PrimList::getTopConfig(int index){
    if((unsigned)index<m_topConfigs.size())return m_topConfigs.at(index);
    else return "";
  }
  ipc::ScanOptions* PrimList::getScanOptions(int index) {
    if((unsigned)index<m_options.size())return m_options.at(index);
    else return 0;
  }
  std::vector<std::string>& PrimList::getDisabledList(int index) {
    assert((unsigned)index<m_disabled.size());
    return m_disabled.at(index);
  }
  std::vector<std::string>& PrimList::getEnabledList(int index) {
    assert((unsigned)index<m_enabled.size());
    return m_enabled.at(index);
  }
  int PrimList::getPause(int index){
    if((unsigned)index<m_scannames.size())return m_pause.at(index);
    else return 0;
  }
  bool PrimList::getUpdateConfig(int index)
  {
	assert((unsigned)index<m_enabled.size());
      return m_updateConfigs.at(index);
  }
  void PrimList::addScan(std::string scanname, PixScan* scan, std::string preScript, std::string postScript, std::vector<std::string> enables, std::vector<std::string> disables, bool updateConfig, std::string topConfig, int pause) {
    m_scannames.push_back(scanname);
    m_preScripts.push_back(preScript);
    m_postScripts.push_back(postScript);
    m_enabled.push_back(enables);
    m_disabled.push_back(disables);
    m_scans.push_back(scan);
    m_updateConfigs.push_back(updateConfig);
    ipc::ScanOptions *scanopt=new ipc::ScanOptions;
    scan->convertScanConfig(*scanopt);
    m_options.push_back(scanopt);
    m_topConfigs.push_back(topConfig);
    m_pause.push_back(pause);
  }

  void PrimList::clearScans()
  {
    for(size_t i=0;i<m_scans.size();i++){
      delete m_scans[i];
      delete m_options[i];
    }
    m_scans.clear();
    m_options.clear();
    m_scannames.clear();
    m_preScripts.clear();
    m_postScripts.clear();
    m_enabled.clear();
    m_disabled.clear();
    m_updateConfigs.clear();
    m_topConfigs.clear();
    m_pause.clear();
  }

  




}




 

