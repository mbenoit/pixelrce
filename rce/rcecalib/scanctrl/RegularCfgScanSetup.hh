#ifndef REGULARCFGSCANSETUP_HH
#define REGULARCFGSCANSETUP_HH

#include "rcecalib/scanctrl/NestedLoop.hh"
#include "rcecalib/scanctrl/LoopSetup.hh"
#include "rcecalib/scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/ConfigIF.hh"

class RegularCfgScanSetup: public LoopSetup{
public:
  RegularCfgScanSetup():LoopSetup(){};
  virtual ~RegularCfgScanSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
