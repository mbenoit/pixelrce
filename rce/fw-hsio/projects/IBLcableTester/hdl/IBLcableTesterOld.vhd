-------------------------------------------------------------------------------
-- Title         : BNL ASIC Test FGPA, Top Level
-- Project       : LCLS Detector, BNL ASIC
-------------------------------------------------------------------------------
-- File          : BnlAsic.vhd
-- Author        : Ryan Herbst, rherbst@slac.stanford.edu
-- Created       : 07/21/2008
-------------------------------------------------------------------------------
-- Description:
-- Top level logic for BNL ASIC test FPGA.
-------------------------------------------------------------------------------
-- Copyright (c) 2008 by Ryan Herbst. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 07/21/2008: created.
-------------------------------------------------------------------------------

LIBRARY ieee;
Library Unisim;
USE ieee. std_logic_1164.ALL;
use ieee. std_logic_arith.all;
use ieee. std_logic_unsigned.all;
USE work.ALL;

entity IBLcableTester is 
   port ( 

      -- PGP Crystal Clock Input, 156.25Mhz
      iPgpRefClkP   : in    std_logic;
      iPgpRefClkM   : in    std_logic;

      -- System clock 125 MHz clock input
      iMainClkP     : in    std_logic;
      iMainClkN     : in    std_logic;

      oClock40      : out std_logic;
      iClock160_p   : in std_logic;
      iClock160_n   : in std_logic;
      oClk160_p   : out std_logic;
      oClk160_n   : out std_logic;
      oClk40_p   : out std_logic;
      oClk40_n   : out std_logic;
      -- PGP Rx/Tx Lines
      iMgtRxN       : in    std_logic;
      iMgtRxP       : in    std_logic;
      oMgtTxN       : out   std_logic;
      oMgtTxP       : out   std_logic;

     -- ATLAS Pixel module pins
      serialin_p     : in std_logic_vector(23 downto 0);
      serialin_n     : in std_logic_vector(23 downto 0);
      serialout_p    : out std_logic_vector(23 downto 0);
      serialout_n    : out std_logic_vector(23 downto 0);
      retclock_p    : in std_logic_vector(23 downto 16);
      retclock_n    : in std_logic_vector(23 downto 16);
      serialoutxtf_p : out std_logic_vector(4 downto 0);
      serialoutxtf_n : out std_logic_vector(4 downto 0);
      serialoutxts_p : out std_logic_vector(4 downto 0);
      serialoutxts_n : out std_logic_vector(4 downto 0);
      doricreset    : out std_logic;
      clkout40      : out std_logic;

      -- Reset button
      iResetInL     : in    std_logic;

      -- LED Display
      oDispClk      : out   std_logic;
      oDispDat      : out   std_logic;
      oDispLoadL    : out   std_logic_vector(1 downto 0);
      oDispRstL     : out   std_logic;

      -- Debug
      oDebug        : out std_logic_vector(15 downto 0);

      -- Misc Signals
      oPdBuff0      : out   std_logic;
      oPdBuff1      : out   std_logic;
      oPdBuff3      : out   std_logic;
      oPdBuff4      : out   std_logic;
      oLemoA        : out   std_logic;
      iLemoB        : in    std_logic;

      -- Transmitter enable
      transDis1   : out   std_logic
   );
end IBLcableTester;


-- Define architecture for top level module
architecture IBLcableTester of IBLcableTester is 

   -- Synthesis control attributes
   attribute syn_useioff    : boolean;
   attribute syn_useioff    of IBLcableTester : architecture is true;
   attribute xc_fast_auto   : boolean;
   attribute xc_fast_auto   of IBLcableTester : architecture is false;
   attribute syn_noclockbuf : boolean;
   attribute syn_noclockbuf of IBLcableTester : architecture is true;

   -- IO Pad components
   component IBUF    port ( O : out std_logic; I  : in  std_logic ); end component;
   component OBUF    port ( O : out std_logic; I  : in  std_logic ); end component;
   component OBUFDS
     generic( IOSTANDARD: STRING:= "LVDS_25";
              SLEW: STRING:="FAST");
     port ( O : out std_logic; OB : out std_logic; I  : in std_logic );
   end component;

   -- Input LVDS with termination
   component IBUFDS 
      generic ( DIFF_TERM : boolean := FALSE;
                IOSTANDARD: STRING := "LVDS_25"); 
      port    ( O : out std_logic; I  : in  std_logic; IB : in std_logic ); 
   end component;

   -- Xilinx global clock buffer component
   component BUFGMUX 
      port ( 
         O  : out std_logic; 
         I0 : in  std_logic;
         I1 : in  std_logic;  
         S  : in  std_logic 
      ); 
   end component;


   -- PGP Clock Generator
   component PgpClkGen 
      generic (
         RefClkEn1  : string  := "ENABLE";  -- ENABLE or DISABLE
         RefClkEn2  : string  := "DISABLE"; -- ENABLE or DISABLE
         DcmClkSrc  : string  := "RefClk1"; -- RefClk1 or RefClk2
         UserFxDiv  : integer := 5;         -- DCM FX Output Divide
         UserFxMult : integer := 4          -- DCM FX Output Divide, 4/5 * 156.25 = 125Mhz
      );
      port (

         -- Reference Clock Pad Inputs
         pgpRefClkInP  : in   std_logic;
         pgpRefClkInN  : in   std_logic;

         -- Power On Reset Input
         ponResetL     : in   std_logic;

         -- Locally Generated Reset
         locReset      : in   std_logic;

         -- Reference Clock To PGP MGT
         -- Use one, See RefClkEn1 & RefClkEn2 Generics
         pgpRefClk1    : out  std_logic;
         pgpRefClk2    : out  std_logic;

         -- Global Clock & Reset For PGP Logic, 156.25Mhz
         pgpClk        : out  std_logic;
         pgpReset      : out  std_logic;

         -- Global Clock & Reset For User Logic, 125Mhz
         userClk       : out  std_logic;
         userReset     : out  std_logic;

         -- Inputs clocks for reset generation connect
         -- to pgpClk and userClk
         pgpClkIn      : in   std_logic;
         userClkIn     : in   std_logic
      );
   end component;


   -- Core Logic
   component IBLcableTesterCore
      port (
         sysClk250    : in  std_logic;
         sysClk125    : in  std_logic;
         sysRst125    : in  std_logic;
         sysRst250    : in  std_logic;
         refClock     : in  std_logic;
         pgpClk       : in  std_logic;
         pgpReset     : in  std_logic;
         mgtRxN       : in  std_logic;
         mgtRxP       : in  std_logic;
         mgtTxN       : out std_logic;
         mgtTxP       : out std_logic;
         serialin     : in  std_logic_vector(23 downto 0);
         serialout    : out std_logic_vector(23 downto 0);
         retclock     : in std_logic_vector(23 downto 16);
         clock160     : in  std_logic;
         clock80      : in  std_logic;
         clock40      : in  std_logic;
         doricreset   : out std_logic;
         resetOut     : out std_logic;
         dispClk      : out std_logic;
         dispDat      : out std_logic;
         dispLoadL    : out std_logic_vector(1 downto 0);
         dispRstL     : out std_logic;
         debug        : out std_logic_vector(15 downto 0)
      );
   end component;
   component clock160
     port (
        CLKIN_N_IN        : in    std_logic; 
          CLKIN_P_IN        : in    std_logic; 
          RST_IN            : in    std_logic; 
          CLKFX_OUT         : out   std_logic; 
          CLKIN_IBUFGDS_OUT : out   std_logic; 
          CLK0_OUT          : out   std_logic; 
          LOCKED_OUT        : out   std_logic);
   end component;
component IDELAY
        generic (IOBDELAY_TYPE : string := "DEFAULT"; --(DEFAULT, FIXED, VARIABLE)
                 IOBDELAY_VALUE : integer := 0 --(0 to 63)
                 );
        port (
              O : out STD_LOGIC;
              I : in STD_LOGIC;
              C : in STD_LOGIC;
              CE : in STD_LOGIC;
              INC : in STD_LOGIC;
              RST : in STD_LOGIC
             );
end component;
--component ila
   --PORT (
     --CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
     --CLK : IN STD_LOGIC;
     --DATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
     --TRIG0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0));
--
--end component;
 --component icon
   --PORT (
     --CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
--
 --end component;


   -- Local signals
   signal resetInL     : std_logic;
   signal tmpClk250    : std_logic;
   signal sysClk125    : std_logic;
   signal sysRst125    : std_logic;
   signal sysRst250    : std_logic;
   signal sysClk250    : std_logic;
   signal refClock     : std_logic;
   signal pgpClk       : std_logic;
   signal clk160     : std_logic;
   signal clk80     : std_logic;
   signal clk40     : std_logic;
   signal pgpReset     : std_logic;
   signal resetOut     : std_logic;
   signal mgtRxN       : std_logic;
   signal mgtRxP       : std_logic;
   signal mgtTxN       : std_logic;
   signal mgtTxP       : std_logic;
   signal dispClk      : std_logic;
   signal dispDat      : std_logic;
   signal dispLoadL    : std_logic_vector(1 downto 0);
   signal dispRstL     : std_logic;
   signal debug        : std_logic_vector(15 downto 0);
   signal sysClk125i   : std_logic;
   signal doricresetb  : std_logic;
   signal mainClk      : std_logic;
   signal clk0      : std_logic;
   signal clkin      : std_logic;
   signal holdrst      : std_logic;
   signal halfclock    : std_logic;
   signal clockfast    : std_logic;
   signal quarterclock : std_logic;
   signal serialoutb   : std_logic_vector(23 downto 0);
   signal serialinb    : std_logic_vector(23 downto 0);
   signal retclockb    : std_logic_vector(23 downto 16);
   signal retclockd    : std_logic_vector(23 downto 16);
   signal retclocki    : std_logic_vector(23 downto 16);
   signal ccontrol: std_logic_vector(35 downto 0);
   signal cdata: std_logic_vector(31 downto 0);
   signal ctrig: std_logic_vector(0 downto 0);

   -- Register delay for simulation
   constant tpd:time := 0.5 ns;

   -- Black Box Attributes
--   attribute syn_noprune : boolean;
--   attribute syn_noprune of chipscope : label is true;
--   attribute syn_noprune of chipscopeicon : label is true;
   attribute syn_black_box : boolean;
   attribute syn_noprune   : boolean;
   attribute syn_black_box of IBUF    : component is TRUE;
   attribute syn_noprune   of IBUF    : component is TRUE;
   attribute syn_black_box of OBUF    : component is TRUE;
   attribute syn_noprune   of OBUF    : component is TRUE;
   attribute syn_black_box of IBUFDS  : component is TRUE;
   attribute syn_noprune   of IBUFDS  : component is TRUE;
   attribute syn_black_box of OBUFDS  : component is TRUE;
   attribute syn_noprune   of OBUFDS  : component is TRUE;
   attribute syn_black_box of BUFGMUX : component is TRUE;
   attribute syn_noprune   of BUFGMUX : component is TRUE;
begin

   -- Reset input
   U_ResetIn: IBUF port map ( I => iResetInL, O => resetInL );

   -- PGP Clock Generator
   U_PgpClkGen: PgpClkGen generic map (
         RefClkEn1  => "ENABLE",
         RefClkEn2  => "DISABLE",
         DcmClkSrc  => "RefClk1",
         UserFxDiv  => 4,
         UserFxMult => 2
      ) port map (
         pgpRefClkInP  => iPgpRefClkP,
         pgpRefClkInN  => iPgpRefClkM,
         ponResetL     => resetInL,
         locReset      => resetOut,
         pgpRefClk1    => refClock,
         pgpRefClk2    => open,
         pgpClk        => pgpClk,
         pgpReset      => pgpReset,
         pgpClkIn      => pgpClk,
         userClk       => sysClk125,
         userReset     => sysRst125,
         userClkIn     => sysClk125
      );

   -- Generate Divided Clock, sample reset
   process ( pgpClk ) begin
      if rising_edge(pgpClk) then
         tmpClk250 <= not tmpClk250 after tpd;
         sysRst250 <= sysRst125     after tpd;
      end if;
   end process;

   -- Global Buffer For 125Mhz Clock
   U_CLK250: BUFGMUX port map (
      O  => sysClk250,
      I0 => tmpClk250,
      I1 => '0',
      S  => '0'
   );

   -- No Pads for MGT Lines
   mgtRxN  <= iMgtRxN;
   mgtRxP  <= iMgtRxP;
   oMgtTxN <= mgtTxN;
   oMgtTxP <= mgtTxP;

   -- LED Display
   U_DispClk    : OBUF   port map ( I  => dispClk      , O => oDispClk      );
   U_DispDat    : OBUF   port map ( I  => dispDat      , O => oDispDat      );
   U_DispLoadL1 : OBUF   port map ( I  => dispLoadL(1) , O => oDispLoadL(1) );
   U_DispLoadL0 : OBUF   port map ( I  => dispLoadL(0) , O => oDispLoadL(0) );
   U_DispRstL   : OBUF   port map ( I  => dispRstL     , O => oDispRstL     );
   U_clkout40   : OBUF   port map ( I  => clk40 , O => clkout40     );


   -- Debug
   U_Debug0     : OBUF   port map ( I  => debug(0)     , O => oDebug(0)   );
   U_Debug1     : OBUF   port map ( I  => debug(1)     , O => oDebug(1)   );
   U_Debug2     : OBUF   port map ( I  => debug(2)     , O => oDebug(2)   );
   U_Debug3     : OBUF   port map ( I  => debug(3)     , O => oDebug(3)   );
   U_Debug4     : OBUF   port map ( I  => debug(4)     , O => oDebug(4)   );
   U_Debug5     : OBUF   port map ( I  => debug(5)     , O => oDebug(5)   );
   U_Debug6     : OBUF   port map ( I  => debug(6)     , O => oDebug(6)   );
   U_Debug7     : OBUF   port map ( I  => debug(7)     , O => oDebug(7)   );
   U_Debug8     : OBUF   port map ( I  => debug(8)     , O => oDebug(8)   );
   U_Debug9     : OBUF   port map ( I  => debug(9)     , O => oDebug(9)   );
   U_Debug10    : OBUF   port map ( I  => debug(10)    , O => oDebug(10)  );
   U_Debug11    : OBUF   port map ( I  => debug(11)    , O => oDebug(11)  );
   U_Debug12    : OBUF   port map ( I  => debug(12)    , O => oDebug(12)  );
   U_Debug13    : OBUF   port map ( I  => debug(13)    , O => oDebug(13)  );
   U_Debug14    : OBUF   port map ( I  => debug(14)    , O => oDebug(14)  );
   U_Debug15    : OBUF   port map ( I  => debug(15)    , O => oDebug(15)  );

--   U_clock40out : OBUF   port map ( I  => quarterclock     , O => oClock40   );
--       U_serialout1  : OBUFDS   generic map (
--                                             IOSTANDARD=>"LVDS_25",
--                                             SLEW=>"FAST")
--                               port map ( I  => mainClk   , O => serialout_p(1) , OB => serialout_n(1)  );
--       U_serialout2  : OBUFDS   generic map (
--                                             IOSTANDARD=>"LVDS_25",
--                                             SLEW=>"FAST")
--                               port map ( I  => clkin   , O => serialout_p(2) , OB => serialout_n(2)  );
--       U_clockout40  : OBUFDS   generic map (
--                                             IOSTANDARD=>"LVDS_25",
----                                             SLEW=>"FAST")
--                               port map ( I  => quarterclock   , O => oClk40_p , OB => oClk40_n  );
--       U_clockout160  : OBUFDS   generic map (
--                                             IOSTANDARD=>"LVDS_25",
--                                             SLEW=>"FAST")
--                               port map ( I  => pgpClk   , O => oClk160_p , OB => oClk160_n  );
--       U_clockin160   : IBUFDS   generic map ( DIFF_TERM=>TRUE,
--                                             IOSTANDARD=>"ULVDS_25")
--                               port map ( I  => iClock160_p  , IB=>iClock160_n   , O => clk160   );

   SERIAL_IO_DATA:
     for I in 0 to 15 generate
       U_serialout  : OBUFDS   generic map (
                                             IOSTANDARD=>"LVDS_25",
                                             SLEW=>"FAST")
                               port map ( I  => serialoutb(I)   , O => serialout_p(I) , OB => serialout_n(I)  );

--   SERIAL_IO_DATA_in:
--     for I in 0 to 15 generate
       U_serialin   : IBUFDS   generic map ( DIFF_TERM=>TRUE,
                                             IOSTANDARD=>"LVDS_25")
                               port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialinb(I)   );
     end generate SERIAL_IO_DATA;
--     end generate SERIAL_IO_DATA_in;

   SERIAL_IO_CMD:
     for I in 16 to 23 generate
--         delayline : IDELAY
--              generic map (
--                           IOBDELAY_TYPE => "FIXED", -- Set to DEFAULT for -- Zero Hold Time Mode
--                           IOBDELAY_VALUE => 10 -- (0 to 63)
--                           )
--              port map (
--                        O => retclockd(I),
--                        I => retclockb(I),
--                        C => '0',
--                        CE => '0',
--                        INC => '0',
--                        RST => '0' 
--                        );
--        --retclockd(I)<= not retclocki(I);
       U_serialout  : OBUFDS   generic map (
                                             IOSTANDARD=>"LVDS_25",
                                             SLEW=>"FAST")
                               port map ( I  => serialoutb(I)   , O => serialout_p(I) , OB => serialout_n(I)  );

       U_serialin   : IBUFDS   generic map ( DIFF_TERM=>TRUE,
                                             IOSTANDARD=>"LVDS_25")
                               port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialinb(I)   );
       U_retclock   : IBUFDS   generic map ( DIFF_TERM=>TRUE,
                                             IOSTANDARD=>"LVDS_25")
                               port map ( I  => retclock_p(I)  , IB=>retclock_n(I)   , O => retclockb(I)   );
  end generate SERIAL_IO_CMD;

  CROSSTALK:
     for I in 0 to 4 generate
       U_xtalkf  : OBUFDS   generic map (
         IOSTANDARD=>"LVDS_25",
         SLEW=>"FAST")
         port map ( I  => serialoutb(15)   , O => serialoutxtf_p(I) , OB => serialoutxtf_n(I)  );

       U_xtalks  : OBUFDS   generic map (
         IOSTANDARD=>"LVDS_25",
         SLEW=>"FAST")
         port map ( I  => serialoutb(23)   , O => serialoutxts_p(I) , OB => serialoutxts_n(I)  );
       
     end generate CROSSTALK;

   U_doric      : OBUF   port map ( I  => doricresetb   , O => doricreset );
   U_link       : OBUF   port map ( I  => '0'   , O => transDis1 );


   -- FPGA Core
    U_IBLcableTesterCore: IBLcableTesterCore port map (
       sysClk250  => sysClk250,  sysRst250 => sysRst250,
       sysClk125  => sysClk125,  sysRst125 => sysRst125,
       refClock   => refClock,   pgpClk    => pgpClk,
       pgpReset   => pgpReset,   mgtRxN    => mgtRxN,
       mgtRxP     => mgtRxP,     mgtTxN    => mgtTxN,
       mgtTxP     => mgtTxP,     serialin  => serialinb,
       serialout  => serialoutb, retclock => retclockb,
       clock160 => pgpClk, 
       clock80   => sysClk250, clock40 => sysClk125,
       doricreset => doricresetb,       resetOut  => resetOut,
       dispClk    => dispClk,    dispDat   => dispDat,
       dispLoadL  => dispLoadL,  dispRstL  => dispRstL,
       debug     => debug
    );
   --sysClk125i <= not sysClk125;
   sysClk125i<=debug(0);
--   U_clock160: clock160 port map(
--     CLKIN_N_IN => iMainClkN,
--     CLKIN_P_IN => iMainClkP,
--     RST_IN => sysRst125,
--     CLKFX_OUT => open,
--     CLKIN_IBUFGDS_OUT => clkin,
--     CLK0_OUT => clk0,
--     LOCKED_OUT => open);
   process(pgpClk)
   begin
     if(pgpClk'event and pgpClk='1') then
       halfclock<= not halfclock;
     end if;
   end process;
   process(halfclock)
   begin
     if(halfclock'event and halfclock='1') then
       quarterclock<= not quarterclock;
     end if;
   end process; 
   process(pgpClk)
   begin
     if(pgpClk'event and pgpClk='1') then
       clk80<= not clk80;
     end if;
   end process;
   process(clk80)
   begin
     if(clk80'event and clk80='1') then
       clk40<= not clk40;
     end if;
   end process;
 --cdata(7 downto 0)<=serialinb(7 downto 0);
 --cdata(31 downto 8) <=(others=>'0');
 --ctrig(0)<='0';
 --chipscope : ila
   --port map (
     --CONTROL => ccontrol,
     --CLK => mainClk,
     --DATA => cdata,
     --TRIG0 => ctrig);
 --chipscopeicon : icon
   --port map (
     --CONTROL0 => ccontrol);

end IBLcableTester;
