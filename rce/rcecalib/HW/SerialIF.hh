#ifndef SERIALIF_HH
#define SERIALIF_HH
#include "rcecalib/HW/BitStream.hh"
#include <vector>


class SerialIF{
public:
  enum Options {DONT_CLEAR=1, BYTESWAP=4, WAITFORDATA=8};
  enum Rate {M40, M80, M160, M320};
  static void send(BitStream *bs, int opt=0);
  static void setChannelInMask(unsigned linkmask);
  static void setChannelOutMask(unsigned linkmask);
  static void destroy();
  static int enableTrigger(bool on);
  static void disableOutput();
  static void setOutputMode(unsigned mode);
  static void setDataRate(Rate rate);
  static unsigned sendCommand(unsigned char opcode);
  static unsigned writeRegister(unsigned addr, unsigned val);
  static unsigned readRegister(unsigned addr, unsigned& val);
  static unsigned writeBlockData(std::vector<unsigned>& data);
  static unsigned readBlockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec);
  static unsigned readBuffers(std::vector<unsigned char>& retvec);
protected:
  SerialIF();
  virtual ~SerialIF(){};
  virtual void Send(BitStream *bs, int opt )=0;
  virtual void SetChannelInMask(unsigned linkmask)=0;
  virtual void SetChannelOutMask(unsigned linkmask)=0;
  virtual int EnableTrigger(bool on)=0;
  virtual void DisableOutput(){}
  virtual void SetOutputMode(unsigned mode){}
  virtual void SetDataRate(Rate rate){};
  virtual unsigned SendCommand(unsigned char opcode){return (unsigned)-1;}
  virtual unsigned WriteRegister(unsigned addr, unsigned val){return (unsigned)-1;}
  virtual unsigned ReadRegister(unsigned addr, unsigned &val){return (unsigned)-1;}
  virtual unsigned WriteBlockData(std::vector<unsigned>& data){return (unsigned)-1;}
  virtual unsigned ReadBlockData(std::vector<unsigned>& data,std::vector<unsigned>& retvec ){return (unsigned)-1;}
  virtual unsigned ReadBuffers(std::vector<unsigned char>& retvec){return(unsigned)-1;}
  static SerialIF* m_serial;
};
  

#endif
