file(GLOB IdlSources "*.idl")
set(OMNI_FLAGS "-I$ENV{TDAQ_INST_PATH}/include" -I${CMAKE_CURRENT_SOURCE_DIR} )
foreach(IdlFile ${IdlSources})
   get_filename_component(IdlFile_Name_WE ${IdlFile} NAME_WE)
   get_filename_component(IdlFile_Name ${IdlFile} NAME)
   set(SkFile "${IdlFile_Name_WE}SK.cc")
   list(APPEND SkFiles ${SkFile})
   set(HeaderFile  "${IdlFile_Name_WE}.hh")
   add_custom_command (OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${SkFile}
          ${CMAKE_CURRENT_BINARY_DIR}/${HeaderFile} COMMAND omniidl -bcxx ${OMNI_FLAGS}
          ${CMAKE_CURRENT_SOURCE_DIR}/${IdlFile_Name} DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${IdlFile_Name})
endforeach(IdlFile)
include_directories(${CMAKE_CURRENT_BINARY_DIR} $ENV{TDAQ_INST_PATH}/include  $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/include/ipc ${CMAKE_CURRENT_SOURCE_DIR}  $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/include )
if(${ARCHITECTURE} STREQUAL "gen1rtems")
add_library(idl STATIC ${SkFiles})
else()
add_library(idl SHARED ${SkFiles})
endif()

