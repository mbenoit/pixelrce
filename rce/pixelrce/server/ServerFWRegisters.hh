#ifndef SERVERFWREGISTERS_HH
#define SERVERFWREGISTERS_HH

#include "config/FWRegisters.hh"

class AbsController;

class ServerFWRegisters: public FWRegisters{
public:
  ServerFWRegisters(AbsController* controller): m_controller(controller){}
  virtual void writeRegister(int rce, unsigned reg, unsigned val);
  virtual unsigned readRegister(int rce, unsigned reg);
  virtual void sendCommand(int rce, unsigned opcode);
private:
  AbsController* m_controller;
};
#endif
