LIBRARY ieee;
use work.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity dataflagff is 
   port (
     eofin      : in std_logic;
     ldin       : in std_logic;
     eofout     : in std_logic;
     ldout      : in std_logic;
     datawaiting: out std_logic;
     moredatawaiting: out std_logic;
     clkin      : in std_logic;
     clkinrate  : in std_logic;
     clkout     : in std_logic;
     rst        : in std_logic
     );
end dataflagff;

architecture DATAFLAGFF of dataflagff is
  COMPONENT dataflagfifo
    PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    full : OUT STD_LOGIC;
    almost_empty : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
    );
  END COMPONENT;

  signal datain: std_logic;
  signal dataout: std_logic;
  signal empty: std_logic;
  signal almostempty: std_logic;
  signal olddatain: std_logic;
  signal oldolddatain: std_logic;
  signal oldoldolddatain: std_logic;
  signal seldatain: std_logic;

  begin
    process(rst, clkin)
    begin
      if(rst='1')then
        datain<='0';
        olddatain<='0';
        oldolddatain<='0';
        oldoldolddatain<='0';
      elsif (rising_edge(clkin))then
        datain<=eofin and ldin;
        olddatain<=datain;
        oldolddatain<=olddatain;
        oldoldolddatain<=oldolddatain;
      end if;
    end process; 

    dataout<=eofout and ldout;
    with clkinrate select
      seldatain<=olddatain when '0',
                 oldoldolddatain when others;
      

  thedataflagfifo : dataflagfifo
    PORT MAP (
    rst => rst,
    wr_clk => clkin,
    rd_clk => clkout,
    din => "1",
    wr_en => seldatain,
    rd_en => dataout,
    dout => open,
    full => open,
    almost_empty => almostempty,
    empty => empty
    );
    datawaiting <= not empty;
    moredatawaiting <= not almostempty;

end DATAFLAGFF;
