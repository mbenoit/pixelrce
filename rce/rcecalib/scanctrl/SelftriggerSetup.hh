#ifndef SELFTRIGGERSETUP_HH
#define SELFTRIGGERSETUP_HH

#include "rcecalib/scanctrl/NestedLoop.hh"
#include "rcecalib/scanctrl/LoopSetup.hh"
#include "rcecalib/scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/ConfigIF.hh"

class SelftriggerSetup: public LoopSetup{
public:
  SelftriggerSetup():LoopSetup(){};
  virtual ~SelftriggerSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
