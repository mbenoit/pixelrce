#ifndef AFPHPTDC__RECORD_HH
#define AFPHPTDC__RECORD_HH

namespace afphptdc{

  class AFPHPTDCRecord{
  public:
    AFPHPTDCRecord(unsigned word){(m_record=word);}
    virtual ~AFPHPTDCRecord(){};
    bool isData(){return ((m_record>>29)==0x2);}
    bool isTdcError(){return ((m_record>>28)==0x6);}
    bool isHeader(){return ((m_record>>16)==0xe9);}
    bool isServiceRecord(){return ((m_record>>16)==0xef);}
    bool isEmptyRecord(){return (m_record==0);}
    int getTdcErrorCode(){return m_record&0x7FFF;}
    int getL1id(){return (m_record>>8)&0x7f;}
    int getBxid(){return m_record&0xff;}
    int getEdge(){return (m_record>>28)&0x1;}
    int getTDCid(){return (m_record>>24)&0xf;}
    int getChannel(){return (m_record>>22)&0x3;}
    int getData(){return ((m_record>>19)&0x3) | ((m_record&0x7ffff)<<2);}
    int getErrorCode(){return (m_record>>10)&0x3f;}
    int getErrorCount(){return m_record&0x3ff;}
  private:
    unsigned m_record;

};

};
#endif
