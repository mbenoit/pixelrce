#include "util/IblFeId.hh"
#include <boost/regex.hpp>

unsigned parseModuleId(const char* module, unsigned felong){
  boost::cmatch matches;
  std::string res="([0-9]+)-([0-9]+)-([0-9]+)"; // parse what's hopefully the Module name
  boost::regex re;
  re=res;
  unsigned fe=felong&0x3f;
  if(boost::regex_search(module, matches, re)){
    if(matches.size()>3){
      std::string match1(matches[1].first, matches[1].second);
      std::string match2(matches[2].first, matches[2].second);
      std::string match3(matches[3].first, matches[3].second);
      unsigned val1=strtoul(match1.c_str(),0,10);
      unsigned val2=strtoul(match2.c_str(),0,10);
      unsigned val3=strtoul(match3.c_str(),0,10);
      return val1*1e6+val2*1e4+val3*1e2+fe;
    }else{
      //std::cout<<"Module string not parsable or FE ID>99. Using SN"<<std::endl;
      return felong;
    }
  }
  //std::cout<<"Module string not parsable. Using SN"<<std::endl;
  return felong;
}
