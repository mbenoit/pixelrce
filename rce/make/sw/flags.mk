# Architecture flags
# ------------------
arch_tgts := ppc-rtems-rce405 i686-slc5-gcc43 ppc-rtems-rceG1 
arch_opts := opt dbg

define arch_opt_template
arch_tgts += $$(addsuffix -$(1),$$(arch_tgts))
endef
$(foreach opt,$(arch_opts),$(eval $(call arch_opt_template,$(opt))))

# Separate the components of tgt_arch using the dash as a separator.
archwords      := $(subst -, ,$(strip $(tgt_arch)))
tgt_opt        := $(filter $(arch_opts),$(archwords))
archwords      := $(filter-out $(arch_opts),$(archwords))
tgt_cpu_family := $(word 1,$(archwords))
tgt_os         := $(word 2,$(archwords))
tgt_board      := $(word 3,$(archwords))


ifeq ($(tgt_os),slc5)
tgt_cpu_family=i386
tgt_gcc_version:= $(tgt_board)
tgt_board=i386
tgt_os:=linux
endif     

# Define a compile-time macro for each architecture component.
DEFINES := -Dtgt_opt_$(tgt_opt)
DEFINES += -Dtgt_cpu_family_$(tgt_cpu_family)
DEFINES += -Dtgt_os_$(tgt_os)
DEFINES += -Dtgt_board_$(tgt_board)


# i386 Linux specific flags
ifeq ($(tgt_os),linux)
ifeq ($(LINUXVERS),SLC6)
DEFINES+= -D__USE_XOPEN2K8
endif
ifeq ($(RCE_CORE_VERSION),2.2)
DEFINES+=-DRCE_V2  -Dtgt_board_i386 
endif
#generic toolchain
# pick compiler shipping with OS
AS.EXE:=as
CC.EXE:=gcc
CXX.EXE:=g++


ifeq ($(tgt_gcc_version),gcc43)
CC.EXE:=gcc43
CXX.EXE:=g++43
endif




LD.EXE=$(CXX.EXE)
LX.EXE=$(CXX.EXE)

AS  := $(AS.EXE)
CPP := $(CC) -E
CC  := $(CC.EXE) 
CXX := $(CXX.EXE)
LD  := $(LD.EXE)
LX  := $(LX.EXE)

ifeq ($(tgt_cpu_family),i386)
LIBEXTNS := so
DEPFLAGS := -MM -m32 $(DEFINES)
DEFINES  += -fPIC -D_REENTRANT -D__pentium__ -Wall
CPPFLAGS :=
CFLAGS   := -m32 -g
CXXFLAGS := $(CFLAGS)
CASFLAGS := -x assembler-with-cpp -P $(CFLAGS)
LDFLAGS  := -m32 -shared -g
LXFLAGS  := -m32 -g 
endif
ifeq ($(tgt_cpu_family),x86_64)
LIBEXTNS := so
DEPFLAGS := -MM -m64 
DEFINES  += -fPIC -D_REENTRANT -D__pentium__ -Wall
CPPFLAGS :=
CFLAGS   := -m64
CXXFLAGS := $(CFLAGS)
CASFLAGS := -x assembler-with-cpp -P $(CFLAGS)
LDFLAGS  := -m64 -shared
LXFLAGS  := -m64
endif
else
# Sparc Solaris specific flags
ifeq ($(tgt_cpu_family)-$(tgt_os),sparc-solaris)
AS  := as
CPP := gcc -E
CC  := gcc
CXX := g++
LD  := g++
LX  := g++

LIBEXTNS := so
DEPFLAGS := -MM
DEFINES  += -fPIC -D_REENTRANT -Wall
CPPFLAGS :=
CFLAGS   :=
CXXFLAGS := $(CFLAGS)
CASFLAGS := -x assembler-with-cpp -P $(CFLAGS)
LDFLAGS  := -shared
LXFLAGS  :=
else
# PowerPC RTEMS specific flags
ifeq ($(tgt_cpu_family)-$(tgt_os),ppc-rtems)


#make sure for cross compile to unset COMPILER_PATH 
# this will make the x-compiler to look for includes in the TDAQ compiler path if set
AS  := unset COMPILER_PATH;powerpc-rtems4.9-as
CPP := unset COMPILER_PATH;powerpc-rtems4.9-cpp
CC  := unset COMPILER_PATH;powerpc-rtems4.9-gcc
CXX := unset COMPILER_PATH;powerpc-rtems4.9-g++
LD  := unset COMPILER_PATH;powerpc-rtems4.9-ld
LX  := unset COMPILER_PATH;powerpc-rtems4.9-g++
AR  := unset COMPILER_PATH;powerpc-rtems4.9-ar
OBJCOPY :=unset COMPILER_PATH;powerpc-rtems4.9-objcopy 

ifeq ($(tgt_board),rceG1)
AS  := unset COMPILER_PATH;powerpc-rtems4.10-as
CPP := unset COMPILER_PATH;powerpc-rtems4.10-cpp
CC  := unset COMPILER_PATH;powerpc-rtems4.10-gcc
CXX := unset COMPILER_PATH;powerpc-rtems4.10-g++
LD  := unset COMPILER_PATH;powerpc-rtems4.10-ld
LX  := unset COMPILER_PATH;powerpc-rtems4.10-g++
AR  := unset COMPILER_PATH;powerpc-rtems4.10-ar
OBJCOPY := unset COMPILER_PATH;powerpc-rtems4.10-objcopy 
endif

ifeq ($(tgt_board),rce405)
RTEMSDIR := $(RELEASE_DIR)/build/rtems/target/powerpc-rtems/rce405/lib
tgt_cpu=403
endif

ifeq ($(tgt_board),rceG1)
RTEMSDIR := $(RELEASE_DIR)/build/rtems/target/powerpc-rtems4.10/virtex4/lib
tgt_cpu=405
endif



ifeq ($(tgt_board),ml405)
RTEMSDIR := $(RELEASE_DIR)/build/rtems/target/powerpc-rtems/ml405/lib
endif
ifeq ($(RCE_CORE),"")
LDTOOLSD := $(RELEASE_DIR)/rce/ldtools
else
LDTOOLSD := $(RCE_CORE)/ldtools
endif
LIBEXTNS := a

DEPFLAGS := -B$(RTEMSDIR) -MM  -Dppc405=ppc405 $(DEFINES)
DEFINES  += -Dppc405=ppc405
MDEFINES  := $(DEFINES) -DEXPORT='__attribute__((visibility("default")))'

ifeq ($(tgt_board),rceG1)
DEFINES += -Dtgt_gen=gen1 -Drtems_majorv_macro=4 -Drtems_minorv_macro=10 -Drtems_revision_macro=2 -DRCE_V2
MDEFINES += -Dtgt_gen=gen1 -Drtems_majorv_macro=4 -Drtems_minorv_macro=10 -Drtems_revision_macro=2 -DRCE_V2
DEPFLAGS+=   -Dtgt_gen=gen1 -Drtems_majorv_macro=4 -Drtems_minorv_macro=10 -Drtems_revision_macro=2 -DRCE_V2
endif
CPPFLAGS :=
CFLAGS   := -B$(RTEMSDIR) -specs bsp_specs -qrtems -mcpu=$(tgt_cpu) -Wall
CXXFLAGS := $(CFLAGS)
CASFLAGS := -x assembler-with-cpp -P $(CFLAGS)
LDFLAGS  := -r
#remove -s flag to not strip symbols
#LXFLAGS  := -B$(RTEMSDIR) -specs $(LDTOOLSD)/dynamic_specs -qrtems -qnolinkcmds -Wl,-T$(LDTOOLSD)/dynamic.ld -mcpu=$(tgt_cpu)
#LXFLAGS  :=   -B$(RTEMSDIR) -specs $(LDTOOLSD)/dynamic_specs -qrtems -qnolinkcmds -Wl,-T$(LDTOOLSD)/dynamic.ld -mcpu=$(tgt_cpu)
#old for debugger
ifeq ($(RCE_CORE_VERSION),2.2)
#LXFLAGS  :=   -B$(RTEMSDIR) -specs $(LDTOOLSD)/dynamic_specs -qrtems -qnolinkcmds -Wl,-T$(LDTOOLSD)/bootLoader.ld -mcpu=$(tgt_cpu)
#for debug image
LXFLAGS  :=   -B$(RTEMSDIR) -specs $(LDTOOLSD)/dynamic_specs -qrtems -qnolinkcmds -Wl,-T$(LDTOOLSD)/dynamic.ld -mcpu=$(tgt_cpu)
else
LXFLAGS  :=  -s -B$(RTEMSDIR) -specs $(LDTOOLSD)/dynamic_specs -qrtems -qnolinkcmds -Wl,-T$(LDTOOLSD)/dynamic.ld -mcpu=$(tgt_cpu)
endif


MANAGERS := timer sem msg event signal part region dpmem io rtmon ext mp

MCFLAGS   := -B$(RTEMSDIR) -specs $(LDTOOLSD)/module_specs -qrtems -Wall -fvisibility=hidden -mlongcall -fno-pic -mcpu=$(tgt_cpu)
MCXXFLAGS := $(MCFLAGS)
MCASFLAGS := -x assembler-with-cpp -P $(MCFLAGS)
MLDFLAGS  := -r
#remove -s flag to not strip symbols
#MLXFLAGS  := -B$(RTEMSDIR) -specs $(LDTOOLSD)/module_specs -qrtems -qnolinkcmds -Wl,-T$(LDTOOLSD)/module.ld -mcpu=$(tgt_cpu) -shared -nostdlib
MLXFLAGS  := -s     -B$(RTEMSDIR) -specs $(LDTOOLSD)/module_specs -qrtems -qnolinkcmds -Wl,-T$(LDTOOLSD)/module.ld -mcpu=$(tgt_cpu) -shared -nostdlib

ifeq ($(tgt_board),rceG1)
LXFLAGS +=  -L$(LDTOOLSD)  -L$(LDTOOLSD)/rceG1
LXFLAGS += $(RELEASE)/build/platform/obj/ppc-rtems-rceG1-opt/startup/src/Init.o 
LXFLAGS += $(RELEASE)/build/platform/obj/ppc-rtems-rceG1-opt/startup/src/extrarefs.o
LXFLAGS += $(RELEASE)/build/platform/obj/ppc-rtems-rceG1-opt/startup/src/rce/Init.o 
LXFLAGS += $(RELEASE)/build/platform/obj/ppc-rtems-rceG1-opt/startup/bldInfo_dpm.2.2.prod.o
MXFLAGS +=  -L$(LDTOOLSD)  -L$(LDTOOLSD)/rceG1
endif

else
ifeq ($(tgt_cpu_family)-$(tgt_os),ppc-linux)
AS  := as
CPP := gcc -E
CC  := gcc
CXX := g++
LD  := g++
LX  := g++

LIBEXTNS := so
DEPFLAGS := -MM
DEFINES  += -D_REENTRANT -Wall
CPPFLAGS :=
CFLAGS   :=
CXXFLAGS := $(CFLAGS)
CASFLAGS := -x assembler-with-cpp -P $(CFLAGS)
LDFLAGS  := -shared
LXFLAGS  :=

endif
endif
endif
endif


ifeq ($(tgt_opt),opt)
DEFINES += -O4
MDEFINES += -O4
endif

ifeq ($(tgt_opt),dbg)
DEFINES += -g
MDEFINES += -g
endif
