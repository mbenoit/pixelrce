#ifndef SCAN_HH
#define SCAN_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include <omnithread.h>
#include "rcecalib/scanctrl/NestedLoop.hh"
#include "rcecalib/util/DataCond.hh"
#include <vector>

class ActionFactory;
class AbsDataHandler;

class Scan{
public:
  Scan();
  ~Scan();
  int configureScan(boost::property_tree::ptree* scanOptions, ActionFactory& af, AbsDataHandler* par);
  int waitForData();
  int stopWaiting();
  int pause();
  int resume();
  int abort();
  int startScan();
  int getStatus(){return (int)m_state;}
  DataCond& getDataCond(){return m_dataCond;}
  void setFailed();
  bool failed(){return m_failed;}
  enum State{IDLE, CONFIGURED, RUNNING, PAUSED, ABORT};
  enum Timeout{CONFIG_TIMEOUT=500};//5 seconds
protected:
  void publishHistos();
  State m_state;
  NestedLoop m_loops;
  omni_mutex m_pause_mutex ;
  omni_condition  m_pause_cond  ;
  DataCond m_dataCond;
  int m_i;
  AbsDataHandler *m_handler;
  unsigned m_timeout_seconds;
  unsigned m_timeout_nanoseconds;
  bool m_failed;
  int m_timeouts;
  int m_allowedTimeouts;
  std::vector<std::string> m_histolist;

};
#endif
