#ifndef IPCSCANROOT_HH
#define IPCSCANROOT_HH

#include "rcecalib/scanctrl/ScanRoot.hh"
#include "ipc/object.h"
#include "ScanOptions.hh"
#include "IPCScanRootAdapter.hh"

class IPCPartition;
class ConfigIF;
class Scan;

template <class TP = ipc::single_thread>
class IPCScanRoot: public IPCNamedObject<POA_ipc::IPCScanRootAdapter,TP>, public ScanRoot {
public:
  IPCScanRoot(IPCPartition & p, const char * name, ConfigIF* cif, Scan* scan);
  ~IPCScanRoot();
  CORBA::Long IPCconfigureScan(const ipc::ScanOptions &options);    
  ipc::StringVect* IPCgetHistoNames(const char* reg);
  ipc::StringVect* IPCgetPublishedHistoNames();
  CORBA::ULong IPCnEvents();
  void IPCresynch();
  void IPCconfigureCallback(ipc::Callback_ptr cb, ipc::Priority pr);
  void shutdown();

};
  

#endif
