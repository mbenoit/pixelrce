#include "rcecalib/config/IPCModuleFactory.hh"
#include "rcecalib/config/DummyFormatter.hh"
#include "rcecalib/config/FEI3/JJFormatter.hh"
#include "rcecalib/config/FEI4/FEI4AFormatter.hh"
#include "rcecalib/config/FEI4/FEI4BFormatter.hh"
#include "rcecalib/config/FEI4/FEI4HitorFormatter.hh"
#include "rcecalib/config/FEI4/FEI4OccFormatter.hh"
#include "rcecalib/config/FEI3/Module.hh"
#include "rcecalib/config/FEI3/IPCModule.cc"
#include "rcecalib/config/FEI4/IPCFEI4AModule.cc"
#include "rcecalib/config/FEI4/IPCFEI4BModule.cc"
#include "rcecalib/config/hitbus/IPCModule.cc"
#include "rcecalib/config/afp-hptdc/IPCModule.cc"
#include "rcecalib/config/afp-hptdc/AFPHPTDCFormatter.hh"
#include "rcecalib/config/Trigger.hh"
#include "rcecalib/config/MultiTrigger.hh"
#include "rcecalib/config/EventFromFileTrigger.hh"
#include "rcecalib/config/MeasurementTrigger.hh"
#include "rcecalib/config/MultiShotTrigger.hh"
#include "rcecalib/config/HitorTrigger.hh"
#include "rcecalib/config/FEI4/TemperatureTrigger.hh"
#include "rcecalib/config/FEI4/MonleakTrigger.hh"
#include "rcecalib/config/FEI4/SNTrigger.hh"
#include "rcecalib/config/FEI4/Fei4RegisterTestTrigger.hh"
#include "rcecalib/util/exceptions.hh"
#include "stdio.h"
#include <iostream>

IPCModuleFactory::IPCModuleFactory(IPCPartition& p):ModuleFactory(),m_partition(p){
  m_modulegroups.push_back(&m_modgroupi3);
  m_modulegroups.push_back(&m_modgroupi4a);
  m_modulegroups.push_back(&m_modgroupi4b);
  m_modulegroups.push_back(&m_modgrouphitbus);
  m_modulegroups.push_back(&m_modgroupafphptdc);
}


AbsFormatter* IPCModuleFactory::createFormatter(const char* formatter, int id){
  
  // create formatter first
  if(std::string(formatter)=="JJ")
    return new JJFormatter(id);
  else if(std::string(formatter)=="")
    return 0;
  else if(std::string(formatter)=="FEI4A")
    return new FEI4AFormatter(id);
  else if(std::string(formatter)=="FEI4B")
    return new FEI4BFormatter(id);
  else if(std::string(formatter)=="FEI4Hitor")
    return new FEI4HitorFormatter(id);
  else if(std::string(formatter)=="FEI4Occ")
    return new FEI4OccFormatter(id);
  else if(std::string(formatter)=="AFPHPTDC")
    return new AFPHPTDCFormatter(id);
  else if(std::string(formatter)=="Dummy")
    return new DummyFormatter(id);
  else{
    std::cout<<"Unknown formatter "<<formatter<<std::endl;
    assert(0);
  }
}

AbsModule* IPCModuleFactory::createModule(const char* name, const char* type, unsigned id, unsigned inLink, unsigned outLink, const char* formatter){
  AbsFormatter* theformatter=createFormatter(formatter, id);
  if(std::string(type)=="FEI3"){
    FEI3::Module *mod= new FEI3::Module(name, id, inLink, outLink, theformatter);
    m_modgroupi3.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI3_singleThread"){
    FEI3::Module *mod= new FEI3::IPCModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi3.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI3_multiThread"){
    FEI3::Module *mod= new FEI3::IPCModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi3.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI4A_singleThread"){
    FEI4::Module *mod= new FEI4::IPCFEI4AModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi4a.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI4A_multiThread"){
    FEI4::Module *mod= new FEI4::IPCFEI4AModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi4a.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI4B_singleThread"){
    FEI4::Module *mod= new FEI4::IPCFEI4BModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi4b.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI4B_multiThread"){
    FEI4::Module *mod= new FEI4::IPCFEI4BModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi4b.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_Hitbus_singleThread"){
    Hitbus::Module *mod= new Hitbus::IPCModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgrouphitbus.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_Hitbus_multiThread"){
    Hitbus::Module *mod= new Hitbus::IPCModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgrouphitbus.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_AFPHPTDC_singleThread"){
    afphptdc::Module *mod= new afphptdc::IPCModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupafphptdc.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_AFPHPTDC_multiThread"){
    std::cout<<"Adding module"<<std::endl;
    afphptdc::Module *mod= new afphptdc::IPCModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    std::cout<<"Added module"<<std::endl;
    m_modgroupafphptdc.addModule(mod);
    return mod;
  }
  std::cout<<"Unknown module type "<<type<<std::endl;
  rcecalib::Unknown_Module_Type issue(ERS_HERE,type);
  throw issue;
  return 0;
} 

AbsTrigger* IPCModuleFactory::createTriggerIF(const char* type, ConfigIF* cif){
  //  std::cout << "Creating trigger of type " << type << std::endl;
  if(std::string(type)=="default") return new FEI3::Trigger;
  else if(std::string(type)=="FEI3")return new FEI3::Trigger;
  else if(std::string(type)=="EventFromFile")return new EventFromFileTrigger;
  else if(std::string(type)=="MultiShot")return new MultiShotTrigger;
  else if(std::string(type)=="MultiTrigger")return new MultiTrigger;
  else if(std::string(type)=="Hitor")return new HitorTrigger(cif);
  else if(std::string(type)=="Temperature")return new FEI4::TemperatureTrigger;
  else if(std::string(type)=="Monleak")return new FEI4::MonleakTrigger;
  else if(std::string(type)=="SerialNumber")return new FEI4::SNTrigger;
  else if(std::string(type)=="Fei4RegisterTest")return new FEI4::Fei4RegisterTestTrigger;
#ifdef __rtems__
  else if(std::string(type)=="Measurement")return new MeasurementTrigger;
#endif
  char msg[128];
  sprintf(msg, "No Trigger type %s",type);
  ERS_ASSERT_MSG(0,msg); 
}
