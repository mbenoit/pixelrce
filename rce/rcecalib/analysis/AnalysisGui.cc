#include "rcecalib/analysis/AnalysisGui.hh"
#include "rcecalib/server/PixScan.hh"
#include "rcecalib/analysis/CalibAnalysis.hh"
#include "rcecalib/analysis/AnalysisFactory.hh"
#include "TApplication.h"
#include "TGMsgBox.h"
#include "TH1.h"
#include "TKey.h"
#include "TGIcon.h"
#include "TGMenu.h"
#include "TCanvas.h"
#include "TGCanvas.h"
#include "TGListTree.h"
#include "TRootEmbeddedCanvas.h"
#include <TGFileDialog.h>
#include <TROOT.h>
#include <TStyle.h>
#include <assert.h>
#include <iostream>
#include <time.h>
#include <sys/stat.h> 
#include <fstream>
#include <list>
#include <pthread.h>
#include <vector>
#include <stdlib.h> 
  
using namespace RCE;

AnalysisGui::~AnalysisGui(){
  Cleanup();
}

AnalysisGui::AnalysisGui(const char* filename, int scantype, int flavor, const TGWindow *p,UInt_t w,UInt_t h)
  : TGMainFrame(p,w,h) {
  
  // connect x icon on window manager
  Connect("CloseWindow()","AnalysisGui",this,"quit()");

  TGMenuBar *menubar=new TGMenuBar(this,1,1,kHorizontalFrame | kRaisedFrame);
  TGLayoutHints *menubarlayout=new TGLayoutHints(kLHintsTop|kLHintsLeft,0,4,0,0);
  // menu "File"
  TGPopupMenu* filepopup=new TGPopupMenu(gClient->GetRoot());
  //filepopup->AddEntry("&Load Config",LOAD);
  //filepopup->AddEntry("&Save Config",SAVE);
  filepopup->AddSeparator();
  filepopup->AddEntry("&Quit",QUIT);
  menubar->AddPopup("&File",filepopup,menubarlayout);

  AddFrame(menubar, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 0,0,0,2));
  
  filepopup->Connect("Activated(Int_t)","AnalysisGui",this,"handleFileMenu(Int_t)");

  TGVerticalFrame* datapanel=new TGVerticalFrame(this,1,1, kSunkenFrame);
  // scan panel
  TGHorizontalFrame *datapanel5 = new TGHorizontalFrame(datapanel, 2, 2, kSunkenFrame);
  datapanel->AddFrame(datapanel5,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  AddFrame(datapanel,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  TGVerticalFrame *treepanel = new TGVerticalFrame(datapanel5, 150, 2, kSunkenFrame);
  datapanel5->AddFrame(treepanel,new TGLayoutHints(kLHintsExpandY));
  TGVerticalFrame *plotpanel = new TGVerticalFrame(datapanel5, 2, 2, kSunkenFrame);
  datapanel5->AddFrame(plotpanel,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  m_tgc=new TGCanvas(treepanel, 300,100);
  //TGViewPort* vp=tgc->GetViewPort();
  m_tree=new TGListTree(m_tgc, kHorizontalFrame);
  m_tree->Connect("Clicked(TGListTreeItem*, Int_t)","AnalysisGui", this, "displayHisto(TGListTreeItem*, Int_t)");
  //tree->AddItem(0,"/");
  treepanel->AddFrame(m_tgc,new TGLayoutHints(kLHintsExpandY));
  m_canvas=new TRootEmbeddedCanvas("Canvas",plotpanel,100,100);
  m_canvas->GetCanvas()->GetPad(0)->SetRightMargin(0.15);
  plotpanel->AddFrame(m_canvas,new TGLayoutHints(kLHintsExpandY|kLHintsExpandX));
  
  SetWindowName("Analysis GUI");
  Resize(w,h);
  Layout();
  MapSubwindows();
  MapWindow();
  m_file=new TFile(filename,"");
  m_anfile=new TFile("analysis.root", "recreate");
  TGListTreeItem* root=m_tree->AddItem(0,"Histos");
  m_file->cd();
  fillHistoTree(root);
  
  // now run the analysis
  std::cout<<"Analyzing"<<std::endl;
  PixScan pixscan((PixScan::ScanType)scantype, (PixLib::EnumFEflavour::FEflavour) flavor);
  std::string type=pixscan.getAnalysisType();
  std::cout<<"Analysis type is "<<type<<std::endl;
  AnalysisFactory af;
  CalibAnalysis* ana=af.getAnalysis(type);
  if(ana){
    ana->analyze(m_file, m_anfile, &pixscan, 0, 0);
    m_anfile->ReOpen("read"); //re-open file for reading
    TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
    TGListTreeItem* anroot=m_tree->AddItem(root,"Analysis");
    const TGPicture *thp = gClient->GetPicture("h1_t.xpm");
    m_anfile->cd();
    TList* histos=gDirectory->GetListOfKeys();
    for(int i=0;i<histos->GetEntries();i++){
      TGListTreeItem* item=m_tree->AddItem(anroot,((TH1*)histos->At(i))->GetName());
      item->SetPictures(thp,thp);
    }
    updateTree();
    delete ana;
  }
}

void AnalysisGui::quit(){
  m_file->Close();
  m_anfile->Close();
  gApplication->Terminate(0);
}


void AnalysisGui::handleFileMenu(int item){
  if(item==QUIT)quit();
}
void AnalysisGui::clearTree(){
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  if(root) m_tree->DeleteChildren(root);
  root->SetOpen(false);
  updateTree();
}
void AnalysisGui::updateTree(){
  int x=m_tgc->GetViewPort()->GetX();
  int y=m_tgc->GetViewPort()->GetY();
  int w=m_tgc->GetViewPort()->GetWidth();
  int h=m_tgc->GetViewPort()->GetHeight();
  m_tree->DrawRegion(x,y,w,h);
}
  
void AnalysisGui::fillHistoTree(TGListTreeItem* branch){
  const TGPicture *thp = gClient->GetPicture("h1_t.xpm");
  TIter nextkey(gDirectory->GetListOfKeys());
   TKey *key;
   while ((key=(TKey*)nextkey())) {
     TObject *obj = key->ReadObj();
     if(std::string(obj->ClassName())=="TDirectoryFile"){
       gDirectory->cd(key->GetName());
       TGListTreeItem* subdir=m_tree->AddItem(branch,key->GetName());
       fillHistoTree(subdir);
     }else{
       TGListTreeItem* his=m_tree->AddItem(branch,key->GetName());
       his->SetPictures(thp, thp);
     }
     delete obj;
   }
   gDirectory->cd("..");
}
void AnalysisGui::displayHisto(TGListTreeItem* item, int b){
  TGListTreeItem *parent=item->GetParent();
  if(parent==0)return;
  const char* histoname=item->GetText();
  if(std::string(histoname).substr(0,4)=="loop")return;
  if(std::string(histoname).substr(0,8)=="Analysis")return;
  if(std::string(parent->GetText())=="Analysis"){
    m_anfile->cd();
    m_histo=(TH1*)gDirectory->Get(histoname);
    assert(m_histo!=0);
  }else{
    TGListTreeItem *parent1=parent->GetParent();
    if(parent1==0)m_file->cd();
    else{
      TGListTreeItem *parent2=parent1->GetParent();
      if(parent2==0)m_file->cd(parent->GetText());
      else{
	char dir[128];
	sprintf(dir, "%s/%s",parent1->GetText(), parent->GetText());
	m_file->cd(dir);
      }
    }
    m_histo=(TH1*)gDirectory->Get(histoname);
    assert(m_histo!=0);
  }
  m_histo->SetMinimum(0);
  if(m_histo->GetDimension()==1)m_histo->Draw();
  else {
    m_histo->Draw("colz");
  }
  m_canvas->GetCanvas()->Update();
}

  
//====================================================================
int main(int argc, char **argv){
  if(argc!=4){
    std::cout<<"Usage: analysisGui filename scantype FE flavor"<<std::endl;
    exit(0);
  }
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);
  const char *filename=argv[1];
  std::cout<<"Filename "<<filename<<std::endl;
  int scantype=atoi(argv[2]);
  int flavor=atoi(argv[3]);
  TApplication theapp("app",&argc,argv);
  new AnalysisGui(filename, scantype, flavor, gClient->GetRoot(),800,600);
  theapp.Run();
  return 0;
}


