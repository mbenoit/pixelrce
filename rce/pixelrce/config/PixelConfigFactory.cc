#include "config/PixelConfigFactory.hh"
#include "config/PixelConfig.hh"
#include "config/FEI4/FEI4AConfig.hh"
#include "config/FEI4/FEI4BConfig.hh"
#include "config/FEI3/ModuleConfig.hh"
#include "config/hitbus/HitbusConfig.hh"
#include "config/afp-hptdc/AFPHPTDCConfig.hh"
#include <boost/regex.hpp>
#include <sys/stat.h> 
#include "util/exceptions.hh"
#include <iostream>

PixelConfig* PixelConfigFactory::createConfig(const char* filename){
  struct stat stFileInfo;
  int intStat;
  // Attempt to get the file attributes 
  intStat = stat(filename, &stFileInfo);
  if(std::string(filename)=="None" || intStat != 0) { //File does not exist
    std::cout<<"File does not exist."<<std::endl;
    rcecalib::Config_File_Error err;
    throw err;
  }else{
    std::ifstream cfgFile(filename);
    std::string inpline;
    getline(cfgFile, inpline); //read the first line to determine configuration format
    cfgFile.close();
    boost::regex r1("TurboDAQ");
    boost::regex r2("FEI4A");
    boost::regex r3("FEI4B");
    boost::regex r4("Hitbus");
    boost::regex r5("Hptdc");
    if(boost::regex_search(inpline, r1)==true){
      return new ModuleConfig(filename);
    }else if(boost::regex_search(inpline, r2)==true){ 
      return new FEI4AConfig(filename);
    }else if(boost::regex_search(inpline, r3)==true){ 
      return new FEI4BConfig(filename);
    } else if(boost::regex_search(inpline, r4)==true){
      return new HitbusConfig(filename);
    } else if(boost::regex_search(inpline, r5)==true){
      return new AFPHPTDCConfig(filename);
    } else {
      std::cout<<"Unknown file type."<<std::endl;
      rcecalib::Config_File_Error err;
      throw err;
    }
  }
  return 0;
}

