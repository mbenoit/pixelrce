#include "HW/BitStream.hh"
#include "config/afp-hptdc/Module.hh"
#include "config/FEI4/FECommands.hh"
#include "HW/SerialIF.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <stdio.h>

namespace afphptdc{

  Module::Module(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt)
    :AbsModule(name, id,inLink, outLink, fmt){
        //std::cout<<"Module"<<std::endl;
    if(!m_initialized)initialize();
    for(int i=0;i<N_FPGA_REGS;i++)m_fpga_registers[i]=0;
    for(int j=0;j<3;j++)
      for(int i=0;i<N_TDC_REGS;i++)
	m_tdc_registers[j][i]=0;
    m_commands=new FEI4::FECommands;
  }
  Module::~Module(){
    delete m_commands;
  }
  void Module::writeHW(){ 
    //FPGA registers
    for(std::map<int, int>::const_iterator it=m_used_regs.begin(); it!=m_used_regs.end(); it++){
      writeRegisterRawHW(it->first, (unsigned short)m_fpga_registers[it->first]);
    }
    for(int i=0;i<3;i++){
      writeTdcBitstreamHW(i);
    }
  }
  void Module::writeTdcBitstreamHW(int i){
    // calculate parity;
    int parity=0;
    for(int j=0;j<=m_regpos;j++){
      for(int k= j==m_regpos? m_bitpos+1 : 0;k<32;k++){
	parity^=(m_tdc_registers[i][j]>>k)&0x1;
      }
    }
    setTdcField("parity", i, parity);
    BitStream *bs=new BitStream;
    m_commands->feWriteCommand(bs);
    (*bs)[bs->size()-1]|=i; //bitstream address
    for(int j=0;j<N_TDC_REGS;j++){
      bs->push_back(m_tdc_registers[i][j]);
    }
    SerialIF::send(bs); //Write bitstream
    delete bs;
  }
  unsigned Module::writeRegisterHW(int i, unsigned short val){
    // i is position in vector, not actual address 
    /*
    m_regw[i]=val; // set register content
    int addr=m_registers[i].reg;
    int rval=val&((1<<m_registers[i].width)-1);
    writeRegisterRawHW(addr, rval);
    */
    return 0;
  }
  void Module::writeRegisterRawHW(int i, unsigned short val){
    BitStream *bs=new BitStream;
    m_commands->writeGlobalRegister(bs, i, val);
    SerialIF::send(bs); //Write register
    delete bs;
  }

  void Module::configureHW(){
    std::cout<<"Configuring HPTDC"<<std::endl;
    resetFE();
    SerialIF::setDataRate(SerialIF::M160);
    writeHW();
    //printRegisters(std::cout);
  }
  void Module::enableDataTakingHW(){
    std::cout<<"AFPHPTDC Enabled data taking Board "<<getId()<<std::endl;
    BitStream *bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    m_commands->switchMode(bs, FEI4::FECommands::RUN);
    m_commands->sendECR(bs);
    m_commands->sendBCR(bs);
    SerialIF::send(bs);
    delete bs;
  } 
  void Module::switchToConfigModeHW(){
    std::cout<<"AFPHPTDC Switched to Config Mode Board "<<getId()<<std::endl;
    BitStream *bs=new BitStream;
    m_commands->switchMode(bs, FEI4::FECommands::CONF);
    bs->push_back(0);
    SerialIF::send(bs, SerialIF::WAITFORDATA);
    delete bs;
  }  

  void Module::resetFE(){
    BitStream *bs=new BitStream;
    m_commands->globalReset(bs);
    delete bs;
  }

  // set a parameter (global or MCC) in the frontend
  int Module::setupParameterHW(const char* name, int val){
    int retval=1;
    /*
    if(m_parameters.find(name)!=m_parameters.end()){
      retval=0;
      writeRegisterHW(m_parameters[name], val);
    }
    */
    return retval;
  }

  int Module::configureScan(boost::property_tree::ptree *scanOptions){
    int retval=0;
    // do nothing for now
    return retval;
  }
  void Module::destroy(){
    delete this;
  }
  ModuleInfo Module::getModuleInfo(){
    return ModuleInfo(m_name, m_id,m_inLink, m_outLink, 0, 0, 0, m_formatter);
  }

  void Module::printRegisters(std::ostream &os){
    std::cout<<"FPGA Registers"<<std::endl;
    for(std::map<int, int>::const_iterator it=m_used_regs.begin(); it!=m_used_regs.end(); it++){
      os<<"Address = "<<it->first<<" Value = 0x"<<std::hex<<m_fpga_registers[it->first]<<std::dec<<std::endl;
    }
    for (int i=0;i<3;i++){
      std::cout<<"TDC Registers Bitstream "<<i<<std::endl;
      for(int j=0;j<N_TDC_REGS;j++){
	std::cout<<std::hex<<m_tdc_registers[i][j]<<std::dec<<std::endl;
      }
    }
  }

  //static functions 
  //void Module::addParameter(const char* name, const char* field){
//    int regnum=findRegister(field);
//    assert(regnum>-1);
//    m_parameters[name]=regnum;
  //}
  void Module::setFpgaField(const char* name, unsigned val){
    char msg[128];
    assert(m_fpga_fields.find(name)!=m_fpga_fields.end());
    FieldParams params=m_fpga_fields[name];
    setFieldFun(&m_fpga_registers[params.reg], params.pos, params.width, val);
  } 
  void Module::setTdcField(const char* name, int index, unsigned val){
    assert(m_tdc_fields.find(name)!=m_tdc_fields.end());
    FieldParams params=m_tdc_fields[name];
    //reverse bit order of fields
    unsigned valRev = 0;
    for (int i=0;i<params.width;i++){
	valRev <<= 1;
        valRev |= val & 1;
        val >>= 1;
    }
    setFieldFun(&m_tdc_registers[index][params.reg], params.pos, params.width, valRev);
  }
  void Module::setFieldFun(unsigned *reg, int bitpos, int width, unsigned val){
    int pwidth=bitpos+width<=32 ? width : 32 - bitpos;
    unsigned mask=(1<<width)-1;   
    mask=(1<<pwidth)-1;
    *reg&=~(mask<<bitpos);
    *reg|=(val&mask)<<bitpos;
    if(pwidth!=width){ // extends over next register
      int rwidth=width-pwidth;
      mask=(1<<rwidth)-1;
      *(reg-1)&=~mask;
      *(reg-1)|=(val>>pwidth);
    }
  }
    
  void Module::addFpgaField(const char* name, int reg, int width, int pos){
    FieldParams temp;
    temp.reg=reg;
    temp.width=width;
    temp.pos=pos;
    m_fpga_fields[name]=temp;
  }
  void Module::addTdcField(const char* name, int width){
    m_bitpos-=width;
    if(m_bitpos<0){ 
      m_bitpos+=32;
      m_regpos+=1;
    }
    FieldParams temp;
    temp.reg=m_regpos;
    temp.width=width;
    temp.pos=m_bitpos;
    m_tdc_fields[name]=temp;
  }
  std::map<std::string, FieldParams> Module::m_fpga_fields;
  std::map<std::string, FieldParams> Module::m_tdc_fields;
  std::map<int, int> Module::m_used_regs;
  bool Module::m_initialized = false;
  int Module::m_regpos=0;
  int Module::m_bitpos=32;

  void Module::initialize(){
    addFpgaField("test", 32, 16, 0);
    addFpgaField("tdcControl", 28, 4, 0);
    addFpgaField("run", 29, 1, 0);
    addFpgaField("bypassLut", 29, 1, 2);
    addFpgaField("localClockEn", 29, 1, 3);
    addFpgaField("calClockEn", 29, 1, 4);
    addFpgaField("refEn", 29, 1, 5);
    addFpgaField("hitTestEn", 29, 1, 6);
    addFpgaField("inputSel", 29, 3, 8);
    addFpgaField("address", 20, 8, 0);
    addFpgaField("fanspeed", 27, 8, 0);
    addFpgaField("channelEn", 26, 12, 0);

    for(std::map<std::string, FieldParams>::const_iterator it=m_fpga_fields.begin(); it!=m_fpga_fields.end(); it++){
      FieldParams p=it->second;
      m_used_regs[p.reg]=1;
    }
    addTdcField("test_select", 4);
    addTdcField("enable_error_mark", 1);
    addTdcField("enable_error_bypass", 1);
    addTdcField("enable_error", 11);
    addTdcField("readout_single_cycle_speed", 3);
    addTdcField("serial_delay", 4);
    addTdcField("strobe_select", 2);
    addTdcField("readout_speed_select", 1);
    addTdcField("token_delay", 4);
    addTdcField("enable_local_trailer", 1);
    addTdcField("enable_local_header", 1);
    addTdcField("enable_global_trailer", 1);
    addTdcField("enable_global_header", 1);
    addTdcField("keep_token", 1);
    addTdcField("master", 1);
    addTdcField("enable_bytewise", 1);
    addTdcField("enable_serial", 1);
    addTdcField("enable_jtag_readout", 1);
    addTdcField("tdc_id", 4);
    addTdcField("select_bypass_inputs", 1);
    addTdcField("readout_fifo_size", 3);
    addTdcField("reject_count_offset", 12);
    addTdcField("search_window", 12);
    addTdcField("match_window", 12);
    addTdcField("leading_resolution", 3);
    addTdcField("fixed_pattern", 28);
    addTdcField("enable_fixed_pattern", 1);
    addTdcField("max_event_size", 4);
    addTdcField("reject_readout_fifo_full", 1);
    addTdcField("enable_readout_occupancy", 1);
    addTdcField("enable_readout_separator", 1);
    addTdcField("enable_overflow_detect", 1);
    addTdcField("enable_relative", 1);
    addTdcField("enable_automatic_reject", 1);
    addTdcField("event_count_offset", 12);
    addTdcField("trigger_count_offset", 12);
    addTdcField("enable_set_counters_on_bunch_reset", 1);
    addTdcField("enable_master_reset_code", 1);
    addTdcField("enable_master_reset_code_on_event_reset", 1);
    addTdcField("enable_reset_channel_buffer_when_separator", 1);
    addTdcField("enable_separator_on_event_reset", 1);
    addTdcField("enable_separator_on_bunch_reset", 1);
    addTdcField("enable_direct_event_reset", 1);
    addTdcField("enable_direct_bunch_reset", 1);
    addTdcField("enable_direct_trigger", 1);
    for (int j=31;j>=0;j--){
      char offs[32];
      sprintf(offs, "offset%d", j);
      addTdcField(offs, 9);
    }
    addTdcField("coarse_count_offset", 12);
    addTdcField("dll_tap_adjust3_0", 12);
    addTdcField("dll_tap_adjust7_4", 12);
    addTdcField("dll_tap_adjust11_8", 12);
    addTdcField("dll_tap_adjust15_12", 12);
    addTdcField("dll_tap_adjust19_16", 12);
    addTdcField("dll_tap_adjust23_20", 12);
    addTdcField("dll_tap_adjust27_24", 12);
    addTdcField("dll_tap_adjust31_28", 12);
    addTdcField("rc_adjust", 12);
    addTdcField("not_used", 3);
    addTdcField("low_power_mode", 1);
    addTdcField("width_select", 4);
    addTdcField("vernier_offset", 5);
    addTdcField("dll_control", 4);
    addTdcField("dead_time", 2);
    addTdcField("test_invert", 1);
    addTdcField("test_mode", 1);
    addTdcField("enable_trailing", 1);
    addTdcField("enable_leading", 1);
    addTdcField("mode_rc_compression", 1);
    addTdcField("mode_rc", 1);
    addTdcField("dll_mode", 2);
    addTdcField("pll_control", 8);
    addTdcField("serial_clock_delay", 4);
    addTdcField("io_clock_delay", 4);
    addTdcField("core_clock_delay", 4);
    addTdcField("dll_clock_delay", 4);
    addTdcField("serial_clock_source", 2);
    addTdcField("io_clock_source", 2);
    addTdcField("core_clock_source", 2);
    addTdcField("dll_clock_source", 3);
    addTdcField("roll_over", 12);
    addTdcField("enable_matching", 1);
    addTdcField("enable_pair", 1);
    addTdcField("enable_ttl_serial", 1);
    addTdcField("enable_ttl_control", 1);
    addTdcField("enable_ttl_reset", 1);
    addTdcField("enable_ttl_clock", 1);
    addTdcField("enable_ttl_hit", 1);
    addTdcField("parity", 1);
    m_initialized=true;
    std::cout<<"Regpos "<<m_regpos<<std::endl;
    std::cout<<"Bitpos "<<m_bitpos<<std::endl;
  }
}
