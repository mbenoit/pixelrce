
#include <ipc/partition.h>
#include <ipc/core.h>
#include "server/CosmicGui.hh"
#include "server/IPCController.hh"
#include "util/HistoManager.hh"
#include "TApplication.h"
#include <TROOT.h>
#include <TStyle.h>
#include <boost/program_options.hpp>
#include <is/infoT.h>
#include <is/infodictionary.h>

int main(int argc, char **argv){
  //
  // Initialize command line parameters with default values
  //
  try {
    IPCCore::init( argc, argv );
  }
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
    return 1;
  }
  
//
// Declare command object and its argument-iterator
//       
  bool start;
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("start,s", boost::program_options::value<bool>(&start)->default_value(false), "Start run when GUI comes up")
    ("partition,p", boost::program_options::value<std::string>(), "partition to work in.");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);
  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }
  const char *p_name=getenv("TDAQ_PARTITION");
  if(p_name==NULL) p_name="rcetest";
  if(vm.count("partition"))p_name=vm["partition"].as<std::string>().c_str();
  try{
    IPCPartition   p(p_name );
    IPCController controller(p);
    AbsController &acontroller(controller);
    new HistoManager(0);
    
    gROOT->SetStyle("Plain");
    TApplication theapp("app",&argc,argv);
    new CosmicGui(acontroller, start, gClient->GetRoot(),800, 735);
    theapp.Run();
    return 0;
  }catch(...){
    std::cout<<"Partition "<<p_name<<" does not exist."<<std::endl;
    exit(0);
  }
}


