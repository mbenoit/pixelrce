#include "analysis/ThresholdAnalysis.hh"
#include "server/PixScan.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>
#include "TH1D.h"
#include "TF1.h"
#include "TStyle.h"


void ThresholdAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  std::cout<<"Threshold analysis"<<std::endl;
  unsigned int numval=scan->getLoopVarValues(0).size();
  gStyle->SetOptFit(10);
  //gStyle->SetOptStat(0);
  std::map<int, TH1D*> histmap;
  TIter nextkey(gDirectory->GetListOfKeys()); // histograms
  TKey *key;
  std::vector<std::string> pos;
  std::vector<float> threshs;
  std::vector<float> thresherrs;
  std::vector<float> sigmas;
  std::vector<float> sigmaerrs;
  while ((key=(TKey*)nextkey())) {
    file->cd();
    std::string hname(key->GetName());
    boost::cmatch matches;
    boost::regex re("_(\\d+)_Mean");
    boost::regex re2("Mean");
    char name[128];
    char title[128];
    if(boost::regex_search(hname.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      std::string match2=addPosition(hname.c_str(), cfg).substr(0,5);
      pos.push_back(match2);
      std::string chi2HistoName = boost::regex_replace (hname, re2, "ChiSquare");
      std::string sigmaHistoName = boost::regex_replace (hname, re2, "Sigma");
      TH2* histo = (TH2*)key->ReadObj();
      TH2* chi2Histo=(TH2*)gDirectory->Get(chi2HistoName.c_str());
      TH2* sigmahisto=(TH2*)gDirectory->Get(sigmaHistoName.c_str());
      assert(chi2Histo!=0);
      assert(sigmahisto!=0);
      int binsx=histo->GetNbinsX();
      int binsy=histo->GetNbinsY();
      sprintf(name, "thresh2d_Mod_%d", id);
      sprintf(title, "Thresholds Module %d at %s", id, findFieldName(cfg, id));
      TH2F* thresh2d=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      thresh2d->GetXaxis()->SetTitle("Column");
      thresh2d->GetYaxis()->SetTitle("Row");
      sprintf(name, "thresh1d_Mod_%d", id);
      sprintf(title, "Thresholds Module %d at %s", id, findFieldName(cfg, id));
      int nbins=binsx*binsy;
      TH1F* thresh1d=new TH1F(name, title, nbins, 0, (float)nbins); 
      thresh1d->GetXaxis()->SetTitle("Channel");
      thresh1d->SetOption("p9");
      sprintf(name, "sigma1d_Mod_%d", id);
      sprintf(title, "Sigma Module %d at %s", id, findFieldName(cfg, id));
      TH1F* sigma1d=new TH1F(name, title, nbins, 0, (float)nbins); 
      sigma1d->GetXaxis()->SetTitle("Channel");
      sigma1d->SetOption("p9");
      sprintf(name, "threshdist_Mod_%d", id);
      sprintf(title, "Threshold distribution Module %d at %s", id, findFieldName(cfg, id));
      TH1F* threshdist=new TH1F(name, title, 600, 0, 6000); 
      threshdist->GetXaxis()->SetTitle("Threshold");
      sprintf(name, "sigmadist_Mod_%d", id);
      sprintf(title, "Sigma distribution Module %d at %s", id, findFieldName(cfg, id));
      TH1F* sigmadist=new TH1F(name, title, 500, 0, 1000); 
      sigmadist->GetXaxis()->SetTitle("Sigma");
      sprintf(name, "BadPixels_Mod_%d", id);
      sprintf(title, "Bad Pixels Module %d at %s", id, findFieldName(cfg, id));
      TH2F* bad=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      bad->GetXaxis()->SetTitle("Column");
      bad->GetYaxis()->SetTitle("Row");
      TH1D* hits1d=new TH1D(Form("HitsPerBin_Mod_%d", id), 
			    Form("Hits per bin Mod %d at %s", id, findFieldName(cfg, id)), 
			    numval, -.5, (float)numval-.5) ;
      hits1d->GetXaxis()->SetTitle("Scan Point");
      PixelConfig* confb=findConfig(cfg, id);
      bool isfei4=(confb->getType()!="FEI3");
      if(isfei4 && scan->clearMasks()==true)clearFEI4Masks(confb);
      
      for (int i=1;i<=histo->GetNbinsX();i++){
	for(int j=1;j<=histo->GetNbinsY();j++){
	  thresh2d->SetBinContent(i,j,histo->GetBinContent(i,j));
	  thresh1d->SetBinContent((i-1)*histo->GetNbinsY()+j, histo->GetBinContent(i,j));
	  threshdist->Fill(histo->GetBinContent(i,j));
	  sigma1d->SetBinContent((i-1)*histo->GetNbinsY()+j, sigmahisto->GetBinContent(i,j));
	  sigmadist->Fill(sigmahisto->GetBinContent(i,j));
	  if(chi2Histo->GetBinContent(i,j)==0){
	    bad->SetBinContent(i,j,1);
	    if(isfei4){
	      confb->setFEMask(0,i,j, confb->getFEMask(0,i,j)&0xfe); //reset bit 0 (enable)
	      confb->setFEMask(0,i,j, confb->getFEMask(0,i,j)|0x8); //reset bit 3 (hitbus)
	    }
	  } 
	}
      }
      TF1 gauss("gauss", "gaus", 100, 10000);
      gauss.SetParameter(0, threshdist->GetMaximum());
      gauss.SetParameter(1, threshdist->GetMaximumBin()*threshdist->GetBinWidth(1));
      threshdist->Fit(&gauss,"q", "", 100, 10000);
      sigmadist->Fit("gaus","q");
      threshs.push_back(threshdist->GetMean());
      thresherrs.push_back(threshdist->GetRMS());
      sigmas.push_back(sigmadist->GetMean());
      sigmaerrs.push_back(sigmadist->GetRMS());

      for(unsigned int k=0;k<numval;k++){
	boost::regex re2("Mean");
	std::string histoName = boost::regex_replace (hname, re2, Form("Occupancy_Point_%03d", k));
	TH2* occHisto=(TH2*)gDirectory->Get(histoName.c_str());
	if(occHisto==0){
	  std::cout<<"No Occupancy histograms found. Won't fill 1-d hit histo."<<std::endl;
	  break;
	}
	hits1d->SetBinContent(k+1, occHisto->GetSumOfWeights());
      }

      if(isfei4) writeConfig(anfile, runno);
      anfile->cd();
      thresh2d->Write();
      thresh2d->SetDirectory(gDirectory);
      thresh1d->Write();
      thresh1d->SetDirectory(gDirectory);
      threshdist->Write();
      threshdist->SetDirectory(gDirectory);
      sigma1d->Write();
      sigma1d->SetDirectory(gDirectory);
      sigmadist->Write();
      sigmadist->SetDirectory(gDirectory);
      bad->Write();
      bad->SetDirectory(gDirectory);
      hits1d->Write();
      hits1d->SetDirectory(gDirectory);
    }
  }
  TH1D* mhis[2];
  mhis[0]=new TH1D(Form("1-Thresholds"), Form("Thresholds 1st stave"), 32, 0, 32);
  mhis[1]=new TH1D(Form("2-Thresholds"), Form("Thresholds 2nd stave"), 32, 0, 32);
  TH1D* shis[2];
  shis[0]=new TH1D(Form("1-Sigma"), Form("Sigma 1st stave"), 32, 0, 32);
  shis[1]=new TH1D(Form("2-Sigma"), Form("Sigma 2nd stave"), 32, 0, 32);
  char position[10];
  for(int i=0;i<4;i++){ //half stave
    if(i%2==0)position[0]='A';
    else position[0]='C';
    //mhis[i]->GetYaxis()->SetTitle("Threshold (e)");
    for(int j=1;j<=8;j++){ //module
      for(int k=1;k<=2;k++){ //FE
	sprintf(&position[1], "%d-%d", j,k);
	int bin=0;
	if(i%2==0)bin=17-((j-1)*2+k); // A side
	else bin=16+(j-1)*2+k;
	mhis[i/2]->GetXaxis()->SetBinLabel(bin,position);
	shis[i/2]->GetXaxis()->SetBinLabel(bin,position);
	for(size_t l=0;l<pos.size();l++){
	  if(std::string(position)==pos[l].substr(1,4) && 
	     pos[l].substr(0,1)==Form("%d", i/2+1)){
	    mhis[i/2]->SetBinContent(bin, threshs[l]);
	    mhis[i/2]->SetBinError(bin, thresherrs[l]);
	    shis[i/2]->SetBinContent(bin, sigmas[l]);
	    shis[i/2]->SetBinError(bin, sigmaerrs[l]);
	  } 
	}
      }
    }
    if(i%2==1){
      anfile->cd();
      mhis[i/2]->Write();
      mhis[i/2]->SetDirectory(gDirectory);
      shis[i/2]->Write();
      shis[i/2]->SetDirectory(gDirectory);
    }
  }

  if(configUpdate())writeTopFile(cfg, anfile, runno);
  
}
