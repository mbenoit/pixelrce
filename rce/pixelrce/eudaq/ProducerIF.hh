#ifndef PRODUCERIF_HH
#define PRODUCERIF_HH

namespace eudaq{
  class Event;
  class Producer;
}

class ProducerIF{
public:
  static void sendEvent(eudaq::Event *ev);
  static void setProducer(eudaq::Producer* producer);
private:
  static eudaq::Producer *m_producer;
  
};

#endif
