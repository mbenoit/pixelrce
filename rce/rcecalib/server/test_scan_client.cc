//
//      test-client.cc
//
//      test application for IPC library
//
//      Sergei Kolos January 2000
//
//      description:
//              Shows how to access remote objects and call their methods
//	    Performs timing for the lookup and remote methods invocations
/////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>

#include <ers/ers.h>

#include <ipc/partition.h>
#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include <owl/timer.h>
#include <cmdl/cmdargs.h>
#include <unistd.h>


#include "IPCScanAdapter.hh"
#include "IPCConfigIFAdapter.hh"
#include "IPCFEI3Adapter.hh"
#include "ScanOptions.hh"
#include "IPCScanAdapter.hh"
#include "IPCScanRootAdapter.hh"

using namespace std;



int main( int argc, char ** argv )
{	
 
    CmdArgStr	partition_name ('p', "partition", "partition-name", "partition to work in.");


//
// Initialize command line parameters with default values
//
   
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmd(*argv, &partition_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmd.parse(arg_iter);
  
    IPCPartition   m_partition( (const char*)partition_name );
    
    // write scan config

    ipc::ScanOptions m_scanPar;
    ipc::PixelModuleConfig cfg;
    char* pointer=(char*)&m_scanPar;
    for (int i=0;i<sizeof(ipc::ScanOptions);i++){
      *pointer++=0;
    }
    pointer=(char *)&cfg;
    for (int i=0;i<sizeof(ipc::PixelModuleConfig);i++){
      *pointer++=0;
    }
    // send minimal module configuration

    // some random vcal gradients for one FEC
    cfg.FEConfig[0].FECalib.cinjLo=8.;
    cfg.FEConfig[0].FECalib.cinjHi=40;
    cfg.FEConfig[0].FECalib.vcalCoeff[0]=0.;
    cfg.FEConfig[0].FECalib.vcalCoeff[1]=0.003;
    cfg.FEConfig[0].FECalib.vcalCoeff[2]=0.000006;
    cfg.FEConfig[0].FECalib.vcalCoeff[3]=0.000000043;
    
    // integer scaled version for FE calib
    cfg.FEConfig[0].FECalibInt.cinjLo=8;
    cfg.FEConfig[0].FECalibInt.cinjHi=40;
    cfg.FEConfig[0].FECalibInt.vcalCoeff[0]=3;
    cfg.FEConfig[0].FECalibInt.vcalCoeff[1]=5;
    cfg.FEConfig[0].FECalibInt.vcalCoeff[2]=6;
    cfg.FEConfig[0].FECalibInt.vcalCoeff[3]=43;

    ipc::IPCConfigIFAdapter_var moduleHandle;
    try {
      moduleHandle = m_partition.lookup<ipc::IPCConfigIFAdapter>( "configIF" );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
      ers::error( ex );
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
      ers::error( ex );
    }
     try {
       ipc::ModSetup s;
       s.id=42;
       s.link=0;
       
       moduleHandle->IPCdeleteModules();
       std::cout<<"before setupmodules"<<std::endl;
       int ret=moduleHandle -> IPCsetupModule( "module","IPC_FEI3_multiThread",s );

       /* set up trigger method */
       std::cout<<"before createtrg"<<std::endl;
       ret=moduleHandle->IPCsetupTriggerIF("EventFromDsp");
       if(ret)std::cout<<"***ERROR*** IPCsetupTrigger failed"<<std::endl;
       std::cout<<"after createtrg"<<std::endl;
       
     }
     catch(CORBA::Exception & ex) {
       std::cerr<<"Corba exception "<<ex._name()<<std::endl;
     }
     ipc::IPCFEI3Adapter_var modhandle;
     try {
       bool v=m_partition.isObjectValid<ipc::IPCFEI3Adapter>("module");
       //if(v)std::cout<<"Valid"<<std::endl;
       if(!v) std::cout<<"Not valid"<<std::endl;
       modhandle = m_partition.lookup<ipc::IPCFEI3Adapter>( "module" );
     }
     catch( daq::ipc::InvalidPartition & ex ) {
       ers::error( ex );
     }
     catch( daq::ipc::ObjectNotFound & ex ) {
       ers::error( ex );
     }
     catch( daq::ipc::InvalidObjectName & ex ) {
       ers::error( ex );
     }

     try {
       modhandle -> IPCdownloadConfig( cfg );
     }
     catch(CORBA::Exception & ex) {
       std::cerr<<"Corba exception "<<ex._name()<<std::endl;
     }
     
     // zero everything - very important to avoid MARSHALL exception
     pointer=(char*)&m_scanPar;
     for (int i=0;i<sizeof(ipc::ScanOptions);i++){
       *pointer++=0;
     }
     // Simple receiver 
     m_scanPar.receiver=CORBA::string_dup("Simple");
     m_scanPar.parser=CORBA::string_dup("Simple");
     /* define data processor */
     
     m_scanPar.histOpt[ipc::IPC_SCAN_OCCUPANCY].optionMask = 1;
     m_scanPar.nLoops++;
     m_scanPar.scanLoop[0].endofLoopAction.Action = ipc::IPC_SCAN_FIT;
     m_scanPar.scanLoop[0].endofLoopAction.fitFunction =ipc::IPC_SCAN_SCURVE;
     /* needs to be defined for Parser Object */
     m_scanPar.trigOpt.nL1AperEvent=16;
     
     /* ScanLoop needs at least these defined */
     m_scanPar.scanLoop[0].nPoints = 101;
     m_scanPar.trigOpt.nEvents=100;
     m_scanPar.nMaskStages=3;
     m_scanPar.scanLoop[0].dataPoints.length(m_scanPar.scanLoop[0].nPoints);
     m_scanPar.scanLoop[0].scanParameter=ipc::IPC_SCAN_VCAL;
     //     m_scanPar.triggerMode=ipc::IPC_SCAN_USE_CLOW;
     
     for(unsigned int k=0;k<m_scanPar.scanLoop[0].nPoints;k++)
       m_scanPar.scanLoop[0].dataPoints[k]=k*2; // default Vcal scaling of 2
     
     ipc::IPCScanRootAdapter_var scanRootHandle;
     try {
       std::cerr << "calling lookup ... {" << std::endl;
       scanRootHandle = m_partition.lookup<ipc::IPCScanRootAdapter>( "scanRoot" );
       std::cerr << "} done" << std::endl;
     }
     catch( daq::ipc::InvalidPartition & ex ) {
       ers::error( ex );
     }
     catch( daq::ipc::ObjectNotFound & ex ) {
       ers::error( ex );
     }
     catch( daq::ipc::InvalidObjectName & ex ) {
       ers::error( ex );
     }



     try {
       std::clog << "Calling configure scan function ... {" << std::endl;
       scanRootHandle -> IPCconfigureScan("Regular", m_scanPar );
       //handle->shutdown();
       std::clog << "} done" << std::endl;
       
     }
     
     catch(CORBA::Exception & ex) {
       std::cerr<<"Corba exception "<<ex._name()  <<std::endl;
     }

     // finally start the scan

     ipc::IPCScanAdapter_var handleScanAdapter;
     try {
       std::cerr << "calling lookup ... {" << std::endl;
       handleScanAdapter = m_partition.lookup<ipc::IPCScanAdapter>( "scanCtrl" );
       std::cerr << "} done" << std::endl;
     }
     catch( daq::ipc::InvalidPartition & ex ) {
       ers::error( ex );
     }
     catch( daq::ipc::ObjectNotFound & ex ) {
       ers::error( ex );
     }
     catch( daq::ipc::InvalidObjectName & ex ) {
       ers::error( ex );
     }
     try {
       std::clog << "calling startScan function ... {" << std::endl;
       handleScanAdapter -> IPCstartScan( );
       std::clog << "} done" << std::endl;      
     }
     catch(CORBA::Exception & ex) {
       std::cerr<<"Corba exception "<<ex._name()<<std::endl;
     }




     return 0;

}
