#ifndef IPCCONFIGIF_CC
#define IPCCONFIGIF_CC

#include "config/IPCConfigIF.hh"

template <class TP>
IPCConfigIF<TP>::IPCConfigIF(IPCPartition & p, const char * name, ModuleFactory* mf):
  IPCNamedObject<POA_ipc::IPCConfigIFAdapter, TP>( p, name ) , ConfigIF(mf){
  try {
    IPCNamedObject<POA_ipc::IPCConfigIFAdapter,TP>::publish();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
}

template <class TP>
IPCConfigIF<TP>::~IPCConfigIF(){
  try {
    IPCNamedObject<POA_ipc::IPCConfigIFAdapter,TP>::withdraw();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::warning( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
}
  
//template <class TP>
//void IPCConfigIF<TP>::IPCsetupParameter(const char* name, CORBA::Long val){
//  setupParameter(name, val);
//}
template <class TP>
CORBA::ULong IPCConfigIF<TP>::IPCsetupParameter(const char* name, CORBA::Long val){
  setupParameter(name, val, false); //do not enable data taking because chip may be unconfigured
  return 0;
}
template <class TP>
void IPCConfigIF<TP>::IPCsetupMaskStage(CORBA::Long stage){
  setupMaskStage(stage);
}
template <class TP>
void IPCConfigIF<TP>::IPCsendTrigger(){
  sendTrigger();
}
template <class TP>
void IPCConfigIF<TP>::IPCenableTrigger(){
  enableTrigger();
}
template <class TP>
void IPCConfigIF<TP>::IPCdisableTrigger(){
  disableTrigger();
}
template <class TP>
void IPCConfigIF<TP>::IPCresetFE(){
  resetFE();
}
template <class TP>
void IPCConfigIF<TP>::IPCconfigureModulesHW(){
  configureModulesHW();
}
template <class TP>
CORBA::Long IPCConfigIF<TP>::IPCverifyModuleConfigHW(CORBA::Long id){
  return verifyModuleConfigHW(id);
}
template <class TP>
CORBA::ULong IPCConfigIF<TP>::IPCwriteHWregister(CORBA::ULong addr, CORBA::ULong val){
  return writeHWregister(addr,val);
}

template <class TP>
CORBA::ULong IPCConfigIF<TP>::IPCreadHWregister(CORBA::ULong addr, CORBA::ULong& val){
  return readHWregister(addr, (unsigned int&)val);
}

template <class TP>
CORBA::ULong IPCConfigIF<TP>::IPCsendHWcommand(CORBA::Octet opcode){
  return sendHWcommand(opcode);
}

template <class TP>
CORBA::ULong IPCConfigIF<TP>::IPCwriteHWblockData(const ipc::blockdata& data){
  std::vector<unsigned> bldat;
  for(size_t i=0;i<data.length();i++)bldat.push_back(data[i]);
  return writeHWblockData(bldat);
}

template <class TP>
CORBA::ULong IPCConfigIF<TP>::IPCreadHWblockData(const ipc::blockdata& data, ipc::blockdata_out retv){
  std::vector<unsigned> bldat;
  for(size_t i=0;i<data.length();i++)bldat.push_back(data[i]);
  std::vector<unsigned> retvec;
  unsigned retval=readHWblockData(bldat, retvec);
  retv=new ipc::blockdata;
  retv->length(retvec.size());
  for(size_t i=0;i<retvec.size();i++)retv[i]=(CORBA::ULong)retvec[i];
  return retval;
}
template <class TP>
CORBA::ULong IPCConfigIF<TP>::IPCreadHWbuffers(ipc::chardata_out retv){
  std::vector<unsigned char> retvec;
  unsigned retval=readHWbuffers(retvec);
  retv=new ipc::chardata;
  retv->length(retvec.size());
  for(size_t i=0;i<retvec.size();i++)retv[i]=(CORBA::Octet)retvec[i];
  return retval;
}
  
template <class TP>
CORBA::Long IPCConfigIF<TP>::IPCnTrigger(){
  return nTrigger();
}
template <class TP>
CORBA::Long IPCConfigIF<TP>::IPCsetupTriggerIF(const char* type){
  setupTriggerIF(type);
  return 0;
}
template <class TP>
CORBA::Long IPCConfigIF<TP>::IPCsetupModule(const char* name, const char* type, const ipc::ModSetup& par, const char* formatter){
  //std::cout<<"IPCsetupModule"<<std::endl;
  return setupModule(name, type, par.id, par.inLink, par.outLink, formatter);
}
template <class TP>
CORBA::Long IPCConfigIF<TP>::IPCdeleteModules(){
  deleteModules();
  return 0;
}
template <class TP>
void IPCConfigIF<TP>::shutdown(){
  std::cout<<"Shutdown"<<std::endl;
}

#endif
