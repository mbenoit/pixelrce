#ifndef SCANFITACTION_HH
#define SCANFITACTION_HH

#include "rcecalib/scanctrl/EndOfLoopAction.hh"
#include "rcecalib/dataproc/AbsDataProc.hh"
#include "rcecalib/scanctrl/RceCallback.hh"

class FitEoLAction: public EndOfLoopAction{
public:
  FitEoLAction(std::string name,  AbsDataProc* proc, std::string fitfunc):
    EndOfLoopAction(name),m_dataProc(proc),m_fitfunc(fitfunc){}
  int execute(){
    ipc::CallbackParams cbp;
    cbp.rce=RceName::getRceNumber();
    cbp.status=ipc::FITTING;
    cbp.maskStage=-1;
    cbp.loop0=-1;
    cbp.loop1=-1;
    RceCallback::instance()->sendMsg(ipc::HIGH,&cbp);
    return m_dataProc->fit(m_fitfunc);
  }
private:
  AbsDataProc* m_dataProc;
  std::string m_fitfunc;
};

#endif
