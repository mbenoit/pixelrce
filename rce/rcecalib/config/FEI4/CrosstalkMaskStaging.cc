#include "rcecalib/config/FEI4/CrosstalkMaskStaging.hh"
#include "rcecalib/config/FEI4/Module.hh"

namespace FEI4{
  
  CrosstalkMaskStaging::CrosstalkMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
    m_nStages=PixelRegister::N_ROWS*PixelRegister::N_COLS;
  }
  
  void CrosstalkMaskStaging::setupMaskStageHW(int maskStage){
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    unsigned rowstage=maskStage%PixelRegister::N_ROWS;
    unsigned colstage=maskStage/PixelRegister::N_ROWS;
    global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
      if(m_masks.xtalk_on&(1<<i)){
	pixel->setupMaskStageCol(colstage+1, i, rowstage, PixelRegister::N_ROWS);
	m_module->writeDoubleColumnHW(i, i, colstage/2, colstage/2); 
      }
      if(m_masks.xtalk_off&(1<<i)){
	pixel->setBitCol(colstage+1, i, 0);
	if(rowstage>0)pixel->setBit(i, rowstage, colstage+1, 1);
	if(rowstage+1<PixelRegister::N_ROWS)pixel->setBit(i, rowstage+2, colstage+1, 1);
	m_module->writeDoubleColumnHW(i, i, colstage/2, colstage/2); 
      }
      //clear previous column
      if(((m_masks.xtalk_off | m_masks.xtalk_on)&(1<<i)) && rowstage==0&&colstage!=0){
	pixel->setBitCol(colstage, i, 0);
	m_module->writeDoubleColumnHW(i, i, (colstage-1)/2, (colstage-1)/2); 
      }
    }
    if(colstage!=79) {
      global->setField("Colpr_Addr", (colstage+1)/2, GlobalRegister::HW); 
    } else {
      global->setField("Colpr_Addr", 39, GlobalRegister::HW); 
    }
  }
}
