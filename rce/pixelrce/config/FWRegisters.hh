#ifndef FWREGISTERS_HH
#define FWREGISTERS_HH


class FWRegisters{
private:
  static const unsigned int CHANNELMASK=0;
  static const unsigned int MODE=3;
  static const unsigned int CHANNELOUTMASK=4;
  static const unsigned int INCDISCDELAY=5;
  static const unsigned int DISCOP=6;
  static const unsigned int RESETDELAYS=7;
  static const unsigned int TRIGGERMASK=11;
  static const unsigned int DEADTIME=15;
  static const unsigned int ENCODING=20;
  static const unsigned int HITBUSOP=21;
  static const unsigned int MULTOPT_0 = 22;
  static const unsigned int MULTOPT_1 =23;
  static const unsigned int L1ROUTE=27;
  static const unsigned int HBDELAY=28;
  static const unsigned int HBDELAYNEG=29;
  static const unsigned int TELESCOPEOP=30;
  static const unsigned int TEMPFREQ=31;
  static const unsigned int TEMPENABLE=32;
  static const unsigned int MAXBUFLENGTH=33;
  static const unsigned int HITDISCCONFIG=96;
  static const unsigned int NEXP=37;
  static const unsigned int EFBTIMEOUT=38;
  static const unsigned int EFBTIMEOUTFIRST=39;
  static const unsigned int EFBMISSINGHEADERTIMEOUT=40;
  static const unsigned int RUNNUMBER=41;
  static const unsigned int NUMMON=42;
  static const unsigned int MONENABLED=43;
  
  static const unsigned int OUTPUTDELAYS=64;
  static const unsigned int EFBCOUNTERS=128;
  static const unsigned int NEVT=21;
  static const unsigned int NEVTNOMON=22;

  static const unsigned int BLOWOFF=0x882;

  static const unsigned int CMD_PRESENT=17;
  
public:
  enum opmode{NORMAL, TDCCALIB, EUDAQ};
  enum trgmask{SCINTILLATORS=1, CYCLIC=2, EUDET=4, EXTERNAL=8, HITBUS=16};
  enum streamencoding{NONE, BIPHASEMARK, MANCHESTER};
  enum routing{SINGLE, PATTERN};
  enum EFBCOUNTER{TIMEOUT, TOOMANYHEADERS, SKIPPEDTRIGGERS, BADHEADERS, MISSINGTRIGGERS, DATANOHEADER, DESYNCHED, OCCUPANCY};
  FWRegisters(){}
  virtual void writeRegister(int rce, unsigned reg, unsigned val)=0;
  virtual unsigned readRegister(int rce, unsigned reg)=0;
  virtual void sendCommand(int rce, unsigned opcode)=0;

  void setChannelmask(int rce, unsigned mask);
  void setMode(int rce, opmode mode);
  void setChannelOutmask(int rce, unsigned mask);
  void incrementDiscDelay(int rce, int channel);
  void setDiscOpMode(int rce, unsigned mode);
  void resetDelays(int rce);
  void setTriggermask(int rce, unsigned mask);
  void setDeadtime(int rce, unsigned deadtime);
  void setEncoding(int rce, streamencoding enc);
  void setHitbusOp(int rce, unsigned hb);
  void setL1Type(int rce, routing rt);
  void setHitbusDataDelay(int rce, unsigned delay);
  void setHitbusDataNegativeDelay(int rce, unsigned delay);
  void setTelescopeOp(int rce, unsigned op);
  void setTemperatureReadoutFrequency(int rce, unsigned ticks);
  void setTemperatureReadoutEnable(int rce, bool on);
  void setNumberofFeFramesPgp(int rce, unsigned num);
  void setRcePresent(int rce);
  void setHitDiscConfig(int rce, int chan, unsigned val);
  void setNExp(int rce, unsigned val);
  void setEfbTimeout(int rce, unsigned val);
  void setEfbTimeoutFirst(int rce, unsigned val);
  void setEfbMissingHeaderTimeout(int rce, unsigned val);
  void setRunNumber(int rce, unsigned val);
  void setOccNormalization(int rce, unsigned val);
  void enableMonitoring(int rce, unsigned val);
  unsigned getEfbCounter(int rce, int chan, EFBCOUNTER cnt);
  void setOutputDelay(int rce, unsigned inlink, unsigned val);
  void enableSLinkBlowoff(int rce, bool on);
  unsigned getNumberOfEvents(int rce);
  unsigned getNumberOfMonMissed(int rce);

};
  
#endif
