#ifndef DELAYSCANSETUP_HH
#define DELAYSCANSETUP_HH

#include "scanctrl/NestedLoop.hh"
#include "scanctrl/LoopSetup.hh"
#include "scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/ConfigIF.hh"

class DelayScanSetup: public LoopSetup{
public:
  DelayScanSetup():LoopSetup(){};
  virtual ~DelayScanSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
