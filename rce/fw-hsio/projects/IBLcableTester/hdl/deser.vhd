--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;

--------------------------------------------------------------

entity deser is

port(	clk: 	    in std_logic;
	rst:	    in std_logic;
	go:	    in std_logic;
	d_in:	    in std_logic;
	d_out:	    out std_logic_vector(31 downto 0);
        ld:         out std_logic
);
end deser;

--------------------------------------------------------------

architecture DESER of deser is

signal reg : std_logic_vector(31 downto 0);
signal going: std_logic;
signal counter: std_logic_vector(4 downto 0);

begin


    process(rst, clk)

    begin
        if(rst='1') then
          reg<=x"00000000";
          d_out<=x"00000000";
          going<='0';
          ld<='0';
        elsif (clk'event and clk='1') then
          if (going='0') then
            if(go='1' and d_in='1')then 
              going<='1';
            else
              counter<="11110";
            end if;
          else
            if (counter="11111") then
              ld<='1';
              d_out<=reg;
              if(go='0')then
                going<='0';
              end if;
            elsif(counter="11110")then
              ld<='0';
            end if;
            counter<=unsigned(counter)-1;
          end if;
          reg(31 downto 1)<=reg(30 downto 0);
          reg(0)<=d_in;
 	end if;
    
    end process;		

end DESER;

--------------------------------------------------------------
