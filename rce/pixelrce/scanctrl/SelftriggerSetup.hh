#ifndef SELFTRIGGERSETUP_HH
#define SELFTRIGGERSETUP_HH

#include "scanctrl/NestedLoop.hh"
#include "scanctrl/LoopSetup.hh"
#include "scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/ConfigIF.hh"

class SelftriggerSetup: public LoopSetup{
public:
  SelftriggerSetup():LoopSetup(){};
  virtual ~SelftriggerSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
