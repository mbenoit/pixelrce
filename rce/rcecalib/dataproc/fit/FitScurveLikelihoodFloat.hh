#ifndef FITSCURVELIKELIHOODFLOAT_HH
#define FITSCURVELIKELIHOODFLOAT_HH
#include "rcecalib/dataproc/fit/AbsFit.hh"
#include "rcecalib/util/RceHisto2d.cc"
#include "rcecalib/config/ConfigIF.hh"

template<typename TP, typename TE>
class  FitScurveLikelihoodFloat:public AbsFit {


  typedef struct
  {
    float mu, sigma, a0;
  } GaussianCurve;
  
  typedef struct {
    float deltaMu, deltaSigma; /* user provides initial deltaMu and deltaSigma */
    float muEpsilon, sigmaEpsilon; /* user provides convergence criterion */
    int nIters;
    int ndf; /* number of degrees of freedom */
    float chi2; /* final result for chi2 */
    int converge;
    float muConverge,sigmaConverge;
    int maxIters;
  void *curve;
  } Fit;
  
  typedef struct
  {
    unsigned int n;
    float *x;
    float *y;
  } FitData;
  
  
public:
  FitScurveLikelihoodFloat(ConfigIF *cif,std::vector<std::vector<RceHisto2d<TP, TE>*> > &histo,std::vector<int> &vcal, int nTrigger);
  int doFit();
  ~FitScurveLikelihoodFloat();
protected:
  inline float *log_ext(float x, GaussianCurve *psp);
  float errf_ext(float x, GaussianCurve *psp);
  float logLikelihood(FitData *pfd, GaussianCurve *s);
  int initFit(Fit *pFit);
  int extractGoodData(FitData *pfdi,FitData *pfdo, float scanA0);
  int initialGuess(FitData *pfd, GaussianCurve *pSParams);
  int fitPixel(FitData *pfd,void *vpfit);
  float chiSquared(FitData *pfd,GaussianCurve *s);
  Fit m_fit;
  /* buffer for fit data */
  float *m_x;
  float *m_y;
  std::vector<std::vector<RceHisto2d<TP, TE>*> > &m_histo;
  std::vector<std::vector<RceHisto2d<float, float>*> > m_fithisto;
};

#endif
