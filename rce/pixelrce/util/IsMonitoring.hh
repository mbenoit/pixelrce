// 
// IS monitoring
// 
// Martin Kocian, SLAC, 2/11/2016
//

#ifndef RCEISMONITORING_HH
#define RCEISMONITORING_HH

#include "util/Monitoring.hh"
class ISInfoDictionary;
class IPCPartition;

class IsMonitoring: public Monitoring{ 
public:
  IsMonitoring(IPCPartition &p);
  virtual ~IsMonitoring();
protected:
  virtual void Publish(const char* name, float val);
  virtual void Publish(const char* name, unsigned val);
  ISInfoDictionary* m_dict;
};
    
#endif
