
#include "config/FEI4/Fei4RegisterTestTrigger.hh"
#include "config/FEI4/FECommands.hh"
#include <boost/property_tree/ptree.hpp>
#include "HW/SerialIF.hh"
#include <iostream>


namespace FEI4{

  Fei4RegisterTestTrigger::Fei4RegisterTestTrigger():AbsTrigger(){
  }
  Fei4RegisterTestTrigger::~Fei4RegisterTestTrigger(){
  }
  int Fei4RegisterTestTrigger::configureScan(boost::property_tree::ptree* scanOptions){
    int retval=0;
    m_i=0; //reset the number of triggers
    try{
      //     int calL1ADelay = scanOptions->get<int>("trigOpt.CalL1ADelay");
    }
     catch(boost::property_tree::ptree_bad_path ex){
       std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
       retval=1;
     }
    return retval;
  }
  int Fei4RegisterTestTrigger::setupParameter(const char* name, int val){
    return 0;
  }

  
  int Fei4RegisterTestTrigger::sendTrigger(){
    m_triggerStream.clear();
    FECommands commands;
    commands.setBroadcast(true);
    int triggersPerStage=13*40;
    int maskstage=m_i/triggersPerStage;
    if(maskstage<2){ //pixel register test
      int trigger=m_i%triggersPerStage;
      int dcol=trigger/13;
      int dcolrev=((dcol&1)<<5) | ((dcol&2)<<3) | ((dcol&4)<<1) | ((dcol&8)>>1) | ((dcol&16)>>3) | ((dcol&32)>>5);
      int bit=trigger%13;
      dcolrev=dcolrev<<2;
      commands.switchMode(&m_triggerStream, FECommands::CONF);
      m_triggerStream.push_back(0);
      //colpr mode and addr
      commands.writeGlobalRegister(&m_triggerStream, 22, dcolrev);
      m_triggerStream.push_back(0);
      //SR clock
      commands.writeGlobalRegister(&m_triggerStream, 27, 0x8002);
      m_triggerStream.push_back(0);
      //set up strobe bit
      commands.writeGlobalRegister(&m_triggerStream, 13, 0xc000|(1<<(13-bit)));
      m_triggerStream.push_back(0);
      commands.globalPulse(&m_triggerStream, FECommands::PULSE_WIDTH);
      SerialIF::send(&m_triggerStream, SerialIF::WAITFORDATA); //global pulse
      //clear strobe/S0S1
      commands.writeGlobalRegister(&m_triggerStream, 13, 0);
      //setup shift readback
      commands.writeGlobalRegister(&m_triggerStream, 27, 0x8200);
      m_triggerStream.push_back(0);
      commands.feWriteCommand(&m_triggerStream);
      for(int i=0;i<21;i++)m_triggerStream.push_back(0); // refill with 0
      m_triggerStream.push_back(0);
      commands.writeGlobalRegister(&m_triggerStream, 27, 0x8000); //shift readback to 0
      m_triggerStream.push_back(0);
      SerialIF::send(&m_triggerStream, SerialIF::WAITFORDATA);
    }else{ //global register readback
      commands.switchMode(&m_triggerStream, FECommands::CONF);
      m_triggerStream.push_back(0);
      for (int i=1;i<=42;i++){ //Has to be 42 because 42 triggers are expected.
	int reg=i;
	if(reg>35)reg=35;
	commands.readGlobalRegister(&m_triggerStream, reg);
	m_triggerStream.push_back(0);
	m_triggerStream.push_back(0);
	m_triggerStream.push_back(0);
	m_triggerStream.push_back(0);
	m_triggerStream.push_back(0);
      }
      SerialIF::send(&m_triggerStream, SerialIF::WAITFORDATA);

    }

    m_i++;
    return 0;
  }

  int Fei4RegisterTestTrigger::enableTrigger(bool on){
    return SerialIF::enableTrigger(on);
    return 0;
  }
  int Fei4RegisterTestTrigger::resetCounters(){
    return 0;
  }
};
