#include "server/RceOfflineProducer.hh"
#include "server/CosmicDataReceiver.hh"
#include "config/ModuleInfo.hh"
#include "server/AbsController.hh"
#include "server/ServerFWRegisters.hh"
#include "eudaq/ProducerIF.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/Utils.hh"
#include "eudaq/Exception.hh"
#include "server/PixScan.hh"
#include "util/exceptions.hh"
#include "config/PixelConfig.hh"
#include "config/PixelConfigFactory.hh"
#include "config/FormatterFactory.hh"
#include "config/AbsFormatter.hh"
#include "util/RceName.hh"
#include <iostream>
#include <ostream>
#include <vector>
#include <stdio.h>
#include <pthread.h>
#include <boost/property_tree/ptree.hpp>

#ifdef __RCF__
#include "server/RCFcompat.hh"
#endif

using namespace CORBA;

namespace{
  static const std::string EVENT_TYPE = "APIX-CT";

  void* startScan(void* arg){
    AbsController* controller=(AbsController*)arg;
    controller->startScan();
    return 0;
  }
}
using namespace RCE;

RceOfflineProducer::RceOfflineProducer(const std::string & name, const std::string & runcontrol, AbsController* controller, int rce) 
  : eudaq::Producer(name, runcontrol), m_run(0), m_PlaneMask(0), m_conseq(1), m_rce(rce), m_controller(controller),
    m_options(0), m_datareceiver(0){
  std::cout<<"Rce producer on RCE "<<m_rce<<std::endl;
}

RceOfflineProducer::~RceOfflineProducer(){
  if (m_datareceiver) delete m_datareceiver;
  m_datareceiver = 0;
}
// This gets called whenever the DAQ is configured
void RceOfflineProducer::OnConfigure(const eudaq::Configuration & config) {
  std::cout << "Configuring: " << config.Name() << std::endl;
    std::cout<<config<<std::endl;
  char msg[64];
  try{
    //Default Trigger IF
    for(size_t i=0;i<m_moduleinfo.size();i++){
      delete m_moduleinfo[i]->getFormatter();
      delete m_moduleinfo[i];
    }
    m_moduleinfo.clear();
    m_controller->removeAllRces();
    m_controller->addRce(m_rce);
    m_controller->setupTrigger();
    m_controller->removeAllModules();
    m_numboards = config.Get("nFrontends", -1);
    if(m_numboards==-1)throw eudaq::Exception("nFrontends not defined");
    m_link.clear();
    m_sensor.clear();
    m_pos.clear();
    m_stype.clear();
    FormatterFactory ff;
    PixelConfigFactory pcf;
    ServerFWRegisters fw(m_controller);
    for(int i=0;i<m_numboards;i++){
      int outlink = config.Get("Module" + eudaq::to_string(i) + ".OutLink", "OutLink", -1);
      if(outlink==-1){
	sprintf(msg,"Module %d: Outlink is not defined.",i);
	throw eudaq::Exception(msg);
      }
      m_link.push_back(outlink);
      int inlink = config.Get("Module" + eudaq::to_string(i) + ".InLink", "InLink", -1);
      if(inlink==-1){
	sprintf(msg,"Module %d: Inlink is not defined.",i);
	throw eudaq::Exception(msg);
      }
      //fw.setOutputDelay(m_rce, inlink, m_config[i]->getPhase());
      int id = config.Get("Module" + eudaq::to_string(i) + ".FEID", "FEID", -1);
      if(id==-1){
	sprintf(msg,"Frontend %d: FEID is not defined.",i);
	throw eudaq::Exception(msg);
      }
      int sid = config.Get("Module" + eudaq::to_string(i) + ".SensorId", "SensorId", -1);
      if(sid==-1){
	sprintf(msg,"Frontend %d: sensor id is not defined.",i);
	throw eudaq::Exception(msg);
      }
      m_sensor.push_back(sid);
      int lr = config.Get("Module" + eudaq::to_string(i) + ".Position", "Position", -1);
      if(lr==-1){
	sprintf(msg,"Frontend %d: Position is not defined.",i);
	throw eudaq::Exception(msg);
      }
      m_pos.push_back(lr);
      int stype = config.Get("Module" + eudaq::to_string(i) + ".ModuleType", "ModuleType", -1);
      if(stype==-1){
	sprintf(msg,"Frontend %d: Module sensor type is not defined.",i);
	throw eudaq::Exception(msg);
      }
      if(stype<1 || stype>4){ //only 1, 2, and 4-chip modules and FEI3 chips are defined at this point
	sprintf(msg,"Frontend %d: Module sensor type %d does not exist.", i, stype);
	throw eudaq::Exception(msg);
      }
      if(stype!=3 && lr>=stype){
	sprintf(msg,"Frontend %d: Position %d does not exist in %d-chip sensors.", i, lr, stype);
	throw eudaq::Exception(msg);
      }
       if(m_stype.find(sid)==m_stype.end())m_stype[sid]=stype;
      else if(m_stype[sid]!=stype){
	sprintf(msg, "Module %d: Definition of module type clashes with the definition for a different FE of the same sensor", i);
	throw eudaq::Exception(msg);
      }
       //std::string modtype = config.Get("Module" + eudaq::to_string(i) + ".Type", "Type", "");
             //if(modtype==""){
	       //sprintf(msg,"Module %d: Module frontend type is not defined.",i);
	       //throw eudaq::Exception(msg);
       //}
      std::string filename = config.Get("Module" + eudaq::to_string(i) + ".File", "File", "");
      if(filename==""){
	sprintf(msg,"Module %d: Configuration filename is not defined.",i);
	throw eudaq::Exception(msg);
      }

      // create module via IPC/RCF
      char modname[32];
      sprintf(modname,"RCE%d_module_%d",m_rce, outlink);
      PixelConfig *cfg;
      try{
	cfg=pcf.createConfig(filename.c_str());
      }catch(rcecalib::Config_File_Error &err){
	std::cout<<err.what()<<std::endl;
	sprintf(msg, "Configuration: File %s does not exist.",filename.c_str());
	throw eudaq::Exception(msg);
      }
      std::string fetype=cfg->getType();
      AbsFormatter* fmt=ff.createFormatter(fetype.c_str(), id);
      if(fmt)cfg->configureFormatter(fmt);
      if(cfg->producesData()){
	m_moduleinfo.push_back(new ModuleInfo(modname, id, inlink, outlink, cfg->nChips(), cfg->nRows(), cfg->nCols(), fmt));
      }
      printf("%s: addModule (%s, %d, %d, %d, %d)\n", fetype.c_str(), modname, id, inlink, outlink, m_rce);
      m_controller->addModule(modname, fetype.c_str(),id, inlink, outlink, m_rce, "");
      m_controller->downloadModuleConfig(m_rce, id,cfg);
      delete cfg;
    } 
    // set up hardware trigger
    fw.setMode(m_rce, FWRegisters::EUDAQ);
    fw.setRcePresent(m_rce);
    
    // Scan configuration
    std::cout<<"Deleting scan options "<<m_options<<std::endl;
    delete m_options;
    PixScan scn(PixScan::COSMIC_DATA, PixLib::EnumFEflavour::PM_FE_I2);
    m_options=new ipc::ScanOptions;
    scn.setName("TCP"); // send data over the network rather than to file
    // Override l1a latency
    int latency = config.Get("Latency", -1);
    if(latency==-1){
      throw eudaq::Exception("Latency is not defined.");
    }
    scn.setLVL1Latency(latency);
    m_conseq = config.Get("ConseqTriggers", -1);
    if(m_conseq==(unsigned)-1){
      throw eudaq::Exception("Number of consecutive triggers is not defined.");
    }
    scn.setConsecutiveLvl1TrigA(0,m_conseq);
    int trgdelay = config.Get("Trgdelay", -1);
    if(trgdelay==-1){
      throw eudaq::Exception("Trigger delay is not defined.");
    }
    scn.setStrobeLVL1Delay(trgdelay);
    int deadtime = config.Get("Deadtime", 0);
    scn.setDeadtime(deadtime);
    int cyclic = config.Get("Cyclic_Period", 40000000);
    scn.setEventInterval(cyclic);
    scn.setTriggerMask(4); // eudet=4

    scn.convertScanConfig(*m_options);
    //m_controller->downloadScanConfig(*m_options);
    // At the end, set the status that will be displayed in the Run Control.
    SetStatus(eudaq::Status::LVL_OK, "Configured (" + config.Name() + ")");
  } catch (const std::exception & e) {
    printf("Caught exception: %s\n", e.what());
    SetStatus(eudaq::Status::LVL_ERROR, "Configuration Error");
  } catch (...) {
    printf("Unknown exception\n");
    SetStatus(eudaq::Status::LVL_ERROR, "Configuration Error");
  }
}

  // This gets called whenever a new run is started
  // It receives the new run number as a parameter
void RceOfflineProducer::OnStartRun(unsigned param) {
    m_run = param;
    std::string port="33000";
    std::cout << "Start Run: " << m_run << std::endl;
    std::stringstream ss("tcp://");
    ss<<getenv("ORBHOST");
    ss << ":" << port;
    std::string address = ss.str();
    std::cout << "<CosmicGui::startRun> : Server address is: " << address << std::endl;
    char name[128];
    sprintf(name,"CosmicGui|%05d|%s",m_run,address.c_str());
    m_options->name=string_dup(name);

    std::cout << "CosmicGui: starting CosmicDataReceiver at tcp://" << port << std::endl;
    std::vector<int> rcesAll;
    if(m_numboards>0) for(int i=0;i<m_numboards;i++)rcesAll.push_back(m_rce); //only one RCE
    else rcesAll.push_back(m_rce); // no FEs
    m_datareceiver = new CosmicDataReceiver(0, m_controller, m_moduleinfo, rcesAll, 
					    m_run, "tcp://" + port, "", false, false);

    m_controller->downloadScanConfig(*m_options);
    // It must send a BORE to the Data Collector
    eudaq::RawDataEvent bore(eudaq::RawDataEvent::BORE(EVENT_TYPE, m_run));
    // You can set tags on the BORE that will be saved in the data file
    // and can be used later to help decoding
    bore.SetTag("nFrontends", eudaq::to_string(m_numboards));
    bore.SetTag("consecutive_lvl1", eudaq::to_string(m_conseq));
    char tagname[128];
    for(int i=0;i<m_numboards;i++){
      sprintf(tagname, "OutLink_%d", i);
      bore.SetTag(tagname, eudaq::to_string(m_link[i]));
      sprintf(tagname, "SensorId_%d", i);
      bore.SetTag(tagname, eudaq::to_string(m_sensor[i]));
      sprintf(tagname, "Position_%d", i);
      bore.SetTag(tagname, eudaq::to_string(m_pos[i]));
      sprintf(tagname, "ModuleType_%d", i);
      bore.SetTag(tagname, eudaq::to_string(m_stype[m_sensor[i]]));
    }
    // Send the event to the Data Collector
    SendEvent(bore);
    // Enable writing events via tcp/ip
    ProducerIF::setProducer(this);
    //start run
    pthread_t mthread;
    pthread_create( &mthread, 0, startScan, (void*)m_controller);
    pthread_detach(mthread);
    //m_controller->startScan();

    std::cout<<"Started scan"<<std::endl;
    // At the end, set the status that will be displayed in the Run Control.
    SetStatus(eudaq::Status::LVL_OK, "Running");
  }

  // This gets called whenever a run is stopped
void RceOfflineProducer::OnStopRun() {
    std::cout << "Stopping Run" << std::endl;

    m_controller->stopWaitingForData();
    //wait for late events to trickle in
    sleep(1);
    //wait for scan to be complete
    bool idle=false;
    for(int i=0;i<50;i++){
      if(m_controller->getScanStatus()==0){
	std::cout<<"called getstatus"<<std::endl;
	idle=true;
	break;
      }
      usleep(100000);
    }
    if(idle==false)std::cout<<"Scan did not go into idle state"<<std::endl;
    //disable sending events via TCP/IP
    if (m_datareceiver) delete m_datareceiver;
    m_datareceiver = 0;

    ProducerIF::setProducer(0);
    // Send an EORE after all the real events have been sent
    // You can also set tags on it (as with the BORE) if necessary
    unsigned evnum=m_controller->getNEventsProcessed();
    SendEvent(eudaq::RawDataEvent::EORE(EVENT_TYPE, m_run, evnum));
    SetStatus(eudaq::Status::LVL_OK, "Stopped");
  }

  // This gets called when the Run Control is terminating,
  // we should also exit.
void RceOfflineProducer::OnTerminate() {
    std::cout << "Terminating..." << std::endl;
    if (m_datareceiver) delete m_datareceiver;
    m_datareceiver = 0;
    exit(0);
  }


