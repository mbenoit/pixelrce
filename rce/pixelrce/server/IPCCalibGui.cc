
#include <ipc/partition.h>
#include <ipc/core.h>
#include "server/CalibGui.hh"
#include "server/IPCController.hh"
#include "server/IPCHistoController.hh"
#include <boost/program_options.hpp>
#include <TROOT.h>
#include <TStyle.h>
#include "TApplication.h"

int main(int argc, char **argv){
  //
  // Initialize command line parameters with default values
  //
  try {
    IPCCore::init( argc, argv );
  }
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
    return 1;
  }
  
//
// Parse arguments
//
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("partition,p", boost::program_options::value<std::string>(), "partition to work in.");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);
  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }
  const char *p_name=getenv("TDAQ_PARTITION");
  if(p_name==NULL) p_name="rcetest";
  if(vm.count("partition"))p_name=vm["partition"].as<std::string>().c_str();
  IPCPartition* partition=new IPCPartition(p_name );
  AbsController* controller=new IPCController(*partition);
  AbsHistoController* hcontroller=new IPCHistoController(*partition);
  //check if somebody else is using the partition
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);

  TApplication theapp("app",&argc,argv);
  new CalibGui(controller, hcontroller, gClient->GetRoot(),800,735);
  theapp.Run();
  return 0;
}

