#ifndef DISABLETRIGGERACTION_HH 
#define DISABLETRIGGERACTION_HH 

#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/config/ConfigIF.hh"

class DisableTriggerAction: public LoopAction{
public:
  DisableTriggerAction(std::string name, ConfigIF* cif):
    LoopAction(name),m_configIF(cif){}
  int execute(int i){
    //std::cout<<"Disabled trigger"<<std::endl;
    return m_configIF->disableTrigger();
  }
private:
  ConfigIF* m_configIF;
};

#endif
