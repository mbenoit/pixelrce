#ifndef HITBUSCONFIGFILE_HH
#define HITBUSCONFIGFILE_HH

#include "HitbusModuleConfig.hh"
#include <string>
#include <map>

class HitbusConfigFile {
public:
  HitbusConfigFile(){}
  ~HitbusConfigFile();
  void readModuleConfig(ipc::HitbusModuleConfig* cfg, std::string filename);
  void writeModuleConfig(ipc::HitbusModuleConfig* config, const std::string &base, const std::string &confdir, 
			 const std::string &configname, const std::string &key);
  void dump(const ipc::HitbusModuleConfig& cfg);
private:
  unsigned short lookupToUShort(std::string par);
  int convertToUShort(std::string par, unsigned short& val);

  std::string m_moduleCfgFilePath;
  std::ifstream *m_moduleCfgFile;
  std::map<std::string, std::string> m_params;
};

#endif
