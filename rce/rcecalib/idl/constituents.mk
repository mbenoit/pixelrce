
ifneq ($(findstring linux,$(tgt_os)),)
libnames := idl 
endif
ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)
 modlibnames := idl 
endif


libsrcs_idl := ScanOptionsSK.cc \
	        CallbackSK.cc \
		PixelModuleConfigSK.cc \
		PixelFEI4GenConfigSK.cc \
		PixelFEI4AConfigSK.cc \
		PixelFEI4BConfigSK.cc \
		HitbusModuleConfigSK.cc \
		AFPHPTDCModuleConfigSK.cc \
		IPCScanRootAdapterSK.cc \
		IPCFEI3AdapterSK.cc \
		IPCFEI4AAdapterSK.cc \
		IPCFEI4BAdapterSK.cc \
		IPCHitbusAdapterSK.cc \
		IPCAFPHPTDCAdapterSK.cc \
		IPCScanAdapterSK.cc \
		IPCConfigIFAdapterSK.cc 




libincs_idl := $(ipc_include_path) \
		$(omniorb_include_path)


