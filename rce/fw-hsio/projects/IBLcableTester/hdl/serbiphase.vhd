--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;

--------------------------------------------------------------

entity serbiphase is

port(	clk: 	    in std_logic;
        ld:         out std_logic;
        go:         in std_logic;
        isgoing:      out std_logic;    -- debugging
        inj  :      in std_logic;  
	rst:	    in std_logic;
	d_in:	    in std_logic_vector(31 downto 0);
	d_out:	    out std_logic
);
end serbiphase;

--------------------------------------------------------------

architecture SERBIPHASE of serbiphase is

signal reg : std_logic_vector(31 downto 0);
signal gogo: std_logic;
signal clk40: std_logic;

signal ldout : std_logic;

begin

  ld<=ldout ;
  isgoing<=gogo;                        -- debugging

    process(clk)
    begin
      if (clk'event and clk='1') then
        clk40<=not clk40;
      end if;
    end process;

    process(rst, clk40)

    variable counter: std_logic_vector(3 downto 0);
    variable going: std_logic;
    begin
        if(rst='1') then
          reg<=x"00000000";
          d_out<='0';
          going:='0';
          ldout<='0';
        elsif (clk40'event and clk40='1') then
            gogo<=going;
            if (going='0' and go='1')then
              going:='1';
              counter:="0010";
            elsif (go='0' and counter="0010") then
              going:='0';  -- Empty flag is asserted instantly so go for
            end if;        -- another 32 bits to process the last word.              
            if  (counter="0000" and going='1') then
              reg<=d_in; 
              counter:="1111";
            else 
              reg(31 downto 2)<=reg(29 downto 0);
              reg(1 downto 0)<="00";
              if(counter="0010" and going='1') then  -- it takes 2 cycles to
                ldout<='1';                               -- get data from the FIFO;
              elsif(counter="0001" and going='1') then
                ldout<='0';                -- Request is only 1 cycle long.
              end if;
              counter:=unsigned(counter)-1;
            end if;
            d_out<=reg(31) xor reg(30) xor inj;
 	end if;
    
    end process;		

end SERBIPHASE;

--------------------------------------------------------------
