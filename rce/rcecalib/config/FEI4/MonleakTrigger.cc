
#include "rcecalib/config/FEI4/MonleakTrigger.hh"
#include "rcecalib/config/FEI4/FECommands.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/HW/SerialIF.hh"
#include "rcecalib/profiler/Profiler.hh"

namespace FEI4{

  MonleakTrigger::MonleakTrigger():AbsTrigger(){
  }
  MonleakTrigger::~MonleakTrigger(){
  }
  int MonleakTrigger::configureScan(boost::property_tree::ptree* scanOptions){
    int retval=0;
    m_i=0; //reset the number of triggers
    try{
      //     int calL1ADelay = scanOptions->get<int>("trigOpt.CalL1ADelay");
      setupTriggerStream();
    }
     catch(boost::property_tree::ptree_bad_path ex){
       rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
       ers::error(issue);
       retval=1;
     }
    return retval;
  }
  int MonleakTrigger::setupParameter(const char* name, int val){
    return 0;
  }

  void MonleakTrigger::setupTriggerStream(){
    m_triggerStream.clear();
    FECommands commands;
    commands.setBroadcast(true);
    
    commands.switchMode(&m_triggerStream, FECommands::CONF);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    //Set GADCSel to 7, which indicates that GADC will return the leakage current
    //Also sets PlsrRiseUpTau to 7 and enables PlsrPwr
    commands.writeGlobalRegister(&m_triggerStream, 31, 0xf007);  
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    //enables GADCStart, which means that when a GlobalPulse is sent, it will be sent
    //to the GADC.  Also, enables EN_PLL bit.  
    commands.writeGlobalRegister(&m_triggerStream, 27, 0x8400);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    commands.globalPulse(&m_triggerStream, 63);
    for(int i=0;i<16;i++)m_triggerStream.push_back(0);
    //readout GADC value.
    commands.readGlobalRegister(&m_triggerStream, 40);
  }
  
  int MonleakTrigger::sendTrigger(){
    SerialIF::send(&m_triggerStream,SerialIF::DONT_CLEAR|SerialIF::WAITFORDATA);
    m_i++;
    return 0;
  }

  int MonleakTrigger::enableTrigger(bool on){
    return SerialIF::enableTrigger(on);
    return 0;
  }
  int MonleakTrigger::resetCounters(){
    return 0;
  }
};
