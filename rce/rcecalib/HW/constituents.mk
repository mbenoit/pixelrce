
ifdef SWAP_DATA
CPPFLAGS+= '-DSWAP_DATA'
endif

ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)
modlibnames := HW 
libsrcs_HW := SerialIF.cc \
	      RCDImaster.cc \
	      SerialPgp.cc \
	      SerialPgpBw.cc \
	      SerialPgpFei4.cc \
              SerialHexdump.cc

libincs_HW := rcecalib \
              rceowl/owl \
              rceers/ers \
	      omniorb/include/$(tgt_arch) \
              boost
endif

ifneq ($(findstring linux,$(tgt_os)),)
libnames := HW 

libsrcs_HW := SerialIF.cc \
              SerialHexdump.cc

libincs_HW := rcecalib \
              rceowl/owl \
              rceers/ers \
	      omniorb/include/$(tgt_arch) \
              boost
endif

