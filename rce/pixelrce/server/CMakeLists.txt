if(${MDW} STREQUAL "IPC")
include_directories(${CMAKE_CURRENT_BINARY_DIR}/../idl)
include_directories($ENV{TDAQ_INST_PATH}/include  ${CMAKE_CURRENT_SOURCE_DIR}  $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/include  $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/include/ipc  $ENV{TDAQC_INST_PATH}/include  )
add_definitions( -D__IPC__ )
elseif(${MDW} STREQUAL "RCF")
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../rcf)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../RCF-2.1.0.0/include  ) 
add_definitions( -D__RCF__ )
endif()
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../config ${CMAKE_CURRENT_SOURCE_DIR}/../util  ) 
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../../../PixLibInterface) 
include_directories($ENV{ROOTSYS}/include)
set (rootlibs Gui Thread MathCore Physics Matrix Postscript Rint Tree Gpad Graf3d Graf Hist Net RIO Core)
LINK_DIRECTORIES($ENV{ROOTSYS}/lib)
LINK_DIRECTORIES($ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/lib)
LINK_DIRECTORIES($ENV{TDAQC_INST_PATH}/$ENV{CMTCONFIG}/lib)
SET(RCECOREPATH /daq/slc5/opt/rcecore/1.4/build/rce/lib/$ENV{CMTCONFIG} )
if(${ARCHITECTURE} STREQUAL "armlinux")
  add_executable(testslink testslink.cc PgpModL.cc)
  target_link_libraries (testslink HW boost_thread boost_system pthread)

  add_library( z SHARED IMPORTED )
  set_target_properties( z PROPERTIES IMPORTED_LOCATION /opt/AtlasRceSdk/V0.11.1/ArchLinux/usr/lib/libz.so)
if(${MDW} STREQUAL "RCF")
  add_executable(calibserver RCFlinux_server_pgp.cc PgpModL.cc ${CMAKE_CURRENT_SOURCE_DIR}/../RCF-2.1.0.0/src/RCF/RCF.cpp)
elseif(${MDW} STREQUAL "IPC")
  add_executable(calibserver IPClinux_server_pgp.cc PgpModL.cc)
endif()
  target_link_libraries (calibserver HW 
                                    rceconfig 
                                    scanctrl 
                                    dataproc 
                                    rceutil 
                                    eudaq 
                                    pthread
                                    boost_thread 
                                    boost_date_time 
                                    boost_regex 
                                    boost_system 
				    dl
				    pthread)
if(${MDW} STREQUAL "IPC")
  target_link_libraries (calibserver idl
                                    omniORB4 
                                    omnithread 
                                    owl 
                                    ipc 
                                    is 
                                    ers 
                                    ErsBaseStreams 
                                    oh 
                                    z)
endif()
  add_dependencies(calibserver scanctrl HW idl rceutil rceconfig dataproc)
#elseif(${ARCHITECTURE} STREQUAL "slclinux")
#  add_executable(calibserver linux_server_hex.cc /home/mkocian/rfc/RCF-2.0.1.101/src/RCF/RCF.cpp)
endif()
if(${ARCHITECTURE} STREQUAL "gen1rtems")
  SET(RCEMAJOR 1)
  SET(RCEMINOR 0)
  SET(RCEPROD prod)
  include_directories(/daq/slc5/opt/rcecore/1.4/include)
  add_library( z STATIC IMPORTED )
  set_target_properties( z PROPERTIES IMPORTED_LOCATION /daq/slc5/sw/lcg/external/zlib/1.2.3p1/ppc-rtems-rce405-opt/lib/libzz.a)
  add_library(calibservermod.${RCEMAJOR}.${RCEMINOR}.${RCEPROD} SHARED runservermod.cc calibserver.cc)
  target_link_libraries (calibservermod.${RCEMAJOR}.${RCEMINOR}.${RCEPROD}
                                    eudaq 
                                    rceconfig 
                                    idl
                                    scanctrl 
                                    dataproc 
                                    HW 
                                    rceutil 
                                    owl 
                                    oh 
                                    z
                                    omniORB4 
                                    boost_regex 
                                    rdb
                                    is 
                                    rdb
                                    ipc 
                                    omnithread 
                                    z
                                    )
  add_dependencies(calibservermod.${RCEMAJOR}.${RCEMINOR}.${RCEPROD} scanctrl HW idl rceutil rceconfig dataproc)
  SET(RCEMODULE ${PROJECT_BINARY_DIR}/lib/calibservermod.${RCEMAJOR}.${RCEMINOR}.${RCEPROD}.so)
  SET(RCELIB ${PROJECT_BINARY_DIR}/lib/libcalibservermod.${RCEMAJOR}.${RCEMINOR}.${RCEPROD}.so)
  add_custom_command(OUTPUT ${RCEMODULE}
  COMMAND powerpc-rtems4.9-objcopy --set-section-flags .bss=alloc,contents,load ${RCELIB} ${RCEMODULE} VERBATIM
  DEPENDS calibservermod.${RCEMAJOR}.${RCEMINOR}.${RCEPROD})
  add_custom_target(rcemodule ALL DEPENDS ${RCEMODULE})

  set(RCE_INTERFACE 1 CACHE INTEGER "RCE eth interface")
  STRING(TIMESTAMP BUILDDATE "\"%Y/%m/%d-%H:%M:%S\"")
  add_definitions(-DRCE_INTERFACE=${RCE_INTERFACE})
  add_definitions(-DBUILDDATE=${BUILDDATE})
  add_definitions(-DBUILDUSER=\"$ENV{USER}\")
  SET(RCEUSRCOREPATH /daq/slc5/opt/rcecore/1.4/build/rceusr/lib/$ENV{CMTCONFIG})
  SET(RTEMSPATH /daq/slc5/opt/rtems-4.9.2/target/powerpc-rtems/rce405/lib )
  add_library( notimer STATIC IMPORTED )
  set_target_properties( notimer PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/no-timer.rel)
  add_library( nosignal STATIC IMPORTED )
  set_target_properties( nosignal PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/no-signal.rel)
  add_library( nopart STATIC IMPORTED )
  set_target_properties( nopart PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/no-part.rel)
  add_library( nodpmem STATIC IMPORTED )
  set_target_properties( nodpmem PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/no-dpmem.rel)
  add_library( nortmon STATIC IMPORTED )
  set_target_properties( nortmon PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/no-rtmon.rel)
  add_library( nomp STATIC IMPORTED )
  set_target_properties( nomp PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/no-mp.rel)
  add_library( rceshell STATIC IMPORTED )
  set_target_properties( rceshell PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librceshell.a)
  add_library( rcenet STATIC IMPORTED )
  set_target_properties( rcenet PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librcenet.a)
  add_library( rceservice STATIC IMPORTED )
  set_target_properties( rceservice PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librceservice.a)
  add_library( rcefci STATIC IMPORTED )
  set_target_properties( rcefci PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librcefci.a)
  add_library( rcedebug STATIC IMPORTED )
  set_target_properties( rcedebug PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librcedebug.a)
  add_library( rcepic STATIC IMPORTED )
  set_target_properties( rcepic PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librcepic.a)
  add_library( rcepgp STATIC IMPORTED )
  set_target_properties( rcepgp PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librcepgp.a)
  add_library( rceethernet STATIC IMPORTED )
  set_target_properties( rceethernet PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librceethernet.a)
  add_library( rcebsdnet STATIC IMPORTED )
  set_target_properties( rcebsdnet PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librcebsdnet.a)
  add_library( rceinit STATIC IMPORTED )
  set_target_properties( rceinit PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librceinit.a)
  add_library( dynalink STATIC IMPORTED )
  set_target_properties( dynalink PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/libdynalink.a)
  add_library( gdbstub STATIC IMPORTED )
  set_target_properties( gdbstub PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/libgdbstub.a)
  add_library( rceusrinit STATIC IMPORTED )
  set_target_properties( rceusrinit PROPERTIES IMPORTED_LOCATION ${RCEUSRCOREPATH}/librceusrinit.a)
  add_library( rcezcpnet STATIC IMPORTED )
  set_target_properties( rcezcpnet PROPERTIES IMPORTED_LOCATION ${RCEUSRCOREPATH}/librcezcpnet.a)
  add_library( rcedummy STATIC IMPORTED )
  set_target_properties( rcedummy PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/libdummy.a)
  add_library( rtemsbsp STATIC IMPORTED )
  set_target_properties( rtemsbsp PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/librtemsbsp.a)
  add_library( telnetd STATIC IMPORTED )
  set_target_properties( telnetd PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/libtelnetd.a)
  add_library( rtemscpu STATIC IMPORTED )
  set_target_properties( rtemscpu PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/librtemscpu.a)
  add_library( nfs STATIC IMPORTED )
  set_target_properties( nfs PROPERTIES IMPORTED_LOCATION ${RTEMSPATH}/libnfs.a)
  SET(CMAKE_EXE_LINKER_FLAGS "-D__powerpc__ -D__GNU__ -D__GLIBC__=2 -DBOOST_THREAD_POSIX -O4 -Dtgt_opt_opt -Dtgt_cpu_family_ppc -Dtgt_os_rtems -Dtgt_board_rce405 -Dppc405=ppc405 -B/daq/slc5/opt/rtems-4.9.2/target/powerpc-rtems/rce405/lib -specs /daq/slc5/opt/rcecore/1.4/ldtools/dynamic_specs -qrtems -qnolinkcmds -Wl,-T/daq/slc5/opt/rcecore/1.4/ldtools/dynamic.ld -mcpu=403 -mlongcall -u pthread_attr_destroy -u _ZN3ers13Configuration15verbosity_levelEi -u _ZN3ers5IssueD2Ev -u sem_trywait -u _ZTIi -u _ZN3ers5Issue15prepend_messageERKSs -u sem_getvalue -u pthread_getspecific -u sem_post -u sem_wait -u __lesf2 -u _ZN3ers11LocalStream7warningERKNS_5IssueE -u pthread_mutex_trylock -u clearerr -u pthread_cond_broadcast -u _ZN3ers13StreamManager3logERKNS_5IssueE -u _ZTIj -u ferror -u __eqsf2 -u _ZN3ers13StreamManager8instanceEv -u sched_yield -u pthread_mutex_destroy -u pthread_mutex_init -u _ZN3ers13Configuration8instanceEv -u sched_get_priority_min -u _ZTIt -u freeifaddrs -u pipe -u __mulsf3 -u _ZN3ers5IssueC2ERKNS_7ContextERKSs -u pthread_mutex_lock -u _ZN3ers11LocalStream8instanceEv -u _ZTIm -u pthread_detach -u _ZN3ers13StreamManager12report_issueENS_8severityERKNS_5IssueE -u _ZTIl -u _ZN3ers12IssueFactory14register_issueERKSsPFPNS_5IssueERKNS_7ContextEE -u _ZN3ers12LocalContext9c_processE -u sem_destroy -u pthread_setschedparam -u pthread_cond_wait -u __floatsisf -u pthread_mutex_unlock -u strtoll -u _ZN3ers11LocalStream5errorERKNS_5IssueE -u pthread_cond_init -u getifaddrs -u __floatundisf -u _ZN3ers11LocalStream5fatalERKNS_5IssueE -u isspace -u pthread_cond_destroy -u __moddi3 -u pthread_attr_getstacksize -u tolower -u pthread_getschedparam -u _ZN3ers12IssueFactory8instanceEv -u __divdi3 -u _ZN3erslsERSoRKNS_5IssueE -u pthread_cond_timedwait -u _ZN3ers5IssueC2ERKNS_7ContextERKSt9exception -u _ZTIh -u _ZN3ers5IssueC2ERKS0_ -u select -u _ZN3ers13StreamManager5debugERKNS_5IssueEi -u _ZTIN3ers5IssueE -u pthread_key_create -u sem_init -u _ZN3ers12LocalContextC1EPKcS2_iS2_ -u pthread_mutexattr_init -u isalnum -u pthread_join -u pthread_cond_signal -u feof -u sched_get_priority_max -u pthread_setspecific -u malloc_report_statistics")
  add_executable(bootloader bootloader.cc rtems_config.cc EthPrimitive.cc CmdDecoder.cc)
  target_link_libraries(bootloader 
                                   notimer 
                                   nosignal 
                                   nopart 
                                   nodpmem 
                                   nortmon 
                                   nomp 
				   rcedummy
                                   rcezcpnet
                                   rceusrinit
                                   gdbstub
                                   dynalink
                                   rceinit
                                   rcebsdnet
                                   rceethernet
                                   rcepgp
                                   rcepic
                                   rcedebug
                                   rcefci
				   rceservice
                                   rcenet
                                   rceshell
				   rtemsbsp
				   telnetd
				   rtemscpu
				   nfs
				   ers
				   boost_date_time
				   boost_thread
                                   )
endif()

if($ENV{CMTCONFIG} STREQUAL "i686-slc5-gcc43-opt")
  include_directories(/daq/slc5/opt/rcecore/1.4/include)
  add_library( rcenet STATIC IMPORTED )
  set_target_properties( rcenet PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librcenet.so)
  add_library( rceservice STATIC IMPORTED )
  set_target_properties( rceservice PROPERTIES IMPORTED_LOCATION ${RCECOREPATH}/librceservice.so)
  add_executable(host_bootloader host_bootloader.cc RceControl.cc)
  target_link_libraries(host_bootloader 
                                        rcenet
                                        rceservice
                                        rt
                                        )
endif()

if(${ARCHITECTURE} STREQUAL "slclinux")
  set(CommonClientSrcFiles PixScan.cc
			   PrimList.cc)
  add_library(commonclient SHARED ${CommonClientSrcFiles})
  add_dependencies(commonclient idl)
if(${MDW} STREQUAL "IPC")
  set(IpcClientSrcFiles IPCController.cc
			IPCRegularCallback.cc
			IPCGuiCallback.cc)
  add_library(ipcclient SHARED ${IpcClientSrcFiles})
  add_dependencies(ipcclient idl)

  set(IpcRootClientSrcFiles IPCHistoController.cc)
  add_library(ipcrootclient SHARED ${IpcRootClientSrcFiles})
elseif(${MDW} STREQUAL "RCF")
  set(RcfClientSrcFiles RCFController.cc
			RCFCallback.cc)
  add_library(rcfclient SHARED ${RcfClientSrcFiles})
  set(RcfRootClientSrcFiles RCFHistoController.cc)
  add_library(rcfrootclient SHARED ${RcfRootClientSrcFiles})

endif()

  # Rootcint
  file(GLOB guiSources "*Linkdef.hh")
  foreach(guiFile ${guiSources})
   get_filename_component(guiFileName ${guiFile} NAME)
   string(REGEX REPLACE "Linkdef.hh" "rootDict.cc" dictFileName ${guiFileName})
   file(STRINGS ${guiFileName} guiDeps)
   set(guifilepath ${CMAKE_CURRENT_SOURCE_DIR}/${guiFileName})
   set(depstring "")
   foreach(guidep ${guiDeps})
     string(REGEX REPLACE ".*class *" "" guidepfinal ${guidep})
     set(depstring ${depstring} ${CMAKE_CURRENT_SOURCE_DIR}/${guidepfinal}.hh)
     list(APPEND depFiles ${CMAKE_CURRENT_SOURCE_DIR}/${guidepfinal}.hh)
   endforeach(guidep)
   get_property(inc_dirs DIRECTORY PROPERTY INCLUDE_DIRECTORIES)
   set(incstring "")
   foreach(incdir ${inc_dirs})
     set(incstring ${incstring} -I${incdir})
   endforeach(incdir)
   add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${dictFileName}
   COMMAND $ENV{ROOTSYS}/bin/rootcint -f ${CMAKE_CURRENT_BINARY_DIR}/${dictFileName} -c ${incstring} ${depstring} VERBATIM
   DEPENDS ${guifilepath} ${depstring})
  endforeach(guiFile)

if(${MDW} STREQUAL "IPC")
  add_executable(sendbitstream sendBitStream.cc)
  target_link_libraries (sendbitstream idl
  				       rceutil
				       rceconfig
				       HW
				       dataproc
				       eudaq
				       ipcclient
				       commonclient
                                       omniORB4 
                                       omnithread 
                                       ipc 
				       is
				       oh
                                       ers 
                                       ErsBaseStreams 
                     		       boost_program_options 
				       )
  add_dependencies(sendbitstream scanctrl HW idl rceutil rceconfig dataproc ipcclient commonclient)
				       
  add_executable(rebootHSIO IPCrebootHSIO.cc)
  target_link_libraries (rebootHSIO    idl
  				       rceutil
				       dataproc
				       rceconfig
				       HW
				       eudaq
				       ipcclient
				       commonclient
                                       omniORB4 
                                       omnithread 
                                       ipc 
				       is
				       oh
                                       ers 
                                       ErsBaseStreams 
				       boost_program_options
				       )
  add_dependencies(rebootHSIO scanctrl HW idl rceutil rceconfig dataproc ipcclient commonclient)

  add_executable(cosmicGui IPCCosmicGui.cc
                           CosmicGui.cc
                           ConfigGui.cc
			   GlobalConfigGui.cc
			   ScanLog.cc
			   ServerFWRegisters.cc
			   cosmicGui_rootDict.cc
			   CosmicDataReceiver.cc
			   CosmicOfflineDataProc.cc
			   CosmicMonitoringGuiEmbedded.cc
			   )
  target_link_libraries (cosmicGui     idl
  				       rceutil
				       rceconfig
				       HW
				       dataproc
				       eudaq
				       ipcclient
				       commonclient
                                       omniORB4 
                                       omnithread 
                                       ipc 
				       is
				       oh
                                       ers 
                                       ErsBaseStreams 
                                       boost_program_options 
				       ${rootlibs}
				       )
  add_dependencies(cosmicGui scanctrl HW idl rceutil rceconfig dataproc ipcclient commonclient)

  add_executable(calibGui  IPCCalibGui.cc 
                           CalibGui.cc
                           ConfigGui.cc
			   GlobalConfigGui.cc
			   ScanGui.cc
			   PrimListGui.cc
			   DataExporter.cc
			   ServerFWRegisters.cc
			   ScanLog.cc
			   CallbackInfo.cc
			   calibGui_rootDict.cc
			   )
  target_link_libraries (calibGui      idl
  				       rceutil
				       analysis
				       rceconfig
				       HW
				       dataproc
				       eudaq
				       ipcclient
				       commonclient
				       ipcrootclient
                                       omniORB4 
                                       omnithread 
                                       ipc 
				       is
				       oh
				       ohroot
                                       ers 
                                       ErsBaseStreams 
                                       boost_program_options
				       ${rootlibs}
				       )
  add_dependencies(calibGui scanctrl HW idl rceutil rceconfig dataproc ipcclient commonclient)

  add_executable(rceOfflineProducer  IPCRceOfflineProducer.cc
                                     RceOfflineProducer.cc
                           	     CosmicDataReceiver.cc
			   	     rceOfflineProducer_rootDict.cc
			   	     CosmicMonitoringGuiEmbedded.cc
			   	     ConfigGui.cc
			   	     ServerFWRegisters.cc
			   	     CosmicOfflineDataProc.cc
			             )
  target_link_libraries (rceOfflineProducer   idl
  				       	      rceutil
				       	      HW
				       	      dataproc
				       	      rceconfig
				       	      eudaq
				       	      ipcclient
				       	      commonclient
                                              omniORB4 
                                              omnithread 
                                              ipc 
				              is
				              oh
                                              ers 
                                              ErsBaseStreams 
                                              boost_program_options 
				              ${rootlibs}
				              )
  add_dependencies(rceOfflineProducer scanctrl HW idl rceutil rceconfig dataproc ipcclient commonclient)

  add_executable(analysisGui AnalysisGui.cc
			   ConfigGui.cc
			   analysisGui_rootDict.cc
			   )
  target_link_libraries (analysisGui	HW
                                       	idl
				        omnithread
                                     	omniORB4
  				       	rceutil
				       	rceconfig
                                       	analysis
                                       	commonclient
				       	boost_regex
                                       	ipc 
  				        is 
				    	oh
				        ${rootlibs}
				        )
  add_dependencies(analysisGui rceconfig idl analysis commonclient)


elseif(${MDW} STREQUAL "RCF")
  add_executable(calibGui  RCFCalibGui.cc 
                           CalibGui.cc
                           ConfigGui.cc
			   GlobalConfigGui.cc
			   ScanGui.cc
			   PrimListGui.cc
			   DataExporter.cc
			   ScanLog.cc
			   CallbackInfo.cc
			   calibGui_rootDict.cc
			   ServerFWRegisters.cc
                            ${CMAKE_CURRENT_SOURCE_DIR}/../RCF-2.1.0.0/src/RCF/RCF.cpp)	   
  target_link_libraries (calibGui      rceutil
				       rceconfig
				       HW
				       dataproc
				       eudaq
				       analysis
				       rcfclient
				       commonclient
				       rcfrootclient
                                       boost_program_options
                                       boost_regex
                                       boost_system
                                       boost_thread
				       ${rootlibs}
				       )
  add_dependencies(calibGui rcfrootclient rcfclient commonclient)

  add_executable(cosmicGui RCFCosmicGui.cc
                           CosmicGui.cc
                           ConfigGui.cc
			   GlobalConfigGui.cc
			   ScanLog.cc
			   ServerFWRegisters.cc
			   cosmicGui_rootDict.cc
			   CosmicDataReceiver.cc
			   CosmicOfflineDataProc.cc
			   CosmicMonitoringGuiEmbedded.cc
                           ${CMAKE_CURRENT_SOURCE_DIR}/../RCF-2.1.0.0/src/RCF/RCF.cpp)	  
  target_link_libraries (cosmicGui     rceutil
				       HW
				       dataproc
				       rceconfig
				       eudaq
				       analysis
				       rcfclient
				       commonclient
				       rcfrootclient
                                       boost_program_options
                                       boost_regex
                                       boost_system
                                       boost_thread
				       ${rootlibs}
				       )
  add_dependencies(cosmicGui rcfrootclient rcfclient commonclient)

  add_executable(rceOfflineProducer  RCFRceOfflineProducer.cc
                                     RceOfflineProducer.cc
                           	     CosmicDataReceiver.cc
			   	     rceOfflineProducer_rootDict.cc
			   	     CosmicMonitoringGuiEmbedded.cc
			   	     ConfigGui.cc
			   	     CosmicOfflineDataProc.cc
			   	     ServerFWRegisters.cc
                                     ${CMAKE_CURRENT_SOURCE_DIR}/../RCF-2.1.0.0/src/RCF/RCF.cpp
			             )
  target_link_libraries (rceOfflineProducer 
                                       rceutil
				       dataproc
				       HW
				       eudaq
				       rcfclient
				       commonclient
				       rcfrootclient
				       rceconfig
                                       boost_program_options
                                       boost_regex
                                       boost_system
                                       boost_thread
				       ${rootlibs}
				       )
  add_dependencies(rceOfflineProducer rcfrootclient rcfclient commonclient)

  add_executable(rebootHSIO RCFrebootHSIO.cc  ${CMAKE_CURRENT_SOURCE_DIR}/../RCF-2.1.0.0/src/RCF/RCF.cpp)
  target_link_libraries (rebootHSIO    rcfclient
				       HW
				       boost_program_options
                                       boost_system
                                       boost_thread
				       dl
				       )
  add_dependencies(rebootHSIO HW rcfclient)

  add_executable(analysisGui AnalysisGui.cc
			   ConfigGui.cc
			   analysisGui_rootDict.cc
			    ${CMAKE_CURRENT_SOURCE_DIR}/../RCF-2.1.0.0/src/RCF/RCF.cpp
			   )
  target_link_libraries (analysisGui	HW
  				       	rceutil
				       	rceconfig
                                       	analysis
                                       	commonclient
				       	boost_regex
                                        boost_system
                                        boost_thread
				        ${rootlibs}
				        )
  add_dependencies(analysisGui rceconfig idl analysis commonclient)
endif()
  add_executable(mergeMaskFilesFei4 MergeMaskFilesFei4.cc ../analysis/Fei4CfgFileWriter.cc)
  target_link_libraries (mergeMaskFilesFei4 ${rootlibs} )

  add_executable(histoViewer HistoViewerGui.cc
			   histoViewer_rootDict.cc
			   )
  target_link_libraries (histoViewer boost_regex
				       ${rootlibs}
				       )
  add_dependencies(histoViewer idl)
if(${MDW} STREQUAL "IPC")
  target_link_libraries (histoViewer omnithread
                                     omniORB4)
endif()

endif()
