#ifndef DISABLETRIGGEREOLACTION_HH
#define DISABLETRIGGEREOLACTION_HH

#include "scanctrl/EndOfLoopAction.hh"
#include "config/ConfigIF.hh"

class DisableTriggerEoLAction: public EndOfLoopAction{
public:
  DisableTriggerEoLAction(std::string name, ConfigIF* cif) :
    EndOfLoopAction(name),m_configIF(cif){}
  int execute(){
    std::cout<<"Disabling trigger"<<std::endl;
    m_configIF->disableTrigger();
    return 0;
  }
private:
  ConfigIF* m_configIF;
};

#endif
