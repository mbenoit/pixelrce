#!/bin/bash
pushd .
if [ -d ~/daq/rce/scripts ]; then
  rtems=buildrtems-tdaq4
  slc=buildslc-tdaq4
  if [ ! -d ~/daq/rce/$rtems ]; then
     echo Creating ~/daq/rce/$rtems
     mkdir ~/daq/rce/$rtems
  fi
  if [ ! -d ~/daq/rce/$slc ]; then
     echo Creating ~/daq/rce/$slc
     mkdir ~/daq/rce/$slc
  fi
  source ~/daq/rce/scripts/setup_rce-04-00-01.sh
  export MAKEFLAGS="-j12 QUICK=1"
  echo cd ~/daq/rce/$rtems
  cd ~/daq/rce/$rtems
  ~/daq/rce/scripts/cmake_script -DCMAKE_TOOLCHAIN_FILE=~/daq/rce/pixelrce/toolchain/tdaq4-rtems ~/daq/rce/pixelrce 
  echo cd ~/daq/rce/$slc
  cd ~/daq/rce/$slc
  ~/daq/rce/scripts/cmake_script -DCMAKE_TOOLCHAIN_FILE=~/daq/rce/pixelrce/toolchain/tdaq4-linux ~/daq/rce/pixelrce 
  export RCE_MOD=~/daq/rce/${rtems}/lib
  export PATH=${PATH}:~/daq/rce/${slc}/bin
  export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/daq/rce/${slc}/lib
  #export TDAQ_PARTITION=rce_$USER

else 
  echo "~/daq/rce/scripts does not exist"
fi
popd

