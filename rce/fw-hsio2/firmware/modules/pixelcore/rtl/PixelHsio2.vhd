-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : PixelHsio2.vhd
-- Author     : Martin Kocian  <kocian@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-10-21
-- Last update: 2014-10-21
-- Platform   : Vivado 2014.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;
use work.SsiPkg.all;
use work.SsiCmdMasterPkg.all;
use work.Pgp3p125GbpsPkg.all;

entity PixelHsio2 is
   port (
      -- Misc.
      extRst  : in  sl;
      led     : out slv(7 downto 0);
      -- PGP IOs
      pgpClkP : in  sl;
      pgpClkN : in  sl;
      pgpRxP  : in  sl;
      pgpRxN  : in  sl;
      pgpTxP  : out sl;
      pgpTxN  : out sl);   
end PixelHsio2;

architecture top_level of PixelHsio2 is

   constant NUM_AXI_MASTERS_C : natural := 2;

   constant VERSION_INDEX_C : natural := 0;
   constant HLS_INDEX_C     : natural := 1;

   constant VERSION_BASE_ADDR_C : slv(31 downto 0) := X"00000000";
   constant HLS_ADDR_C          : slv(31 downto 0) := X"00001000";
   
   constant AXI_CROSSBAR_MASTERS_CONFIG_C : AxiLiteCrossbarMasterConfigArray(NUM_AXI_MASTERS_C-1 downto 0) := (
      VERSION_INDEX_C => (
         baseAddr     => VERSION_BASE_ADDR_C,
         addrBits     => 12,
         connectivity => X"0001"),
      HLS_INDEX_C     => (
         baseAddr     => HLS_ADDR_C,
         addrBits     => 12,
         connectivity => X"0001")); 

   signal sAxiReadMaster  : AxiLiteReadMasterType;
   signal sAxiReadSlave   : AxiLiteReadSlaveType;
   signal sAxiWriteMaster : AxiLiteWriteMasterType;
   signal sAxiWriteSlave  : AxiLiteWriteSlaveType;

   signal mAxiWriteMasters : AxiLiteWriteMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiWriteSlaves  : AxiLiteWriteSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadMasters  : AxiLiteReadMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadSlaves   : AxiLiteReadSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);

   signal txAxisMaster : AxiStreamMasterType;
   signal txAxisSlave  : AxiStreamSlaveType;
   signal rxAxisMaster : AxiStreamMasterType;
   signal rxAxisSlave  : AxiStreamSlaveType;

   signal txReady,
      rxReady,
      sysClk,
      sysClkEn,
      sysRst,
      stableClk,
      stableRst : sl;
      
   attribute KEEP_HIERARCHY : string;
   attribute KEEP_HIERARCHY of
      PgpFrontEnd_Inst : label is "TRUE";      
   
begin

   led(7) <= '1';
   led(6) <= '0';
   led(5) <= txReady;
   led(4) <= rxReady;

   led(3 downto 0) <= (others => '0');

   ---------------------
   -- PGP Front End Core
   ---------------------
   PgpFrontEnd_Inst : entity work.PgpFrontEnd
      generic map(
         SLAVE_AXI_CONFIG_G  => ssiAxiStreamConfig(4),
         MASTER_AXI_CONFIG_G => ssiAxiStreamConfig(4))
      port map (
         -- Axi Master Interface - Registers (sysClk domain)      
         mAxiLiteReadMaster  => sAxiReadMaster,
         mAxiLiteReadSlave   => sAxiReadSlave,
         mAxiLiteWriteMaster => sAxiWriteMaster,
         mAxiLiteWriteSlave  => sAxiWriteSlave,
         -- Streaming Links (sysClk domain)      
         sAxisMaster         => txAxisMaster,
         sAxisSlave          => txAxisSlave,
         mAxisMaster         => rxAxisMaster,
         mAxisSlave          => rxAxisSlave,
         -- Clock, Resets, and Status Signals
         extRst              => extRst,
         stableClk           => stableClk,
         stableRst           => stableRst,
         sysClk              => sysClk,
         sysClkEn            => sysClkEn,
         sysClk40MHz         => open,
         sysClk100MHz        => sysClk,
         sysClk160MHz        => open,
         sysRst              => sysRst,
         txReady             => txReady,
         rxReady             => rxReady,
         -- GT Pins
         gtClkP              => pgpClkP,
         gtClkN              => pgpClkN,
         gtTxP               => pgpTxP,
         gtTxN               => pgpTxN,
         gtRxP               => pgpRxP,
         gtRxN               => pgpRxN); 

   -------------------------
   -- AXI-Lite Crossbar Core
   -------------------------         
   AxiLiteCrossbar_Inst : entity work.AxiLiteCrossbar
      generic map (
         NUM_SLAVE_SLOTS_G  => 1,
         NUM_MASTER_SLOTS_G => NUM_AXI_MASTERS_C,
         MASTERS_CONFIG_G   => AXI_CROSSBAR_MASTERS_CONFIG_C)
      port map (
         sAxiWriteMasters(0) => sAxiWriteMaster,
         sAxiWriteSlaves(0)  => sAxiWriteSlave,
         sAxiReadMasters(0)  => sAxiReadMaster,
         sAxiReadSlaves(0)   => sAxiReadSlave,
         mAxiWriteMasters    => mAxiWriteMasters,
         mAxiWriteSlaves     => mAxiWriteSlaves,
         mAxiReadMasters     => mAxiReadMasters,
         mAxiReadSlaves      => mAxiReadSlaves,
         axiClk              => sysClk,
         axiClkRst           => sysRst);        

   --------------------------
   -- AXI-Lite Version Module
   --------------------------          
   AxiVersion_Inst : entity work.AxiVersion
      generic map (
         EN_DEVICE_DNA_G => true)   
      port map (
         axiReadMaster  => mAxiReadMasters(VERSION_INDEX_C),
         axiReadSlave   => mAxiReadSlaves(VERSION_INDEX_C),
         axiWriteMaster => mAxiWriteMasters(VERSION_INDEX_C),
         axiWriteSlave  => mAxiWriteSlaves(VERSION_INDEX_C),
         axiClk         => sysClk,
         axiRst         => sysRst);            

   ------------------------------
   -- AXI-Lite HLS Example Module
   ------------------------------            
   AxiLiteExample_Inst : entity work.AxiLiteExample
      port map (
         axiClk         => sysClk,
         axiRst         => sysRst,
         axiReadMaster  => mAxiReadMasters(HLS_INDEX_C),
         axiReadSlave   => mAxiReadSlaves(HLS_INDEX_C),
         axiWriteMaster => mAxiWriteMasters(HLS_INDEX_C),
         axiWriteSlave  => mAxiWriteSlaves(HLS_INDEX_C));        

   ------------------------------------
   -- AXI Streaming: HLS Example Module
   ------------------------------------
   AxiStreamExample_Inst : entity work.AxiStreamExample
      port map (
         axisClk     => sysClk,
         axisRst     => sysRst,
         -- Slave Port
         sAxisMaster => rxAxisMaster,
         sAxisSlave  => rxAxisSlave,
         -- Master Port
         mAxisMaster => txAxisMaster,
         mAxisSlave  => txAxisSlave);   

end top_level;
