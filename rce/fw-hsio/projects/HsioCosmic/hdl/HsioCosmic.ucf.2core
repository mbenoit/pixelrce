
#-----------------------------------------------------------------------------
# Title         : LCLS BNL ASIC Test FPGA, Constraints File 
# Project       : LCLS BNL ASIC Test FPGA
#-----------------------------------------------------------------------------
# File          : HsioCosmic.ucf
# Author        : Ryan Herbst, rherbst@slac.stanford.edu
# Created       : 07/21/2008
#-----------------------------------------------------------------------------
# Description:
# This file contains all of the user constraints required to implement the
# LCLS BNL ASIC Test FPGA
#-----------------------------------------------------------------------------
# Copyright (c) 2008 by Ryan Herbst. All rights reserved.
#-----------------------------------------------------------------------------
# Modification history
# 07/21/2008: created.
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
#-------------------------- Timing Constraints -------------------------------
#-----------------------------------------------------------------------------
# This section contains the timing constraints for the FPGA
#-----------------------------------------------------------------------------

# Define system clocks
NET pgpClk     TNM_NET = FFS pgpClk;
NET sysClk125  TNM_NET = FFS sysClk125;
#NET clk320     TNM_NET = FFS clk320;
#NET sysClk250  TNM_NET = FFS sysClk250;
#NET mainClk  TNM_NET = FFS mainClk;
INST "U_triggerlogic/thetdc/stopcontrol" RLOC_ORIGIN="X44Y117";

# Define system clocks
TIMESPEC TS_pgpClk     = PERIOD pgpClk    6.4 ns HIGH 50%;
TIMESPEC TS_sysClk125  = PERIOD sysClk125 25.6 ns HIGH 50%;
#TIMESPEC TS_clk320     = PERIOD clk320    3.2 ns HIGH 50%;
#TIMESPEC TS_sysClk250  = PERIOD sysClk250 12.8 ns HIGH 50%;
#TIMESPEC TS_mainClk  = PERIOD mainClk 6.4 ns HIGH 50%;

# Define time groups for inter-clock constraints
TIMEGRP TG_pgpClk_r    = RISING  pgpClk;
TIMEGRP TG_sysClk125_r = RISING  sysClk125;
#TIMEGRP TG_sysClk250_r = RISING  sysClk250;
#TIMEGRP TG_mainClk_r = RISING  mainClk;

# Inter Domain Constraints
# TIMESPEC TS_pgpClk_r_sysClk125_r     = FROM TG_pgpClk_r    TO TG_sysClk125_r 6.4ns;
TIMESPEC TS_sysClk125_r_pgpClk_r     = FROM TG_sysClk125_r TO TG_pgpClk_r    6.4ns;
#TIMESPEC TS_sysClk125_r_sysClk250_r  = FROM TG_sysClk125_r TO TG_sysClk250_r 8ns;
# NET U_HsioFei4Core/go TIG;
NET U_HsioCosmicCore*/channelmask(*) TIG;
NET U_HsioCosmicCore*/channeloutmask(*) TIG;

timespec ts_02 = from ffs(*bz(0):*cz(0):*dz(0):*dz(1)) to ffs 640 MHz;
#timespec ts_03 = from ffs(*fei4fifo*) to ffs(*fei4dataflag*) 6.4ns;
#timespec ts_04 = from rams(*fei4fifo*) to ffs(*fei4dataflag*) 6.4ns;

# Nets to ignore for timing
NET "iResetInL" TIG;

NET serialinb(?) maxskew = 250 ps ;
#NET U_HsioCosmicCore/deldata maxskew = 250 ps ;

INST "U_HsioCosmicCore*/*receivedata/ff_*" IOB=FALSE;


# Nets to ignore for timing
NET "iResetInL" TIG;


#-----------------------------------------------------------------------------
#-------------------------- Pin Location Constraints -------------------------
#-----------------------------------------------------------------------------
# This section contains the pin location constraints for the design
#-----------------------------------------------------------------------------
# Old P5 = New P5
# Old P4 = New P4
# Old P9 = New P3
# Old P3 = New P2


NET "iPgpRefClkP"    LOC = "J1";
NET "iPgpRefClkM"    LOC = "K1";
#NET "iMainClkP"      LOC = "H17";
#NET "iMainClkN"      LOC = "J17";
# fiber 1
NET "iMgtRxN"        LOC = "N1";
NET "iMgtRxP"        LOC = "M1";
NET "oMgtTxN"        LOC = "T1";
NET "oMgtTxP"        LOC = "R1";
# fiber 2
#NET "iMgtRxN2"        LOC = "AA1";
#NET "iMgtRxP2"        LOC = "Y1";
#NET "oMgtTxN2"        LOC = "V1";
#NET "oMgtTxP2"        LOC = "U1";

NET "iMgtRxN2"        LOC = "AD1";
NET "iMgtRxP2"        LOC = "AC1";
NET "oMgtTxN2"        LOC = "AG1";
NET "oMgtTxP2"        LOC = "AF1";

NET "oReload"        LOC = "G16";
NET "iResetInL"      LOC = "AJ19";
NET "oDispClk"       LOC = "AG22";
NET "oDispDat"       LOC = "AJ22";
NET "oDispLoadL(1)"  LOC = "AK17";
NET "oDispLoadL(0)"  LOC = "Ak18";
NET "oDispRstL"      LOC = "AH22";
#NET "rclk"           LOC = "AM21";
# fiber 1
NET "transDis1"      LOC = "AH4";
NET "transDis1"      IOSTANDARD="LVCMOS33";
# fiber 2
#NET "transDis2"      LOC = "AK4";
#NET "transDis2"      IOSTANDARD="LVCMOS33";
NET "transDis2"      LOC = "Y4";
NET "transDis2"      IOSTANDARD="LVCMOS33";

NET "iExtreload"      LOC = "G15";
NET "iExtreload"      IOSTANDARD="LVCMOS25";
NET "iExtreload"      PULLUP;

NET "refclk_p"      LOC = "J25";
NET "refclk_p"      IOSTANDARD="LVDS_25";
NET "refclk_n"      LOC = "H25";
NET "refclk_n"      IOSTANDARD="LVDS_25";

NET "xclk_p"      LOC = "E24";
NET "xclk_p"      IOSTANDARD="LVDS_25";
NET "xclk_n"      LOC = "F24";
NET "xclk_n"      IOSTANDARD="LVDS_25";

NET "clkout40"      LOC = "AJ29";
NET "clkout40"      IOSTANDARD="LVCMOS33";
NET "clkout40"      SLEW="FAST";

NET "RD2"      LOC = "AJ24";
NET "RD2"      IOSTANDARD="LVCMOS33";
NET "RD2"      SLEW="FAST";

NET "AuxClk"      LOC = "AK29";
NET "AuxClk"      IOSTANDARD="LVCMOS33";
NET "AuxClk"      SLEW="FAST";

NET "RA"      LOC = "AM30";
NET "RA"      IOSTANDARD="LVCMOS33";
NET "RA"      SLEW="FAST";

NET "RD1"      LOC = "AH24";
NET "RD1"      IOSTANDARD="LVCMOS33";
NET "RD1"      SLEW="FAST";

NET "IOMXIN(3)"      LOC = "AG3";
NET "IOMXIN(3)"      IOSTANDARD="LVCMOS33";

NET "IOMXIN(2)"      LOC = "AF3";
NET "IOMXIN(2)"      IOSTANDARD="LVCMOS33";

NET "IOMXIN(1)"      LOC = "W9";
NET "IOMXIN(1)"      IOSTANDARD="LVCMOS33";

NET "IOMXIN(0)"      LOC = "Y9";
NET "IOMXIN(0)"      IOSTANDARD="LVCMOS33";

NET "IOMXSEL(2)"      LOC = "AB6";
NET "IOMXSEL(2)"      IOSTANDARD="LVCMOS33";
NET "IOMXSEL(2)"      SLEW="FAST";

NET "IOMXSEL(1)"      LOC = "AA6";
NET "IOMXSEL(1)"      IOSTANDARD="LVCMOS33";
NET "IOMXSEL(1)"      SLEW="FAST";

NET "IOMXSEL(0)"      LOC = "Y6";
NET "IOMXSEL(0)"      IOSTANDARD="LVCMOS33";
NET "IOMXSEL(0)"      SLEW="FAST";

NET "HITOR"      LOC = "AL29";
NET "HITOR"      IOSTANDARD="LVCMOS33";

NET "IOMXOUT(0)"      LOC = "AC4";
NET "IOMXOUT(0)"      IOSTANDARD="LVCMOS33";

NET "IOMXOUT(1)"      LOC = "AA8";
NET "IOMXOUT(1)"      IOSTANDARD="LVCMOS33";

NET "IOMXOUT(2)"      LOC = "AA9";
NET "IOMXOUT(2)"      IOSTANDARD="LVCMOS33";

NET "SELALTBUS"      LOC = "AK6";
NET "SELALTBUS"      IOSTANDARD="LVCMOS33";
NET "SELALTBUS"      SLEW="FAST";

NET "REGABDACLD"      LOC = "AD5";
NET "REGABDACLD"      IOSTANDARD="LVCMOS33";
NET "REGABDACLD"      SLEW="FAST";

NET "REGABSTBLD"      LOC = "AD4";
NET "REGABSTBLD"      IOSTANDARD="LVCMOS33";
NET "REGABSTBLD"      SLEW="FAST";

NET "SELCMD"      LOC = "AE6";
NET "SELCMD"      IOSTANDARD="LVCMOS33";
NET "SELCMD"      SLEW="FAST";

NET "CMDEXTTRIGGER"      LOC = "AF6";
NET "CMDEXTTRIGGER"      IOSTANDARD="LVCMOS33";
NET "CMDEXTTRIGGER"      SLEW="FAST";

NET "CMDALTPLS"      LOC = "AC3";
NET "CMDALTPLS"      IOSTANDARD="LVCMOS33";
NET "CMDALTPLS"      SLEW="FAST";

NET "iHSIOtrigger(0)"  LOC="J14";
NET "iHSIOtrigger(0)"  IOSTANDARD="LVTTL";
NET "iHSIOtrigger(0)"  SLEW=FAST;

NET "iHSIOtrigger(1)"  LOC="L14";
NET "iHSIOtrigger(1)"  IOSTANDARD="LVTTL";
NET "iHSIOtrigger(1)"  SLEW=FAST;

#NET "oHSIObusy"  LOC="H19";
#NET "oHSIObusy"  IOSTANDARD="LVTTL";
#NET "oHSIObusy"  SLEW=FAST;

NET "oHSIOtrigger"  LOC="L15";
NET "oHSIOtrigger"  IOSTANDARD="LVTTL";
NET "oHSIOtrigger"  SLEW=FAST;

NET "iHSIObusy"  LOC="H19";
NET "iHSIObusy"  IOSTANDARD="LVTTL";
NET "iHSIObusy"  SLEW=FAST;
NET "iHSIObusy"  PULLDOWN;

# Temperature ADC MAX145
#NET "ADC_Data_in_p"  LOC = "L4";
#NET "ADC_Data_in_n"  LOC = "L3";
#NET "ADC_Clk_p"      LOC = "T9";
#NET "ADC_Clk_n"      LOC = "R9";
#NET "ADC_nCS_p"      LOC = "U3";
#NET "ADC_nCS_n"      LOC = "T3";

# DI1
NET "oExttrgclkP"          LOC = "AH15";
NET "oExttrgclkN"          LOC = "AJ15";
# DI3
NET "oExtbusyP"            LOC = "AD14";
NET "oExtbusyN"            LOC = "AC13";
#DTO1
NET "iExttriggerP"        LOC = "J31";
NET "iExttriggerN"        LOC = "J30";
# DTO3
NET "iExtrstP"            LOC = "G30";
NET "iExtrstN"            LOC = "F30";

NET "serialout_p(0)"      LOC = "AG12";
NET "serialout_n(0)"      LOC = "AH12";
NET "serialout_p(1)"      LOC = "AF10";
NET "serialout_n(1)"      LOC = "AG10";
NET "serialout_p(3)"      LOC = "AH14";
NET "serialout_n(3)"      LOC = "AJ14";
NET "serialout_p(2)"      LOC = "AH10";
NET "serialout_n(2)"      LOC = "AJ10";
NET "serialout_p(4)"      LOC = "AJ12";
NET "serialout_n(4)"      LOC = "AK12";
NET "serialout_p(5)"      LOC = "AH8";
NET "serialout_n(5)"      LOC = "AH7";
NET "serialout_p(6)"      LOC = "AF11";
NET "serialout_n(6)"      LOC = "AG11";
NET "serialout_p(7)"      LOC = "AJ9";
NET "serialout_n(7)"      LOC = "AH9";
#NET "oExttrgclkP"          LOC = "AJ9";
#NET "oExttrgclkN"          LOC = "AH9";


NET "serialout_p(11)"      LOC = "AF15";
NET "serialout_n(11)"      LOC = "AG15";
NET "serialout_p(12)"      LOC = "AB13";
NET "serialout_n(12)"      LOC = "AA13";
NET "serialout_p(13)"      LOC = "AD10";
NET "serialout_n(13)"      LOC = "AD9";
NET "serialout_p(14)"      LOC = "AB11";
NET "serialout_n(14)"      LOC = "AA11";
#NET "serialout_p(15)"     LOC = "AD14";
#NET "serialout_n(15)"     LOC = "AC13";
#NET "oExtbusyP"            LOC = "AD14";
#NET "oExtbusyN"            LOC = "AC13";

NET "serialout(8)"      LOC = "AL21";

NET "serialin_p(0)"       LOC = "AK7";
NET "serialin_n(0)"       LOC = "AJ7";
NET "serialin_p(1)"       LOC = "AF9";
NET "serialin_n(1)"       LOC = "AE9";
NET "serialin_p(3)"       LOC = "AL10";
NET "serialin_n(3)"       LOC = "AM10";
NET "serialin_p(2)"       LOC = "AL9";
NET "serialin_n(2)"       LOC = "AK9";
NET "serialin_p(4)"       LOC = "AL11";
NET "serialin_n(4)"       LOC = "AM11";
NET "serialin_p(5)"       LOC = "AK13";
NET "serialin_n(5)"       LOC = "AL13";
NET "serialin_p(6)"       LOC = "AK14";
NET "serialin_n(6)"       LOC = "AL14";
NET "serialin_p(7)"       LOC = "AM8";
NET "serialin_n(7)"       LOC = "AM7";
#NET "iExttriggerP"        LOC = "AM8";
#NET "iExttriggerN"        LOC = "AM7";
NET "serialin_p(8)"       LOC = "AM13";
NET "serialin_n(8)"       LOC = "AM12";

NET "serialin_p(11)"      LOC = "K28";
NET "serialin_n(11)"      LOC = "J27";
NET "serialin_p(12)"      LOC = "H30";
NET "serialin_n(12)"      LOC = "H29";
NET "serialin_p(13)"      LOC = "C23";
NET "serialin_n(13)"      LOC = "C22";
NET "serialin_p(14)"      LOC = "E23";
NET "serialin_n(14)"      LOC = "F23";
#NET "serialin_p(15)"      LOC = "G30";
#NET "serialin_n(15)"      LOC = "F30";
#NET "iExtrstP"            LOC = "G30";
#NET "iExtrstN"            LOC = "F30";


NET "discinP(0)"         LOC = "AD7";
NET "discinP(1)"         LOC = "AD6";
NET "discinP(2)"         LOC = "AM6";
NET "discinP(3)"         LOC = "AL6";

NET "discinP(0)"         IOSTANDARD = "LVCMOS33";
NET "discinP(1)"         IOSTANDARD = "LVCMOS33";
NET "discinP(2)"         IOSTANDARD = "LVCMOS33";
NET "discinP(3)"         IOSTANDARD = "LVCMOS33";

#-------------------------- IO Standard Constraints --------------------------
#-----------------------------------------------------------------------------
# This section defines the IO types, IO delays and other IO parameters 
#-----------------------------------------------------------------------------

NET "iResetInL"      IOSTANDARD = "LVCMOS33";
NET "oDispClk"       IOSTANDARD = "LVCMOS33";
NET "oDispDat"       IOSTANDARD = "LVCMOS33";
NET "oDispLoadL(1)"  IOSTANDARD = "LVCMOS33";
NET "oDispLoadL(0)"  IOSTANDARD = "LVCMOS33";
NET "oDispRstL"      IOSTANDARD = "LVCMOS33";

NET "serialin_p(0)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(0)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(1)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(1)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(2)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(2)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(3)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(3)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(4)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(4)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(5)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(5)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(6)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(6)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(7)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(7)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(8)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(8)"  IOSTANDARD = "LVDS_25";

NET "serialin_p(11)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(11)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(12)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(12)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(13)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(13)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(14)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(14)"  IOSTANDARD = "LVDS_25";
NET "serialin_p(15)"  IOSTANDARD = "LVDS_25";
NET "serialin_n(15)"  IOSTANDARD = "LVDS_25";

NET "serialout_p(0)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(0)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(1)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(1)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(2)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(2)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(3)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(3)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(4)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(4)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(5)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(5)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(6)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(6)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(7)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(7)"  IOSTANDARD = "LVDS_25";
NET "serialout(8)"  IOSTANDARD = "LVCMOS33";

NET "serialout(8)"  SLEW = "FAST";

NET "serialout_p(11)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(11)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(12)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(12)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(13)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(13)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(14)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(14)"  IOSTANDARD = "LVDS_25";
NET "serialout_p(15)"  IOSTANDARD = "LVDS_25";
NET "serialout_n(15)"  IOSTANDARD = "LVDS_25";
