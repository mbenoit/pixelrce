#ifndef FEI4DIGITALMASKSTAGING_HH
#define FEI4DIGITALMASKSTAGING_HH

#include "rcecalib/config/Masks.hh"
#include "rcecalib/config/MaskStaging.hh"

namespace FEI4{
  class Module;
  
  class DigitalMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    DigitalMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  };
  
}
#endif
