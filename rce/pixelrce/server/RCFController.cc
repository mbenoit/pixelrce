
#include "util/RceName.hh"
#include "server/RCFController.hh"
#include "server/RCFCallback.hh"
#include "server/PixScan.hh"
#include "RCFFEI3Adapter.hh"
#include "RCFFEI4AAdapter.hh"
#include "RCFFEI4BAdapter.hh"
#include "RCFHitbusAdapter.hh"
#include "RCFAFPHPTDCAdapter.hh"
#include "RCFScanAdapter.hh"
#include "RCFConfigIFAdapter.hh"
#include "RCFScanRootAdapter.hh"
#include "ScanOptions.hh"
#include <stdio.h>
#include <string>

RCFController::RCFController(){}

void RCFController::addRce(int rce){
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)return; //RCE has already been added.
  m_rces.push_back(rce);
  char rcename[32];
  sprintf(rcename, "rce%d", rce);
  RcfClient<I_RCFScanRootAdapter> rclient(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
  rclient.RCFsetRceNumber(rce);
}

void RCFController::downloadModuleConfig(int rce, int id, PixelConfig* config){
  config->downloadConfig(rce, id);
}

void RCFController::addModule(const char* name, const char* type, int id, int inLink, int outLink, int rce, const char* formatter){
  char rcename[32];
  sprintf(rcename, "rce%d", rce);
  try {
    RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    ModSetup s;
    s.id=id;
    s.inLink=inLink;
    s.outLink=outLink;
    std::string typestring;
    if(std::string(type)=="FEI3")typestring="RCF_FEI3";
    else if(std::string(type)=="FEI4A")typestring="RCF_FEI4A";
    else if(std::string(type)=="FEI4B")typestring="RCF_FEI4B";
    else if(std::string(type)=="Hitbus")typestring="RCF_Hitbus";
    else if(std::string(type)=="HPTDC")typestring="RCF_AFPHPTDC";
    else(assert(0));
    client.RCFsetupModule( name,typestring.c_str(),s , formatter);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error in addModule: "<<ex.getErrorString()<<std::endl;
  }
}

int RCFController::setupTrigger(const char* type){
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFsetupTriggerIF(type);
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error in setupTrigger: "<<ex.getErrorString()<<std::endl;
    }
  }
  return 0;
}

void RCFController::removeAllModules(){
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFdeleteModules();
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error removeAllModules: "<<ex.getErrorString()<<std::endl;
    }
  }
}
void RCFController::startScan(){
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFScanAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFstartScan( );
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
}

void RCFController::abortScan(){
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFScanAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFabort( );
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
}

void RCFController::stopWaitingForData(){
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFScanAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFstopWaiting( );
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
}

void RCFController::getEventInfo(unsigned* nevent) {
  char rcename[32];
  sprintf(rcename, "rce%d", m_rces[0]);
  try {
    RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    *nevent=client.RCFnTrigger();
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
}
int RCFController::verifyModuleConfigHW(int rce, int id) {
  char rcename[32];
  sprintf(rcename, "rce%d", rce);
  int retval=0;
  try {
    RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    retval|=client.RCFverifyModuleConfigHW(id);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return retval;
}

void RCFController::setupParameter(const char* par, int val) {
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFsetupParameter(par, val);
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error in setupParameter: "<<ex.getErrorString()<<std::endl;
    }
  }
}

void RCFController::setupMaskStage(int stage) {
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFsetupMaskStage(stage);
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error in setupMaskStage: "<<ex.getErrorString()<<std::endl;
    }
  }
}

  
void RCFController::downloadScanConfig(ipc::ScanOptions& options){
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFScanRootAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFconfigureScan(options);
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error in downloadScanConfig: "<<ex.getErrorString()<<std::endl;
    }
  }
}

void RCFController::waitForScanCompletion(ipc::Priority pr, CallbackInfo* callb){
  RCFCallback *cb=new RCFCallback(callb);
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFScanRootAdapter> rclient(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      rclient.RCFconfigureCallback(pr);
      cb->addRce(m_rces[i]);
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
  cb->run();
  delete cb;
}
void RCFController::runScan(ipc::Priority pr, CallbackInfo* callb){
  RCFCallback *cb=new RCFCallback(callb);
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFScanAdapter> sclient(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      RcfClient<I_RCFScanRootAdapter> rclient(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      rclient.RCFconfigureCallback(pr);
      cb->addRce(m_rces[i]);
      sclient.RCFstartScan();
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error in runScan: "<<ex.getErrorString()<<std::endl;
    }
  }
  cb->run();
  delete cb;
}  

unsigned RCFController::getNEventsProcessed(){
  assert(m_rces.size()>0);
  char rcename[32];
  sprintf(rcename, "rce%d", m_rces[0]);
  try {
    RcfClient<I_RCFScanRootAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    return client.RCFnEvents();
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 0;
}

void RCFController::resynch(){
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFScanRootAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFresynch();
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
}

void RCFController::resetFE(){
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFresetFE();
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
}

void RCFController::configureModulesHW(){
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      client.RCFconfigureModulesHW();
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
}

unsigned RCFController::writeHWglobalRegister(const char* name, int reg, unsigned short val){
  char rcename[32];
  sprintf(rcename, "rce%d", m_rces[0]);
  try {
    RcfClient<I_RCFFEI4AAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT), name);
    return client.RCFwriteHWglobalRegister(reg,val);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 1000;
}

unsigned RCFController::readHWglobalRegister(const char* name, int reg, unsigned short &val){
  char rcename[32];
  sprintf(rcename, "rce%d", m_rces[0]);
  try {
    RcfClient<I_RCFFEI4AAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT), name);
    return client.RCFreadHWglobalRegister(reg,val);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 1000;
}

unsigned RCFController::writeHWdoubleColumn(const char* name, unsigned bit, unsigned dcol, std::vector<unsigned> data, std::vector<unsigned> &retvec){
  char rcename[32];
  sprintf(rcename, "rce%d", m_rces[0]);
  try {
    RcfClient<I_RCFFEI4AAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT), name);
    return client.RCFwriteDoubleColumnHW(bit, dcol, data, retvec);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 1000;
}
unsigned RCFController::readHWdoubleColumn(const char* name, unsigned bit, unsigned dcol, std::vector<unsigned> &retvec){
  char rcename[32];
  sprintf(rcename, "rce%d", m_rces[0]);
  try {
    RcfClient<I_RCFFEI4AAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT), name);
    return client.RCFreadDoubleColumnHW(bit, dcol, retvec);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 1000;
}

unsigned RCFController::sendHWcommand(int rce, unsigned char opcode){
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char rcename[32];
  sprintf(rcename, "rce%d", rce);
  try {
    RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    return client.RCFsendHWcommand(opcode);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 1000;
}

unsigned RCFController::writeHWregister(int rce, unsigned addr, unsigned val){
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char rcename[32];
  sprintf(rcename, "rce%d", rce);
  try {
    RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    return client.RCFwriteHWregister(addr, val);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 1000;
}

unsigned RCFController::readHWregister(int rce, unsigned addr, unsigned &val){
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char rcename[32];
  sprintf(rcename, "rce%d", rce);
  try {
    RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    return client.RCFreadHWregister(addr, val);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 1000;
}

unsigned RCFController::writeHWblockData(int rce, std::vector<unsigned>& data){
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char rcename[32];
  sprintf(rcename, "rce%d", rce);
  try {
    RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    return client.RCFwriteHWblockData(data);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 1000;
}
unsigned RCFController::readHWblockData(int rce, std::vector<unsigned>& data,std::vector<unsigned>& retvec ){
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char rcename[32];
  sprintf(rcename, "rce%d", rce);
  try {
    RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    return client.RCFreadHWblockData(data, retvec);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return 1000;
}

unsigned RCFController::readHWbuffers(int rce, std::vector<unsigned char>& retvec ){
  bool foundRce=false;
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)foundRce=true;
  assert(foundRce);
  char rcename[32];
  sprintf(rcename, "rce%d", rce);
  try {
    RcfClient<I_RCFConfigIFAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
    return client.RCFreadHWbuffers(retvec);
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
  return (unsigned)-1;
}

int RCFController::getScanStatus(){
  int status=0;
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, "rce%d", m_rces[i]);
    try {
      RcfClient<I_RCFScanAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      status |=client.RCFgetStatus( );
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
  return status;
}

