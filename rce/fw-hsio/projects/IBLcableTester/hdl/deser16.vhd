--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;

--------------------------------------------------------------

entity deser16 is

port(	clk: 	    in std_logic;
	rst:	    in std_logic;
	go:	    in std_logic;
	d_in:	    in std_logic;
	d_out:	    out std_logic_vector(15 downto 0);
        ld:         out std_logic
);
end deser16;

--------------------------------------------------------------

architecture DESER16 of deser16 is

signal reg : std_logic_vector(15 downto 0);
signal counter: std_logic_vector(3 downto 0);
signal going: std_logic;

begin


    process(rst, clk)

    begin
        if(rst='1') then
          reg<=x"0000";
          d_out<=x"0000";
          going<='0';
          ld<='0';
        elsif (clk'event and clk='1') then
          if(going='0' and go='1' and d_in='1')then 
            going<='1';
            counter<="1110";
          else
            if (going='1' and counter="1111") then
              ld<='1';
              d_out<=reg;
              if(go='0')then
                going<='0';
              end if;
            elsif(counter="1110")then
              ld<='0';
            end if;
            counter<=unsigned(counter)-1;
          end if;
          reg(15 downto 1)<=reg(14 downto 0);
	  reg(0)<=d_in;
 	end if;
    
    end process;		

end DESER16;

--------------------------------------------------------------
