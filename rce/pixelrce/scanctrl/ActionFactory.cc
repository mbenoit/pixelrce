
#include "scanctrl/ActionFactory.hh"

#include "config/ConfigIF.hh"
#include "dataproc/AbsDataProc.hh"
#include "scanctrl/Scan.hh"

#include <iostream>

// Loop actions
#include "scanctrl/SetupMaskStageAction.hh"
#include "scanctrl/SetupParamAction.hh"
#include "scanctrl/ChangeBinAction.hh"
#include "scanctrl/SendTriggerAction.hh"
#include "scanctrl/EnableTriggerAction.hh"
#include "scanctrl/DisableTriggerAction.hh"
#include "scanctrl/DisableAllChannelsAction.hh"
#include "scanctrl/ConfigureModulesAction.hh"
#include "scanctrl/ConfigureModulesNEAction.hh"
#include "scanctrl/CosmicSetInputDelayAction.hh"
#include "scanctrl/CosmicEnableTDCAction.hh"
#include "scanctrl/SetupTriggerAction.hh"
#include "scanctrl/CosmicEnableScintillatorTriggerAction.hh"
#include "scanctrl/PauseAction.hh"
#include "scanctrl/IfWaitAction.hh"
#include "scanctrl/SetChannelMaskAction.hh"

// End of loop actions 
#include "scanctrl/FitEoLAction.hh"
#include "scanctrl/CloseFileEoLAction.hh"
#include "scanctrl/DisableTriggerEoLAction.hh"
#include "scanctrl/DisableAllChannelsEoLAction.hh"
#include "scanctrl/ResetFEEoLAction.hh"

LoopAction* ActionFactory::createLoopAction(std::string action, boost::property_tree::ptree* pt){
  // create various loop actions based on action string
  if(action=="SETUP_MASK")
    return new SetupMaskStageAction(action,m_configIF,m_dataProc, pt);
  else if (action=="SETUP_PARAM")
    return new SetupParamAction(action,m_configIF, pt);
  else if (action=="CHANGE_BIN")
    return new ChangeBinAction(action,m_dataProc);
  else if (action == "SEND_TRIGGER")
    return new SendTriggerAction(action,m_configIF);
  else if (action == "SETUP_CHANNELMASK")
    return new SetChannelMaskAction(action,m_configIF);
  else if (action == "DISABLE_ALL_CHANNELS")
    return new DisableAllChannelsAction(action,m_configIF);
  else if (action == "CONFIGURE_MODULES")
    return new ConfigureModulesAction(action,m_configIF);
  else if (action == "CONFIGURE_MODULES_NO_ENABLE")
    return new ConfigureModulesNEAction(action,m_configIF);
  else if (action == "ENABLE_TRIGGER")
    return new EnableTriggerAction(action,m_configIF);
  else if (action == "DISABLE_TRIGGER")
    return new DisableTriggerAction(action,m_configIF);
  else if (action == "PAUSE")
    return new PauseAction(action,m_scan);
  else if (action == "COSMIC_SET_INPUT_DELAY")
    return new CosmicSetInputDelayAction(action,m_configIF,pt);
  else if (action == "COSMIC_ENABLE_TDC")
    return new CosmicEnableTDCAction(action,m_configIF);
  else if (action == "COSMIC_ENABLE_SCINTILLATOR_TRIGGER")
    return new CosmicEnableScintillatorTriggerAction(action,m_configIF);
  else if (action == "SETUP_TRIGGER")
    return new SetupTriggerAction(action,m_configIF, pt);
  else if (action == "IF_WAIT")
    return new IfWaitAction(action);
  else{
    std::cerr<<"Unknown Action"<<std::endl;
    return 0;
  }
}

EndOfLoopAction* ActionFactory::createEndOfLoopAction(std::string action, std::string fitfunc){
  if(action=="FIT")
    return new FitEoLAction(action,m_dataProc,fitfunc);
  if(action=="CALCULATE_MEAN_SIGMA")
    return new FitEoLAction(action,m_dataProc,fitfunc);
  if(action=="CLOSE_FILE")
    return new CloseFileEoLAction(action,m_dataProc);
  if(action=="DISABLE_TRIGGER")
    return new DisableTriggerEoLAction(action,m_configIF);
  if(action=="DISABLE_ALL_CHANNELS")
    return new DisableAllChannelsEoLAction(action,m_configIF);
  if(action=="RESET_FE")
    return new ResetFEEoLAction(action,m_configIF);
  else if(action=="NO_ACTION")
    return 0;
  else return 0;
}
