#ifndef COSMICDATASETUP_HH
#define COSMICDATASETUP_HH

#include "rcecalib/scanctrl/NestedLoop.hh"
#include "rcecalib/scanctrl/LoopSetup.hh"
#include "rcecalib/scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/ConfigIF.hh"

class CosmicDataSetup: public LoopSetup{
public:
  CosmicDataSetup():LoopSetup(){};
  virtual ~CosmicDataSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
