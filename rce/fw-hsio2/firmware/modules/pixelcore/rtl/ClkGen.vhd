-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : ClkGen.vhd
-- Author     : Martin Kocian  <kocian@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-10-23
-- Last update: 2016-01-25
-- Platform   : Vivado 2014.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Clock module for Hsio Pixel Core.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;

library unisim;
use unisim.vcomponents.all;

entity ClkGen is
   generic (
           -- Quad PLL Configurations
      QPLL_FBDIV_IN_G      : integer;
      QPLL_FBDIV_45_IN_G   : integer;
      QPLL_REFCLK_DIV_IN_G : integer);
   port (
      -- Clock, Resets
      extRstL            : in  sl;
      ttc_clk_40         : in sl;
      sysClk40           : out sl;
      sysRst40           : out sl;
      sysClk80          : out sl;
      sysClk160          : out sl;
      sysClk40Unbuf      : out sl;
      sysClk40Unbuf90    : out sl;
      sysClk160Unbuf     : out sl;
      sysClk160Unbuf90   : out sl;
      sysClkLock         : out sl;
      sysClkRst          : in sl;

      sysClk100           : out sl;
      sysRst100           : out sl;
      inpClk160           : out sl;
      axiClk40            : out sl;
      axiRst40            : out sl;

      pgpClk              : out sl;
      pgpRst              : out sl;
      -- QPll outputs
      QPllRefClk          : out slv(1 downto 0);
      QPllClk             : out slv(1 downto 0);
      QPllLock            : out slv(1 downto 0);
      QPllRefClkLost      : out slv(1 downto 0);
      QPllReset           : in  slv(1 downto 0);
      -- GT Pins
      gtClkP              : in  sl;
      gtClkN              : in  sl;
      gtClk2P             : in  sl;
      gtClk2N             : in  sl);
end ClkGen;

-- Define architecture
architecture mapping of ClkGen is



   --Misc.
   signal stableClock,
      stableClock2,
      stableReset,
      stableReset2,
      pgpClkInt,
      pgpRstInt,
      sysClk,
      sysClk100int,
      locked100,
      gtClkout,
      gtClkout2,
      gtClkDiv2,
      sysClkRstint,
      axiClk40int,
      locked : sl;
   signal PllReset: slv(1 downto 0);
   signal QPllLockDetClk: slv(1 downto 0);
   signal QPllRefClkIn: slv(1 downto 0);
   
begin

   pgpClk<=stableClock;
   pgpRst<=stableReset;
   sysClkLock<=locked;
   sysClk40<=sysClk;

   -- GT Reference Clock
   IBUFDS_GTE2_Inst : IBUFDS_GTE2
      port map (
         I     => gtClkP,
         IB    => gtClkN,
         CEB   => '0',
         ODIV2 => gtClkDiv2,
         O     => gtClkout);

   IBUFDS_GTE2_Inst2 : IBUFDS_GTE2
      port map (
         I     => gtClk2P,
         IB    => gtClk2N,
         CEB   => '0',
         ODIV2 => open,
         O     => gtClkout2);

   BUFG_Inst : BUFG
      port map (
         I => gtClkDiv2,
         O => stableClock);

   BUFG_Inst2 : BUFG
      port map (
         I => gtClkOut2,
         O => stableClock2);

   -- Power Up Reset      
   PwrUpRst_Inst : entity work.PwrUpRst
      generic map(
         IN_POLARITY_G => '0')
      port map (
         arst   => extRstL,
         clk    => stableClock,
         rstOut => stableReset);

   PwrUpRst_Inst2 : entity work.PwrUpRst
      generic map(
         IN_POLARITY_G => '0')
      port map (
         arst   => extRstL,
         clk    => stableClock2,
         rstOut => stableReset2);

   SysClkGen_Inst : entity work.SysClkGen
     port map ( 
       
       -- Clock in ports
       --clk_in1 => stableClock2,
       clk_in1 => ttc_clk_40,
       -- Clock out ports  
       sysClk40 => sysClk,
       sysClk320 => sysClk80,
       sysClk160 => sysClk160,
       sysClk40Unbuf => sysClk40Unbuf,
       sysClk40Unbuf90 => sysClk40Unbuf90,
       sysClk160Unbuf => sysClk160Unbuf,
       sysClk160Unbuf90 => sysClk160Unbuf90,
       -- Status and control signals                
       reset => sysClkRstint,
       locked => locked            
       );
   sysClkRstint <= not locked100 or sysClkRst;
   SysRst_Inst : entity work.RstSync
      generic map(
         IN_POLARITY_G  => '0',
         OUT_POLARITY_G => '1')
      port map(
         clk      => sysClk,
         asyncRst => locked,
         syncRst  => sysRst40);  

   SysClk100_inst: entity work.SysClk100
     port map(
       clk_in250 => stableClock2,
       clk_out100 => sysClk100int,
       clk_out160 => inpClk160,
       clk_out40 => axiClk40int,
       reset => stableReset2,
       locked => locked100);
   sysClk100 <= sysClk100int;
   axiClk40 <= axiClk40int;
   SysRst100_Inst : entity work.RstSync
      generic map(
         IN_POLARITY_G  => '0',
         OUT_POLARITY_G => '1')
      port map(
         clk      => sysClk100int,
         asyncRst => locked100,
         syncRst  => sysRst100);  
   SysRstaxi40_Inst : entity work.RstSync
      generic map(
         IN_POLARITY_G  => '0',
         OUT_POLARITY_G => '1')
      port map(
         clk      => axiClk40int,
         asyncRst => locked100,
         syncRst  => axiRst40);  

   QPllLockDetClk <= stableClock2 & stableClock2;
   PllReset(0) <= QPllReset(0) or stablereset;
   PllReset(1) <= QPllReset(1) or stablereset;
   qPllRefClkIn <= gtClkout & gtClkout;

   Gtp7QuadPll_Inst : entity work.Gtp7QuadPll  
      generic map (
         PLL0_REFCLK_SEL_G    => "010",
         PLL0_FBDIV_IN_G      => QPLL_FBDIV_IN_G,
         PLL0_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_G,
         PLL0_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_G,
         PLL1_REFCLK_SEL_G    => "010",
         PLL1_FBDIV_IN_G      => QPLL_FBDIV_IN_G,
         PLL1_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_G,
         PLL1_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_G)          
      port map (
         qPllRefClk     => qPllRefClkIn,
         qPllOutClk     => QPllClk,
         qPllOutRefClk  => QPllRefClk,
         qPllLock       => QPllLock,
         qPllLockDetClk => QPllLockDetClk,
         qPllRefClkLost => QPllRefClkLost,
         qPllReset      => PllReset);  

end mapping;
