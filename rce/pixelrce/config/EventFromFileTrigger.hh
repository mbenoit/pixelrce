#ifndef EVENTFROMFILETRIGGER_HH
#define EVENTFROMFILETRIGGER_HH

#include "config/AbsTrigger.hh"
#include <vector>


  class EventFromFileTrigger: public AbsTrigger{
  public:
    EventFromFileTrigger();
    ~EventFromFileTrigger(){};
    int sendTrigger();
    int enableTrigger(bool on){return 0;}
  private:
    std::vector<unsigned> m_event;
  };


#endif
