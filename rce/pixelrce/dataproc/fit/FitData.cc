#include "dataproc/fit/FitData.hh"

const int FitDataInt::binomial_weight[WEIGHT_LUT_LENGTH]={
#include "dataproc/fit/binomial_weight_int.dat"
};
const int FitDataInt::data_logx[2*LUT_LENGTH]={
#include "dataproc/fit/logx_ext_int.dat"
};
const int FitDataInt::data_errf[LUT_LENGTH]={
#include "errf_ext_int.dat"
};

const int FitDataFastInt::binomial_weight[WEIGHT_LUT_LENGTH]={
#include "dataproc/fit/binomial_weight_int.dat"
};
const int FitDataFastInt::data_logx[2*FAST_LUT_LENGTH]={
#include "dataproc/fit/logx_ext_fastint.dat"
};
const int FitDataFastInt::data_errf[FAST_LUT_LENGTH]={
#include "errf_ext_fastint.dat"
};
//FIXME
//#ifdef INCLUDE_FLOAT_FIT
const float FitDataFloat::binomial_weight[WEIGHT_LUT_LENGTH]={
#include "dataproc/fit/binomial_weight.dat"
};
const float FitDataFloat::data_logx[2*LUT_LENGTH]={
#include "dataproc/fit/logx_ext.dat"
};
const float FitDataFloat::data_errf[LUT_LENGTH]={
#include "errf_ext.dat"
};
//#endif


