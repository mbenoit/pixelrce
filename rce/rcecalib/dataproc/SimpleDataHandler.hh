#ifndef SIMPLEDATAHANDLER_HH
#define SIMPLEDATAHANDLER_HH

#include "rcecalib/dataproc/AbsDataHandler.hh"
#include "rcecalib/util/DataCond.hh"

class SimpleDataHandler:public AbsDataHandler{
public:
  SimpleDataHandler(AbsDataProc* proc, DataCond& datacond):
    AbsDataHandler(proc, datacond, 0){
  }
  virtual ~SimpleDataHandler(){
  }
  void handle(unsigned link, unsigned* data, int size) {
    if(size>0){
      m_dataProc->processData(link, data, size);
    }
  //m_scan->stopWaiting();
  omni_mutex_lock pl( m_dataCond.mutex );
  if(m_dataCond.waitingForData==true)m_dataCond.cond.signal();
  };
  
};
#endif
