#include "rcecalib/config/MaskStageFactory.hh"
#include "rcecalib/config/FEI3/Module.hh"
#include "rcecalib/config/FEI3/PixelRegister.hh"
#include "rcecalib/config/FEI3/RegularMaskStaging.hh"
#include "rcecalib/config/FEI3/CrosstalkMaskStaging.hh"
#include "rcecalib/config/Masks.hh"
#include <assert.h>
#include <string>
#include <iostream>

using namespace FEI3;
  
template<> Masks MaskStageFactory::createIOmasks<FEI3::Module>(const char* maskStagingMode){
  Masks m;
  m.oMask=m.iMask=m.xtalk_on=m.xtalk_off=m.crosstalking=0;
  std::string stagingMode(maskStagingMode);
  if(std::string(stagingMode)=="SEL"){
    m.oMask=(1<<fieldPos[PixelRegister::select])
      | (1<<fieldPos[PixelRegister::hitbus]);
    m.iMask=(1<<fieldPos[PixelRegister::select]);
  } else if(std::string(stagingMode)=="ENA"){
    m.oMask=(1<<fieldPos[PixelRegister::hitbus])
      | (1<<fieldPos[PixelRegister::enable]);
    m.iMask=(1<<fieldPos[PixelRegister::enable]);
  } else if(std::string(stagingMode)=="SEL_ENA"){
    m.oMask=(1<<fieldPos[PixelRegister::hitbus])
      | (1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::select]);
    m.iMask=(1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::select]);
  } else if(std::string(stagingMode)=="SEL_ENA_PRE"){
    m.oMask=(1<<fieldPos[PixelRegister::hitbus])
      | (1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::select])
      | (1<<fieldPos[PixelRegister::kill]);
    m.iMask=(1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::select])
      | (1<<fieldPos[PixelRegister::kill]);
  } else if(std::string(stagingMode)=="SEL_PRE"){
    m.oMask=(1<<fieldPos[PixelRegister::hitbus])
      | (1<<fieldPos[PixelRegister::select])
      | (1<<fieldPos[PixelRegister::kill]);
    m.iMask=(1<<fieldPos[PixelRegister::select])
      | (1<<fieldPos[PixelRegister::kill]);
  } else if(std::string(stagingMode)=="SENS_XTALK"){
    m.crosstalking = 1;
    m.oMask=(1<<fieldPos[PixelRegister::hitbus])
      | (1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::select]);
    m.iMask=(1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::select])
      | (1<<fieldPos[PixelRegister::kill]);
    m.xtalk_off=(1<<fieldPos[PixelRegister::select])
      | (1<<fieldPos[PixelRegister::kill]);
    m.xtalk_on=(1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::kill]);
  } else if(std::string(stagingMode)=="XTALK"){
    m.crosstalking = 1;
    m.oMask=(1<<fieldPos[PixelRegister::hitbus])
      | (1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::select])
      | (1<<fieldPos[PixelRegister::kill]);
    m.iMask=(1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::select])
      | (1<<fieldPos[PixelRegister::kill]);
    m.xtalk_off=(1<<fieldPos[PixelRegister::select]);
    m.xtalk_on=(1<<fieldPos[PixelRegister::enable])
      | (1<<fieldPos[PixelRegister::kill]);
  }
  m.oMask=~m.oMask; // invert
  return m;
}

template<>
MaskStaging<FEI3::Module>* MaskStageFactory::createMaskStaging(FEI3::Module* mod, const char* maskStagingType, const char* maskStagingMode){
  Masks iomask=createIOmasks<FEI3::Module>(maskStagingMode);
  std::string stagingType(maskStagingType);
  if(stagingType.substr(0,6)=="STEPS_" && iomask.crosstalking==false)
    return new FEI3::RegularMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="STEPS_2880" && iomask.crosstalking==true)
    return new FEI3::CrosstalkMaskStaging(mod, iomask, stagingType);
  else{
    std::cout<<"Wrong staging type "<<stagingType<<std::endl;
    assert(0);
  }
}
