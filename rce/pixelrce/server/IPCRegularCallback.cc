
#include "server/IPCRegularCallback.hh"

void IPCRegularCallback::notify(const ipc::CallbackParams& msg){
  if(msg.status==ipc::SCANNING){
    std::cout<<"RCE "<<msg.rce<<": Mask Stage "<<msg.maskStage<<std::endl;
  }else if(msg.status==ipc::FITTING){
    std::cout<<"RCE "<<msg.rce<<": Fitting"<<std::endl;
  }
}

void IPCRegularCallback::stopServer(){
  m_rce--;
  if(m_rce<=0){
    std::cout<<"Done"<<std::endl;
    stop();
  }
}

