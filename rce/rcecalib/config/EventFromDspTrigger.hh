#ifndef EVENTFROMDSPRIGGER_HH
#define EVENTFROMDSPTRIGGER_HH
#include <fstream>
#include "rcecalib/config/AbsTrigger.hh"
#include <vector>


  class EventFromDspTrigger: public AbsTrigger{
  public:
    EventFromDspTrigger();
    ~EventFromDspTrigger(){if(m_inpf.is_open()) m_inpf.close();};
    int sendTrigger();
    int enableTrigger(bool on){return 0;}
  private:
    unsigned m_event[2048];
    std::ifstream m_inpf;
    int m_bin;
    int m_stage;
    int m_nBins;
    int m_nStages;
    int m_frameSize;
    unsigned int m_frame_header[4];
    unsigned int m_frame_trailer;
    unsigned int m_frame[256];
  };


#endif
