#include "server/DataExporter.hh"
#include "server/ConfigGui.hh"
#include "stdlib.h"
#include "TFile.h"
#include "TKey.h"
#include "TROOT.h"
#include "TClass.h"
#include <iostream>
#include <sys/stat.h> 
#include <boost/regex.hpp>

const std::string DataExporter::basedir=getExportBaseDir();

const std::string DataExporter::getExportBaseDir(){
  char * var = getenv( "EXPORT_DIR" );
  if(var) return var;
  char *home=getenv("HOME");
  return std::string(home)+"/export";
}

void DataExporter::exportData(std::string &runname, std::string &topconfigname, std::string &rcdir, TFile* file, TFile* anfile){
  struct stat stFileInfo;
  int intStat;
  intStat = stat(basedir.c_str(),&stFileInfo);
  if(intStat != 0) { //File does not exist
    std::cout<<"Export directory "<<basedir<<" does not exist. Not exporting data."<<std::endl;
    return;
  }
  std::map<std::string, std::map<int, int> > ids;
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(!m_cfg[i]->isIncluded())continue;
    std::string modname="M";
    modname+=m_cfg[i]->getModuleId();
    std::string moddir=basedir+"/"+modname;
    intStat = stat(moddir.c_str(),&stFileInfo);
    if(intStat != 0) mkdir (moddir.c_str(),0777);
    std::string configdir=moddir+"/configs";
    intStat = stat(configdir.c_str(),&stFileInfo);
    if(intStat != 0) mkdir (configdir.c_str(),0777);
    std::string datadir=moddir+"/data";
    intStat = stat(datadir.c_str(),&stFileInfo);
    if(intStat != 0) mkdir (datadir.c_str(),0777);
    // Copy config
    char cmd[512];
    sprintf(cmd,"cp %s/globalconfig.txt %s/globalconfig_%s.txt", rcdir.c_str(), configdir.c_str(), runname.c_str());
    system (cmd);
    sprintf(cmd,"cp %s/%s %s/%s", rcdir.c_str(), topconfigname.c_str(), configdir.c_str(), topconfigname.c_str());
    system (cmd);
    sprintf(cmd,"cp %s/scanconfig_%s.txt %s/scanconfig_%s.txt", rcdir.c_str(), runname.c_str(), configdir.c_str(), runname.c_str());
    system (cmd);
    m_cfg[i]->copyConfig(configdir.c_str());
    int feid=m_cfg[i]->getId();
    ids[modname][feid]=1;
  }
  for (std::map <std::string, std::map<int, int> >::const_iterator it = ids.begin(); it != ids.end(); ++it){
    std::string modname=it->first;
    std::string datadir=basedir+"/"+modname+"/data/";
    std::string hfilename=modname+"_"+runname+".root";
    TFile hfile((datadir+"/"+hfilename).c_str(), "recreate", file->GetTitle());
    CopyFile(file->GetName(), it->second, "histos");
    if(anfile!=0)CopyFile(anfile->GetName(), it->second, "analysis");
  } 
}

void DataExporter::CopyDir(TDirectory *source, const std::map<int, int> &ids, const char* dirname) {
  //copy all objects and subdirs of directory source as a subdir of the current directory   
  TDirectory *savdir = gDirectory;
  TDirectory *adir = savdir->mkdir(dirname);
  adir->cd();
  //loop on all entries of this directory
  TKey *key;
  TIter nextkey(source->GetListOfKeys());
  while ((key = (TKey*)nextkey())) {
    const char *classname = key->GetClassName();
    TClass *cl = gROOT->GetClass(classname);
    if (!cl) continue;
    if (cl->InheritsFrom(TDirectory::Class())) {
      source->cd(key->GetName());
      TDirectory *subdir = gDirectory;
      adir->cd();
      CopyDir(subdir, ids, subdir->GetName());
      adir->cd();
    } else {
      source->cd();
      boost::regex re("Mod_(\\d+)");
      std::string name(key->GetName());
      boost::cmatch matches;
      if(boost::regex_search(name.c_str(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	int id=strtol(match.c_str(),0,10);
	if( ids.find(id)!=ids.end()){
	  TObject *obj = key->ReadObj();
	  adir->cd();
	  obj->Write();
	  delete obj;
	}
      }
    }
  }
  adir->SaveSelf(kTRUE);
  savdir->cd();
}
void DataExporter::CopyFile(const char *fname, const std::map<int, int> &ids, const char* topdir) {
  //Copy all objects and subdirs of file fname as a subdir of the current directory
  TDirectory *target = gDirectory;
  TFile *f = TFile::Open(fname);
  if (!f || f->IsZombie()) {
    printf("Cannot copy file: %s\n",fname);
    target->cd();
    return;
  }
  target->cd();
  CopyDir(f, ids, topdir);
  delete f;
  target->cd();
}
