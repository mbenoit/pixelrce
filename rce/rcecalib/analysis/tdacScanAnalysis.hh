#ifndef TDACSCANANALYSIS_HH
#define TDACSCANANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"
#include "rcecalib/server/PixScan.hh"

class ConfigGui;
class TFile;
class TH2D;
namespace RCE{
  class PixScan;
}

class tdacScanAnalysis: public CalibAnalysis{
public:
  tdacScanAnalysis(): CalibAnalysis(){}
  ~tdacScanAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
