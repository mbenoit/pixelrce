#ifndef MODULEFACTORY_HH
#define MODULEFACTORY_HH

#include "rcecalib/config/AbsModule.hh"
#include "rcecalib/config/AbsModuleGroup.hh"
#include "rcecalib/config/AbsTrigger.hh"

class ConfigIF;

class ModuleFactory{
public:
  ModuleFactory(){};
  virtual ~ModuleFactory(){};
  virtual AbsModule* createModule(const char* name, const char* type, unsigned id, unsigned inlink, unsigned outlink, const char* formatter)=0;
  virtual AbsTrigger* createTriggerIF(const char* type, ConfigIF* cif)=0;
  std::vector<AbsModuleGroup*> &getModuleGroups(){return m_modulegroups;}
protected:
  std::vector<AbsModuleGroup*> m_modulegroups;
};
  

#endif
