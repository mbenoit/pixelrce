#ifndef FITSCURVELIKELIHOODINT_HH
#define FITSCURVELIKELIHOODINT_HH
#include "dataproc/fit/AbsFit.hh"
#include "util/RceHisto2d.cc"
#include <vector>

template<typename TP, typename TE>
class  FitScurveLikelihoodInt:public AbsFit {

typedef unsigned int UINT32;
typedef int INT32;
typedef unsigned long long UINT64;
typedef long long INT64;
typedef unsigned char UINT8;

typedef struct FitData_
{
  UINT32 n;
  UINT32 *x;
  UINT8 *y;
} FitData;


typedef struct GaussianCurve_
{
  UINT32 mu, sigma, a0;
} GaussianCurve;

typedef struct {
  INT32 deltaMu, deltaSigma; /* user provides initial deltaMu and deltaSigma */
  INT32 muEpsilon, sigmaEpsilon; /* user provides convergence criterion */
  INT32 nIters;
  INT32 ndf; /* number of degrees of freedom */
  INT32 chi2; /* final result for chi2 */
  INT32 converge;
  INT32 muConverge,sigmaConverge;
  INT32 maxIters;
  void *curve;
} Fit;


public:
  FitScurveLikelihoodInt(ConfigIF *cif,std::vector<std::vector<RceHisto2d<TP, TE>*> > &histo,std::vector<int> &vcal, int nTrigger, const char* name="");
  ~FitScurveLikelihoodInt();
  int doFit(int);
protected:
  inline INT32 *log_ext(INT32 x, GaussianCurve *psp);
  UINT32 errf_ext(INT32 x, GaussianCurve *psp);
  UINT32 logLikelihood(FitData *pfd, GaussianCurve *s);
  int initFit(Fit *pFit);
  int extractGoodData(FitData *pfdi,FitData *pfdo, UINT32 nTriggers);
  int initialGuess(FitData *pfd, GaussianCurve *pSParams);
  int fitPixel(FitData *pfd,void *vpfit);
  UINT32 chiSquared(FitData *pfd,GaussianCurve *s);
  Fit m_fit;
  UINT32 *m_x;
  UINT8 *m_y;
  std::vector<std::vector<RceHisto2d<TP, TE>*> > &m_histo;
  std::vector<std::vector<RceHisto2d<float, float>*> > m_fithisto;
  const char* m_name;
  
};



#endif
