source /sw/atlas/setup.sh
export TDAQ_PARTITION=rce_$USER
export TDAQ_IPC_INIT_REF=file:~/daq/ipc_root.ref
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/daq/rce/buildarm-tdaq5/lib
export PATH=~/daq/rce/buildarm-tdaq5/bin:${PATH}
