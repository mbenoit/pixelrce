#ifndef IFSCANSETUP_HH
#define IFSCANSETUP_HH

#include "rcecalib/scanctrl/NestedLoop.hh"
#include "rcecalib/scanctrl/LoopSetup.hh"
#include "rcecalib/scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/ConfigIF.hh"

class IfScanSetup: public LoopSetup{
public:
  IfScanSetup():LoopSetup(){};
  virtual ~IfScanSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
