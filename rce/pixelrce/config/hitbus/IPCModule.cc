#ifndef HITBUS__IPCMODULE_CC
#define HITBUS__IPCMODULE_CC
#include "util/RceName.hh"
#include "config/hitbus/IPCModule.hh"
#include "ipc/partition.h"
#include "ers/ers.h"
#include <boost/lexical_cast.hpp>

namespace Hitbus{

template <class TP>
IPCModule<TP>::IPCModule(IPCPartition & p, const char * name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt):
  IPCNamedObject<POA_ipc::IPCHitbusAdapter, TP>( p, std::string(boost::lexical_cast<std::string>(id)+"_RCE"+boost::lexical_cast<std::string>(RceName::getRceNumber())).c_str()) , 
  Module(name, id, inlink, outlink, fmt){
  //  std::cout<<"IPCModule"<<std::endl;
  try {
    IPCNamedObject<POA_ipc::IPCHitbusAdapter,TP>::publish();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
}

template <class TP>
IPCModule<TP>::~IPCModule(){
  try {
    IPCNamedObject<POA_ipc::IPCHitbusAdapter,TP>::withdraw();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::warning( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
}

template <class TP>
CORBA::Long IPCModule<TP>::IPCdownloadConfig(const ipc::HitbusModuleConfig &config){
  setRegister("bpm", config.bpm);
  setRegister("delay_tam1", config.delay_tam1);
  setRegister("delay_tam2", config.delay_tam2);
  setRegister("delay_tam3", config.delay_tam3);
  setRegister("delay_tbm1", config.delay_tbm1);
  setRegister("delay_tbm2", config.delay_tbm2);
  setRegister("delay_tbm3", config.delay_tbm3);
  setRegister("bypass_delay", config.bypass_delay);
  setRegister("clock", config.clock);
  setRegister("function_A", config.function_A);
  setRegister("function_B", config.function_B);
  std::cout<<"Configure done"<<std::endl;
  return 0;
}

template <class TP>
void IPCModule<TP>::shutdown(){
  std::cout<<"Shutdown"<<std::endl;
}

template <class TP>
void IPCModule<TP>::destroy(){
  this->_destroy();
}

  

};

#endif
