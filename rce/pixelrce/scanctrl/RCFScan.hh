#ifndef RCFSCAN_HH
#define RCFSCAN_HH

#include "scanctrl/Scan.hh"
#include "rcf/RCFScanAdapter.hh"


class RCFScan: public RCFScanAdapter, public Scan {
public:
  RCFScan();
  ~RCFScan();
  void RCFpause();
  void RCFresume();
  void RCFwaitForData();
  void RCFstopWaiting();
  void RCFabort();
  void RCFstartScan();
  int32_t RCFgetStatus();
  static void *startScanStatic(void *arg);

};
  

#endif
