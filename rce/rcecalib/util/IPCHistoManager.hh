#ifndef IPC_HISTO_MANAGER_HH
#define IPC_HISTO_MANAGER_HH

#include <vector>
#include <string>
#include <map>


class IPCPartition;

class AbsRceHisto;
class OHBins;
template<class T> class OHRawProvider;
#include <iostream>

class IPCHistoManager{
public:
  IPCHistoManager(IPCPartition& p, const char* servername, const char* providername);
  ~IPCHistoManager();
  static IPCHistoManager* instance(){
    return m_manager;
  }
  void addToInventory(AbsRceHisto*);
  void removeFromInventory(AbsRceHisto*);
  void publish(const char* reg);
  std::vector<std::string> getHistoNames(const char* reg);
  std::vector<std::string> getPublishedHistoNames();
private:
  std::map<std::string,AbsRceHisto*> m_inventory;
  std::map<std::string,AbsRceHisto*> m_published;
  OHRawProvider<OHBins>* m_provider;
  static IPCHistoManager* m_manager;
};

#endif
