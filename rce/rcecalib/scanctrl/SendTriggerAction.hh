#ifndef SENDTRIGGERACTION_HH 
#define SENDTRIGGERACTION_HH 

#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/config/ConfigIF.hh"

class SendTriggerAction: public LoopAction{
public:
  SendTriggerAction(std::string name, ConfigIF* cif):
    LoopAction(name),m_configIF(cif){}
  int execute(int i){
    //std::cout<<"SendTriggerAction"<<std::endl;
    return m_configIF->sendTrigger();
  }
private:
  ConfigIF* m_configIF;
};

#endif
