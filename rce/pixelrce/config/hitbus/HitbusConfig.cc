#include "config/hitbus/HitbusConfig.hh"
#include <boost/lexical_cast.hpp>
#include <iostream>
#ifdef __IPC__
#include "ipc/partition.h"
#include "IPCHitbusAdapter.hh"
int HitbusConfig::downloadConfig(IPCPartition&p, int rce, int id){
  ipc::IPCHitbusAdapter_var modhandle;
  std::string names=boost::lexical_cast<std::string>(id)+"_RCE"+boost::lexical_cast<std::string>(rce);
  const char* name=names.c_str();
  try {
    bool v=p.isObjectValid<ipc::IPCHitbusAdapter>(name);
    if(!v) {
      std::cout<<"Not valid"<<std::endl;
      assert(0);
    }
    modhandle = p.lookup<ipc::IPCHitbusAdapter>( name );
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  try {
    modhandle -> IPCdownloadConfig( *m_config );
  }
  catch(CORBA::Exception & ex) {
    std::cerr<<"Corba exception "<<ex._name()<<std::endl;
  }
}
#else
#include <RCF/RCF.hpp>
#include "RCFHitbusAdapter.hh"
#include "util/RceName.hh"
int HitbusConfig::downloadConfig(int rce, int id){
  char binding[32];
  char rcename[32];
  sprintf(binding, "I_RCFHitbusAdapter_%d", id);
  sprintf(rcename, "rce%d", rce);
  try {
    RcfClient<I_RCFHitbusAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT), binding);
    client.RCFdownloadConfig( *m_config );
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
}
#endif
HitbusConfig::HitbusConfig(std::string filename): PixelConfig("Hitbus", filename, false, 0, 0, 0), 
				      m_config(new ipc::HitbusModuleConfig){
    HitbusConfigFile turbo;
    turbo.readModuleConfig(m_config,filename);
    m_name=(const char*)m_config->idStr;
    m_id=strtol((const char*)m_config->idStr,0,10);
    m_valid=true;
  }
void HitbusConfig::writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key){
    HitbusConfigFile cf;
    cf.writeModuleConfig(m_config, base, confdir, configname, key);
  }
