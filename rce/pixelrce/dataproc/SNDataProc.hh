#ifndef SNDATAPROC_HH
#define SNDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include "dataproc/fit/AbsFit.hh"
#include <vector>
#include "util/RceHisto1d.cc"

class SNDataProc: public AbsDataProc{
public:
  SNDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~SNDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);

protected:
  std::vector<RceHisto1d<int, int>*> m_histo_occ;
};

#endif
