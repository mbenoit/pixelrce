#include "rcecalib/server/RceProducer.hh"
#include "rcecalib/eudaq/ProducerIF.hh"
#include "rcecalib/eudaq/RawDataEvent.hh"
#include "rcecalib/eudaq/Utils.hh"
#include "rcecalib/eudaq/Exception.hh"
#include "rcecalib/server/PixScan.hh"
#include "rcecalib/server/TurboDaqFile.hh"
#include "rcecalib/server/FEI4AConfigFile.hh"
#include "rcecalib/server/FEI4BConfigFile.hh"
#include "rcecalib/util/RceName.hh"
#include <iostream>
#include <ostream>
#include <vector>
#include <stdio.h>
#include <sys/stat.h> 


namespace{
  static const std::string EVENT_TYPE = "APIX-CT";

  void startScan(void* arg){
    IPCController* controller=(IPCController*)arg;
    controller->startScan();
  }
}
using namespace RCE;

// This gets called whenever the DAQ is configured
void RceProducer::OnConfigure(const eudaq::Configuration & config) {
  std::cout << "Configuring: " << config.Name() << std::endl;
    std::cout<<"Deleting scan options "<<m_options<<std::endl;
  char msg[64];
  unsigned rce=RceName::getRceNumber();
  try{
    //Default Trigger IF
    m_controller.removeAllRces();
    m_controller->addRce(rce);
    m_controller->setupTrigger();
    m_controller->removeAllModules();
    int numboards = config.Get("NumModules", -1);
    if(numboards==-1)throw eudaq::Exception("NumModules not defined");
    for(int i=0;i<numboards;i++){
      int outlink = config.Get("Module" + eudaq::to_string(i) + ".OutLink", "OutLink", -1);
      if(outlink==-1){
	sprintf(msg,"Module %d: Outlink is not defined.",i);
	throw eudaq::Exception(msg);
      }
      m_PlaneMask|=1<<outlink;
      int inlink = config.Get("Module" + eudaq::to_string(i) + ".InLink", "InLink", -1);
      if(inlink==-1){
	sprintf(msg,"Module %d: Inlink is not defined.",i);
	throw eudaq::Exception(msg);
      }
      int id = config.Get("Module" + eudaq::to_string(i) + ".Id", "Id", -1);
      if(id==-1){
	sprintf(msg,"Module %d: id is not defined.",i);
	throw eudaq::Exception(msg);
      }
      std::string modtype = config.Get("Module" + eudaq::to_string(i) + ".Type", "Type", "");
      if(modtype==""){
	sprintf(msg,"Module %d: Module type is not defined.",i);
	throw eudaq::Exception(msg);
      }
      std::string filename = config.Get("Module" + eudaq::to_string(i) + ".File", "File", "");
      if(filename==""){
	sprintf(msg,"Module %d: Configuration filename is not defined.",i);
	throw eudaq::Exception(msg);
      }

      filename="/nfs/moduleconfigs/"+filename;
      struct stat stFileInfo;
      int intStat;
      // Attempt to get the file attributes
      intStat = stat(filename.c_str(),&stFileInfo);
      if(intStat!=0) { //file does not exist
	sprintf(msg, "Configuration: File %s does not exist.",filename.c_str());
	throw eudaq::Exception(msg);
      }
      // create module via IPC
      char modname[32];
      sprintf(modname,"RCE%d_module_%d",rce, outlink);
      if(modtype=="FEI4A"){
	printf("FEI4A: addModule (%s, %d, %d, %d, %d)\n",modname, 0, inlink, outlink, rce);
	m_controller->addModule(modname, "FEI4A",id, inlink, outlink, rce, "FEI4A");
	FEI4AConfigFile fei4af;
	ipc::PixelFEI4AConfig *cfg=new ipc::PixelFEI4AConfig;
	fei4af.readModuleConfig(cfg, filename);
	m_controller->downloadModuleConfig(modname,*cfg);
	delete cfg;
	    //	    assert(m_controller.writeHWregister(15,0x80)==0); //setup mux
      } else if(modtype=="FEI4B"){
	printf("FEI4B: addModule (%s, %d, %d, %d, %d)\n",modname, id, inlink, outlink, rce);
	m_controller->addModule(modname, "FEI4B",id, inlink, outlink, rce, "FEI4B");
	FEI4BConfigFile fei4bf;
	ipc::PixelFEI4BConfig *cfg=new ipc::PixelFEI4BConfig;
	fei4bf.readModuleConfig(cfg, filename);
	m_controller->downloadModuleConfig(modname,*cfg);
	delete cfg;
      }else{
	printf("FEI3: addModule (%s, %d, %d, %d, %d)\n",modname, inlink, inlink, rce, outlink);
	m_controller->addModule(modname, "FEI3",id, inlink, outlink, rce, "JJ");
	TurboDaqFile turbo;
	ipc::PixelModuleConfig *cfg=new ipc::PixelModuleConfig;
	turbo.readModuleConfig(cfg, filename);
	m_controller->downloadModuleConfig(modname,*cfg);
	delete cfg;
      }
    } 
    // set up hardware trigger
    unsigned serstat=0;
    serstat=m_controller->writeHWregister(RceName::getRceNumber(), 3,1); //runmode 0=normal 1=tdccalib 2=eudaq
    assert(serstat==0);
    //m_controller.writeHWregister(3,0); //Normal mode
    
    // Scan configuration
    std::cout<<"Deleting scan options "<<m_options<<std::endl;
    delete m_options;
    PixScan scn(PixScan::COSMIC_RCE, PixLib::EnumFEflavour::PM_FE_I2);
    m_options=new ipc::ScanOptions;
    scn.setName("TCP"); // send data over the network rather than to file
    // Override l1a latency
    int latency = config.Get("Latency", -1);
    if(latency==-1){
      throw eudaq::Exception("Latency is not defined.");
    }
    scn.setLVL1Latency(latency);
    m_conseq = config.Get("ConseqTriggers", -1);
    if(m_conseq==(unsigned)-1){
      throw eudaq::Exception("Number of consecutive triggers is not defined.");
    }
    scn.setConsecutiveLvl1TrigA(0,m_conseq);
    int trgdelay = config.Get("Trgdelay", -1);
    if(trgdelay==-1){
      throw eudaq::Exception("Trigger delay is not defined.");
    }
    scn.setStrobeLVL1Delay(trgdelay);
    int deadtime = config.Get("Deadtime", 0);
    scn.setDeadtime(deadtime);
    int cyclic = config.Get("Cyclic_Period", 40000000);
    scn.setEventInterval(cyclic);
    scn.setTriggerMask(4); // eudet=4

    scn.convertScanConfig(*m_options);
    //m_controller->downloadScanConfig(*m_options);
    // At the end, set the status that will be displayed in the Run Control.
    SetStatus(eudaq::Status::LVL_OK, "Configured (" + config.Name() + ")");
  } catch (const std::exception & e) {
    printf("Caught exception: %s\n", e.what());
    SetStatus(eudaq::Status::LVL_ERROR, "Configuration Error");
  } catch (...) {
    printf("Unknown exception\n");
    SetStatus(eudaq::Status::LVL_ERROR, "Configuration Error");
  }
}

  // This gets called whenever a new run is started
  // It receives the new run number as a parameter
void RceProducer::OnStartRun(unsigned param) {
    m_run = param;
    std::cout << "Start Run: " << m_run << std::endl;
    char name[20];
    sprintf(name,"TCP_%d",m_run);
    m_options->name=CORBA::string_dup(name);

    m_controller->downloadScanConfig(*m_options);
    // It must send a BORE to the Data Collector
    eudaq::RawDataEvent bore(eudaq::RawDataEvent::BORE(EVENT_TYPE, m_run));
    // You can set tags on the BORE that will be saved in the data file
    // and can be used later to help decoding
    bore.SetTag("PlaneMask", eudaq::to_string(m_PlaneMask));
    bore.SetTag("nFrames", eudaq::to_string(m_conseq));
    // Send the event to the Data Collector
    SendEvent(bore);
    // Enable writing events via tcp/ip
    ProducerIF::setProducer(this);
    //start run
    omni_thread::create(startScan,(void*)m_controller);
    //m_controller->startScan();

    std::cout<<"Started scan"<<std::endl;
    // At the end, set the status that will be displayed in the Run Control.
    SetStatus(eudaq::Status::LVL_OK, "Running");
  }

  // This gets called whenever a run is stopped
void RceProducer::OnStopRun() {
    std::cout << "Stopping Run" << std::endl;

    m_controller->stopWaitingForData();
    //wait for late events to trickle in
    sleep(1);
    //wait for scan to be complete
    bool idle=false;
    for(int i=0;i<50;i++){
      if(m_controller->getScanStatus()==0){
	std::cout<<"called getstatus"<<std::endl;
	idle=true;
	break;
      }
      usleep(100000);
    }
    if(idle==false)std::cout<<"Scan did not go into idle state"<<std::endl;
    //disable sending events via TCP/IP
    ProducerIF::setProducer(0);
    // Send an EORE after all the real events have been sent
    // You can also set tags on it (as with the BORE) if necessary
    unsigned evnum=m_controller->getNEventsProcessed();
    SendEvent(eudaq::RawDataEvent::EORE(EVENT_TYPE, m_run, evnum));
    SetStatus(eudaq::Status::LVL_OK, "Stopped");
  }

  // This gets called when the Run Control is terminating,
  // we should also exit.
void RceProducer::OnTerminate() {
    std::cout << "Terminating..." << std::endl;
  }

