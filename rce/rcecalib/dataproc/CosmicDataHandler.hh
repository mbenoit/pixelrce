#ifndef COSMICDATAHANDLER_HH
#define COSMICDATAHANDLER_HH

#include <vector>
#include <map>
#include <assert.h>

#include "rcecalib/dataproc/AbsDataHandler.hh"
#include <boost/property_tree/ptree_fwd.hpp>

class AbsFormatter;
class DataCond;

class CosmicDataHandler: public AbsDataHandler{
public:
  CosmicDataHandler(AbsDataProc* dataproc, DataCond &datacond, ConfigIF* cif, boost::property_tree::ptree* scanOptions);
  virtual ~CosmicDataHandler();
  void handle(unsigned desc, unsigned* data, int size);
  enum MD{MAXMODULES=16};
protected:
  int m_nModules;
  int *m_linkToIndex;
  unsigned *m_parsedData;
  std::vector<AbsFormatter*> m_formatter;
  AbsFormatter * m_dummy;
};
#endif
