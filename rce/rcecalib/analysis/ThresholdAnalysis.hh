#ifndef THRESHOLDANALYSIS_HH
#define THRESHOLDANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"
#include <map>

class ConfigGui;
class TFile;
class TH2;
class TH1D;

namespace RCE{
  class PixScan;
}

class ThresholdAnalysis: public CalibAnalysis{
public:
  ThresholdAnalysis(): CalibAnalysis(){}
  ~ThresholdAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
