#ifndef LOOPSETUP_HH
#define LOOPSETUP_HH

#include "rcecalib/scanctrl/NestedLoop.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/scanctrl/ActionFactory.hh"

class LoopSetup{
public:
  LoopSetup(){};
  virtual ~LoopSetup(){};
  virtual int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af)=0;
};
  

#endif
