#ifndef IFFANALYSIS2_HH
#define IFFANALYSIS2_HH

#include "rcecalib/analysis/CalibAnalysis.hh"
#include "rcecalib/server/PixScan.hh"

class TFile;
class TH2;
namespace RCE{
  class PixScan;
}

class IffAnalysis: public CalibAnalysis{
public:
  struct odata{
    TH2* occ;
    TH2* tot;
    TH2* tot2;
  };
  IffAnalysis(): CalibAnalysis(){}
  ~IffAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
