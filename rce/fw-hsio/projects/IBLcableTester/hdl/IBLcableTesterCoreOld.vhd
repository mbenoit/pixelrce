-------------------------------------------------------------------------------
-- Title         : BNL ASIC Test FGPA Core 
-- Project       : LCLS Detector, BNL ASIC
-------------------------------------------------------------------------------
-- File          : IBLcableTesterCore.vhd
-- Author        : Ryan Herbst, rherbst@slac.stanford.edu
-- Created       : 07/21/2008
-------------------------------------------------------------------------------
-- Description:
-- Core logic for BNL ASIC test FPGA.
-------------------------------------------------------------------------------
-- Copyright (c) 2008 by Ryan Herbst. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 07/21/2008: created.
-------------------------------------------------------------------------------

LIBRARY ieee;
use work.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity IBLcableTesterCore is 
   port ( 

      -- Master system clock, 250Mhz, 125Mhz
      sysClk250    : in  std_logic;
      sysClk125    : in  std_logic;
      sysRst125    : in  std_logic;
      sysRst250    : in  std_logic;

      -- PGP Clocks
      refClock     : in  std_logic;
      pgpClk       : in  std_logic;
      pgpReset     : in  std_logic;

      -- MGT Serial Pins
      mgtRxN       : in  std_logic;
      mgtRxP       : in  std_logic;
      mgtTxN       : out std_logic;
      mgtTxP       : out std_logic;

      -- ATLAS Pixel module pins
      serialin     : in std_logic_vector(23 downto 0);
      serialout    : out std_logic_vector(23 downto 0);
      retclock     : in std_logic_vector(23 downto 16);
      clock160     : in std_logic;
      clock80      : in std_logic;
      clock40      : in std_logic;
      doricreset   : out std_logic;
--      serialclkout : out std_logic;

      -- Reset out to PGP Clock generation
      resetOut     : out std_logic;

      -- LED Display
      dispClk      : out std_logic;
      dispDat      : out std_logic;
      dispLoadL    : out std_logic_vector(1 downto 0);
      dispRstL     : out std_logic;


      -- Debug
      debug         : out std_logic_vector(15 downto 0)
   );
end IBLcableTesterCore;


-- Define architecture
architecture IBLcableTesterCore of IBLcableTesterCore is

   -- LED Display Controller
   component DisplayControl
      port (
         sysClk       : in  std_logic;
         sysRst       : in  std_logic;
         dispStrobe   : in  std_logic;
         dispUpdate   : in  std_logic;
         dispRotate   : in  std_logic_vector(1 downto 0);
         dispDigitA   : in  std_logic_vector(7 downto 0);
         dispDigitB   : in  std_logic_vector(7 downto 0);
         dispDigitC   : in  std_logic_vector(7 downto 0);
         dispDigitD   : in  std_logic_vector(7 downto 0);
         dispClk      : out std_logic;
         dispDat      : out std_logic;
         dispLoadL    : out std_logic;
         dispRstL     : out std_logic
      );
   end component;
   component pattern_mem is
      generic(DELAY: integer);
      port(
        clk: 	    in std_logic;
	serclk:	    in std_logic;
	deserclk:   in std_logic;
	rst:	    in std_logic;
        pgpEnable:  in std_logic;
        pgpRW:      in std_logic;
        pgpAck:     out std_logic;
        pgpErr:     out std_logic;
        dataFromPgp:in std_logic_vector(31 downto 0);
        addrPgp:    in std_logic_vector(3 downto 0);
        dataToPgp:  out std_logic_vector(31 downto 0);
	dataFromFE: in std_logic;
	dataToFE:   out std_logic
     );
   end component;
   component pattern_cmd is
      port(
        clk: 	    in std_logic;
	serclk:	    in std_logic;
	deserclk:   in std_logic;
	retclk:     in std_logic;
	rst:	    in std_logic;
	doricreset: out std_logic;
        pgpEnable:  in std_logic;
        pgpRW:      in std_logic;
        pgpAck:     out std_logic;
        pgpErr:     out std_logic;
        dataFromPgp:in std_logic_vector(31 downto 0);
        addrPgp:    in std_logic_vector(3 downto 0);
        dataToPgp:  out std_logic_vector(31 downto 0);
	dataFromFE: in std_logic;
	dataToFE:   out std_logic
     );
    end component;
   component clock200
   port( CLKIN_IN   : in    std_logic; 
          RST_IN     : in    std_logic; 
          CLKFX_OUT  : out   std_logic; 
          CLK0_OUT   : out   std_logic; 
          LOCKED_OUT : out   std_logic);
   end component;
   component IDELAYCTRL
   port ( RDY    : out std_logic;
          REFCLK : in std_logic;
          RST    : in std_logic
        );
   end component;
   component phaseshift
   port ( CLKIN_IN    : in    std_logic; 
          DADDR_IN    : in    std_logic_vector (6 downto 0); 
          DCLK_IN     : in    std_logic; 
          DEN_IN      : in    std_logic; 
          DI_IN       : in    std_logic_vector (15 downto 0); 
          DWE_IN      : in    std_logic; 
          RST_IN      : in    std_logic; 
          CLK0_OUT    : out   std_logic; 
          CLKFX_OUT   : out   std_logic; 
          CLK2X_OUT   : out   std_logic; 
          DRDY_OUT    : out   std_logic; 
          LOCKED_OUT  : out   std_logic);
          --PSDONE_OUT  : out   std_logic); 
   end component;  

   component OBUF    port ( O : out std_logic; I  : in  std_logic ); end component;
   -- PGP Front End Wrapper

   component PgpFrontEnd
      generic (
         MgtMode    : string  := "A";
         RefClkSel  : string  := "REFCLK1"
      );
      port (
         pgpRefClk1       : in  std_logic;
         pgpRefClk2       : in  std_logic;
         mgtRxRecClk      : out std_logic;
         pgpClk           : in  std_logic;
         pgpReset         : in  std_logic;
         pgpDispA         : out std_logic_vector(7 downto 0);
         pgpDispB         : out std_logic_vector(7 downto 0);
         resetOut         : out std_logic;
         locClk           : in  std_logic;
         locReset         : in  std_logic;
         cmdEn            : out std_logic;
         cmdOpCode        : out std_logic_vector(7  downto 0);
         cmdCtxOut        : out std_logic_vector(23 downto 0);
         regReq           : out std_logic;
         regOp            : out std_logic;
         regAck           : in  std_logic;
         regFail          : in  std_logic;
         regAddr          : out std_logic_vector(23 downto 0);
         regDataOut       : out std_logic_vector(31 downto 0);
         regDataIn        : in  std_logic_vector(31 downto 0);
         frameTxEnable    : in  std_logic;
         frameTxSOF       : in  std_logic;
         frameTxEOF       : in  std_logic;
         frameTxEOFE      : in  std_logic;
         frameTxData      : in  std_logic_vector(15 downto 0);
         valid            : out std_logic;
         eof              :out std_logic;
         sof              :out std_logic;
         mgtRxN           : in  std_logic;
         mgtRxP           : in  std_logic;
         mgtTxN           : out std_logic;
         mgtTxP           : out std_logic;
         mgtCombusIn      : in  std_logic_vector(15 downto 0);
         mgtCombusOut     : out std_logic_vector(15 downto 0)
      );
   end component;


   -- Local Signals
   signal cmdEn           : std_logic;
   signal cmdOpCode       : std_logic_vector(7  downto 0);
   signal cmdCtxOut       : std_logic_vector(23 downto 0);
   signal readDataValid   : std_logic;
   signal readDataSOF     : std_logic;
   signal readDataEOF     : std_logic;
   signal readDataEOFE    : std_logic;
   signal readData        : std_logic_vector(15 downto 0);
   signal dispDatA        : std_logic;
   signal dispDatB        : std_logic;
   signal pgpDispA        : std_logic_vector(7 downto 0);
   signal pgpDispB        : std_logic_vector(7 downto 0);
   signal regReq          : std_logic;
   signal regOp           : std_logic;
   signal regAck          : std_logic;
   signal regFail         : std_logic;
   signal regAddr         : std_logic_vector(23 downto 0);
   signal regDataOut      : std_logic_vector(31 downto 0);
   signal regDataIn       : std_logic_vector(31 downto 0);
   signal dispDigitA      : std_logic_vector(7 downto 0);
   signal dispDigitB      : std_logic_vector(7 downto 0);
   signal dispDigitC      : std_logic_vector(7 downto 0);
   signal dispDigitD      : std_logic_vector(7 downto 0);
   signal dispDigitE      : std_logic_vector(7 downto 0);
   signal dispDigitF      : std_logic_vector(7 downto 0);
   signal dispDigitG      : std_logic_vector(7 downto 0);
   signal dispDigitH      : std_logic_vector(7 downto 0);
   signal dispStrobe      : std_logic;
   signal dispUpdateA     : std_logic;
   signal dispUpdateB     : std_logic;
   signal sysClkCnt       : std_logic_vector(15 downto 0);
   signal ack             : std_logic_vector(23 downto 0);
   signal err             : std_logic_vector(23 downto 0);
   signal eof             : std_logic;
   signal sof             : std_logic;
   signal vvalid          : std_logic;
   signal regAck1         : std_logic;
   signal regAck2         : std_logic;
   signal regAck3         : std_logic;
   signal regFail1         : std_logic;
   signal regFail2         : std_logic;
   signal regFail3         : std_logic;
   type readback is array(23 downto 0) of std_logic_vector(31 downto 0);
   signal thedata         : readback;
   signal req             : std_logic_vector(23 downto 0);
   signal counter1        : std_logic_vector(7 downto 0);
   signal counter2        : std_logic_vector(7 downto 0);
   signal idcounter       : std_logic_vector(2 downto 0);
   signal daddr           : std_logic_vector (6 downto 0);
   signal denfx           : std_logic;
   signal denphase        : std_logic;
   signal phaseclk        : std_logic;
   signal fxclock         : std_logic;
   signal serclk          : std_logic;
   signal drdy            : std_logic;
   signal drdy2           : std_logic;
   signal lockedfx        : std_logic;
   signal lockedid        : std_logic;
   signal oldlockedid     : std_logic;
   signal lockedphase     : std_logic;
   signal lockednophase   : std_logic;
   signal reqclkfx        : std_logic;
   signal reqclkphase     : std_logic;
   signal oldreqclkfx     : std_logic;
   signal oldreqclkphase  : std_logic;
   signal clkenafx        : std_logic;
   signal clkenaphase     : std_logic;
   signal psdone          : std_logic;
   signal holdrst         : std_logic;
   signal phaserst        : std_logic;
   signal clockrst        : std_logic;
   signal clockrst2       : std_logic;
   signal idctrlrst       : std_logic;
   signal clkdata         : std_logic_vector(15 downto 0);
   signal holdctr         : std_logic_vector(24 downto 0);
   signal clockidctrl     : std_logic;
   signal dreset          : std_logic_vector(23 downto 16);

   -- Register delay for simulation
   constant tpd:time := 0.5 ns;
   type DELAR is array (0 to 15) of integer;
   constant setting: DELAR := (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
   attribute syn_noprune : boolean;
   attribute syn_noprune of U_idelayctrl: label is true;

begin

   -- Display Controller A
   U_DispCntrlA: DisplayControl port map (
      sysClk     => sysClk125,   sysRst     => sysRst125,
      dispStrobe => dispStrobe,  dispUpdate => dispUpdateA,
      dispRotate => "01",        dispDigitA => dispDigitA,
      dispDigitB => dispDigitB,  dispDigitC => dispDigitC,
      dispDigitD => dispDigitD,  dispClk    => dispClk,
      dispDat    => dispDatA,    dispLoadL  => dispLoadL(0),
      dispRstL   => dispRstL
   );

   -- Display Controller B
   U_DispCntrlB: DisplayControl port map (
      sysClk     => sysClk125,   sysRst     => sysRst125,
      dispStrobe => dispStrobe,  dispUpdate => dispUpdateB,
      dispRotate => "01",        dispDigitA => dispDigitE,
      dispDigitB => dispDigitF,  dispDigitC => dispDigitG,
      dispDigitD => dispDigitH,  dispClk    => open,
      dispDat    => dispDatB,    dispLoadL  => dispLoadL(1),
      dispRstL   => open
   );

   -- Output LED Data
   dispDat    <= dispDatA or dispDatB;
   dispDigitA <= pgpDispA;
   dispDigitB <= pgpDispB;
   dispDigitD <= regDataIn(7 downto 0);
   dispDigitC <= "0000"&lockedid&lockednophase &lockedphase & lockedfx;
   dispDigitE <= "0000" & counter1(7 downto 4);
   dispDigitF <= "0000" & counter1(3 downto 0);
   dispDigitG <=  "0000" & counter2(7 downto 4);
   dispDigitH <="0000" & counter2(3 downto 0);


   -- Generate display strobe (200ns) and update control
   process ( sysClk125, sysRst125 ) begin
      if sysRst125 = '1' then
         sysClkCnt   <= (others=>'0') after tpd;
         dispStrobe  <= '0'           after tpd;
         dispUpdateA <= '0'           after tpd;
         dispUpdateB <= '0'           after tpd;
      elsif rising_edge(sysClk125) then

         -- Display strobe, 320ns
         dispStrobe <= sysClkCnt(4) after tpd;

         -- Update Display 0
         if sysClkCnt(15 downto 0) = x"8000" then
            dispUpdateA <= '1' after tpd;
         else
            dispUpdateA <= '0' after tpd;
         end if;

         -- Update Display B
         if sysClkCnt(15 downto 0) = x"0000" then
            dispUpdateB <= '1' after tpd;
         else
            dispUpdateB <= '0' after tpd;
         end if;

         -- Update counter
         sysClkCnt <= sysClkCnt + 1 after tpd;

         if req(0)='1' then
           counter1<=unsigned(counter1)+1;
         end if;

         if req(1)='1' then
           counter2<=unsigned(counter2)+1;
         end if;

      end if;
   end process;


   -- PGP Front End
   U_PgpFrontEnd: PgpFrontEnd port map (
      pgpRefClk1    => refClock,       pgpRefClk2    => '0',
      mgtRxRecClk   => open,           pgpClk        => pgpClk,
      pgpReset      => pgpReset,       pgpDispA      => pgpDispA,
      pgpDispB      => pgpDispB,       resetOut      => resetOut,
      locClk        => sysClk125,      locReset      => sysRst125,
      cmdEn         => cmdEn,          cmdOpCode     => cmdOpCode,
      cmdCtxOut     => cmdCtxOut,      regReq        => regReq,
      regOp         => regOp,          regAck        => regAck,
      regFail       => regFail,        regAddr       => regAddr,
      regDataOut    => regDataOut,     regDataIn     => regDataIn,
      frameTxEnable => readDataValid,  frameTxSOF    => readDataSOF,
      frameTxEOF    => readDataEOF,    frameTxEOFE   => readDataEOFE,
      frameTxData   => readData,       valid => vvalid,
      eof => eof, sof => sof,
      mgtRxN        => mgtRxN,
      mgtRxP        => mgtRxP,         mgtTxN        => mgtTxN,
      mgtTxP        => mgtTxP,         mgtCombusIn   => (others=>'0'),
      mgtCombusOut  => open
   );
   
   clkData<=(others=>'0');
   readDataValid<='0';
   vvalid<='0';
   readDataSOF<='0';
   readDataEOF<='0';
   readDataEOFE<='0';
   readData<=(others=>'0');
   regAck1<= (ack(0) or ack(1) or ack(2) or ack(3) or ack(4) or ack(5) or ack(6) or ack(7));   
   regAck2<= (ack(8) or ack(9) or ack(10) or ack(11) or ack(12) or ack(13) or ack(14) or ack(15));   
   regAck3<= (ack(16) or ack(17) or ack(18) or ack(19) or ack(20) or ack(21) or ack(22) or ack(23));   
   regAck<= (regAck1 or regAck2 or regAck3 or drdy or drdy2);
   regFail1<= (err(0) or err(1) or err(2) or err(3) or err(4) or err(5) or err(6) or err(7));   
   regFail2<= (err(8) or err(9) or err(10) or err(11) or err(12) or err(13) or err(14) or err(15));   
   regFail3<= (err(16) or err(17) or err(18) or err(19) or err(20) or err(21) or err(22) or err(23));   
   regFail<= (regFail1 or regFail2 or regFail3);
   req(0)<='1' when regReq='1' and (regAddr(8 downto 4) = "00000" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(1)<='1' when regReq='1' and (regAddr(8 downto 4) = "00001" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(2)<='1' when regReq='1' and (regAddr(8 downto 4) = "00010" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(3)<='1' when regReq='1' and (regAddr(8 downto 4) = "00011" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(4)<='1' when regReq='1' and (regAddr(8 downto 4) = "00100" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(5)<='1' when regReq='1' and (regAddr(8 downto 4) = "00101" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(6)<='1' when regReq='1' and (regAddr(8 downto 4) = "00110" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(7)<='1' when regReq='1' and (regAddr(8 downto 4) = "00111" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(8)<='1' when regReq='1' and (regAddr(8 downto 4) = "01000" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(9)<='1' when regReq='1' and (regAddr(8 downto 4) = "01001" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(10)<='1' when regReq='1' and (regAddr(8 downto 4) = "01010" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(11)<='1' when regReq='1' and (regAddr(8 downto 4) = "01011" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(12)<='1' when regReq='1' and (regAddr(8 downto 4) = "01100" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(13)<='1' when regReq='1' and (regAddr(8 downto 4) = "01101" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(14)<='1' when regReq='1' and (regAddr(8 downto 4) = "01110" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(15)<='1' when regReq='1' and (regAddr(8 downto 4) = "01111" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11110") else '0';   
   req(16)<='1' when regReq='1' and (regAddr(8 downto 4) = "10000" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11101") else '0';   
   req(17)<='1' when regReq='1' and (regAddr(8 downto 4) = "10001" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11101") else '0';   
   req(18)<='1' when regReq='1' and (regAddr(8 downto 4) = "10010" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11101") else '0';   
   req(19)<='1' when regReq='1' and (regAddr(8 downto 4) = "10011" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11101") else '0';   
   req(20)<='1' when regReq='1' and (regAddr(8 downto 4) = "10100" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11101") else '0';   
   req(21)<='1' when regReq='1' and (regAddr(8 downto 4) = "10101" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11101") else '0';   
   req(22)<='1' when regReq='1' and (regAddr(8 downto 4) = "10110" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11101") else '0';   
   req(23)<='1' when regReq='1' and (regAddr(8 downto 4) = "10111" or regAddr(8 downto 4) = "11111" or regAddr(8 downto 4) = "11101") else '0';   
   reqclkphase <='1' when regReq='1' and (regAddr(8 downto 4) = "11000" or regAddr(8 downto 4) = "11001" or regAddr(8 downto 4) = "11010") else '0';
   reqclkfx<='1'when regreq='1' and (regAddr(8 downto 4) = "11011" or regAddr(8 downto 4) = "11100") else '0';   
   with regAddr(8 downto 4) select
        regDataIn <= thedata(0) when "00000",
                     thedata(1) when "00001",
                     thedata(2) when "00010",
                     thedata(3) when "00011",
                     thedata(4) when "00100",
                     thedata(5) when "00101",
                     thedata(6) when "00110",
                     thedata(7) when "00111",
                     thedata(8) when "01000",
                     thedata(9) when "01001",
                     thedata(10) when "01010",
                     thedata(11) when "01011",
                     thedata(12) when "01100",
                     thedata(13) when "01101",
                     thedata(14) when "01110",
                     thedata(15) when "01111",
                     thedata(16) when "10000",
                     thedata(17) when "10001",
                     thedata(18) when "10010",
                     thedata(19) when "10011",
                     thedata(20) when "10100",
                     thedata(21) when "10101",
                     thedata(22) when "10110",
                     thedata(23) when "10111",
                     x"0000" & clkdata when "11010",
                     x"ffffffff" when others;
   
   PATTERN_GEN_DATA: 
   for I in 0 to 15 generate
   
   U_pattern_mem: pattern_mem
     generic map(
       DELAY=>setting(I))
     port map (
     clk         => sysClk125,
     serclk      => pgpClk,             --was serclk
     deserclk    => phaseclk,
     rst         => sysRst125,
     pgpEnable   => req(I),
     pgpRW       => regOp,
     pgpAck      => ack(I), 
     pgpErr      => err(I), 
     dataFromPgp => regDataOut,
     addrPgp     => regAddr(3 downto 0),
     dataToPgp   => thedata(I),
     dataFromFe  => serialin(I),
     dataToFe    =>  serialout(I));
end generate PATTERN_GEN_DATA;
   PATTERN_GEN_CMD: 
   for I in 16 to 23 generate
   
   U_pattern_cmd: pattern_cmd port map (
     clk         => sysClk125,
     serclk      => clock80,
     deserclk    => clock40,
     retclk      => retclock(I),
     rst         => sysRst125,
     doricreset  => dreset(I),
     pgpEnable   => req(I),
     pgpRW       => regOp,
     pgpAck      => ack(I), 
     pgpErr      => err(I), 
     dataFromPgp => regDataOut,
     addrPgp     => regAddr(3 downto 0),
     dataToPgp   => thedata(I),
     dataFromFe  => serialin(I),
     dataToFe    =>  serialout(I));
end generate PATTERN_GEN_CMD;
     doricreset<=dreset(20);

     clockrst<=holdrst;
     clockrst2<=phaserst;
     U_idelctrlclk: clock200 port map(
       CLKIN_IN  => clock160,
       RST_IN    => clockrst,
       CLKFX_OUT => clockidctrl,
       CLK0_OUT  => open,
       LOCKED_OUT => lockedid);  
     U_idelayctrl: IDELAYCTRL port map(
       RDY     => open,
       REFCLK  => clockidctrl,
       RST     => idctrlrst);

      U_clockmult: phaseshift port map(
        CLKIN_IN  => clock80,            -- was clock160
        DADDR_IN  => daddr,
        DCLK_IN   => sysClk125,
        DEN_IN    => denfx,
        DI_IN     => regDataOut(15 downto 0),
        --DO_OUT     => clkdata,
        DWE_IN    => regOp,
        RST_IN    => clockrst,
        CLK0_OUT  => open,
        CLKFX_OUT  => fxclock,
        CLK2X_OUT  => open,
        DRDY_OUT  => drdy,
        LOCKED_OUT=> lockedfx);
        --PSDONE_OUT=> open);
     U_phaseshift: phaseshift port map(
       CLKIN_IN  => pgpClk,            -- was fxclock
       DADDR_IN  => daddr,
       DCLK_IN   => sysClk125,
       DEN_IN    => denphase,
       DI_IN     => regDataOut(15 downto 0),
       --DO_OUT     => (others=>'0'),
       DWE_IN    => regOp,
       RST_IN    => clockrst2,
       CLK0_OUT  => phaseclk,           -- was open
       CLKFX_OUT  => open,
       CLK2X_OUT  => open,              -- was phaseclk
       DRDY_OUT  => drdy2,
       LOCKED_OUT=> lockedphase);
       --PSDONE_OUT=> psdone);
      U_noshift: phaseshift port map(
        CLKIN_IN  => fxclock,
        DADDR_IN  => (others=>'0'),
        DCLK_IN   => sysClk125,
        DEN_IN    => '0',
        DI_IN     => (others=>'0'),
        --DO_OUT     => (others=>'0'),
        DWE_IN    => '0',
        RST_IN    => clockrst2,
        CLK0_OUT  => open,
        CLKFX_OUT  => open,
        CLK2X_OUT  => serclk,
        DRDY_OUT  => open,
        LOCKED_OUT=> lockednophase);
        --PSDONE_OUT=> open);


     with regAddr(8 downto 4) select
       daddr<="1010101" when "11000",
              "0010001" when "11001",
              "0000000" when "11010",
              "1010000" when "11011",
              "1010010" when "11100",
              "0000000" when others;
     process(sysRst125, sysClk125) -- clock interface
     begin
       if (sysRst125='1') then
         holdctr<=(others=>'1');
         holdrst<='1';
         clkenafx<='1';
       elsif(sysClk125'event and sysClk125='1') then
         if (reqclkfx='1' and oldreqclkfx='0') then
           clkenafx<='1';
           holdctr<=x"ffffff"&'1';
           holdrst<='1';
         else
           clkenafx<='0';
           if (holdctr=x"000000"&'0') then
             phaserst<='0';
           else
             if (holdctr(24)='0') then
               holdrst<='0';
               phaserst<='1';
             end if;
             holdctr<=unsigned(holdctr)-1;
           end if;
         end if;
         if (reqclkphase='1' and oldreqclkphase='0') then
           clkenaphase<='1';
         else
           clkenaphase<='0';
         end if;
         denfx<=clkenafx;
         denphase<=clkenaphase;
         oldreqclkfx<=reqclkfx;
         oldreqclkphase<=reqclkphase;
       end if;
     end process;
     process(sysClk125, sysRst125) -- reset logic for IDELAYCTRL
     begin
       if(sysRst125='1') then
         idctrlrst<='0';
         idcounter<="000";
         oldlockedid<='0';
       elsif(sysClk125'event and sysClk125='1') then
         if(lockedid='1' and oldlockedid='0')then
           idcounter<="111";
           idctrlrst<='1';
         elsif(unsigned(idcounter)>0)then
           idcounter<=unsigned(idcounter)-1;
         else
           idctrlrst<='0';
         end if;
         oldlockedid<=lockedid;
       end if;
     end process;
            
end IBLcableTesterCore;

