#include "config/IPCModuleFactory.hh"
#include "config/FormatterFactory.hh"
#include "config/FEI3/Module.hh"
#include "config/FEI3/IPCModule.cc"
#include "config/FEI4/IPCFEI4AModule.cc"
#include "config/FEI4/IPCFEI4BModule.cc"
#include "config/hitbus/IPCModule.cc"
#include "config/afp-hptdc/IPCModule.cc"
#include "config/Trigger.hh"
#include "config/MultiTrigger.hh"
#include "config/EventFromFileTrigger.hh"
#include "config/MeasurementTrigger.hh"
#include "config/MultiShotTrigger.hh"
#include "config/HitorTrigger.hh"
#include "config/FEI4/TemperatureTrigger.hh"
#include "config/FEI4/MonleakTrigger.hh"
#include "config/FEI4/SNTrigger.hh"
#include "config/FEI4/Fei4RegisterTestTrigger.hh"
#include "util/exceptions.hh"
#include "stdio.h"
#include <iostream>

IPCModuleFactory::IPCModuleFactory(IPCPartition& p):ModuleFactory(),m_partition(p){
  m_modulegroups.push_back(&m_modgroupi3);
  m_modulegroups.push_back(&m_modgroupi4a);
  m_modulegroups.push_back(&m_modgroupi4b);
  m_modulegroups.push_back(&m_modgrouphitbus);
  m_modulegroups.push_back(&m_modgroupafphptdc);
}


AbsModule* IPCModuleFactory::createModule(const char* name, const char* type, unsigned id, unsigned inLink, unsigned outLink, const char* formatter){
  FormatterFactory ff;
  AbsFormatter* theformatter=ff.createFormatter(formatter, id);
  if(std::string(type)=="FEI3"){
    FEI3::Module *mod= new FEI3::Module(name, id, inLink, outLink, theformatter);
    m_modgroupi3.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI3_singleThread"){
    FEI3::Module *mod= new FEI3::IPCModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi3.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI3_multiThread"){
    FEI3::Module *mod= new FEI3::IPCModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi3.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI4A_singleThread"){
    FEI4::Module *mod= new FEI4::IPCFEI4AModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi4a.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI4A_multiThread"){
    FEI4::Module *mod= new FEI4::IPCFEI4AModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi4a.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI4B_singleThread"){
    FEI4::Module *mod= new FEI4::IPCFEI4BModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi4b.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_FEI4B_multiThread"){
    FEI4::Module *mod= new FEI4::IPCFEI4BModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupi4b.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_Hitbus_singleThread"){
    Hitbus::Module *mod= new Hitbus::IPCModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgrouphitbus.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_Hitbus_multiThread"){
    Hitbus::Module *mod= new Hitbus::IPCModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgrouphitbus.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_AFPHPTDC_singleThread"){
    afphptdc::Module *mod= new afphptdc::IPCModule<ipc::single_thread>(m_partition, name,id, inLink, outLink, theformatter);
    m_modgroupafphptdc.addModule(mod);
    return mod;
  }
  if(std::string(type)=="IPC_AFPHPTDC_multiThread"){
    std::cout<<"Adding module"<<std::endl;
    afphptdc::Module *mod= new afphptdc::IPCModule<ipc::multi_thread>(m_partition, name,id, inLink, outLink, theformatter);
    std::cout<<"Added module"<<std::endl;
    m_modgroupafphptdc.addModule(mod);
    return mod;
  }
  std::cout<<"Unknown module type "<<type<<std::endl;
  rcecalib::Unknown_Module_Type issue;
  throw issue;
  return 0;
} 

AbsTrigger* IPCModuleFactory::createTriggerIF(const char* type, ConfigIF* cif){
  //  std::cout << "Creating trigger of type " << type << std::endl;
  if(std::string(type)=="default") return new FEI3::Trigger;
  else if(std::string(type)=="FEI3")return new FEI3::Trigger;
  else if(std::string(type)=="EventFromFile")return new EventFromFileTrigger;
  else if(std::string(type)=="MultiShot")return new MultiShotTrigger;
  else if(std::string(type)=="MultiTrigger")return new MultiTrigger;
  else if(std::string(type)=="Hitor")return new HitorTrigger(cif);
  else if(std::string(type)=="Temperature")return new FEI4::TemperatureTrigger;
  else if(std::string(type)=="Monleak")return new FEI4::MonleakTrigger;
  else if(std::string(type)=="SerialNumber")return new FEI4::SNTrigger;
  else if(std::string(type)=="Fei4RegisterTest")return new FEI4::Fei4RegisterTestTrigger;
#ifdef __rtems__
  else if(std::string(type)=="Measurement")return new MeasurementTrigger;
#endif
  char msg[128];
  printf("No Trigger type %s",type);
  assert(0); 
}
