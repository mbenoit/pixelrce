#include "config/RceFWRegisters.hh"
#include "config/ConfigIF.hh"
#include "util/exceptions.hh"

void RceFWRegisters::writeRegister(int rce, unsigned reg, unsigned val){
  int badHSIOconnection=m_configIF->writeHWregister(reg, val);
  if(badHSIOconnection){
    rcecalib::Pgp_Problem err;
    throw err;
  }
}
unsigned RceFWRegisters::readRegister(int rce, unsigned reg){
  unsigned val;
  int badHSIOconnection=m_configIF->readHWregister(reg, val);
  if(badHSIOconnection){
    rcecalib::Pgp_Problem err;
    throw err;
  }
  return val;
}
void RceFWRegisters::sendCommand(int rce, unsigned opcode){
    m_configIF->sendHWcommand(opcode);
}
