# Checks
# ------
# Check release location variables
# number of parallel make jobs
ifneq ($(JOBS),)
JOBS:=16
endif

ifeq ($(RELEASE_DIR),)
export RELEASE_DIR := $(PWD)
endif

export RCE_CORE := /daq/slc5/opt/rcecore/$(RCE_CORE_VERSION)
# Includes
# --------

ifeq ($(RCE_CORE_VERSION),2.2)
.coredep:
	mkdir -p $(RELEASE_DIR)/build
	ln -sfn $(RCE_CORE)/build/configuration  $(RELEASE_DIR)/build
	ln -sfn $(RCE_CORE)/build/oldPpi  $(RELEASE_DIR)/build
	ln -sfn $(RCE_CORE)/build/platform  $(RELEASE_DIR)/build
	ln -sfn $(RCE_CORE)/build/service  $(RELEASE_DIR)/build
	ln -sfn $(RCE_CORE)/build/tool  $(RELEASE_DIR)/build
else
.coredep:
	mkdir -p $(RELEASE_DIR)/build
	ln -sfn $(RCE_CORE)/build/rce  $(RELEASE_DIR)/build
	ln -sfn $(RCE_CORE)/build/rceusr  $(RELEASE_DIR)/build
	ln -sfn $(RCE_CORE)/build/rceapp  $(RELEASE_DIR)/build
endif



.softlinks:	.coredep
	ln -sfn $(RTEMS)  $(RELEASE_DIR)/build/rtems
	ln -sfn $(ROOTSYS) $(RELEASE_DIR)/build

.idl:	
	+$(MAKE) -j$(JOBS)  -C $(RELEASE_DIR)/rcecalib/idl $(RCE_ARCH) $(HOST_ARCH) 



.HW:	.softlinks .idl
	+$(MAKE) -j$(JOBS) -C $(RELEASE_DIR)/rcecalib/HW  $(RCE_ARCH)  $(HOST_ARCH)
.eudaq:
	+$(MAKE) -j$(JOBS) -C $(RELEASE_DIR)/rcecalib/eudaq $(RCE_ARCH) $(HOST_ARCH) 
.dataproc:
	+$(MAKE) -j$(JOBS) -C $(RELEASE_DIR)/rcecalib/dataproc $(RCE_ARCH) $(HOST_ARCH)
.profiler:
	+$(MAKE) -j$(JOBS) -C $(RELEASE_DIR)/rcecalib/profiler $(RCE_ARCH) $(HOST_ARCH)
.scanctrl:
	+$(MAKE) -j$(JOBS) -C $(RELEASE_DIR)/rcecalib/scanctrl $(RCE_ARCH) $(HOST_ARCH)
.analysis:
	+$(MAKE) -j$(JOBS) -C $(RELEASE_DIR)/rcecalib/analysis $(RCE_ARCH) $(HOST_ARCH)
.config:
	+$(MAKE) -j$(JOBS) -C $(RELEASE_DIR)/rcecalib/config  $(RCE_ARCH) $(HOST_ARCH)
.util:
	+$(MAKE) -j$(JOBS) -C $(RELEASE_DIR)/rcecalib/util  $(RCE_ARCH) $(HOST_ARCH)
.server:	.HW .eudaq .dataproc .profiler .scanctrl .analysis .config .util 
	+$(MAKE) -j$(JOBS) -C $(RELEASE_DIR)/rcecalib/server  $(RCE_ARCH) $(HOST_ARCH)
.gpib:	.server
	+$(MAKE) -j$(JOBS) -C rcedcs/gpib  $(HOST_ARCH)
#	+$(MAKE) -j$(JOBS) $(RELEASE_DIR)/rcedcs/labjack  $(HOST_ARCH)
