#include "rcecalib/scanctrl/SelftriggerSetup.hh"
#include "rcecalib/scanctrl/ScanLoop.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/scanctrl/ActionFactory.hh"
#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/scanctrl/EndOfLoopAction.hh"
#include "rcecalib/util/exceptions.hh"

int SelftriggerSetup::setupLoops( NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af){
  int retval=0; 
  loop.clear();
  try{ //catch bad scan option parameters
    // Data Taking has a single loop with one entry
    ScanLoop *scanloop=new ScanLoop("Selftrigger",1);
    LoopAction* conf=af->createLoopAction("CONFIGURE_MODULES");
    scanloop->addLoopAction(conf);
    //    LoopAction* linkmask=af->createLoopAction("SETUP_CHANNELMASK");
        //scanloop->addLoopAction(linkmask);
    // End of loop actions
    //The fitaction is used here to close the file with the hits if available.
    EndOfLoopAction* fitaction=af->createEndOfLoopAction("FIT","CLOSE_FILE");
    scanloop->addEndOfLoopAction(fitaction);
    EndOfLoopAction* resetfe=af->createEndOfLoopAction("RESET_FE","");
    scanloop->addEndOfLoopAction(resetfe);
    EndOfLoopAction* disablechannels=af->createEndOfLoopAction("DISABLE_ALL_CHANNELS","");
    scanloop->addEndOfLoopAction(disablechannels);
    // loop is the nested loop object
    loop.addNewInnermostLoop(scanloop);
  }
  catch(boost::property_tree::ptree_bad_path ex){
    rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
    ers::error(issue);
    retval=1;
  }
  //  loop.print();
  return retval;
}
