#include "rcecalib/server/ConfigGui.hh"
#include "rcecalib/server/TurboDaqFile.hh"
#include "rcecalib/server/FEI4AConfigFile.hh"
#include "rcecalib/server/FEI4BConfigFile.hh"
#include "rcecalib/server/HitbusConfigFile.hh"
#include "rcecalib/server/AFPHPTDCConfigFile.hh"
#include "rcecalib/util/exceptions.hh"
#include <TGFileDialog.h>
#include <sys/stat.h> 
#include <boost/regex.hpp>
#include "PixelModuleConfig.hh"
#include <iostream>

std::string ConfigGui::m_rootdir="";

ConfigGui::ConfigGui(const char* name, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options, const char* prefix):
  TGVerticalFrame(p,w,h,options), m_modName(name), m_valid(false), m_cfg(0), m_cfgfei4a(0), m_cfgfei4b(0), m_cfghitbus(0),
  m_filename("None"), m_id(-1) {
  std::string namelabel(prefix);
  namelabel+=name;
  m_name=new TGLabel(this,namelabel.c_str());
  AddFrame(m_name,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 2, 2, 0, 0));
  FontStruct_t labelfont;
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  m_name->SetTextFont(labelfont);

  TGHorizontalFrame *confnl = new TGHorizontalFrame(this, 2,2 );
  AddFrame(confnl,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *cfg=new TGLabel(confnl,"File:");
  confnl->AddFrame(cfg,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_configname=new TGLabel(confnl,"None");
  confnl->AddFrame(m_configname,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  //TGLabel *cfgkey=new TGLabel(confnl,"-");
  //confnl->AddFrame(cfgkey,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_key=new TGLabel(confnl,"");
  confnl->AddFrame(m_key,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));

  TGHorizontalFrame *confdnl = new TGHorizontalFrame(this, 2,2 );
  AddFrame(confdnl,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *cfgd=new TGLabel(confdnl,"Dir:");
  confdnl->AddFrame(cfgd,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_confdir=new TGLabel(confdnl,"None");
  confdnl->AddFrame(m_confdir,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));

  TGHorizontalFrame *confidl = new TGHorizontalFrame(this, 2,2 );
  AddFrame(confidl,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *cfgid=new TGLabel(confidl,"FEID:");
  confidl->AddFrame(cfgid,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_idl=new TGLabel(confidl,"-1");
  confidl->AddFrame(m_idl,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_modid=new TGLabel(confidl,"");
  confidl->AddFrame(m_modid,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));
  TGLabel *modid=new TGLabel(confidl,"ModID:");
  confidl->AddFrame(modid,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));

  TGHorizontalFrame *buttonl = new TGHorizontalFrame(this, 2,2 );
  AddFrame(buttonl,new TGLayoutHints(kLHintsExpandX ));
  m_choosebutton=new TGTextButton(buttonl,"Choose Config");
  buttonl->AddFrame(m_choosebutton,new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX ,2,2,0,0));
  m_choosebutton->Connect("Clicked()", "ConfigGui", this, "openFileWindow()");
  TGTextButton *reload=new TGTextButton(buttonl,"Reload");
  buttonl->AddFrame(reload,new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX ,2,2,0,0));
  reload->Connect("Clicked()", "ConfigGui", this, "setConfig()");

  TGHorizontalFrame *conf2 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf2,new TGLayoutHints(kLHintsExpandX ));

  TGLabel *cfgtype=new TGLabel(conf2,"Type:");
  conf2->AddFrame(cfgtype,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_configtype=new TGLabel(conf2,"");
  conf2->AddFrame(m_configtype,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));

  m_inlink=new TGNumberEntry(conf2, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 15);
  conf2->AddFrame(m_inlink,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 0, 2, 0, 0));
  TGLabel *linklabel2=new TGLabel(conf2,"Inlink:");
  conf2->AddFrame(linklabel2,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));

  TGHorizontalFrame *conf1 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf1,new TGLayoutHints(kLHintsExpandX ));

  m_configvalid=new TGCheckButton(conf1,"Valid");
  //  m_configvalid->SetEnabled(false);
  m_configvalid->SetOn(false);
  conf1->AddFrame(m_configvalid,new TGLayoutHints(kLHintsCenterY | kLHintsLeft | kLHintsExpandX ,2,2,0,0));
  m_configvalid->Connect("Toggled(Bool_t)", "ConfigGui", this, "dummy(Bool_t)");


  m_outlink=new TGNumberEntry(conf1, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 15);
  conf1->AddFrame(m_outlink,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 0, 2, 0, 0));
  TGLabel *linklabel=new TGLabel(conf1,"Outlink:");
  conf1->AddFrame(linklabel,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));


  TGHorizontalFrame *conf3 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf3,new TGLayoutHints(kLHintsExpandX ));

  m_included=new TGCheckButton(conf3,"Included");
  m_included->SetOn(false);
  conf3->AddFrame(m_included,new TGLayoutHints(kLHintsCenterY | kLHintsLeft | kLHintsExpandX ,2,2,0,0));
  m_included->Connect("Toggled(Bool_t)", "ConfigGui", this, "setIncluded(Bool_t)");

  m_rce=new TGNumberEntry(conf3, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 99);
  conf3->AddFrame(m_rce,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 0, 2, 0, 0));
  TGLabel *linklabel3=new TGLabel(conf3,"Rce:");
  conf3->AddFrame(linklabel3,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));

  /*
  TGHorizontalFrame *conf4 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf4,new TGLayoutHints(kLHintsExpandX ));
  m_phase=new TGNumberEntry(conf4, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 3);
  conf4->AddFrame(m_phase,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 0, 2, 0, 0));
  TGLabel *linklabel4=new TGLabel(conf4,"Phase:");
  conf4->AddFrame(linklabel4,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));
  */
}

ConfigGui::~ConfigGui(){
  Cleanup();
}

void ConfigGui::openFileWindow(){
  const char *filetypes[] = { "Config files",     "*.cfg",
                              "All files",     "*",
			      0,               0 };
  boost::cmatch matches;
  boost::regex re("(^.*\\/)(.+\\.cfg)");
  std::string path(m_rootdir);
  std::string filename;
  if(boost::regex_search(m_filename.c_str(), matches, re)){
    if(matches.size()>2){
      path=std::string(matches[1].first, matches[1].second);
      filename=std::string(matches[2].first, matches[2].second);
    }
  }
  TGFileInfo fileinfo;
  fileinfo.fIniDir=StrDup(path.c_str());
  fileinfo.fFilename=StrDup(filename.c_str());
  fileinfo.fFileTypes = filetypes;
  new TGFileDialog(gClient->GetRoot(), 0, kFDOpen, &fileinfo);
  if(!fileinfo.fFilename){
    printf("Scheisse\n");
    return;
  }
  m_filename=fileinfo.fFilename;
  setConfig();
}
void ConfigGui::setId(int id){
  m_id=id; 
  char a[128];
  sprintf(a, "%d", id);
  updateText(m_idl, a);
  setModuleName();
}

void ConfigGui::setModuleName(){
  char modname[128];
  sprintf(modname,"RCE%d_module_%d",getRce(), getId());
  m_modulename=modname;
}

void ConfigGui::setConfig(){
  //printf("%s\n",filename);
  struct stat stFileInfo;
  int intStat;
  // Attempt to get the file attributes 
  intStat = stat(m_filename.c_str(),&stFileInfo);
  if(std::string(m_filename)=="None" || intStat != 0) { //File does not exist
    resetConfig();
  }else{
    std::ifstream cfgFile(m_filename.c_str());
    std::string inpline;
    getline(cfgFile, inpline); //read the first line to determine configuration format
    cfgFile.close();
    boost::regex r1("TurboDAQ");
    boost::regex r2("FEI4A");
    boost::regex r3("FEI4B");
    boost::regex r4("Hitbus");
    boost::regex r5("Hptdc");
    if(boost::regex_search(inpline, r1)==true){
      TurboDaqFile turbo;
      delete m_cfg;
      delete m_cfgfei4a;
      delete m_cfgfei4b;
      delete m_cfghitbus;
      delete m_cfghptdc;
      m_cfgfei4a=0;
      m_cfgfei4b=0;
      m_cfghitbus=0;
      m_cfghptdc=0;
      m_cfg=new ipc::PixelModuleConfig;
      try{
	turbo.readModuleConfig(m_cfg,m_filename);
      }catch(rcecalib::Config_File_Error &err){
	ers::error(err);
	resetConfig();
	return;
      }
      int id=strtol((const char*)m_cfg->idStr,0,10);
      setId(id);
      setType("FEI3");
      updateText(m_modid, (const char*)m_cfg->idStr);
    }else if(boost::regex_search(inpline, r2)==true){ 
      FEI4AConfigFile fei4afile;
      delete m_cfg;
      delete m_cfgfei4b;
      delete m_cfghitbus;
      delete m_cfghptdc;
      m_cfg=0;
      m_cfgfei4b=0;
      m_cfghitbus=0;
      m_cfghptdc=0;
      delete m_cfgfei4a;
      m_cfgfei4a=new ipc::PixelFEI4AConfig;
      try{
	fei4afile.readModuleConfig(m_cfgfei4a, m_filename);
      }catch(rcecalib::Config_File_Error &err){
	ers::error(err);
	resetConfig();
	return;
      }
      setType("FEI4A");
      updateText(m_modid, (const char*)m_cfgfei4a->idStr);
      unsigned newmodid=parseModuleId((const char*)m_cfgfei4a->idStr,m_cfgfei4a->FEGlobal.Chip_SN); 
      setId(newmodid);
    }else if(boost::regex_search(inpline, r3)==true){ 
      FEI4BConfigFile fei4bfile;
      delete m_cfg;
      delete m_cfgfei4a;
      delete m_cfghitbus;
      delete m_cfghptdc;
      m_cfg=0;
      m_cfgfei4a=0;
      m_cfghitbus=0;
      m_cfghptdc=0;
      delete m_cfgfei4b;
      m_cfgfei4b=new ipc::PixelFEI4BConfig;
      try{
	fei4bfile.readModuleConfig(m_cfgfei4b, m_filename);
      }catch(rcecalib::Config_File_Error &err){
	ers::error(err);
	resetConfig();
	return;
      }
      setType("FEI4B");
      updateText(m_modid, (const char*)m_cfgfei4b->idStr);
      unsigned newmodid=parseModuleId((const char*)m_cfgfei4b->idStr,m_cfgfei4b->FEGlobal.Chip_SN); 
      setId(newmodid);
    } else if(boost::regex_search(inpline, r4)==true){
      HitbusConfigFile turbo;
      delete m_cfg;
      delete m_cfgfei4a;
      delete m_cfgfei4b;
      delete m_cfghitbus;
      delete m_cfghptdc;
      m_cfgfei4a=0;
      m_cfgfei4b=0;
      m_cfghptdc=0;
      m_cfg=0;
      m_cfghitbus=new ipc::HitbusModuleConfig;
      try{
	turbo.readModuleConfig(m_cfghitbus,m_filename);
      }catch(rcecalib::Config_File_Error &err){
	ers::error(err);
	resetConfig();
	return;
      }
      int id=strtol((const char*)m_cfghitbus->idStr,0,10);
      setId(id);
      setType("Hitbus");
      updateText(m_modid, (const char*)m_cfghitbus->idStr);
    } else if(boost::regex_search(inpline, r5)==true){
      AFPHPTDCConfigFile turbo;
      delete m_cfg;
      delete m_cfgfei4a;
      delete m_cfgfei4b;
      delete m_cfghitbus;
      delete m_cfghptdc;
      m_cfgfei4a=0;
      m_cfgfei4b=0;
      m_cfghitbus=0;
      m_cfg=0;
      m_cfghptdc=new ipc::AFPHPTDCModuleConfig;
      try{
	turbo.readModuleConfig(m_cfghptdc,m_filename);
      }catch(rcecalib::Config_File_Error &err){
	ers::error(err);
	resetConfig();
	return;
      }
      int id=strtol((const char*)m_cfghptdc->idStr,0,10);
      setId(id);
      setType("HPTDC");
      updateText(m_modid, (const char*)m_cfghptdc->idStr);
    }else{
      std::cout<<"Unknown configuration format."<<std::endl;
      resetConfig();
      return;
    }
     
    setValid(true);
    setIncluded(true);
  }
  boost::cmatch matches;
  std::string res="/([^/]*)\\.cfg$"; // parse what's hopefully the config file name
  boost::regex re;
  re=res;
  if(boost::regex_search(m_filename.c_str(), matches, re)){
    if(matches.size()>1){
      std::string match(matches[1].first, matches[1].second);
      res="(.*)__([0-9]+)$"; // parse what's hopefully the key
      re=res;
      if(boost::regex_search(match.c_str(), matches, re)){
	if(matches.size()>2){
	  std::string match1(matches[1].first, matches[1].second);
	  std::string match2(matches[2].first, matches[2].second);
	  updateText(m_configname,match1.c_str());
	  updateText(m_key,match2.c_str());
	}else{
	  updateText(m_configname,match.c_str());
	  updateText(m_key,"");
	}
      }else{
	updateText(m_configname,match.c_str());
	updateText(m_key,"");
      }
    }else{
      updateText(m_configname,"None");
      updateText(m_key,"");
    }
  }else{
    updateText(m_configname,"None");
    updateText(m_key,"");
  }
  res="/([^/]*)/[^/]*/*[^/]*$"; // parse what's hopefully the module name
  re=res;
  if(boost::regex_search(m_filename.c_str(), matches, re)){
    if(matches.size()>1){
      std::string match(matches[1].first, matches[1].second);
      updateText(m_confdir, match.c_str());
      m_path=m_filename.substr(0,matches.position()+1);
    }else{
      updateText(m_confdir, "None");
    }
  }else{
    updateText(m_confdir, "None");
  }
  Layout();
}
unsigned ConfigGui::parseModuleId(const char* module, unsigned felong){
  boost::cmatch matches;
  std::string res="([0-9]+)-([0-9]+)-([0-9]+)"; // parse what's hopefully the Module name
  boost::regex re;
  re=res;
  unsigned fe=felong&0x3f;
  if(boost::regex_search(module, matches, re)){
    if(matches.size()>3){
      std::string match1(matches[1].first, matches[1].second);
      std::string match2(matches[2].first, matches[2].second);
      std::string match3(matches[3].first, matches[3].second);
      unsigned val1=strtoul(match1.c_str(),0,10);
      unsigned val2=strtoul(match2.c_str(),0,10);
      unsigned val3=strtoul(match3.c_str(),0,10);
      return val1*1e6+val2*1e4+val3*1e2+fe;
    }else{
      //std::cout<<"Module string not parsable or FE ID>99. Using SN"<<std::endl;
      return felong;
    }
  }
  //std::cout<<"Module string not parsable. Using SN"<<std::endl;
  return felong;
}
void ConfigGui::resetConfig(){
  setIncluded(false);
  setValid(false);
  setType("");
  setInLink(0);
  setOutLink(0);
  setRce(0);
  updateText(m_configname,"None");
  updateText(m_modid, "None");
  m_filename="None";
  setId(-1);
}  
void ConfigGui::updateText(TGLabel* label, const char* newtext){
  unsigned len=strlen(label->GetText()->GetString());
  label->SetText(newtext);
  if (strlen(newtext)>len)Layout();
}

void ConfigGui::setIncluded(bool on){
  if(m_configvalid->IsOn())
    m_included->SetOn(on);
  else 
    m_included->SetOn(false);
}

void ConfigGui::setValid(bool on){
  m_configvalid->SetOn(on);
  m_valid=on;
}
void ConfigGui::setType(const char* type){
  updateText(m_configtype, type);
}
void ConfigGui::dummy(bool on){
  // disable clicking
  m_configvalid->SetOn(m_valid);
}

bool ConfigGui::isValid(){
  return m_valid;
}

const std::string ConfigGui::getType(){
  return std::string(m_configtype->GetText()->Data());
}

bool ConfigGui::isIncluded(){
  return m_included->IsOn() || m_included->IsDisabledAndSelected();
}
void ConfigGui::enableControls(bool on){
  m_choosebutton->SetEnabled(on);
  if((m_included->GetState()==kButtonDisabled)==on)
    m_included->SetEnabled(on);
  m_inlink->SetState(on);
  m_outlink->SetState(on);
  m_rce->SetState(on);
  m_configvalid->SetEnabled(on);
  //m_phase->SetState(true);
}
  
void ConfigGui::writeConfig(std::ofstream &ofile){
  copyConfig(ofile, m_filename.c_str());
}

void ConfigGui::copyConfig(std::ofstream &ofile, const char* filename){
  std::string fn(filename);
  if(fn.substr(0,m_rootdir.size())==m_rootdir){
    ofile<<fn.substr(m_rootdir.size())<<std::endl;
  }else{
    ofile<<filename<<std::endl;
  }
  ofile<<isIncluded()<<std::endl;
  ofile<<getInLink()<<std::endl;
  ofile<<getOutLink()<<std::endl;
  ofile<<getRce()<<std::endl;
  ofile<<getPhase()<<std::endl;
}
void ConfigGui::readConfig(std::ifstream &ifile){
  ifile>>m_filename;
  if(m_filename[0]!='/')m_filename=m_rootdir+m_filename; //relative path
  setConfig();
  bool incl;
  ifile>>incl;
  setIncluded(incl);
  int link;
  ifile >> link;
  setInLink(link);
  ifile >> link;
  setOutLink(link);
  ifile >> link;
  setRce(link);
  ifile >> link;
  setPhase(link);
}
void ConfigGui::copyConfig(const char* dirname){
  if(getType()=="FEI3"){
    boost::cmatch matches;
    std::string res="(.*)/[^/]*/[^/]*$"; // parse what's hopefully the module name
    boost::regex re;
    re=res;
    if(boost::regex_search(m_filename.c_str(), matches, re)){
      if(matches.size()>1){
	std::string match(matches[1].first, matches[1].second);
	char cmd[512];
	sprintf(cmd,"cp -r %s %s", match.c_str(), dirname);
	system (cmd);
      }else
	std::cout<<"Bad config file name"<<std::endl;
    }else
      std::cout<<"Bad config file name"<<std::endl;
  }else if(getType()=="FEI4A"){
    FEI4AConfigFile cf;
    cf.writeModuleConfig(getFEI4AConfig(), dirname, getConfdir(), getConfigName(), getKey());
  }else if(getType()=="FEI4B"){
    FEI4BConfigFile cf;
    cf.writeModuleConfig(getFEI4BConfig(), dirname, getConfdir(), getConfigName(), getKey());
  }else if(getType()=="Hitbus"){
    HitbusConfigFile cf;
    cf.writeModuleConfig(getHitbusConfig(), dirname, getConfdir(), getConfigName(), getKey());
  }else if(getType()=="HPTDC"){
    AFPHPTDCConfigFile cf;
    cf.writeModuleConfig(getHPTDCConfig(), dirname, getConfdir(), getConfigName(), getKey());
  }else{
    std::cout<<"Unknown config type"<<std::endl;
    assert(0);
  }
}
    
