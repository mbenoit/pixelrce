#ifndef IPCREGULARCALLBACK_HH
#define IPCREGULARCALLBACK_HH

#include "server/IPCCallback.hh"

class IPCRegularCallback : public IPCCallback {
public:
  IPCRegularCallback(): IPCCallback(){}
  void notify( const ipc::CallbackParams & msg );
  void stopServer();
};

#endif
