#ifndef ANALYSISGUI_HH
#define ANALYSISGUI_HH

#include "TGMdiMainFrame.h"
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include "TFile.h"
#include "TH2F.h"
#include <string>


class TGListTree;
class TGListTreeItem;
class TH1;
class TRootEmbeddedCanvas;
class IPCGuiCallback;

class AnalysisGui: public TGMainFrame {
public:
  AnalysisGui(const char* filename, int scantype, int flavor, const TGWindow *p,UInt_t w,UInt_t h);
  virtual ~AnalysisGui();
  void handleFileMenu(Int_t);
  void quit();
  void clearTree();
  void updateTree();
  void displayHisto(TGListTreeItem* item, int b);
private:

  void fillHistoTree(TGListTreeItem* branch);
  enum Filemenu {LOAD, SAVE, QUIT};
  TGTextButton* *m_quit;
  TGListTree* m_tree;
  TH1 *m_histo;
  TRootEmbeddedCanvas *m_canvas;
  TFile *m_file, *m_anfile;
  std::string m_dir;
  bool m_delhisto;
  TGCanvas *m_tgc;
  int m_indx;

ClassDef (AnalysisGui,0)
};
#endif
