#ifndef IPCSCAN_CC
#define IPCSCAN_CC

#include "rcecalib/scanctrl/IPCScan.hh"
#include "ers/ers.h"
#include "ipc/partition.h"
#include <boost/property_tree/ptree.hpp>

#include <string>
#include <iostream>
#include "rcecalib/profiler/Profiler.hh"

template <class TP>
void* IPCScan<TP>::startScanStatic(void* arg){
  ((IPCScan<TP>*)arg)->startScan();
  return 0;
}

template <class TP>
IPCScan<TP>::IPCScan(IPCPartition & p, const char * name):
  IPCNamedObject<POA_ipc::IPCScanAdapter, TP>( p, name ) , 
  Scan() {

  try {
    IPCNamedObject<POA_ipc::IPCScanAdapter,TP>::publish();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
}

  

template <class TP>
IPCScan<TP>::~IPCScan(){
  try {
    IPCNamedObject<POA_ipc::IPCScanAdapter,TP>::withdraw();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::warning( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
}

template <class TP>
void IPCScan<TP>::IPCpause(){
  std::cout<<"Pause"<<std::endl;
  pause();
}
template <class TP>
void IPCScan<TP>::IPCresume(){
  std::cout<<"Resume"<<std::endl;
  resume();
}
template <class TP>
void IPCScan<TP>::IPCabort(){
  std::cout<<"Abort"<<std::endl;
  abort();
}
template <class TP>
void IPCScan<TP>::IPCstartScan(){
  pthread_t mthread;
  pthread_attr_t attr;
  int ret;
  // setting a new size
  int stacksize = (PTHREAD_STACK_MIN + 0x20000);
  pthread_attr_init(&attr);
  ret=pthread_attr_setstacksize(&attr, stacksize);
  pthread_create( &mthread, &attr , startScanStatic, this);
  pthread_detach(mthread);
  //startScan();
}
template <class TP>
void IPCScan<TP>::IPCwaitForData(){
  waitForData();
}
template <class TP>
void IPCScan<TP>::IPCstopWaiting(){
  stopWaiting();
}
template <class TP>
CORBA::Long IPCScan<TP>::IPCgetStatus(){
  return getStatus();
}
  
template <class TP>
void IPCScan<TP>::shutdown(){
  std::cout<<"Shutdown"<<std::endl;
}

#endif
