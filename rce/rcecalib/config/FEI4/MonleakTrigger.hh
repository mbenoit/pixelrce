#ifndef MONLEAKTRIGGER_HH
#define MONLEAKTRIGGER_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/AbsTrigger.hh"
#include "rcecalib/HW/BitStream.hh"

namespace FEI4{

  class MonleakTrigger: public AbsTrigger{
  public:
    MonleakTrigger();
    ~MonleakTrigger();
    int configureScan(boost::property_tree::ptree* scanOptions);
    int setupParameter(const char* name, int val);
    int sendTrigger();
    int enableTrigger(bool on);
    int resetCounters();
    void setupTriggerStream();
  private:
    BitStream m_triggerStream;
  };

};

#endif
