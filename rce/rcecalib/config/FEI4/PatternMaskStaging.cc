#include "rcecalib/config/FEI4/PatternMaskStaging.hh"
#include "rcecalib/config/FEI4/Module.hh"

namespace FEI4{
  
  PatternMaskStaging::PatternMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
    std::cout<<"Set up pattern mask staging"<<std::endl;
    PixelRegister::setHitBusMode(1);
  }
  
  void PatternMaskStaging::setupMaskStageHW(int maskStage){
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    global->setField("Conf_AddrEnable", 1, GlobalRegister::HW); 
    global->setField("Colpr_Mode", 3, GlobalRegister::SW); 
    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
      int thestage= (unsigned)i==PixelRegisterFields::fieldPos[PixelRegister::hitbus]? maskStage : 1-maskStage;
      pixel->setupMaskStage(i, thestage, 2);
      m_module->writeDoubleColumnHW(i, i, 0, 0); 
    }
  }
}
