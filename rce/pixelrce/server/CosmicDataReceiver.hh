#ifndef COSMICDATARECEIVER_HH
#define COSMICDATARECEIVER_HH

#include <pthread.h>
#include <string>

class CosmicGui;
class ModuleInfo;
class AbsController;

#include "eudaq/TransportServer.hh"
#include "eudaq/Event.hh"
#include "dataproc/CosmicEventReceiver.hh"
#include "server/CosmicOfflineDataProc.hh"

class CosmicDataReceiver: public CosmicEventReceiver
{
  public:
  CosmicDataReceiver(CosmicGui * gui,
		     AbsController* controller,
		     std::vector<ModuleInfo*> modinfo,
		     std::vector<int> rcesAll,
		     unsigned runnum,
		     const std::string & listenAddress, const std::string& outputFile, 
		     bool writeEudet, bool writeRaw);
    ~CosmicDataReceiver();

    void OnReceive(counted_ptr<eudaq::Event> ev);

    void DataThread();

  private:
    void DataHandler(eudaq::TransportEvent & ev);

    CosmicGui * m_gui;               // Pointer to the gui to pass information back
    eudaq::TransportServer * m_dataserver;  // Receives the data packets
    pthread_t m_thread;
    pthread_attr_t m_threadattr;
    bool m_done;
    bool m_listening;
    std::vector<eudaq::ConnectionInfo*> m_connection;
    CosmicOfflineDataProc* m_dataproc;
};

#endif
