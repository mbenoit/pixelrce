#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "dataproc/Fei4RegisterTestDataProc.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"
#include "config/FEI4/FEI4ARecord.hh"

int Fei4RegisterTestDataProc::fit(std::string fitfun) {
  return 0;
}

int Fei4RegisterTestDataProc::processData(unsigned link, unsigned *buffer, int buflen){
  //m_timer.Start();
  if(buflen==0)return 0;
  int nL1A=0;
  int module=m_linkToIndex[link];
  unsigned char* bytepointer=(unsigned char*)buffer;
  unsigned char* last=bytepointer+buflen*sizeof(unsigned)-3;
  ::FEI4::FEI4ARecord rec;
  int address=-1;
  int row=0, col=0;
  while(bytepointer<=last){
    rec.setRecord(bytepointer); 
    if(rec.isEmptyRecord()){ //includes empty record type
      // do nothing
    }else if(rec.isData()){ //includes empty record type
      std::cout<<"Data"<<std::endl;
    } else if(rec.isDataHeader()){
      //nL1A++;
      std::cout<<"Data Header"<<std::endl;
    }else if(rec.isServiceRecord()){
      //unsigned int moduleId=m_info[module].getId();
      //if(rec.getErrorCode()!=10 && (rec.getErrorCode()!=16 || (rec.getErrorCount()&0x200)) )
      // printf("Service record FE %d . Error code: %d. Count: %d \n", moduleId, rec.getErrorCode(), rec.getErrorCount());
      //if( rec.getErrorCode()<32)m_errhist[module]->fill(rec.getErrorCode(), rec.getErrorCount());
      //header=false;
    }else if(rec.isValueRecord()){ //val rec without addr rec
      nL1A++;
      //printf("Value record: %04x.\n",rec.getValue());
      if(address==-1)std::cout<<"Address record missing"<<std::endl;
      if(m_currentMaskStage<2){ //Pixel register
	if(address==15)m_counter[module]++; //new bit or dcol
	int dcol=m_counter[module]/13;
	int bit=m_counter[module]%13;
	for (int i=0;i<16;i++){
	  if(address>335){
	    row=address-i-336;
	    col=dcol*2;
	  }else{
	    row=335-address+i;
	    col=dcol*2+1;
	  }
	  int oldval=(*m_pix[module])(row, col);
	  oldval&=0x1fff; // clear initial setting to show that the value was read
	  int val=rec.getValue()&(1<<i);
	  bool bad=false;
	  if(m_currentMaskStage==(row%2)){
	    if(!val)bad=true;
	  }else{
	    if(val)bad=true;
	  }
	  if(bad){
	    oldval|=1<<bit;
	  }
	  m_pix[module]->set(row,col, oldval);
	}
      }else{ //global register
	if(address>=0&&address<=35){
	  m_glob[module]->set(address, rec.getValue());
	}
      }
    }else if(rec.isAddressRecord()){ // address record
      //std::cout<<"FE "<<m_info[module].getId()<<": Address record for ";
      //if(rec.isGlobal())std::cout<<" global register ";
      //else std::cout<<" shift register ";
      //std::cout<<rec.getAddress()<<std::endl;
      address=rec.getAddress();
    }else{
      std::cout<<"FE "<<m_info[module].getId()<<": Unexpected record type: "<<std::hex<<rec.getUnsigned()<<std::dec<<std::endl;
      //return FEI4::FEI4ARecord::BadRecord;
      return 0;
    }
    bytepointer+=3;
  }
  //m_timer.Stop();
  return nL1A;
}

Fei4RegisterTestDataProc::Fei4RegisterTestDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif) {
  std::cout<<"Register Test Data Proc"<<std::endl;
  m_counter.clear();
  try{ //catch bad scan option parameters
    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      char name[128];
      char title[128];
      RceHisto2d<int, int> *histo;
      RceHisto1d<int, int> *histo1d;
      unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
      unsigned int rows=m_info[module].getNRows();
      unsigned int moduleId=m_info[module].getId();
      std::string moduleName=m_info[module].getName();
      sprintf(title,"Pixel Register Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Pixel_Reg", moduleId);
      histo=new RceHisto2d<int, int>(name,title,rows,0,rows,cols,0,cols, true);
      histo->setAxisTitle(1,"Column");
      histo->setAxisTitle(0, "Row");
      //initialize to non-zero value
      for(unsigned int i=0;i<rows;i++){
	for(unsigned int j=0;j<cols;j++){
	  histo->set(i, j, 1<<13);
	}
      }
      m_pix.push_back(histo);
      m_counter.push_back(-1);
      sprintf(title,"Global Register Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Global_Reg", moduleId);
      histo1d=new RceHisto1d<int, int>(name,title,36, -0.5, 35.5);
      histo1d->setAxisTitle(0, "Register");
      m_glob.push_back(histo1d);
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
  //m_timer.Reset();
}

Fei4RegisterTestDataProc::~Fei4RegisterTestDataProc(){
  for (size_t module=0;module<m_pix.size();module++){
    delete m_pix[module];
    delete m_glob[module];
  }
}
  
int Fei4RegisterTestDataProc::setMaskStage(int stage){
  m_currentMaskStage=stage;
  for(unsigned i=0;i<m_counter.size();i++)m_counter[i]=-1;
  return 0;
}

