--------------------------------------------------------------
-- SI5338 clock buffer setup
-- Martin Kocian 5/2015
--------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.StdRtlPkg.all;
use work.I2cPkg.all;
use work.all;

--------------------------------------------------------------

entity clockbuf is
generic( PRESCALE_G: integer range 0 to 65535 := 79);
port(	clk: 	    in std_logic;
        trig:       in std_logic;
        fail:       out std_logic:='0';
        done:       out std_logic:='0';
        scl:        inout std_logic;
        sda:        inout std_logic
);
end clockbuf;

--------------------------------------------------------------

architecture CLOCKBUF of clockbuf is

  constant los_mask: std_logic_vector(7 downto 0):=x"04";
  constant lock_mask: std_logic_vector(7 downto 0):=x"15";
  constant NUM_REGS_MAX: integer := 349;
  constant regnum: NaturalArray(0 to NUM_REGS_MAX-1):= (
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75,
 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88,
 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101,
102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114,
115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128,
129, 130, 131, 132, 133, 134, 135, 136, 137, 138,
139, 140, 141, 142, 143, 144, 145, 146, 147, 148,
149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162,
163, 164, 165, 166, 167, 168, 169, 170, 171, 172,
173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186,
187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197,
198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210,
211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
224, 225, 226, 227, 228, 229, 231, 232, 233, 234, 235,
236, 237, 238, 239, 240, 242, 243, 244, 245, 247, 248, 249, 250,
251, 252, 253, 254, 255, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69,
 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84,
 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 255);
--CDR 160 MHz
  constant regval: Slv8Array(0 to NUM_REGS_MAX-1) := (
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"70", x"0F", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"70", x"03", x"42", x"42", x"C0", x"C0", x"C0", x"C0", x"55",
x"06", x"06", x"06", x"06", x"84", x"10", x"24", x"00", x"00", x"00", x"00", x"14",
x"A2", x"00", x"C4", x"07", x"10", x"00", x"06", x"00", x"00", x"00", x"00", x"01",
x"00", x"00", x"00", x"10", x"00", x"06", x"00", x"00", x"00", x"00", x"01", x"00",
x"00", x"00", x"10", x"00", x"06", x"00", x"00", x"00", x"00", x"01", x"00", x"00",
x"00", x"10", x"00", x"06", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"00",
x"10", x"00", x"1E", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"80", x"00",
x"00", x"00", x"40", x"00", x"00", x"00", x"40", x"00", x"80", x"00", x"40", x"00",
x"00", x"00", x"C0", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"FF", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"0D", x"00", x"00", x"F4", x"F0", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"14", x"00", x"00",
x"00", x"F0", x"00", x"00", x"00", x"00", x"A8", x"00", x"84", x"00", x"00", x"00",
x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00",
x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00",
x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00",
x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00",
x"00");
-- CDR 120 MHz
--  constant regval: Slv8Array(0 to NUM_REGS_MAX-1) := (
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"70", x"0F", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"70", x"03", x"42", x"42", x"C0", x"C0", x"C0", x"C0", x"00",
--x"06", x"06", x"06", x"06", x"63", x"0C", x"23", x"00", x"00", x"00", x"00", x"14",
--x"9D", x"10", x"C5", x"07", x"10", x"00", x"08", x"00", x"00", x"00", x"00", x"01",
--x"00", x"00", x"00", x"10", x"00", x"08", x"00", x"00", x"00", x"00", x"01", x"00",
--x"00", x"00", x"10", x"80", x"05", x"00", x"00", x"00", x"00", x"01", x"00", x"00",
--x"00", x"10", x"80", x"05", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"00",
--x"10", x"00", x"1C", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"80", x"00",
--x"00", x"00", x"40", x"00", x"00", x"00", x"40", x"00", x"80", x"00", x"40", x"00",
--x"00", x"00", x"C0", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"FF", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"0D", x"00", x"00", x"F4", x"F0", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"14", x"00", x"00",
--x"00", x"F0", x"00", x"00", x"00", x"00", x"A8", x"00", x"84", x"00", x"00", x"00",
--x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00",
--x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00",
--x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00",
--x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00");
-- XTAL 120 MHz
--  constant regval: Slv8Array(0 to NUM_REGS_MAX-1) := (
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"70", x"0F", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"70", x"0B", x"4A", x"42", x"C0", x"C0", x"C0", x"C0", x"00", 
--x"06", x"06", x"06", x"06", x"63", x"0C", x"23", x"00", x"00", x"00", x"00", x"14",
--x"9D", x"10", x"C5", x"07", x"10", x"00", x"08", x"00", x"00", x"00", x"00", x"01",
--x"00", x"00", x"00", x"10", x"00", x"08", x"00", x"00", x"00", x"00", x"01", x"00",
--x"00", x"00", x"10", x"80", x"05", x"00", x"00", x"00", x"00", x"01", x"00", x"00",
--x"00", x"10", x"80", x"05", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"00",
--x"10", x"00", x"1C", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"80", x"00",
--x"00", x"00", x"40", x"00", x"00", x"00", x"40", x"00", x"80", x"00", x"40", x"00",
--x"00", x"00", x"C0", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"FF", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"0D", x"00", x"00", x"F4", x"F0", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"14", x"00", x"00",
--x"00", x"F0", x"00", x"00", x"00", x"00", x"A8", x"00", x"84", x"00", x"00", x"00",
--x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00",
--x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00",
--x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00",
--x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00");
-- XTAL 160 MHz
--  constant regval: Slv8Array(0 to NUM_REGS_MAX-1) := (
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"70", x"0F", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"70", x"0B", x"4A", x"42", x"C0", x"C0", x"C0", x"C0", x"00", 
--x"06", x"06", x"06", x"06", x"63", x"0C", x"23", x"00", x"00", x"00", x"00", x"14",
--x"A2", x"00", x"C4", x"07", x"10", x"00", x"06", x"00", x"00", x"00", x"00", x"01",
--x"00", x"00", x"00", x"10", x"00", x"06", x"00", x"00", x"00", x"00", x"01", x"00",
--x"00", x"00", x"10", x"00", x"06", x"00", x"00", x"00", x"00", x"01", x"00", x"00",
--x"00", x"10", x"00", x"06", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"00",
--x"10", x"00", x"1E", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"80", x"00",
--x"00", x"00", x"40", x"00", x"00", x"00", x"40", x"00", x"80", x"00", x"40", x"00",
--x"00", x"00", x"C0", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"FF", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"0D", x"00", x"00", x"F4", x"F0", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"14", x"00", x"00",
--x"00", x"F0", x"00", x"00", x"00", x"00", x"A8", x"00", x"84", x"00", x"00", x"00",
--x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00",
--x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00",
--x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00",
--x"00", x"00", x"00", x"00", x"00", x"00", x"01", x"00", x"00", x"90", x"31", x"00",
--x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00", x"90", x"31", x"00", x"00", x"01", x"00", x"00", x"00", x"00", x"00", x"00",
--x"00");
  constant regmask: Slv8Array(0 to NUM_REGS_MAX-1) := (
x"00", x"00", x"00", x"00", x"00", x"00", x"1D", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"80", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"1F", x"1F", x"1F", x"1F", x"FF", x"7F", x"3F", x"00", x"00", x"FF", x"FF", x"3F",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"FF", x"FF", x"3F", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"FF", x"3F", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"3F", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"3F",
x"00", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"BF", x"FF",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"0F", x"0F", x"FF",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"FF", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"FF", x"FF", x"FF", x"FF",
x"FF", x"FF", x"0F", x"0F", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"FF", x"0F", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"0F",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"FF", x"FF", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"02", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"FF", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"FF", x"FF", x"FF", x"FF",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"0F", x"00", x"00", x"00",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"0F", x"00", x"00", x"00", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF",
x"FF", x"FF", x"FF", x"FF", x"0F", x"00", x"00", x"00", x"FF", x"FF", x"FF", x"FF",
x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"FF", x"0F", x"00", x"00", x"00",
x"FF");
  type state_type is (idle, oeball, dislol, writeallack, writeall0, writeall1, writeall2,
                      los, losack, loswait, fcalovrden, fcalovrdenwr, softreset, resetwait,
                      dislol2, dislol2ack, lock, lockack, lockwait, fcalcopy, fcalwrite1,
                      fcalcopy2, fcalwrite2, read47, read237, write47, read49, write49, write230);
  signal state: state_type;
  signal adcI2cIn   : i2c_in_type;
  signal adcI2cOut  : i2c_out_type;
  signal i2cregmasterin: I2cRegMasterInType := I2C_REG_MASTER_IN_INIT_C;
  signal i2cregmasterout: I2cRegMasterOutType;
  signal index: natural range 0 to NUM_REGS_MAX:=0;
  signal val: slv(7 downto 0):=x"00";
  signal lockretry: integer range 0 to 127:=0;
  signal resetcounter: integer range 0 to 2000000:=0;
begin
   sda    <= adcI2cOut.sda when adcI2cOut.sdaoen = '0' else 'Z';
   adcI2cIn.sda <= to_x01z(sda);
   scl    <= adcI2cOut.scl when adcI2cOut.scloen = '0' else 'Z';
   adcI2cIn.scl <= to_x01z(scl);

   i2cregmasterin.regDataSize<="00";
   i2cregmasterin.i2cAddr<="0001110000";  --0x70

   I2cRegMaster_Inst: entity work.I2cRegMaster
     generic map(
       PRESCALE_G => PRESCALE_G)
     port map(
       clk => clk,
       regIn => i2cregmasterin,
       regOut => i2cregmasterout,
       i2ci => adcI2cIn,
       i2co => adcI2cOut);

   process begin
     wait until (rising_edge(clk));
     i2cregmasterin.regReq<='0';
     if(state=idle)then
       if(trig='1') then
         index<=0;
         state<=oeball;
       end if;
     elsif (state=oeball)then
       i2cregmasterin.regAddr <= toSlv(230, 32);
       i2cregmasterin.regWrData(7 downto 0)<=x"10";
       i2cregmasterin.regOp<='1';       --write
       i2cregmasterin.regReq<='1';
       state<=dislol;
     elsif(state=dislol)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(241, 32);
         i2cregmasterin.regWrData(7 downto 0)<=x"e5";
         i2cregmasterin.regOp<='1';       --write
         i2cregmasterin.regReq<='1';
         state<=writeallack;
       end if;
     elsif(state=writeallack)then
       if(i2cregmasterout.regAck='1')then
         if(index/=NUM_REGS_MAX)then
           state<=writeall0;
         else
           state<=los;
           lockretry<=100;
         end if;
       end if;
     elsif(state=writeall0)then
       if(regmask(index)=x"00")then
         index<=index+1;
       elsif(regmask(index)=x"ff")then
           val<=regval(index);
           state<=writeall2;
       else
         i2cregmasterin.regAddr <= toSlv(regnum(index), 32);
         i2cregmasterin.regOp<='0';       --read
         i2cregmasterin.regReq<='1';
         state<=writeall1;
       end if;
     elsif(state=writeall1)then
       if(i2cregmasterout.regAck='1')then
         val<=(regval(index) and regmask(index)) or (i2cregmasterout.regRdData(7 downto 0) and not regmask(index));
         state<=writeall2;
       end if;
     elsif(state=writeall2)then
       i2cregmasterin.regAddr <= toSlv(regnum(index), 32);
       i2cregmasterin.regWrData(7 downto 0)<=val;
       i2cregmasterin.regOp<='1';       --write
       i2cregmasterin.regReq<='1';
       index<=index+1;
       state<=writeallack;
     elsif(state=los)then
       i2cregmasterin.regAddr <= toSlv(218, 32);
       i2cregmasterin.regOp<='0';       --read
       i2cregmasterin.regReq<='1';
       state<=losack;       
     elsif(state=losack)then
       if(i2cregmasterout.regAck='1')then
         if((i2cregmasterout.regRdData(7 downto 0) and los_mask) = x"00")then
           state<=fcalovrden;
         elsif(lockretry=0)then
           fail<='1';
           state<=idle;
         else
           lockretry<=lockretry-1;
           resetcounter<=40000;
           state<=loswait;
         end if;
       end if;
     elsif(state=loswait)then
       resetcounter<=resetcounter-1;
       if(resetcounter=0)then
         state<=los;
       end if;
     elsif(state=fcalovrden)then
       i2cregmasterin.regAddr <= toSlv(49, 32);
       i2cregmasterin.regOp<='0';       --read
       i2cregmasterin.regReq<='1';
       state<=fcalovrdenwr;       
     elsif(state=fcalovrdenwr)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(49, 32);
         i2cregmasterin.regWrData(7 downto 0)<=i2cregmasterout.regRdData(7 downto 0) and x"7f";
         i2cregmasterin.regOp<='1';       --write
         i2cregmasterin.regReq<='1';
         state<=softreset;
       end if;
     elsif(state=softreset)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(246, 32);
         i2cregmasterin.regWrData(7 downto 0)<=x"02";
         i2cregmasterin.regOp<='1';       --write
         i2cregmasterin.regReq<='1';
         resetcounter<=2000000;
         state<=resetwait;
       end if;
     elsif(state=resetwait)then
       resetcounter<=resetcounter-1;
       if(resetcounter=0)then
         state<=dislol2;
       end if;
     elsif(state=dislol2)then
       i2cregmasterin.regAddr <= toSlv(241, 32);
       i2cregmasterin.regWrData(7 downto 0)<=x"65";
       i2cregmasterin.regOp<='1';       --write
       i2cregmasterin.regReq<='1';
       lockretry<=100;
       state<=dislol2ack;
     elsif(state=dislol2ack)then
       if(i2cregmasterout.regAck='1')then
         state<=lock;
       end if;
     elsif(state=lock)then
       i2cregmasterin.regAddr <= toSlv(218, 32);
       i2cregmasterin.regOp<='0';       --read
       i2cregmasterin.regReq<='1';
       state<=lockack;       
     elsif(state=lockack)then
       if(i2cregmasterout.regAck='1')then
         if((i2cregmasterout.regRdData(7 downto 0) and lock_mask) = x"00")then
         state<=fcalcopy;
         elsif(lockretry=0)then
           fail<='1';
           state<=idle;
         else
           lockretry<=lockretry-1;
           resetcounter<=40000;
           state<=lockwait;
         end if;
       end if;
     elsif(state=lockwait)then
       resetcounter<=resetcounter-1;
       if(resetcounter=0)then
         state<=lock;
       end if;
     elsif(state=fcalcopy)then
       i2cregmasterin.regAddr <= toSlv(235, 32);
       i2cregmasterin.regOp<='0';       --read
       i2cregmasterin.regReq<='1';
       state<=fcalwrite1;       
     elsif(state=fcalwrite1)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(45, 32);
         i2cregmasterin.regWrData(7 downto 0)<=i2cregmasterout.regRdData(7 downto 0);
         i2cregmasterin.regOp<='1';       --write
         i2cregmasterin.regReq<='1';
         state<=fcalcopy2;
       end if;
     elsif(state=fcalcopy2)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(236, 32);
         i2cregmasterin.regOp<='0';       --read
         i2cregmasterin.regReq<='1';
         state<=fcalwrite2;       
       end if;
     elsif(state=fcalwrite2)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(46, 32);
         i2cregmasterin.regWrData(7 downto 0)<=i2cregmasterout.regRdData(7 downto 0);
         i2cregmasterin.regOp<='1';       --write
         i2cregmasterin.regReq<='1';
         state<=read47;
       end if;
     elsif(state=read47)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(47, 32);
         i2cregmasterin.regOp<='0';       --read
         i2cregmasterin.regReq<='1';
         state<=read237;       
       end if;
     elsif(state=read237)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(237, 32);
         i2cregmasterin.regOp<='0';       --read
         i2cregmasterin.regReq<='1';
         val<=i2cregmasterout.regRdData(7 downto 0) and x"fc";
         state<=write47;       
       end if;
     elsif(state=write47)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(47, 32);
         i2cregmasterin.regWrData(7 downto 0)<=(i2cregmasterout.regRdData(7 downto 0) and x"03") or val;
         i2cregmasterin.regOp<='1';       --write
         i2cregmasterin.regReq<='1';
         state<=read49;
       end if;
     elsif(state=read49)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(49, 32);
         i2cregmasterin.regOp<='0';       --read
         i2cregmasterin.regReq<='1';
         state<=write49;       
       end if;
     elsif(state=write49)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(49, 32);
         i2cregmasterin.regWrData(7 downto 0)<=i2cregmasterout.regRdData(7 downto 0) or x"80";
         i2cregmasterin.regOp<='1';       --write
         i2cregmasterin.regReq<='1';
         state<=write230;
       end if;
     elsif(state=write230)then
       if(i2cregmasterout.regAck='1')then
         i2cregmasterin.regAddr <= toSlv(230, 32);
         i2cregmasterin.regWrData(7 downto 0)<=x"00";
         i2cregmasterin.regOp<='1';       --write
         i2cregmasterin.regReq<='1';
         done<='1';
         state<=idle;
       end if;
    end if;
   end process;
       

end CLOCKBUF;

--------------------------------------------------------------
