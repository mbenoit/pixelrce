#ifndef FEI4__FEI4AMODULE_HH
#define FEI4__FEI4AMODULE_HH

#include "rcecalib/config/FEI4/Module.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/profiler/Profiler.hh"
#include <string>

class AbsFormatter;

namespace FEI4{

  class FEI4AModule: public Module{
  public:
    FEI4AModule(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter *fmt);
    virtual ~FEI4AModule();
    void resetHW();
    int verifyModuleConfigHW();
    virtual void destroy();
    void setupGlobalPulseHW(int pulsereg);

};

};
#endif
