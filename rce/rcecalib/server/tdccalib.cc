#include <stdio.h>

#include <ers/ers.h>

#include <ipc/partition.h>
#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include <owl/timer.h>
#include <cmdl/cmdargs.h>
#include <unistd.h>

#include "rcecalib/server/TurboDaqFile.hh"
#include "rcecalib/server/IPCController.hh"
#include "rcecalib/server/PixScan.hh"

#include "IPCScanAdapter.hh"
#include "IPCConfigIFAdapter.hh"
#include "IPCFEI3Adapter.hh"
#include "ScanOptions.hh"
#include "PixelModuleConfig.hh"

#include <TApplication.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH2F.h>
#include <TF1.h>
#include <TFile.h>
#include "rcecalib/server/Mean.hh"

int acquireDataPoint(IPCController& controller, int rce);

int main( int argc, char ** argv )
{	
 
  TApplication *tapp;
    CmdArgStr	partition_name ('p', "partition", "partition-name", "partition to work in.");

//
// Initialize command line parameters with default values
//
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmd(*argv, &partition_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmd.parse(arg_iter);
  
    IPCPartition   p( (const char*)partition_name );
    IPCController controller(p);

    tapp=new TApplication("bla", &argc, argv);

    gROOT->SetStyle("Plain");
    gStyle->SetOptStat(0);
    gStyle->SetPalette(1);
    gStyle->SetOptFit(111);
    gStyle->SetErrorX(0);
    
    TH2F* occ2=new TH2F("tdc2","TDC Linearity",64,-.5,63.5,251,-.05,25.05);
    //TCanvas *c2=new TCanvas("c2","Canvas",600,600);
    //c2->Draw();
    //occ2->Draw();

    unsigned serstat;
    int rce=0;
    serstat=controller.writeHWregister(rce, 3,1); //TDC calibration mode
    assert(serstat==0);
    std::cout<<"Part 1: Measurement of tap unit"<<std::endl;
    Mean p2;
    // Set Clock to 90 deg phase
    serstat=controller.writeHWregister(rce, 2,1);
    assert(serstat==0);
    serstat=controller.writeHWregister(rce, 0x180,0); 
    assert(serstat==0);
    serstat=controller.writeHWregister(rce, 0x190,0); 
    assert(serstat==0);
    for (int i=0;i<100;i++){
      int bitpos=acquireDataPoint(controller, rce);
      if(bitpos>-1)p2.accumulate((float)bitpos);
    }
    assert(p2.nEntries()>0);
    std::cout<<"TDC at 90 deg is "<<p2.mean()<<" +- "<<p2.sigma()/(float)p2.nEntries()<<std::endl;
      
    Mean p0;
    serstat=controller.writeHWregister(rce, 2,0); //0 deg
    assert(serstat==0);
    int index=0;
    for (int i=500;i<800;i+=10){
      p0.clear();
      std::cout<<"Data point "<<i<<std::endl;
      controller.writeHWregister(rce, 0x180,i); //set phase in DCM
      controller.writeHWregister(rce, 0x190,0); // move DCM to new phase
      for (int j=0;j<100;j++){
	int bitpos=acquireDataPoint(controller, rce);
	if(bitpos>-1)p0.accumulate((float)bitpos);
      }
      if(p0.mean()>p2.mean()){
	index=i;
	break;
      }
    }
    assert(index>0);
    int index2=index;
    for (int i=index-9;i<index;i++){
      p0.clear();
      std::cout<<"Data point "<<i<<std::endl;
      serstat=controller.writeHWregister(rce, 0x180,i); //set phase in DCM
      assert(serstat==0);
      serstat=controller.writeHWregister(rce, 0x190,0); // move DCM to new phase
      assert(serstat==0);
      for (int j=0;j<100;j++){
	int bitpos=acquireDataPoint(controller, rce);
	if(bitpos>-1)p0.accumulate((float)bitpos);
      }
      if(p0.mean()>p2.mean()){
	index2=i-1;
	break;
      }
    }
    std::cout<<"Overlap at setting "<<index2<<std::endl;
    float tunit=6400./(float)index2;
    std::cout<<"1 tap delay is "<<tunit<<" ps."<<std::endl;
    
    int npoints=(int)(25000./tunit/10);
    TH1F* occ=new TH1F("tdc","TDC Linearity",npoints+1,-5e-3*tunit,25.+5e-3*tunit );
    occ->GetXaxis()->SetTitle("Delay (ns)");
    occ->GetYaxis()->SetTitle("TDC value");
    occ->SetMaximum(70);
    occ->SetMarkerStyle(20);
    occ->SetMarkerSize(.5);
    TCanvas *c1=new TCanvas("c1","Canvas",600,600);
    c1->Draw();
    occ->Draw();
      
    for (int k=0;k<npoints*10;k+=10){
      if(k%500==0)std::cout<<"Data point "<<k<<std::endl;
      unsigned int phase=k/index2; // 6.4 ns clock divided by 9.8 ps tap delay
      unsigned int setting=k%index2;
      controller.writeHWregister(rce, 2,phase);
      controller.writeHWregister(rce, 0x180,setting); //set phase in DCM
      controller.writeHWregister(rce, 0x190,0); // move DCM to new phase
      int nloops=100;
      Mean mn;
      for (int i=0;i<nloops;i++){
	int bitpos=acquireDataPoint(controller, rce);
	if(bitpos>-1){
	  mn.accumulate((float)bitpos);
	  occ2->Fill(bitpos,(float)tunit/1000.*k);
	}
      }
      float mean=mn.mean();
      float sig=mn.sigma();
      occ->SetBinContent(k/10+1,mean);
      if(sig<10)occ->SetBinError(k/10+1,sig);
      occ->Draw("ep");
      c1->Update();
      //occ2->Draw();
      //c2->Update();
      usleep(100000);
    }
    occ->Fit("pol1");
    float fitpar=occ->GetFunction("pol1")->GetParameter(1);
    //    float fitparerr=occ->GetFunction("pol1")->GetParError(1);
    float fact=1./fitpar;
    std::cout<<"Fit result: "<<fact<<" nanoseconds per TDC count"<<std::endl;
    serstat=controller.writeHWregister(rce, 3,0); //Return to normal mode
    assert(serstat==0);
    TFile *file=new TFile("histos.root","recreate");
    occ->Write();
    occ2->Write();
    file->Close();

    tapp->Run();

}

int acquireDataPoint(IPCController& controller, int rce){
  controller.writeHWregister(rce, 1,0); //start TDC measurement
  unsigned counter1;
  controller.readHWregister(rce, 0, counter1);
  unsigned counter2;
  controller.readHWregister(rce, 1, counter2);
  unsigned long long counter=counter2;
  counter=(counter<<32) | counter1;
  int bitpos=-1;
  for (int j=0;j<64;j++){
    if(((counter>>j)&1)==0){
      bitpos=j;
      break;
    }
  } 
  return bitpos;
}
