#ifndef IFWAITACTION_HH
#define IFWAITACTION_HH

#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/scanctrl/Scan.hh"

class IfWaitAction: public LoopAction{
public:
  IfWaitAction(std::string name):
    LoopAction(name){}
  enum del{DELAY=100000};
  int execute(int i){
    //std::cout<<"Pausing scan"<<std::endl;
    usleep(DELAY);
    return 0;
    //std::cout<<"Done pausing scan"<<std::endl;
  }
private:
  Scan* m_scan;
};

#endif
