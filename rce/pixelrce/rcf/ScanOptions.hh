#ifndef SCANOPTIONS_HH
#define SCANOPTIONS_HH
#include <vector>
#include <string>
#include <stdint.h>
#include <SF/string.hpp>
#include <SF/vector.hpp>

namespace ipc{


/* Trigger setup                                                */
/* bit mask optionsMask selects TriggerOptions                  */
/* Override of global VCAL and delays with individual values    */
/* from ModuleConfig via special USE_MODULECONFIG_DEFAULT value */
/* custom bitstream for cal trigger command sequence            */
/* define Triggermodes compatible with RODregisters             */

struct TriggerOptions{
  std::string triggerMode;
  uint16_t    triggerMask;
  uint16_t    moduleTrgMask;
  uint32_t    nEvents;
  uint8_t     triggerDataOn;
  uint8_t     nL1AperEvent;
  uint8_t     nTriggersPerGroup;
  uint8_t     Lvl1_Latency;
  uint8_t     Lvl1_Latency_Secondary;
  uint8_t     injectForTrigger;
  uint8_t     hitbusConfig;
  uint16_t    strobeDuration;
  uint16_t    deadtime;
  uint32_t    strobeMCCDelay;    
  uint32_t    strobeMCCDelayRange;    
  uint32_t    CalL1ADelay;
  uint32_t    eventInterval;
  uint32_t    vcal_charge;
  uint16_t    threshold;
  uint32_t    nTriggers;
  std::vector<std::string> optionsMask; /* this is TriggOptions */
  void serialize(SF::Archive &ar){
    ar & triggerMode & triggerMask & moduleTrgMask& nEvents;
    ar & triggerDataOn & nL1AperEvent & nTriggersPerGroup;
    ar & Lvl1_Latency & Lvl1_Latency_Secondary;
    ar & injectForTrigger & hitbusConfig & strobeDuration;
    ar & deadtime & strobeMCCDelay & strobeMCCDelayRange;
    ar & CalL1ADelay & eventInterval & vcal_charge;
    ar & threshold & nTriggers & optionsMask;
  }
};

struct PixelScanFE {
  uint8_t phi;
  uint8_t totThresholdMode;
  uint8_t totMinimum;
  uint8_t totTwalk;
  uint8_t totLeMode;
  uint8_t hitbus;
  void serialize(SF::Archive &ar){
    ar & phi & totThresholdMode & totMinimum & totTwalk;
    ar & totLeMode & hitbus;
  }
};

/* Action to be performed after the scan, provision for different fit functions */

struct ActionOptions {
  std::string Action;
  std::string fitFunction;
  uint32_t targetThreshold;
  void serialize(SF::Archive &ar){
    ar & Action & fitFunction & targetThreshold;
  }
};


struct ScanLoopDef{
  std::string     scanParameter;
  ActionOptions endofLoopAction; 
  uint32_t nPoints;
  std::vector<int32_t> dataPoints;
  void serialize(SF::Archive &ar){
    ar & scanParameter & endofLoopAction & nPoints & dataPoints;
  }
};

  typedef std::vector<ScanLoopDef> ScanLoops;

struct timeoutstruct{
  uint32_t seconds;
  uint32_t nanoseconds;
  int32_t allowedTimeouts;
  void serialize(SF::Archive &ar){
    ar & seconds & nanoseconds & allowedTimeouts;
  }
};


struct ScanOptions{
  std::string              stagingMode;
  std::string              maskStages;
  uint16_t                 nMaskStages;
  uint16_t                 firstStage;
  uint16_t                 stepStage;
  uint8_t                  nLoops;
  uint32_t                 runNumber;
  TriggerOptions           trigOpt;
  PixelScanFE              feOpt;
  ScanLoops                scanLoop;
  std::string              scanType;
  std::string              receiver;
  std::string              dataHandler;
  std::string              dataProc;
  std::string              name;
  timeoutstruct            timeout;
  std::vector<std::string> histos;

  void serialize(SF::Archive &ar){
    ar & stagingMode & maskStages & nMaskStages & firstStage & stepStage & nLoops & runNumber;  
    ar & trigOpt & feOpt & scanLoop & scanType & receiver & dataHandler & dataProc & name;
    ar & timeout & histos;
  }
};
 
};

#endif
