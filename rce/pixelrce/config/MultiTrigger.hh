#ifndef MULTITRIGGER_HH
#define MULTITRIGGER_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "config/AbsTrigger.hh"
#include "HW/BitStream.hh"


#ifdef __rtems__
#include "rce/pic/Pool.hh"
#include "rce/pic/Tds.hh"
#endif


  class MultiTrigger: public AbsTrigger{
  public:
    MultiTrigger();
    ~MultiTrigger();
    int configureScan(boost::property_tree::ptree* scanOptions);
    int setupParameter(const char* name, int val);
    int sendTrigger();
    int enableTrigger(bool on);
    int resetCounters();
    void setupTriggerStream();
  private:
    void createByteStream(std::vector<char>& stream, unsigned int command, unsigned int len, unsigned int offset);
    BitStream m_triggerStream;
    int m_calL1ADelay;
    int m_repetitions;
    int m_interval;
    int m_nIntervals;
    std::vector<int> m_intervals;
    int m_injectForTrigger;
#ifdef __rtems__
    RcePic::Tds* m_tds;
    RcePic::Pool *m_pool;
#endif
  };


#endif
