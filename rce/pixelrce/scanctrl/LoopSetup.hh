#ifndef LOOPSETUP_HH
#define LOOPSETUP_HH

#include "scanctrl/NestedLoop.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/ConfigIF.hh"
#include "scanctrl/ActionFactory.hh"

class LoopSetup{
public:
  LoopSetup(){};
  virtual ~LoopSetup(){};
  virtual int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af)=0;
};
  

#endif
