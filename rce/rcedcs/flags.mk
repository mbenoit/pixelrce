include $(RELEASE_DIR)/make/sw/flags.mk

tdaq_cmdline_lib=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/cmdline

CPPFLAGS+=-I$(TDAQ_INST_PATH)/include

CPPFLAGS += -I$(RCE_CORE)/include

LXFLAGS  += -L$(RCE_CORE)/$(tgt_arch)/lib
