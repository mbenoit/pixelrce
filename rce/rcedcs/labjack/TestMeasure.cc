#include "rce/net/Getaddr.hh"
#include "rce/net/IpAddress.hh"
#include "rce/net/SocketTcp.hh"
#include "rce/net/Error.hh"
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <boost/regex.hpp>

#include <arpa/inet.h> // for htonl

const char* sendCommand(std::string inpline, RceNet::IpAddress address, const char* rce);

int main(int argc, char** argv)
{
  const char* rce=argv[1];
  unsigned address = RceNet::getaddr(rce);
  RceNet::IpAddress dst(address, 1444);
  std::string inpline("measure");
  for(int i=0;i<100;i++)
  sendCommand(inpline,dst,rce);
}

const char* sendCommand(std::string inpline, RceNet::IpAddress dst, const char* rce){
  const char* hostname=getenv("HOST");
  std::cout<<hostname<<" => "<<rce<<": "<<inpline<<std::endl;
  RceNet::SocketTcp socket;
  socket.connect(dst);
  socket.send(inpline.c_str(), inpline.size());
  int timeout= 10000;
  static char line[128];
  try{
    socket.setrcvtmo(timeout);
    int bytes=socket.recv(line,128);
    line[bytes]=0;
    std::cout<<hostname<<" <= "<<rce<<": "<<line<<std::endl;
    //if(std::string(line)=="Rebooting...")sleep(1); // wait for reboot to finish
  }
  catch (...){
    std::cout<<"Network error. Exiting."<<std::endl;
    exit(0);
  }
  socket.close();
  return line;
}
