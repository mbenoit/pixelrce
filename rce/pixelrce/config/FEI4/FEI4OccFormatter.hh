#ifndef FEI4OCCFORMATTER_HH
#define FEI4OCCFORMATTER_HH

#include "config/AbsFormatter.hh"
#include "util/RceHisto1d.cc"


class FEI4OccFormatter:public AbsFormatter{
public:
  FEI4OccFormatter(int id);
  virtual ~FEI4OccFormatter();
  int decode(const unsigned* data, int size, unsigned* parsedData, int &parsedsize, int &nL1A);
  void configure(boost::property_tree::ptree* scanOptions);

private:
  RceHisto1d<int, int>* m_errhist;
};
#endif
