
#include "ScanLoop.hh"
#include "ers/ers.h"
#include <stdio.h>

ScanLoop::ScanLoop(const char* name, unsigned n): m_nPoints(n),m_nextOuterLoop(0),m_i(0){
    strcpy(m_name,name);
    ERS_ASSERT_MSG(m_nPoints>0,"a scan loop must have at least one scan point");
  }
ScanLoop::~ScanLoop(){
  std::list<LoopAction*>::iterator listit;
  for(listit = m_loopActions.begin(); listit != m_loopActions.end(); listit++){
    delete *listit;
  }
  std::list<EndOfLoopAction*>::iterator listit2;
  for(listit2 = m_endOfLoopActions.begin(); listit2 != m_endOfLoopActions.end(); listit2++){
    delete *listit2;
  }
}
void ScanLoop::addLoopAction(LoopAction *la){
  m_loopActions.push_back(la);
}

void ScanLoop::addEndOfLoopAction(EndOfLoopAction *ea){
  m_endOfLoopActions.push_back(ea);
}

bool ScanLoop::next(){
  m_i++;
  bool done=false;
  if(m_i>=m_nPoints){ // loop is over
    endOfLoopAction();
    if(m_nextOuterLoop==0){
      done=true;
    }else{
      done=m_nextOuterLoop->next();
      reset();
    }
  }
  if(done==false){
    loopAction(m_i); // perform loop action unless the nested loop is at its end
  }
  return done;
}

void ScanLoop::loopAction(unsigned i){
  std::list<LoopAction*>::iterator listit;
  for(listit = m_loopActions.begin(); listit != m_loopActions.end(); listit++){
    (*listit)->execute(i);
  }
}
void ScanLoop::endOfLoopAction(){
  std::list<EndOfLoopAction*>::iterator listit;
  for(listit = m_endOfLoopActions.begin(); listit != m_endOfLoopActions.end(); listit++){
    (*listit)->execute();
  }
}

void ScanLoop::setNextOuterLoop(ScanLoop* loop){
  m_nextOuterLoop=loop;
}
void ScanLoop::firstEvent(){
  reset();
  loopAction(m_i);
}

void ScanLoop::print(const char* prefix){
  std::cout<<prefix<<name()<<" 0 ... "<<m_nPoints - 1<<std::endl;
  std::list<LoopAction*>::iterator lait=m_loopActions.begin();
  if(lait!=m_loopActions.end()){
    std::cout<<prefix<<"Loop Actions:"<<std::endl;
    for(; lait != m_loopActions.end(); lait++){
      std::cout<<prefix<<" "<<(*lait)->name()<<std::endl;
    }
  }
  std::list<EndOfLoopAction*>::iterator elait=m_endOfLoopActions.begin();
  if(elait!=m_endOfLoopActions.end()){
    std::cout<<prefix<<"End of Loop Actions:"<<std::endl;
    for(; elait != m_endOfLoopActions.end(); elait++){
      std::cout<<prefix<<" "<<(*elait)->name()<<std::endl;
    }
  }
}
