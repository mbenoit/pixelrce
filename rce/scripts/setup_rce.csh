#--> These aliases are used to manipulate environmental variables used to hold
#--> directory paths, e.g. PATH, MANPATH.  They allow one to append, prepend
#--> or remove specific directories from such variables.
alias addpath_clean 'delpath \!:*; setenv \!:1 ${\!:1}\:\!:2'
alias addpath2_clean 'delpath \!:*; setenv \!:1 \!:2\:${\!:1}'
# add to end of path
alias addpath 'if ( $\!:1 != \!:2 && $\!:1 !~ \!:2\:* && $\!:1 !~ *\:\!:2\:* && $\!:1 !~ *\:\!:2 ) setenv \!:1 ${\!:1}\:\!:2'
# add to front of path
alias addpath2 'if ( $\!:1 != \!:2 && $\!:1 !~ \!:2\:* && $\!:1 !~ *\:\!:2\:* && $\!:1 !~ *\:\!:2 ) setenv \!:1 \!:2\:${\!:1}; if ( $\!:1 != \!:2 && $\!:1 !~ \!:2\:* ) setenv \!:1 \!:2\:`echo ${\!:1} | sed -e s%^\!:2\:%% -e s%:\!:2\:%:%g -e s%:\!:2\$%%`'
alias delpath 'setenv \!:1 `echo ${\!:1} | sed -e s%^\!:2\$%% -e s%^\!:2\:%% -e s%:\!:2\:%:%g -e s%:\!:2\$%%`'
alias modpath 'setenv \!:1 `echo ${\!:1} | sed -e s%^\!:2\$%\!:3% -e s%^\!:2\:%\!:3\:% -e s%:\!:2\:\%:\!:3\:\%g -e s%:\!:2\$%\:\!:3\%`'

if ( ! -d /home/$USER ) then
exit 0
endif  
setenv PATH /usr/lib/qt-3.3/bin:/bin:/sbin:/usr/sbin/:/usr/local/bin:/usr/bin:/usr/bin/X11:/usr/sue/bin:/usr/X11R6/bin

setenv LD_LIBRARY_PATH
setenv TDAQ_VERSION 3

setenv ORBHOST ''
setenv ORBHOST `/sbin/ifconfig | grep "inet addr:1[79]2" | head -1 | gawk -F: '{print $2}' | awk '{print $1}'`
if ( $ORBHOST != '' ) then
setenv ORBendPoint giop:tcp:${ORBHOST}:0
else
unsetenv ORBHOST
endif

setenv TDAQ_IPC_INIT_REF file:${HOME}/ipc_root.ref
if ( `uname -m` == 'x86_64' ) then
setenv HOST_ARCH  x86_64-slc5-gcc43-opt
setenv TDAQ_HOST_ARCH x86_64-slc5-gcc43-opt
endif
if (`uname -m` == 'i386' ) then
setenv HOST_ARCH   i686-slc5-gcc43-opt
setenv TDAQ_HOST_ARCH i686-slc5-gcc43-opt
endif

#override for now - x86_64 build is broken
setenv HOST_ARCH i686-slc5-gcc43-opt
setenv TDAQ_HOST_ARCH i686-slc5-gcc43-opt

setenv RCE_ARCH ppc-rtems-rce405-opt
setenv RCE ${HOME}/daq/rce

#ambush setup
if ( -e /reg/g/atlas/ambush ) then
setenv AMBUSH  /reg/g/atlas/ambush
addpath2 LD_LIBRARY_PATH ${AMBUSH}/lib
addpath2 PATH ${AMBUSH}/bin
endif

#setup python
if ( -e  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt ) then
addpath2 LD_LIBRARY_PATH  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt/lib
addpath2 PATH  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt/bin
else
  echo Error: Python is not installed
  exit 1
endif 

if ( -e /daq/slc5/opt/rtems-4.9.2 ) then
  setenv RTEMS /daq/slc5/opt/rtems-4.9.2
else
  echo Error: RTEMS is not installed
endif

#setup rtems cross compilers
set RTEMS_GCC=''

if ( -e  /daq/slc5/opt/powerpc-rtems49-gcc432 ) then
  set RTEMS_GCC=/daq/slc5/opt/powerpc-rtems49-gcc432
else
  echo "rtems gcc not found"
  exit 1
endif

if ( $RTEMS_GCC != '' ) then
addpath2 LD_LIBRARY_PATH $RTEMS_GCC/lib
addpath2 PATH $RTEMS_GCC/bin
endif

#setup ROOT
setenv ROOTSYS ''
if ( -e /daq/slc5/sw/lcg/app/releases/ROOT/5.26.00d_python2.6/$TDAQ_HOST_ARCH ) then
  setenv ROOTSYS /daq/slc5/sw/lcg/app/releases/ROOT/5.26.00d_python2.6/$TDAQ_HOST_ARCH/root
# this needs the TDAQ gcc
  if ( `uname -m` == 'x86_64') then
    source  /daq/slc5/sw/lcg/contrib/gcc/4.3/x86_64-slc5-gcc34-opt/setup.csh
    addpath2 LD_LIBRARY_PATH /daq/slc5/sw/lcg/contrib/gcc/4.3/x86_64-slc5-gcc34-opt/lib
  else
    source  /daq/slc5/sw/lcg/contrib/gcc/4.3/slc4_ia32_gcc34/setup.csh
    addpath2  LD_LIBRARY_PATH /daq/slc5/sw/lcg/contrib/gcc/4.3/slc4_ia32_gcc34-opt/lib
  endif
else
  echo "TDAQ gcc not installed"
  exit 1
endif
if ( $ROOTSYS != '' ) then
addpath2 PATH $ROOTSYS/bin
addpath2 LD_LIBRARY_PATH $ROOTSYS/lib
endif

#setup RCE client software 
addpath2 PATH  ${RCE}/build/rceis/bin/${HOST_ARCH}:${RCE}/build/rceipc/bin/${HOST_ARCH}:${RCE}/build/rcecalib/bin/${HOST_ARCH}:${RCE}/rcecalib/scripts
addpath2 LD_LIBRARY_PATH  ${RCE}/build/rcecalib/lib/${HOST_ARCH}:${RCE}/build/rceers/lib/${HOST_ARCH}:${RCE}/build/rceowl/lib/${HOST_ARCH}:${RCE}/build/rceipc/lib/${HOST_ARCH}:${RCE}/build/rceowl/lib/${HOST_ARCH}:${RCE}/build/rceoh/lib/${HOST_ARCH}:${RCE}/build/rceis/lib/${HOST_ARCH}

setenv RELEASE ${RCE}
setenv RCE_BIN ${RCE}/build/rcecalib/bin/${RCE_ARCH}
setenv RCE_MOD ${RCE}/build/rcecalib/mod/${RCE_ARCH}

setenv XMD_INI ${RCE}/rcecalib/xmd.ini
setenv TDAQ_IPC_INIT_REF file:/${HOME}/ipc_root.ref
setenv SVNROOT svn+ssh://svn.cern.ch/reps/RceCimDev
setenv TDAQ_PARTITION rcetest_${USER}

alias rce_ipc_server 'ipc_server -p $TDAQ_PARTITION'
alias rce_is_server  'is_server -p  $TDAQ_PARTITION -n RceIsServer'
alias rce_ipc_ls     'ipc_ls ; ipc_ls -p $TDAQ_PARTITION'
alias rce_load       'echo "reboot\nsetenv TDAQ_PARTITION $TDAQ_PARTITION\nsetenv TDAQ_IS_COMPRESSION_THRESHOLD 100000000\n" | host_bootloader -r \!:1 -l $RCE_MOD/calibservermod.1.0.prod.so' 
alias rce_killall    'pkill -u $USER ipc_server; pkill -u $USER is_server;'

if ( ! -d ~/calibData ) then
mkdir ~/calibData
endif

#tdaq setup
setenv TDAQ_INST_PATH /daq/slc5/tdaq/tdaq-03-00-01/installed
setenv TDAQC_INST_PATH  /daq/slc5/tdaq-common/tdaq-common-01-16-02/installed
setenv TDAQC_EXT_PATH /daq/slc5/tdaq-common/tdaq-common-01-16-02/external
setenv TDAQ_BOOST /daq/slc5/sw/lcg/external/Boost/1.42.0_python2.6
addpath2 PATH             $TDAQ_INST_PATH/$HOST_ARCH/bin
addpath2 LD_LIBRARY_PATH  $TDAQ_INST_PATH/$HOST_ARCH/lib
addpath2 PATH             $TDAQC_INST_PATH/$HOST_ARCH/bin
addpath2 LD_LIBRARY_PATH  $TDAQC_INST_PATH/$HOST_ARCH/lib
addpath2 PATH             $TDAQC_EXT_PATH/$HOST_ARCH/bin
addpath2 LD_LIBRARY_PATH  $TDAQC_EXT_PATH/$HOST_ARCH/lib
addpath2 PATH             $TDAQ_INST_PATH/share/bin
addpath2 LD_LIBRARY_PATH  $TDAQ_INST_PATH/share/lib
setenv PYTHONPATH $TDAQ_INST_PATH/$HOST_ARCH/lib
setenv ARCH $HOST_ARCH
setenv PIXLIBINTERFACE $RCE/../PixLibInterface
rehash




