//  bool PixScanBase::presetRCE(ScanType presetName, FEflavour feFlavour) {
    m_FECounter = 0;
    m_tuningStartsFromConfigValues = false;
    m_dynamicThresholdTargetValue = 0;  
    m_runType=NORMAL_SCAN;
    m_thresholdTargetValue = 3000;  
    m_repetitions= 50;
    m_maskStageTotalSteps= STEPS_32;
    m_maskStageSteps= 3; 
    m_maskStageMode= SEL_ENA;
    m_LVL1Latency= 248; 
    m_strobeLVL1DelayOverride= true;
    m_strobeLVL1Delay= 240;
    m_strobeDuration= 50; 
    m_dspFEbyFE=false;
    m_strobeMCCDelay= 0;
    m_strobeMCCDelayRange= 5;
    m_consecutiveLvl1TrigA[0]= 16;
    m_consecutiveLvl1TrigA[1]= 0;
    m_lvl1HistoBinned= false;
    m_feVCal = 0;
    m_selfTrigger=false;    m_totTargetValue=10;
    m_totTargetCharge=16000;
    m_chargeInjCapHigh=false;
    for(int i=0;i<MAX_LOOPS;i++){
      m_loopParam[i]=NO_PAR;
      m_dspProcessing[i]=false;
      m_loopActive[i] = false;
      m_loopAction[i]=NO_ACTION;
    }
    for (unsigned int i=0; i<4; i++) {
      m_moduleMask[i]= 0xffffffff;
      m_configEnabled[i]= true;
      m_strobeEnabled[i]= true;
      m_triggerEnabled[i]= true;
      m_readoutEnabled[i]= true;
    }

    bool fei3=(feFlavour == PM_FE_I2);
    bool fei4=(feFlavour == PM_FE_I4A || feFlavour == PM_FE_I4B || feFlavour == PM_FE_I4);
    if (presetName == DIGITAL_TEST && fei3) {
      m_maskStageSteps = 32;
      m_strobeDuration = 50;
      m_repetitions = 50;
      m_digitalInjection = true;    
    } else if (presetName == DIGITAL_TEST && fei4) {
      m_maskStageTotalSteps= STEPS_32;
      m_maskStageMode = FEI4_ENA_NOCAP; 
      m_maskStageSteps = 32;
      m_strobeDuration = 1;
      m_digitalInjection = true;    
      m_repetitions = 50;
      m_LVL1Latency = 14; 
      m_consecutiveLvl1TrigA[0]= 16; 
      m_loopActive[1] = true;
      m_loopParam[1] = GDAC_COARSE;
      m_dspProcessing[1] = false;
      setLoopVarValues(1, 200, 200, 1);
    } else if (presetName == DIGITALTEST_SELFTRIGGER && fei4) {
      m_maskStageTotalSteps= FEI4_26880;
      m_maskStageMode = FEI4_ENA_HITBUS_DIG; 
      m_maskStageSteps = 26880;
      m_strobeDuration = 1;
      m_digitalInjection = true;    
      m_repetitions = 10;
      m_LVL1Latency = 235; 
      m_strobeLVL1Delay= 0xffff; // means calstrobe only
      m_selfTrigger=true;
      m_consecutiveLvl1TrigA[0]= 16; 
    } else if (presetName == MULTISHOT && fei4) {
      m_maskStageTotalSteps= STEPS_32;//FEI4_COL_DIG_40;
      m_maskStageMode = FEI4_ENA_NOCAP; 
      m_maskStageSteps = 2;
      m_strobeDuration = 1;
      m_digitalInjection = false;    
      m_repetitions = 1;
      m_LVL1Latency = 14; 
      m_consecutiveLvl1TrigA[1]= 2; //n L1A
      m_consecutiveLvl1TrigA[0]= 5;// triggers per L1A
      m_strobeLVL1Delay = 0;
    } else if (presetName == ANALOG_TEST && fei3) {
      m_maskStageSteps = 32;
      m_strobeDuration = 500;
      m_feVCal = 400;
      m_digitalInjection = false;    
    } else if (presetName == TOT_TEST && fei3) {
      m_repetitions = 25;
      m_maskStageSteps = 32;
      m_strobeDuration = 500;
      m_totTargetCharge = 20000;
      m_digitalInjection = false;    
    } else if (presetName == ANALOG_TEST && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_strobeMCCDelay= 0;
      m_repetitions = 50;
      m_strobeDuration = 12; 
      //m_strobeDuration = 500;
      m_feVCal = 500;
      m_digitalInjection = false;    
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
    } else if (presetName == TOT_TEST && fei4) {
      //m_maskStageTotalSteps= FEI4_COL_ANL_40; //by double column
      m_totTargetCharge = 16000;
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_strobeMCCDelay= 0;
      m_repetitions = 50;
      m_strobeDuration = 12; 
      m_digitalInjection = false;    
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
    } else if (presetName == TOT_CALIB && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_strobeMCCDelay= 0;
      m_repetitions = 50;
      m_strobeDuration = 12; 
      m_digitalInjection = false;    
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_loopActive[0] = true;
      m_loopParam[0] = NO_PAR;
      m_dspProcessing[0] = true;
      m_loopActive[1] = true;
      m_loopParam[1] = VCAL;
      m_dspProcessing[1] = false;
      setLoopVarValues(0, 0, 0, 1);
      setLoopVarValues(1, 5, 500, 100);
      m_loopAction[0] = NO_ACTION;
      m_dspLoopAction[0] = false;
    } else if (presetName == THRESHOLD_SCAN && fei3) {
      m_repetitions = 50;
      m_strobeDuration = 500;
      m_maskStageSteps = 32;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_maskLoop = 1;
      m_dspProcessing[0] = true;
      m_dspFEbyFE=false;
      setLoopVarValues(0, 0, 100, 101);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
    } else if (presetName == THRESHOLD_SCAN && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_SCAP;
      m_thresholdTargetValue = 3000;
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_maskLoop =1;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 300, 151);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
    } else if (presetName == TEMPERATURE_SCAN && fei4) {
      m_maskStageTotalSteps= FEI4_COL_ANL_40;
      m_consecutiveLvl1TrigA[0]= 1;
      m_repetitions = 100;
      m_maskStageSteps = 1;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = TEMP;
      m_dspProcessing[0] = true;
      std::vector<float> st;
      st.push_back(1);
      st.push_back(3);
      setLoopVarValues(0, st);
      m_loopAction[0] = NORMALIZE;
      m_dspLoopAction[0] = true;
    } else if (presetName == MONLEAK_SCAN && fei4) {
      //m_maskStageTotalSteps= FEI4_COL_ANL_40;  //need to change --BL
      
      m_maskStageTotalSteps= FEI4_26880;
      m_maskStageMode = FEI4_MONLEAK; //sets hitbus=1 on selected pixels, other pixels have hitbus=0
      //m_maskStageMode = FEI4_NOISE;
      //      m_maskStageSteps = 26880;
      m_consecutiveLvl1TrigA[0]= 1;
      m_repetitions = 100;
      
      m_maskStageSteps = 26880;
      //m_maskStageSteps = 600;
      
      m_digitalInjection = false;
    } else if (presetName == SERIAL_NUMBER_SCAN && fei4) {
      m_maskStageTotalSteps= FEI4_COL_ANL_40;
      m_consecutiveLvl1TrigA[0]= 1;
      m_repetitions = 1;
      m_maskStageSteps = 1;
      m_digitalInjection = false;
    } else if (presetName == MODULE_CROSSTALK && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_SCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 000, 300, 101);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      m_maskStageTotalSteps= FEI4_MODULECROSSTALK;
    } else if (presetName == OFFSET_SCAN && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 24; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 4; 
      }
      m_maskStageMode = FEI4_ENA_SCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 300, 101);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_dspProcessing[1] = false;
      m_dspLoopAction[1] = false;
      setLoopVarValues(1, 0, 1, 2);
      m_loopParam[1] = NO_PAR;
      m_loopAction[1] = OFFSET;
    } else if (presetName == CROSSTALK_SCAN && fei4) {
      m_maskStageTotalSteps= FEI4_XTALK_40x8;
      m_maskStageMode = FEI4_XTALK;
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_maskStageSteps = 320;//normally 320
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 1000, 26);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
    } else if (presetName == DIFFUSION && fei4) {
      
      m_maskStageTotalSteps= FEI4_DIFFUSION;  //enable all pixels at once, but only inject into one
      m_maskStageMode = FEI4_XTALK;  
      
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      
      m_maskStageSteps = 1;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 1000, 11);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      
      m_loopActive[1] = true;
      m_dspProcessing[1] = false;
      m_loopParam[1] = NO_PAR;
      std::vector<float> pixels; // = row + 336*col + 1 (row from 0-335, col from 0-79.  We add 1 because maskStage==0 has special meaning)
      pixels.push_back(1);
      setLoopVarValues(1, pixels);

      m_loopAction[1] = SETMASK;
      m_dspLoopAction[1] = false;
          
    } else if (presetName == TWOTRIGGER_THRESHOLD && fei4) {

      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_BCAP;
      m_LVL1Latency = 21; //FE-I4.  If doing only 2 bc readout, then lat=10 and lvl1delay=252 seems to work well.
      // m_strobeLVL1Delay  = 252;
      // must be <=(m_strobeLVL1Delay-14) or >=m_strobeLVL1Delay.  Overwritten by MULTITRIG_INTERVAL loop
      
      m_consecutiveLvl1TrigA[0]= 8;  //Can't be more than 8, since only 16 triggers possible
      m_consecutiveLvl1TrigA[1]= 2;  //Right now, only 2 is possible
      
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 000, 100, 101);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      
      m_loopActive[1] = true;
      m_loopParam[1] = MULTITRIG_INTERVAL;
      m_dspProcessing[1] = false;
      setLoopVarValues(1, 240, 280, 9);
      //setLoopVarValues(1, 20, 50, 2);  
      
    }
    else if (presetName == TWOTRIGGER_NOISE && fei4) {
      
      m_maskStageTotalSteps= FEI4_ALLCOLS;  //enable all pixels at once
      m_maskStageMode = FEI4_NOISE;  //don't inject
      m_LVL1Latency = 21; //FE-I4
      
      m_consecutiveLvl1TrigA[0]= 8;  //Can't be more than 8, since only 16 triggers possible
      m_consecutiveLvl1TrigA[1]= 2;  //Right now, only 2 is possible
      
      m_repetitions = 50000;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_maskStageSteps = 1;
      m_digitalInjection = false;
      
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 100, 100, 1); //doesn't matter what VCAL value, since no injection
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      
      m_loopActive[1] = true;
      m_loopParam[1] = MULTITRIG_INTERVAL;
      m_dspProcessing[1] = false;
      setLoopVarValues(1, 220, 260, 9);
      
      
    } else if (presetName == GDAC_SCAN && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 24; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 5; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_thresholdTargetValue = 1600;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 100, 101);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_loopParam[1] = GDAC;
      m_dspProcessing[1] = false;
      std::vector<float> st;
//      st.push_back(60);
      st.push_back(80);
      st.push_back(110);
      st.push_back(140);
      st.push_back(170);
      setLoopVarValues(1, st);
      m_loopAction[1] = NO_ACTION;
      m_dspLoopAction[1] = false;
      m_loopActive[2] = true;
      m_loopParam[2] = TDACS;
      m_dspProcessing[2] = false;
      setLoopVarValues(2, 15, 15, 1);
      m_loopAction[2] = NO_ACTION;
      m_dspLoopAction[2] = false;
    } else if ((presetName == GDAC_TUNE || presetName == GDAC_RETUNE) && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 24; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 4; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_thresholdTargetValue = 3000;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 100, 101);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_loopParam[1] = NO_PAR;
      m_dspProcessing[1] = false;
      std::vector<float> st;
      if(presetName == GDAC_TUNE){
	st.push_back(160); //starting value
	st.push_back(50);  //diffs
	st.push_back(25);
	st.push_back(15);
      }else{ //retune
	st.push_back(-1); //start with value from file
	st.push_back(12);
	st.push_back(6);
	st.push_back(3);
      }	
      setLoopVarValues(1, st);
      m_loopAction[1] = GDAC_TUNING;
      m_dspLoopAction[1] = false;
      m_loopActive[2] = true;
      if(presetName == GDAC_TUNE){
	m_loopParam[2] = TDACS;
        m_loopAction[2] = GDAC_TUNING;
      }else{
	m_loopParam[2] = NO_PAR;
        m_loopAction[2] = NO_ACTION;
      }
      m_dspProcessing[2] = false;
      setLoopVarValues(2, 15, 15, 1);
      m_dspLoopAction[2] = false;
    } else if ((presetName == GDAC_FAST_TUNE || presetName == GDAC_FAST_RETUNE) && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 24; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 12; 
      }
      m_maskStageMode = FEI4_ENA_SCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 100;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = CHARGE;
      m_dspProcessing[0] = true;
      m_thresholdTargetValue = 3000;
      setLoopVarValues(0, m_thresholdTargetValue, m_thresholdTargetValue, 1);
      m_loopAction[0] = NO_ACTION;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_loopParam[1] = NO_PAR;
      m_dspProcessing[1] = false;
      std::vector<float> st;
      if(presetName == GDAC_FAST_TUNE){
	if(feFlavour == PM_FE_I4A){ //FEI4A
	  st.push_back(160); //starting value
	  st.push_back(48);  //diffs
	  st.push_back(24);
	  st.push_back(12);
	  st.push_back(6);
	  st.push_back(3);
	  st.push_back(1);
	  st.push_back(1);
	}
	else { //FEI4B
	  st.push_back(160); //starting value
	  st.push_back(20);  //diffs
	  st.push_back(20);
	  st.push_back(20);
	  st.push_back(20);
	  st.push_back(10);
	  st.push_back(5);
	  st.push_back(3);
	  st.push_back(1);
	}
      }else{ //retune
	st.push_back(-1); //start with value from file
	if(feFlavour == PM_FE_I4A){ //FEI4A
	  st.push_back(16);
	  st.push_back(8);
	  st.push_back(4);
	  st.push_back(2);
	  st.push_back(1);
	}else{ //FEI4B
	  st.push_back(8);
	  st.push_back(8);
	  st.push_back(8);
	  st.push_back(8);
	  st.push_back(4);
	  st.push_back(2);
	  st.push_back(1);
	}
      }	
      setLoopVarValues(1, st);
      m_loopAction[1] = GDAC_FAST_TUNING;
      m_dspLoopAction[1] = false;
      m_loopActive[2] = true;
      if(presetName == GDAC_FAST_TUNE){
	m_loopParam[2] = TDACS;
        m_loopAction[2] = GDAC_TUNING;
      }else{
	m_loopParam[2] = NO_PAR;
        m_loopAction[2] = NO_ACTION;
      }
      m_dspProcessing[2] = false;
      setLoopVarValues(2, 15, 15, 1);
      m_dspLoopAction[2] = false;
    } else if (presetName == GDAC_COARSE_FAST_TUNE && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 24; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 12; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 100;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = CHARGE;
      m_dspProcessing[0] = true;
      m_thresholdTargetValue = 3000;
      setLoopVarValues(0, m_thresholdTargetValue, m_thresholdTargetValue, 1);
      m_loopAction[0] = NO_ACTION;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_loopParam[1] = NO_PAR;
      m_dspProcessing[1] = false;
      setLoopVarValues(1, 0, 5, 6);
      m_loopAction[1] = GDAC_COARSE_FAST_TUNING;
      m_dspLoopAction[1] = false;
      m_loopActive[2] = true;
      m_loopParam[2] = TDACS;
      m_dspProcessing[2] = false;
      setLoopVarValues(2, 15, 15, 1);
      m_loopAction[2] = GDAC_TUNING;
      m_dspLoopAction[2] = false;
    } else if (presetName == IF_TUNE && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 25;
      m_strobeDuration = 12;
      m_totTargetCharge = 16000;
      m_totTargetValue = 10;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = IF;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 255, 52);
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_loopParam[1] = FDACS;
      m_dspProcessing[1] = false;
      setLoopVarValues(1, 7, 7, 1);
    } else if (presetName == FDAC_TUNE && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 100;
      m_strobeDuration = 12;
      m_totTargetCharge = 16000;
      m_totTargetValue = 10;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = FDACS;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 15, 16);
      m_dspLoopAction[0] = true;
    } else if (presetName == VTHIN_SCAN && fei4) {
      m_maskStageTotalSteps= FEI4_COL_ANL_40x8;
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_maskStageSteps = 320;
      m_digitalInjection = false;
      m_feVCal = 500;
      m_loopActive[0] = true;
      m_loopParam[0] = GDAC;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 160, 100, 61);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
    } else if (presetName == DELAY_SCAN && fei4) {
      m_maskStageTotalSteps= FEI4_COL_ANL_40;
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_repetitions = 1;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_feVCal = 500;
      m_maskStageSteps = 40;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = STROBE_DELAY;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 63, 64);
      m_loopAction[0] = NO_ACTION;
      m_dspLoopAction[0] = false;
    } else if (presetName == MEASUREMENT_SCAN) {
      m_maskStageTotalSteps= FEI4_COL_ANL_1;
      m_maskStageMode = SEL_ENA; //non-existent
      m_LVL1Latency = 255; //FE-I4
      m_consecutiveLvl1TrigA[0]= 1;
      m_repetitions = 10;
      m_feVCal = 500;
      m_strobeDuration = 4;
      m_strobeMCCDelay= 0;
      m_maskStageSteps = 1;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 1020,103 );
      m_loopAction[0] = NORMALIZE;
      m_dspLoopAction[0] = true;
    } else if (presetName == SELFTRIGGER && fei4) {
      m_consecutiveLvl1TrigA[0]= 2;
      m_LVL1Latency = 239;
      m_digitalInjection = false;
      m_selfTrigger=true;
      m_feVCal = 500;
    } else if (presetName == REGISTER_TEST && fei4) {
      m_LVL1Latency= 100; 
      m_consecutiveLvl1TrigA[0]= 42;// triggers per L1A
      m_digitalInjection = false;
      m_strobeDuration = 12; 
      m_maskStageSteps= 3; 
      m_maskStageTotalSteps= FEI4_PATTERN;
      m_repetitions= 13*40;
      m_feVCal=341;
    } else if (presetName == EXT_REGISTER_VERIFICATION && fei4) {
      m_digitalInjection = false;
    } else if (presetName == NOISESCAN && fei4) {
      m_digitalInjection = false;
      m_consecutiveLvl1TrigA[0]= 1; 
    } else if (presetName == NOISESCAN_SELFTRIGGER && fei4) {
      m_LVL1Latency = 230;
      m_digitalInjection = false;
      m_selfTrigger=true;
      m_consecutiveLvl1TrigA[0]= 1; 
    } else if (presetName == STUCKPIXELS && fei4) {
      m_maskStageTotalSteps= FEI4_26880;
      m_maskStageMode = FEI4_ENA_HITBUS; //sets hitbus=0 on selected pixels, other pixels have hitbus=1
      m_maskStageSteps = 26880;
      m_consecutiveLvl1TrigA[0]= 1;  //can only do one readout per pixel
      m_repetitions = 1;
      m_digitalInjection = false;
    } else if (presetName == LV1LATENCY_SCAN && fei3) {
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = LATENCY;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 240, 16);
    } else if (presetName == SCINTDELAY_SCAN) {
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, -60, 60, 13);
    } else if (presetName == COSMIC_DATA) {
      m_digitalInjection = false;
    } else if (presetName == COSMIC_RCE) {
      m_digitalInjection = false;
    } else if (presetName == EXTTRIGGER && fei4) {
      //m_LVL1Latency = 210;
      //m_strobeLVL1Delay = 22;
      m_LVL1Latency = 8;
      m_strobeLVL1Delay = 22;
      m_digitalInjection = false;
      m_selfTrigger=false;
      m_consecutiveLvl1TrigA[0]= 15;
    } else if ((presetName == TDAC_TUNE || presetName == GDAC_TUNE) && fei3) {
      m_repetitions = 25;
      m_maskStageMode = SEL_ENA;
      m_strobeDuration = 500;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_maskLoop = 1;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 200, 201);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_dspProcessing[1] = false;
      m_dspLoopAction[1] = false;
      if (presetName == TDAC_TUNE) {
	m_maskStageSteps = 32;
	m_maskLoop = 1;
	std::vector<float> st;
	st.push_back(70);
	st.push_back(16);
	st.push_back(8);
	st.push_back(4);
	st.push_back(2);
	st.push_back(1);
	setLoopVarValues(1, st);
	m_loopParam[1] = NO_PAR;
	m_loopAction[1] = TDAC_TUNING;
	m_thresholdTargetValue = 3200;
      } else {
	m_maskStageSteps = 8;
	m_loopParam[1] = GDAC;
	m_maskLoop = 1;
	setLoopVarValues(1, 7, 19, 4);
	m_loopAction[1] = NO_ACTION;
	m_thresholdTargetValue = 3200;
        m_loopActive[2] = true;
        m_loopParam[2] = TDACS;
        m_dspProcessing[2] = false;
        setLoopVarValues(2, 70, 70, 1);
        m_loopAction[2] = NO_ACTION;
        m_dspLoopAction[2] = false;
      }
    } else if ((presetName == TDAC_TUNE || presetName==TDAC_TUNE_ITERATED) && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_strobeMCCDelay= 0;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_thresholdTargetValue=1600;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 100, 101);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_dspProcessing[1] = false;
      m_dspLoopAction[1] = false;
      std::vector<float> st;
      if(presetName==TDAC_TUNE){
	st.push_back(15); //initial value
	st.push_back(8); //increments
	st.push_back(4);
	st.push_back(2);
	st.push_back(1);
	st.push_back(1);
      }else{
	st.push_back(-1); // means use values from file
	st.push_back(4);
	st.push_back(2);
	st.push_back(1);
      }
      setLoopVarValues(1,st);
      //setLoopVarValues(1, 0, 5, 6);
      m_loopParam[1] = NO_PAR;
      m_loopAction[1] = TDAC_TUNING;
    } else if ((presetName == TDAC_FAST_TUNE || presetName==TDAC_FAST_RETUNE) && fei4) {
      if(feFlavour == PM_FE_I4A){
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_SCAP; 
      m_LVL1Latency = 14; //FE-I4
      m_consecutiveLvl1TrigA[0]= 16;
      m_strobeMCCDelay= 0;
      m_repetitions = 100;
      m_strobeDuration = 12;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] =  CHARGE;
      m_dspProcessing[0] = true;
      m_thresholdTargetValue = 3000;
      setLoopVarValues(0, m_thresholdTargetValue, m_thresholdTargetValue, 1);
      m_loopAction[0] = NO_ACTION;
      m_dspLoopAction[0] = false;
      m_loopActive[1] = true;
      m_dspProcessing[1] = false;
      m_dspLoopAction[1] = false;
      std::vector<float> st;
      if(presetName==TDAC_FAST_TUNE){
	st.push_back(15); //initial value
	st.push_back(3); //increments
	st.push_back(3);
	st.push_back(3);
	st.push_back(3);
	st.push_back(2);
	st.push_back(1);
      }else{
	st.push_back(-1); // means use values from file
	st.push_back(2);
	st.push_back(2);
	st.push_back(2);
	st.push_back(2);
	st.push_back(2);
	st.push_back(2);
	st.push_back(1);
      }
      setLoopVarValues(1,st);
      //setLoopVarValues(1, 0, 5, 6);
      m_loopParam[1] = NO_PAR;
      m_loopAction[1] = TDAC_FAST_TUNING;
    } else if (presetName == T0_SCAN && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 24; //FE-I4
      m_consecutiveLvl1TrigA[0]= 12;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_totTargetCharge = 10000;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = STROBE_DELAY;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 62, 63);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_dspProcessing[1] = false; //this must be false for loop-1
      m_dspLoopAction[1] = false;
      m_loopParam[1] = LATENCY;
      setLoopVarValues(1, 24, 25, 2); //248,251,4
    } else if (presetName == TIMEWALK_MEASURE && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_BCAP; 
      m_LVL1Latency = 25; //26 is too large
      m_consecutiveLvl1TrigA[0]= 12;  //14
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = STROBE_DELAY;
      m_dspProcessing[0] = true;
      setLoopVarValues(0, 0, 62, 63);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_dspProcessing[1] = false; //this must be false for loop-1
      m_dspLoopAction[1] = false;
      m_loopParam[1] = CHARGE;
      setLoopVarValues(1, 4400, 15000, 12); 
    } else if (presetName == INTIME_THRESH_SCAN && fei4) {
      //T0 scan should be run prior to this, to set the
      //value for PlsrDelay in the config file
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 120; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 24; 
      }
      m_maskStageMode = FEI4_ENA_SCAP; 
      m_LVL1Latency = 25; //currently it's impossible to obtain this from config file,
      //since the config value just gets overridden later, so for now
      //we hard code this.
      m_consecutiveLvl1TrigA[0]= 1;
      m_repetitions = 50;
      m_strobeDuration = 12;
      m_strobeMCCDelay= 0;  // this is the CMDcnt delay, in clock cycles. 
      // Should be zero.  This is added to the 
      // fine PlsrDelay (set by looping over STROBE_DELAY)
      m_digitalInjection = false;
      m_loopActive[0] = true; 
      m_dspProcessing[0] = true;
      m_loopParam[0] = VCAL;
      setLoopVarValues(0, 000, 400, 101);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
    } else if ((presetName == FDAC_TUNE || presetName == IF_TUNE) && fei3) {
      m_repetitions = 25;
      m_maskStageSteps = 32;
      m_maskStageMode = SEL_ENA;
      m_strobeDuration = 500;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_dspProcessing[0] = true;
      m_dspFEbyFE=false;
      m_dspLoopAction[0] = false;
      m_loopAction[0] = NO_ACTION;
      if (presetName == FDAC_TUNE) {
	m_loopParam[0] = FDACS;
	m_maskLoop = 1;
	setLoopVarValues(0, 0, 7, 8);
      } else {
	m_loopParam[0] = IF;
	m_maskLoop = 1;
	setLoopVarValues(0, 25, 50, 26);
	m_loopActive[1] = true;
	m_loopParam[1] = FDACS;
	m_dspProcessing[1] = false;
	setLoopVarValues(1, 3, 3, 1);
      }
      m_totTargetValue = 30;
      m_totTargetCharge = 20000;
      if (presetName == FDAC_TUNE) {
      } else {
      }
    } else if (presetName == TOT_CALIB && fei3) {
      m_repetitions = 50;
      m_maskStageSteps = 32;
      m_strobeDuration = 500;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_maskLoop = 1;
      m_dspProcessing[0] = true;
      m_dspFEbyFE=false;
      setLoopVarValues(0, 70, 950, 23);
      m_loopAction[0] = TOTCAL_FIT; 
      m_dspLoopAction[0] = true;
    } else if (presetName == T0_SCAN && fei3) {
      m_repetitions = 25;
      m_maskStageSteps = 32;
      m_feVCal = 0x1fff;
      m_totTargetCharge = 100000;
      m_chargeInjCapHigh = true;
      m_consecutiveLvl1TrigA[0] = 3; //1
      m_consecutiveLvl1TrigB[0] = 3; //1
      m_maskStageMode = SEL_ENA;
      m_strobeDuration = 500;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = STROBE_DELAY;
      m_maskLoop = 1;
      m_dspProcessing[0] = true;
      m_dspFEbyFE=false;
      setLoopVarValues(0, 0, 63, 64);
      m_loopAction[0] = SCURVE_FIT; // nothing
      m_dspLoopAction[0] = true;   //false 
      m_loopActive[1] = true;
      m_dspProcessing[1] = false;
      m_dspLoopAction[1] = false;
      m_loopParam[1] = TRIGGER_DELAY;
      m_maskLoop = 1;
      setLoopVarValues(1, 249, 250, 2); //248,251,4
      m_loopParam[2] = STROBE_DEL_RANGE;
      m_loopActive[2] = true;
      m_dspProcessing[2] = false;
      m_dspLoopAction[2] = false;
      setLoopVarValues(2, 3, 8, 6); //248,251,4
    } else if (presetName == TIMEWALK_MEASURE && fei3) {
      m_repetitions = 25;
      m_maskStageSteps = 32;
      m_chargeInjCapHigh = false;
      m_consecutiveLvl1TrigA[0] = 1;
      m_consecutiveLvl1TrigB[0] = 1;
      m_maskStageMode = SEL_ENA;
      m_strobeDuration = 500;
      m_digitalInjection = false;
      m_strobeLVL1DelayOverride = false;
      m_loopActive[0] = true;
      m_loopParam[0] = STROBE_DELAY;
      m_maskLoop = 1;
      m_dspProcessing[0] = true;
      m_dspFEbyFE=false;
      setLoopVarValues(0, 0, 63, 64);
      m_loopAction[0] = MCCDEL_FIT;
      m_dspLoopAction[0] = false;//true;
      m_loopActive[1] = true;
      m_dspProcessing[1] = false;
      m_dspLoopAction[1] = false;
      m_loopParam[1] = VCAL;
      m_maskLoop = 1;
      std::vector<float> vcalVals;
      vcalVals.push_back(70);
      vcalVals.push_back(75);
      vcalVals.push_back(80);
      vcalVals.push_back(85);
      vcalVals.push_back(90);
      vcalVals.push_back(95);
      vcalVals.push_back(100);
      vcalVals.push_back(110);
      vcalVals.push_back(120);
      vcalVals.push_back(130);
      vcalVals.push_back(140);
      vcalVals.push_back(160);
      vcalVals.push_back(180);
      vcalVals.push_back(200);
      vcalVals.push_back(220);
      vcalVals.push_back(240);
      vcalVals.push_back(280);
      vcalVals.push_back(320);
      vcalVals.push_back(360);
      vcalVals.push_back(400);
      vcalVals.push_back(500);
      vcalVals.push_back(600);
      vcalVals.push_back(700);
      vcalVals.push_back(800);
      vcalVals.push_back(1000);
      setLoopVarValues(1, vcalVals);
      m_loopAction[1] = NO_ACTION;
    } else if (presetName == INTIME_THRESH_SCAN && fei3) {
      m_repetitions = 100;
      m_strobeDuration = 500;
      m_maskStageSteps = 32;
      m_consecutiveLvl1TrigA[0] = 1;
      m_consecutiveLvl1TrigB[0] = 1;
      m_strobeLVL1DelayOverride = false;
      m_strobeMCCDelayRange = 31;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_maskLoop = 1;
      m_dspProcessing[0] = true;
      m_dspFEbyFE=false;
      setLoopVarValues(0, 0, 200, 201);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
    } else if (presetName == CROSSTALK_SCAN && fei3) {
      m_repetitions = 100;
      m_strobeDuration = 500;
      m_maskStageSteps = 32;
      m_maskStageMode = XTALK;
      m_digitalInjection = false;
      m_chargeInjCapHigh = true;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_maskLoop = 1;
      m_dspProcessing[0] = true;
      m_dspFEbyFE=false;
      setLoopVarValues(0, 0, 1000, 26);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
    } else if (presetName == INCREMENTAL_TDAC_SCAN && feFlavour == PM_FE_I2) {
      m_repetitions = 25;
      m_maskStageSteps = 32;
      m_maskStageMode = SEL_ENA;
      m_strobeDuration = 500;
      m_digitalInjection = false;
      m_loopActive[0] = true;
      m_loopParam[0] = VCAL;
      m_maskLoop = 1;
      m_dspProcessing[0] = true;
      m_dspFEbyFE=false;
      setLoopVarValues(0, 0, 200, 201);
      m_loopAction[0] = SCURVE_FIT;
      m_dspLoopAction[0] = true;
      m_loopActive[1] = true;
      m_dspProcessing[1] = false;
      m_dspLoopAction[1] = false;
      m_loopParam[1] = TDACS_VARIATION;
      m_maskLoop = 1;
      std::vector<float> st;
      st.push_back(-5);
      setLoopVarValues(1, st);
      setLoopVarValuesFree(1);
      m_loopAction[1] = MIN_THRESHOLD;
      /* no RCE BOC scans at this point */
      /*    } else if (presetName == BOC_RX_DELAY_SCAN) { return false;
    } else if (presetName == BOC_V0_RX_DELAY_SCAN) { return false;
    } else if (presetName == BOC_THR_RX_DELAY_SCAN) { return false; */
    } else {

   
  std::cout<<"Scan not implemented (index= "<< presetName<<  ")"<<std::endl;

    }
	m_scanTypeEnum=presetName;
    return true;
//  }

