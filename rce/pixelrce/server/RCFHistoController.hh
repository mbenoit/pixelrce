#ifndef RCFHISTOCONTROLLER_HH
#define RCFHISTOCONTROLLER_HH

#include <RCF/RCF.hpp>
#include "server/AbsHistoController.hh"
#include "util/RCFPublisher.hh"
#include <vector>

#include <TH1.h>


class RCFHistoController: public AbsHistoController{
public:
  RCFHistoController();
  ~RCFHistoController();
  
  void addRce(int rce);
  void removeAllRces();
  std::vector<TH1*> getHistos(const char* reg); 
  std::vector<std::string> getHistoNames(const char* reg);
  std::vector<std::string> getPublishedHistoNames();
  bool clear();
  void publishHisto(std::string name, std::string title, RCFRceHistoAxis xaxis,
		    RCF::ByteBuffer bins, RCF::ByteBuffer errors, int, int);
  void publishHisto(std::string name, std::string title, RCFRceHistoAxis xaxis,
		    RCF::ByteBuffer bins, RCF::ByteBuffer errors, float, float);
  void publishHisto(std::string name, std::string title, RCFRceHistoAxis xaxis,
		    RCF::ByteBuffer bins, RCF::ByteBuffer errors, short, short);
  void publishHisto(std::string name, std::string title, RCFRceHistoAxis xaxis,
		    RCFRceHistoAxis yaxis, RCF::ByteBuffer bins, RCF::ByteBuffer errors, 
		    int, int);
  void publishHisto(std::string name, std::string title, RCFRceHistoAxis xaxis,
		    RCFRceHistoAxis yaxis, RCF::ByteBuffer bins, RCF::ByteBuffer errors, 
		    char, char);
  void publishHisto(std::string name, std::string title, RCFRceHistoAxis xaxis,
		    RCFRceHistoAxis yaxis, RCF::ByteBuffer bins, RCF::ByteBuffer errors, 
		    short, short);
  void publishHisto(std::string name, std::string title, RCFRceHistoAxis xaxis,
		    RCFRceHistoAxis yaxis, RCF::ByteBuffer bins, RCF::ByteBuffer errors, 
		    float, float);
  static void onSubDisconnect(RCF::RcfSession & session);
private:
  std::vector<TH1*> m_histos;
  RCF::RcfServer* m_server;
  std::vector<RCF::SubscriptionPtr> m_subscriptions;
};

#endif
