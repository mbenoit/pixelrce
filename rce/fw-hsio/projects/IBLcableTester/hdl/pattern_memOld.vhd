--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 08/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;

--------------------------------------------------------------

entity pattern_mem is
generic( DELAY: integer:=0);
port(	clk: 	    in std_logic;
	serclk:	    in std_logic;
	deserclk:   in std_logic;
	rst:	    in std_logic;
        pgpEnable:  in std_logic;
        pgpRW:      in std_logic;
        pgpAck:     out std_logic;
        pgpErr:     out std_logic;
        dataFromPgp:in std_logic_vector(31 downto 0);
        addrPgp:    in std_logic_vector(3 downto 0);
        dataToPgp:  out std_logic_vector(31 downto 0);
	dataFromFE: in std_logic;
	dataToFE:   out std_logic
);
end pattern_mem;

--------------------------------------------------------------

architecture PATTERN_MEM of pattern_mem is

signal nloaded : std_logic_vector(9 downto 0);
signal pointer : std_logic_vector(9 downto 0);
signal header : std_logic_vector(9 downto 0);
signal nwords: std_logic_vector(63 downto 0);
signal nerr: std_logic_vector(63 downto 0);
signal current: std_logic_vector(31 downto 0);
signal doutb: std_logic_vector(31 downto 0);
signal douta: std_logic_vector(31 downto 0);
signal dout2: std_logic_vector(35 downto 0);
signal din: std_logic_vector(35 downto 0);
signal dataout: std_logic_vector(31 downto 0);
signal serdata: std_logic_vector(31 downto 0);
signal deserdata: std_logic_vector(31 downto 0);
signal xorword: std_logic_vector(31 downto 0);
signal xorword2: std_logic_vector(31 downto 0);
signal xorbit: std_logic;
signal going: std_logic;
signal oldgoing: std_logic;
signal ld: std_logic;
signal dld: std_logic;
signal oldld: std_logic;
signal olddld: std_logic;
signal ena: std_logic;
signal memena: std_logic;
signal resena: std_logic;
signal delena: std_logic;
signal delrstena: std_logic;
signal delayreset: std_logic;
signal delacc: std_logic;
signal delrstacc: std_logic;
signal memacc: std_logic;
signal resacc: std_logic;
signal enaold: std_logic;
signal pgpEnaOld: std_logic;
signal full: std_logic;
signal empty: std_logic;
signal dataFE: std_logic;
signal dataFromFEd: std_logic;
signal rescount: std_logic;
signal rescountena: std_logic;
signal injacc: std_logic;
signal oldinjacc: std_logic;
signal injena: std_logic;
signal loopback: std_logic;
signal ccontrol: std_logic_vector(35 downto 0);
signal cdata: std_logic_vector(31 downto 0);
signal ctrig: std_logic_vector(0 downto 0);
signal overflownerr: std_logic;
signal overflownwords: std_logic;
signal carrynerr: std_logic;
signal carrynwords: std_logic;
signal nerrce: std_logic;
signal oldoverflownerr: std_logic;
signal dataToFEs: std_logic;


function vectorize(s: std_logic) return std_logic_vector is
variable v: std_logic_vector(0 downto 0);
begin
v(0) := s;
return v;
end;

function vectorize(v: std_logic_vector) return std_logic_vector is
begin
return v;
end;
component ser
   port(	clk: 	    in std_logic;
                ld:         out std_logic;
                go:         in std_logic;
                inj:        in std_logic;
                rst:	    in std_logic;
                d_in:	    in std_logic_vector(31 downto 0);
                d_out:	    out std_logic
                );
end component;
component deser
   port(	clk: 	    in std_logic;
 	        rst:	    in std_logic;
                go:	    in std_logic;
                d_in:	    in std_logic;
                d_out:	    out std_logic_vector(31 downto 0);
                ld:         out std_logic
                );
 end component;
component pattern_blk_mem
	port (
	clka: IN std_logic;
	dina: IN std_logic_VECTOR(31 downto 0);
	addra: IN std_logic_VECTOR(9 downto 0);
	ena: IN std_logic;
	wea: IN std_logic_VECTOR(0 downto 0);
	douta: OUT std_logic_VECTOR(31 downto 0);
	clkb: IN std_logic;
	dinb: IN std_logic_VECTOR(31 downto 0);
	addrb: IN std_logic_VECTOR(9 downto 0);
	enb: IN std_logic;
	web: IN std_logic_VECTOR(0 downto 0);
	doutb: OUT std_logic_VECTOR(31 downto 0));
end component;
COMPONENT pattern_blk_mem_62
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    addrb : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;
component iblfifo
        port (
        din: IN std_logic_VECTOR(35 downto 0);
        rd_clk: IN std_logic;
        rd_en: IN std_logic;
        rst: IN std_logic;
        wr_clk: IN std_logic;
        wr_en: IN std_logic;
        dout: OUT std_logic_VECTOR(35 downto 0);
        empty: OUT std_logic;
        full: OUT std_logic);
end component;
component counter30
        port (
        clk: IN std_logic;
        ce: IN std_logic;
        aclr: IN std_logic;
        q_thresh0: OUT std_logic;
        q: OUT std_logic_VECTOR(29 downto 0));
end component;
component IDELAY
        generic (IOBDELAY_TYPE : string := "DEFAULT"; --(DEFAULT, FIXED, VARIABLE)
                 IOBDELAY_VALUE : integer := 0 --(0 to 63)
                 );
        port (
              O : out STD_LOGIC;
              I : in STD_LOGIC;
              C : in STD_LOGIC;
              CE : in STD_LOGIC;
              INC : in STD_LOGIC;
              RST : in STD_LOGIC
             );
end component;

--component ila
   --PORT (
     --CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
     --CLK : IN STD_LOGIC;
     --DATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
     --TRIG0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0));
--
--end component;
 --component icon
   --PORT (
     --CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
--
 --end component;

-- attribute syn_noprune : boolean;
-- attribute syn_noprune of chipscope : label is true;
-- attribute syn_noprune of chipscopeicon : label is true;

begin
 
    dataToFE<=dataToFEs;
    -- serdesrst<=rst or softreset;
    pgpAck<=enaold;
    memena<= ena and memacc;
    resena<= ena and resacc;
    delena<= ena and delacc;
    delrstena<= ena and delrstacc;
    rescountena<= ena and rescount;
    din<="0000" & serdata;
    with loopback select
      dataFE <= dataFromFEd when '0',
                dataToFEs when '1',
                '0' when others;
--    with pgpRW select
 --       maddr <= nloaded when '1',
 --                "0000000" & addrPgp when '0';
    with memacc select
        dataTopgp <= dataout when '0',
                     douta when '1',
                     (others=>'0') when others;

    process(rst, clk) -- user interface

    begin
        if(rst='1') then
          nloaded<="1111111111";
          header<="0000000000";
          current<=x"00000000";
          going<='0';
          ena<='1';
          enaold<='0';
          pgpEnaOld<='0';
          memacc<='0';
          rescount<='0';
          pgpErr<='0';          
          resacc<='1';
          delrstacc<='0';
          delacc<='0';
          oldgoing<='0';
          loopback<='0';
        elsif (clk'event and clk='1') then          
         if(pgpEnable='1' and pgpEnaOld='0') then
            ena<='1';
            case pgpRW is
              when '1'  =>  -- WRITE
                case going is
                  when '0' =>  -- not running
                    case addrPgp is 
                      when "0000" => -- start
                        if(nloaded/="1111111111") then
                          going<='1';
                        end if;
                        memacc<='0';
                        pgpErr<='0';
                        resacc<='0';
                        rescount<='1';
                        injacc<='0';
                        delacc<='0';
                        delrstacc<='0';
                      when "0001" => -- stop
                        pgpErr<='1';
                        memacc<='0';
                        resacc<='0';
                        rescount<='0';
                        injacc<='0';
                        delacc<='0';
                        delrstacc<='0';
                      when "0010" => -- reset
                        nloaded<="1111111111";
                        header<="0000000000";
                        current<=x"00000000";
                        going<='0';
                        memacc<='0';
                        pgpErr<='0';      
                        resacc<='1';
                        rescount<='1';
                        injacc<='0';
                        delacc<='0';
                        delrstacc<='0';
                      when "0011" => -- headersize
                        if unsigned(dataFromPgp(9 downto 0))>=unsigned(nloaded)+1 then
                          pgpErr<='1';
                          memacc<='0';
                          resacc<='0';
                          injacc<='0';
                          delacc<='0';
                          rescount<='0';
                          delrstacc<='0';
                        else
                          pgpErr<='0';
                          memacc<='0';
                          resacc<='0';
                          rescount<='0';
                          injacc<='0';
                          delacc<='0';
                          delrstacc<='0';
                          header<=dataFromPgp(9 downto 0);
                        end if;
                      when "0100" => -- write memory
                        if nloaded = "1111111110" then
                          memacc<='0';
                          resacc<='0';
                          pgpErr<='1';
                          rescount<='0';
                          injacc<='0';
                          delacc<='0';
                          delrstacc<='0';
                        else
                          memacc<='1';
                          resacc<='0';
                          pgpErr<='0';
                          nloaded<=unsigned(nloaded)+1;
                          rescount<='0';
                          injacc<='0';
                          delacc<='0';
                          delrstacc<='0';
                        end if;
                      when "0101" => -- reset counters
                        rescount<='1';
                        memacc<='0';
                        resacc<='0';
                        pgpErr<='0';
                        delacc<='0';
                        injacc<='0';
                        delrstacc<='0';
                      when "0111" => -- loopback on
                        rescount<='0';
                        memacc<='0';
                        resacc<='0';
                        pgpErr<='0';
                        injacc<='0';
                        delacc<='0';
                        loopback<='1';
                        delrstacc<='0';
                      when "1000" => -- loopback off
                        rescount<='0';
                        memacc<='0';
                        resacc<='0';
                        pgpErr<='0';
                        injacc<='0';
                        delacc<='0';
                        loopback<='0';
                        delrstacc<='0';
                      when "1001" => -- increment delay for incoming serial data
                        memacc<='0';
                        resacc<='0';
                        pgpErr<='0';
                        rescount<='0';
                        injacc<='0';
                        delacc<='1';
                        delrstacc<='0';
                      when "1010" => -- reset delay
                        memacc<='0';
                        resacc<='0';
                        pgpErr<='0';
                        rescount<='0';
                        injacc<='0';
                        delacc<='0';
                        delrstacc<='1';
                      when others => -- error
                        memacc<='0';
                        resacc<='0';
                        pgpErr<='1';
                        rescount<='0';
                        injacc<='0';
                        delacc<='0';
                        delrstacc<='0';
                    end case;
                  when '1' => -- running
                    case addrPgp is
                      when "0001" => -- stop
                        pgpErr<='0';
                        memacc<='0';
                        resacc<='1';
                        rescount<='0';
                        injacc<='0';
                        delacc<='0';
                        going<='0';
                        delrstacc<='0';
                      when "0101" => -- reset counters
                        pgpErr<='0';
                        memacc<='0';
                        resacc<='0';
                        rescount<='1';
                        injacc<='0';
                        delacc<='0';
                        delrstacc<='0';
                      when "0110" => -- inject error
                        rescount<='0';
                        memacc<='0';
                        resacc<='0';
                        pgpErr<='0';
                        injacc<='1';
                        delacc<='0';
                        delrstacc<='0';
                      when "1001" => -- increment delay for incoming serial data
                        pgpErr<='0';
                        memacc<='0';
                        resacc<='0';
                        injacc<='0';
                        rescount<='0';
                        delacc<='1';
                        delrstacc<='0';
                      when "1010" => -- reset delay for incoming serial data
                        pgpErr<='0';
                        memacc<='0';
                        resacc<='0';
                        injacc<='0';
                        rescount<='0';
                        delacc<='0';
                        delrstacc<='1';
                      when others =>
                        pgpErr<='1';
                        memacc<='0';
                        resacc<='0';
                        injacc<='0';
                        rescount<='0';
                        delacc<='0';
                        delrstacc<='0';
                    end case;
                   when others =>
                end case;
              when '0' => -- READ (both running and stopped)
                resacc<='0';
                injacc<='0';
                rescount<='0';
                delacc<='0';
                delrstacc<='0';
                case addrPgp is
                  when "0000" =>  -- number of words MSB
                    memacc<='0';
                    pgpErr<='0';
                    dataout<=nwords(63 downto 32);
                  when "0001" =>  -- number of words LSB
                    memacc<='0';
                    pgpErr<='0';
                    dataout<=nwords(31 downto 0);
                  when "0010" =>  -- number of errors MSB
                    memacc<='0';
                    pgpErr<='0';
                    dataout<=nerr(63 downto 32);
                  when "0011" =>  -- number of errors LSB
                    memacc<='0';
                    pgpErr<='0';
                    dataout<=nerr(31 downto 0);
                  when "0100" => -- number of loaded words
                    memacc<='0';
                    pgpErr<='0';
                    dataout<="0000000000000000000000" & unsigned(nloaded)+1;
                  when "0101" => -- last loaded word
                    if (nloaded="1111111111") then
                      pgpErr<='1';
                      memacc<='0';
                      dataout<=x"ffffffff";
                    else
                      pgpErr<='0';
                      memacc<='1';
                    end if;
                  when "0110" => -- empty and full flags
                    memacc<='0';
                    pgpErr<='0';
                    dataout<=x"0000000" &'0'& going & full & empty;
                  when others =>
                    memacc<='0';
                    pgpErr<='1';
                    dataout<=x"ffffffff";
                 end case;
               when others =>
            end case;
          else
            ena<='0';
          end if;
          pgpEnaOld<=pgpEnable;  
          enaold<=ena;
          oldgoing<=going;
          delayreset<=delrstena or rst;  
 	end if;
    
    end process;		

    process(serclk,rst) -- serializer 
    begin
      if (rst='1') then
        oldld<='0';
        pointer<="0000000000";
      elsif (serclk'event and serclk='1') then          
        if (going='1') then
          if (ld='0' and oldld='1') then
            if pointer=nloaded then
              pointer<=header;
            else
              pointer<=unsigned(pointer)+1;
            end if;
          end if;
        elsif (going='0' and oldgoing='1') then
          pointer<="0000000000" ;
        end if;
        if (injacc='1' and oldinjacc='0') then
          injena<='1';
        else
          injena<='0';
        end if;
        oldinjacc<=injacc;
        oldld<=ld;
      end if;
    end process;

    process(deserclk,rst) -- deserializer 
    begin
      if (rst='1') then
        olddld<='0';
        nerrce<='0';
        oldoverflownerr<='0';
        xorword<=x"00000000";
        xorword2<=x"00000000";
        xorbit<='0';
      elsif (deserclk'event and deserclk='1') then          
        if (olddld='1') then
          xorword2<=dout2(31 downto 0);
          xorword<= deserdata ;
--          xorword<=x"12345678";
        else
          xorword(31 downto 1) <= xorword(30 downto 0);
          xorword(0)<='0';
          xorword2(31 downto 1) <= xorword2(30 downto 0);
          xorword2(0)<='0';
        end if;
        xorbit<=xorword(31) xor xorword2(31);
        nerrce<=xorbit;
        if(overflownerr='1' and oldoverflownerr='0')then
          carrynerr<='1';
        else
          carrynerr<='0';
        end if;
        carrynwords<=overflownwords and dld;
        olddld<=dld;
        oldoverflownerr<=overflownerr;
      end if;
    end process;

the_mem : pattern_blk_mem
		port map (
			clka => clk,
			dina => dataFromPgp,
			addra => nloaded,
			ena => memena,
			wea => vectorize(pgpRW),
			douta => douta,
			clkb => serclk,
			dinb => (others=>'0'),
			addrb => pointer,
			enb => ld,
			web => vectorize('0'),
			doutb => serdata);
-- the_mem : pattern_blk_mem_62
--   PORT MAP (
--     clka => clk,
--     ena => memena,
--     wea => vectorize(pgpRW),
--     addra => nloaded,
--     dina => dataFromPgp,
--     clkb => serclk,
--     enb => ld,
--     addrb => pointer,
--     doutb => serdata
--   );
serializer : ser
          port map (
               clk => serclk,
               ld => ld,
               go => going,
               inj => injena,
               rst => resena,
               d_in => serdata,
               d_out => dataToFEs );

valstore : iblfifo    -- stores the values to be checked 
                port map (
                        din => din,
                        rd_clk => deserclk,
                        rd_en => dld,
                        rst => resena,
                        wr_clk => serclk,
                        wr_en => oldld,
                        dout => dout2,
                        empty => empty,
                        full => full);
deserializer: deser 
             port map (
               clk => deserclk,
               rst => resena,
               go  => going,
               d_in => dataFE,
               d_out => deserdata,
               ld => dld);
nerrlow : counter30
             port map (
               clk => deserclk,
               ce => nerrce,
               aclr => rescountena,
               q_thresh0 => overflownerr,
               q => nerr(29 downto 0));
nerrhigh : counter30
             port map (
               clk => deserclk,
               ce => carrynerr,
               aclr => rescountena,
               q_thresh0 => open,
               q => nerr(59 downto 30));

nerr(63 downto 60)<="0000";

nwordslow : counter30
             port map (
               clk => deserclk,
               ce => olddld,
               aclr => rescountena,
               q_thresh0 => overflownwords,
               q => nwords(29 downto 0));
nwordshigh : counter30
             port map (
               clk => deserclk,
               ce => carrynwords,
               aclr => rescountena,
               q_thresh0 => open,
               q => nwords(59 downto 30));

nwords(63 downto 60)<="0000";

delayline : IDELAY
            generic map (
                         IOBDELAY_TYPE => "VARIABLE", -- Set to DEFAULT for -- Zero Hold Time Mode
                         IOBDELAY_VALUE => 0 -- (0 to 63)
                         )
            port map (
                      O => dataFromFEd,
                      I => dataFromFE,
                      C => clk,
                      CE => delena,
                      INC => '1',
                      RST => delayreset 
                      );



-- cdata(0)<=dld;
-- cdata(1)<=olddld;
-- cdata(2)<=overflownerr;
-- cdata(3)<=carrynerr;
-- cdata(4)<=nerrce;
-- cdata(5)<=nerr(30);
-- cdata(6)<=nerr(31);
-- cdata(7)<=nerr(0);
-- cdata(8)<=nerr(1);
-- cdata(3)<=olddld;
-- cdata(4)<=docount;
-- cdata(5)<=datafe;
-- cdata(6)<=xorword(31);
-- cdata(7)<=xorword2(31);
-- cdata(8)<=loopback;
-- cdata(9)<=dataFromFE;
-- cdata(31 downto 9) <=(others=>'0');
-- ctrig(0)<=overflownerr;
-- chipscope : ila
--   port map (
--     CONTROL => ccontrol,
--     CLK => deserclk,
--     DATA => cdata,
--     TRIG0 => ctrig);
-- chipscopeicon : icon
--   port map (
--     CONTROL0 => ccontrol);

end PATTERN_MEM;
--------------------------------------------------------------
