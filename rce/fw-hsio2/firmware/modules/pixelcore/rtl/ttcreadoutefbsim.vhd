--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.StdRtlPkg.all;
use work.AtlasTtcRxPkg.all;
use work.all;

--------------------------------------------------------------

entity ttcreadoutefbsim is
port(	clk: 	    in std_logic;
        enabled:    in std_logic;
        l1a:        in std_logic;
        trgtime:    in std_logic_vector(63 downto 0);
        l1count:    in std_logic_vector(31 downto 0);
	d_out:	    out std_logic_vector(51 downto 0):=(others => '0');
        ld:         out std_logic:='0'
);
end ttcreadoutefbsim;

--------------------------------------------------------------

architecture TTCREADOUTEFB of ttcreadoutefbsim is

  begin

    process
    begin
        wait until rising_edge(clk);
        if(enabled='1' and l1a='1') then -- L1A
          d_out(31 downto 0)<=l1count;
          d_out(51 downto 32)<=trgtime(19 downto 0);
          ld<='1';
        else
          ld<='0';
        end if;
    end process;		

end TTCREADOUTEFB;

--------------------------------------------------------------
