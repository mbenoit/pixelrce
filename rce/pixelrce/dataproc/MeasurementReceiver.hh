#ifndef MEASUREMENTRECEIVER_HH
#define MEASUREMENTRECEIVER_HH

#include "dataproc/PgpReceiver.hh"
#include "config/TriggerReceiverIF.hh"
#include <netinet/in.h>

#include <boost/property_tree/ptree_fwd.hpp>

class MeasurementReceiver: public PgpReceiver, public TriggerReceiverIF{
public:
  MeasurementReceiver(AbsDataHandler* handler, boost::property_tree::ptree* scanOptions);
  void Receive(unsigned *data, int size);
  void receive(PgpTrans::PgpData* pgpdata){}; //pgp dummy
  const char* sendMeasurementCommand();
private:
  struct sockaddr_in m_addr;
};
#endif
