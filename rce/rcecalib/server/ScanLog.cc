#include "rcecalib/server/ScanLog.hh"
#include <stdlib.h>
#include <sys/stat.h> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <ctime>
#include <TROOT.h>
#ifdef SQLDB
#include <mysql.h>
#warning Including SQL database interface
#else
#warning Not including SQL database interface
#endif
//#define SQLDEBUG 1
namespace{
  const char* hostname="atlpix01.cern.ch";
  const int port=443;
}

ScanLog::ScanLog(){
  const char *ou_name=getenv("OLDUSER");
  const char *u_name=getenv("USER");
  m_user="Unknown";
  if(ou_name!=NULL) m_user=ou_name;
  else if(u_name!=NULL)m_user=u_name;
}


void ScanLog::logScan(LogData& logdata){
  std::string logdir=std::string(getenv("HOME"))+"/scanlog";
  struct stat stFileInfo;
  int intStat;
  intStat = stat(logdir.c_str(),&stFileInfo);
  if(intStat != 0) { //File does not exist
    std::cout<<"Creating directory for scan logs"<<std::endl;
    mkdir(logdir.c_str(), 0777);
  }
  std::string runnrfilename=logdir+"/currentRunNumber.txt";
  ofstream runnrfile(runnrfilename.c_str());
  runnrfile<<m_runnumber<<std::endl;
  runnrfile.close(); 
  m_infofilename=logdir+Form("/calibSetup_%06d.txt",m_runnumber);
  char command[512];
  sprintf(command, "cd ~/daq; svn info | grep URL>%s;cd -", m_infofilename.c_str());
  system(command);
  fstream infofile;
  infofile.open(m_infofilename.c_str(),fstream::out | fstream::app);
  infofile<<"Comment: "<<logdata.comment<<std::endl;
  infofile<<"Run number: "<<m_runnumber<<std::endl;
  infofile<<"User: "<<m_user<<std::endl;
  infofile<<"Host: ";
  const char *host_name=getenv("HOST");
  if(host_name!=0)infofile<<host_name<<std::endl;
  else infofile<<"Unknown"<<std::endl;
  infofile<<"Scan Type: "<<m_scanname<<std::endl;
  infofile<<"Debugging Run: "<<m_debugging<<std::endl;
  infofile<<"Data exported: "<<logdata.exported<<std::endl;
  infofile<<"Data saved and analyzed: "<<logdata.saved<<std::endl;
  infofile<<"Data Path: ";
  if(logdata.saved==true)infofile<<logdata.datapath<<std::endl;
  else infofile<<std::endl;
  for (unsigned int i=0;i<logdata.configs.size();i++)
    infofile<<logdata.configs[i]<<std::endl;
  infofile<<"Started: " << formattedCurrentTime() << std::endl;
  infofile.close();
  #ifdef SQLDB
	startLogToDB();
  #endif
  #ifdef ELOG
  #warning ELOG included
  if(m_debugging==false)writeToElog();
  #else
  #warning ELOG not included
  #endif
}
const char* ScanLog::formattedCurrentTime()
{
	time_t now = time(0);
        tm *ltm = localtime(&now);
	char formattedTime [128];
	sprintf(formattedTime, "%04d-%02d-%02d %02d:%02d:%02d", (1900+ltm->tm_year), (1+ltm->tm_mon), (ltm->tm_mday), (ltm->tm_hour),  (ltm->tm_min), (ltm->tm_sec));
	std::string toReturn(formattedTime);
	return toReturn.c_str();	
}
void ScanLog::finishLogFile(std::string runstatus, std::string lstatus){
  fstream infofile;
  infofile.open(m_infofilename.c_str(),fstream::out | fstream::app);
  infofile<<"Finished: " << formattedCurrentTime() << std::endl;
  infofile<<"Status: "<<runstatus<<std::endl;
  #ifdef ELOG
  if(m_debugging==false)updateElog(lstatus);
  #endif
  #ifdef SQLDB
  	finishLogToDB();
  #endif
}

void ScanLog::writeToElog(){
  char elogcmd[1024];
  char elogfile[512];
  sprintf(elogfile,"/tmp/elog_%d.txt", m_runnumber);
  sprintf(elogcmd,"elog -h %s -s -x -p %d -d elog -l \"IBL Stave Test\" -u calibGUI rce -a Author=%s -a Title=\"Scan %d %s\" -a Category=\"Datataking\" -a \"Type of Entry\"=\"Scan Log\" -a Status=\"In progress\" -a \"People present\"=\"%s\" -a Automated=1 -m %s | tee %s &", 
	  hostname, port, m_author.c_str(), m_runnumber, m_scanname.c_str(), 
	  m_user, m_infofilename.c_str(), elogfile);  
  system(elogcmd);
}

void ScanLog::updateElog(std::string lstatus){
  struct stat stFileInfo;
  int intStat;
  char elogfile[512];
  sprintf(elogfile,"/tmp/elog_%d.txt", m_runnumber);
  intStat = stat(elogfile,&stFileInfo);
  if(intStat!=0){
    std::cout<<"Logbook update failed"<<std::endl;
    return;
  }
  int n=30;
  ifstream reply(elogfile);
  char rep[512];
  do{
    reply.getline(rep,512);
    if(!reply.eof())break;
    std::cout<<"Waiting for logbook"<<std::endl;
    sleep(1);
    reply.close();
    reply.open(elogfile);
  }while(n-->0);
  if(n==0){
    std::cout<<"ELOG entry failed"<<std::endl;
  }else{
    int id;
    int stat=sscanf (rep,"Message successfully transmitted, ID=%d",&id);
    if(stat==EOF)std::cout<<"Bad logbook entry"<<std::endl;
    else{
      char elogcmd[1024];
      sprintf(elogcmd,"elog -h %s -s -x -p %d -d elog -l \"IBL Stave Test\" -u calibGUI rce -a Author=%s -a Title=\"Scan %d %s\" -a Category=\"Datataking\" -a \"Type of Entry\"=\"Scan Log\" -a Status=\"%s\" -a \"People present\"=\"%s\" -a Automated=1 -m %s -e %d &", 
	      hostname, port, m_author.c_str(), m_runnumber, m_scanname.c_str(), 
	      lstatus.c_str(), m_user, m_infofilename.c_str(),id);  
      system(elogcmd);
      remove(elogfile);
    }
  }
}
void ScanLog::startLogToDB()
{
	#ifdef SQLDB
	ifstream log;
	log.open(m_infofilename.c_str());
	std::string tmp, url, user, host, scanType, dataPath, startTime, endTime, comment, status;
	int runNum;
	bool debuggingRun, dataExported, dataSaved;
	//std::vector <int> FEIds;
	std::vector <std::string> modIds, cfgs, FEIds;
   	std::getline(log,tmp, ':');
	std::getline(log, url);
	boost::algorithm::trim(url);
	std::getline(log,tmp, ':');
	std::getline(log, comment);
	boost::algorithm::trim(comment);
	std::getline(log,tmp, ':');
	log >> runNum;
	std::getline(log,tmp, ':');
	std::getline(log, user);
	boost::algorithm::trim(user);
	std::getline(log,tmp, ':');
	std::getline(log, host);
	boost::algorithm::trim(host);
	std::getline(log,tmp, ':');
	std::getline(log, scanType);
	boost::algorithm::trim(scanType);
	std::getline(log,tmp, ':');
	log >> debuggingRun;
	std::getline(log,tmp, ':');
	log >> dataExported;
	std::getline(log,tmp, ':');
	log >> dataSaved;
	std::getline(log,tmp, ':');
	std::getline(log, dataPath);
	boost::algorithm::trim(dataPath);
	std::getline(log, tmp, ':');
	boost::algorithm::trim(tmp);
	while (tmp.compare("Module") == 0)
	{
		std::string modId, cfgPath, FEId;
		log >> modId;
		boost::algorithm::trim(modId);
		std::getline(log,tmp, ':');
		log >> FEId;
		boost::algorithm::trim(FEId);
		std::getline(log,tmp, ':');
		std::getline(log,cfgPath);
		boost::algorithm::trim(cfgPath);
		std::getline(log,tmp, ':');
		modIds.push_back(modId);
		FEIds.push_back(FEId);
		cfgs.push_back(cfgPath);
	}
	std::getline(log,startTime);
	boost::algorithm::trim(startTime);
	std::getline(log,tmp, ':');//times require special parsing bc whitespace
	log.close();
	//Now insert into SQL DB

	MYSQL *conn;
	conn = mysql_init(NULL);
	char queryStr [1056];
	sprintf(queryStr, "UPDATE CalibRunLog SET URL='%s', User='%s', Host='%s', ScanType='%s', Debugging='%d', DataExported=%d, DataSaved=%d, DataPath='%s', StartTime='%s' WHERE RunNum=%d;",  url.c_str(), user.c_str(), host.c_str(), scanType.c_str(), debuggingRun, dataExported, dataSaved, dataPath.c_str(), startTime.c_str(), runNum);
	std::string dbName="RCECalibRuns";
	#ifdef SQLDEBUG
		dbName="RCECalibRunsDev";
	#endif
	if(mysql_real_connect(conn, "pixiblsr1daq1", "DBWriter", "W#1ter", dbName.c_str(), 0, NULL, 0)==NULL)
	
	{
		std::cout << "SQL connection to " << dbName << " failed!! Nothing will be inserted!" <<std::endl;
		return;
	}
	mysql_query(conn, queryStr);
	//Now insert Module info
	
	for (unsigned int i=0; i < modIds.size(); i++)
	{
	    sprintf(queryStr, "INSERT INTO ModuleLog (RunNum, ModuleId, FEId, CfgName) VALUES (%d, '%s', '%s', '%s');", runNum, modIds.at(i).c_str(), FEIds.at(i).c_str(), cfgs.at(i).c_str());
		mysql_query(conn, queryStr);
	}
	mysql_close(conn);
#endif
}
void ScanLog::finishLogToDB()
{
#ifdef SQLDB
	ifstream log;
	log.open(m_infofilename.c_str());
	std::string tmp, url, user, host, scanType, dataPath, startTime, endTime, comment, status;
	int runNum;
	bool debuggingRun, dataExported, dataSaved;
	//std::vector <int> FEIds;//Read in everything even though it's junk
	std::vector <std::string> modIds, cfgs, FEIds;
   	std::getline(log,tmp, ':');
	std::getline(log, url);
	boost::algorithm::trim(url);
	std::getline(log,tmp, ':');
	std::getline(log, comment);
	boost::algorithm::trim(comment);
	std::getline(log,tmp, ':');
	log >> runNum;
	std::getline(log,tmp, ':');
	std::getline(log, user);
	boost::algorithm::trim(user);
	std::getline(log,tmp, ':');
	std::getline(log, host);
	boost::algorithm::trim(host);
	std::getline(log,tmp, ':');
	std::getline(log, scanType);
	boost::algorithm::trim(scanType);
	std::getline(log,tmp, ':');
	log >> debuggingRun;
	std::getline(log,tmp, ':');
	log >> dataExported;
	std::getline(log,tmp, ':');
	log >> dataSaved;
	std::getline(log,tmp, ':');
	std::getline(log, dataPath);
	boost::algorithm::trim(dataPath);
	std::getline(log, tmp, ':');
	boost::algorithm::trim(tmp);
	while (tmp.compare("Module") == 0)
	{
		std::string modId, cfgPath, FEId;
		log >> modId;
		boost::algorithm::trim(modId);
		std::getline(log,tmp, ':');
		log >> FEId;
		boost::algorithm::trim(FEId);
		std::getline(log,tmp, ':');
		std::getline(log,cfgPath);
		boost::algorithm::trim(cfgPath);
		std::getline(log,tmp, ':');
		modIds.push_back(modId);
		FEIds.push_back(FEId);
		cfgs.push_back(cfgPath);
	}
	std::getline(log,startTime);
	boost::algorithm::trim(startTime);
	std::getline(log,tmp, ':');//times require special parsing bc whitespace
	std::getline(log,endTime);
	boost::algorithm::trim(endTime);
	std::getline(log,tmp, ':');
	std::getline(log, status);
	boost::algorithm::trim(status);
	
	//Now insert into SQL DB

	MYSQL *conn;
	conn = mysql_init(NULL);
	char queryStr [1056];
	sprintf(queryStr, "UPDATE CalibRunLog SET EndTime='%s', Comment='%s', Status='%s' WHERE RunNum=%d", endTime.c_str(), comment.c_str(), status.c_str(), runNum);
	std::string dbName="RCECalibRuns";
	#ifdef SQLDEBUG
		dbName="RCECalibRunsDev";
	#endif
	if(mysql_real_connect(conn, "pixiblsr1daq1", "DBWriter", "W#1ter", dbName.c_str(), 0, NULL, 0)==NULL)
	
	{
		std::cout << "SQL connection to " << dbName << " failed!! Nothing will be inserted!" <<std::endl;
		return;
	}	
	mysql_query(conn, queryStr);
	mysql_close(conn);
#endif
   }


