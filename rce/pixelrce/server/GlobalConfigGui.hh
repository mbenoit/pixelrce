#ifndef GLOBALCONFIGGUI_HH
#define GLOBALCONFIGGUI_HH

#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGLabel.h>
#include <TGComboBox.h>
#include <fstream>
#include <string>
#include <map>
#include "TGNumberEntry.h"

class ConfigGui;

class GlobalConfigGui: public TGVerticalFrame{
public:
  GlobalConfigGui(ConfigGui* cfg[], const char* filename, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options);
  virtual ~GlobalConfigGui();
  void print(std::ostream &os);
  void enableControls(bool on);
  void updateAvailable(bool on);
  void save();
  void saveConfig();
  void copyConfig(const char* filename);
  void saveDefault();
  void load();
  void load(const char* filename);
  void loadConfig();
  void update();
  void setupDisplay();
  void readGuiConfig(std::ifstream &in);
  void writeGuiConfig(std::ofstream &out);
  void setConfigDir(const char* name);
  void filePrompt();
  bool oldFileLoaded();
  std::string setDir();
  void setRootDir(const char* dd);
  void guiSetRootDir();
  void setAbsPath();
  std::string getConfigName(){ return m_cfgpluskey; }
  const char* getDataDir(){return m_ddir.c_str();}
  void setDataDir(const char* dd);
  void guiSetDataDir();
  int  getRunNumber(){return m_runno->GetIntNumber();}
  void setRunNumber(int runno){m_runno->SetIntNumber(runno);}
  int incrementRunNumber();
private:
  int maxRunNum();
  TGLabel *m_name;
  TGLabel *m_key;
  TGLabel *m_rootdirl;
  TGLabel *m_datadir;
  TGNumberEntry *m_runno;
  TGTextButton *m_update, *m_save, *m_load;
  TGHorizontalFrame *m_conf1, *m_conf2;
  ConfigGui **m_config;
  bool m_upd;
  std::string m_filename;
  std::string m_configdir;
  std::string m_path;
  std::string m_cfgpluskey;
  std::string m_homedir;
  std::string m_rootdir;
  std::string m_ddir;
  std::string m_defaultfile;
  Pixel_t m_red;
  void updateText(TGLabel* label, const char* newtext);
  ClassDef(GlobalConfigGui,0);
};

#endif
