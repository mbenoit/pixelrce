// Mathmatical helper functions

#ifndef RCEMATH_HH
#define RCEMATH_HH

#include <stdio.h>

namespace RceMath {
	float fastCoarseSqrt(float x);
	float fastQuakeSqrt(float x);
}

#endif
