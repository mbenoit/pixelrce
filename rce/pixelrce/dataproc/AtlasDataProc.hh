#ifndef ATLASDATAPROC_HH
#define ATLASDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include <fstream>
#include <pthread.h>

class RceFWRegisters;

class AtlasDataProc: public AbsDataProc{
public:
  AtlasDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~AtlasDataProc();
  int processData(unsigned link, unsigned *data, int size);
  void dumpEvent(unsigned *data, int size);
  int fit(std::string);
  unsigned nEvents();
  void monitoring();
  static void* monitoring(void*);
protected:
  
  int m_nModules;
  unsigned m_runNo;
  int m_nfrag;
  pthread_t m_thread;
  RceFWRegisters *m_fwregs;
  float m_normalization;
  char** m_modlabels;
  int *m_outlink;
  int m_rceNumber;
};

#endif
