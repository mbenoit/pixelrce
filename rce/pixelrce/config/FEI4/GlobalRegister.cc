
#include "config/FEI4/GlobalRegister.hh"
#include "config/FEI4/FECommands.hh"
#include "config/FEI4/FEI4ARecord.hh"
#include "config/FEI4/Utils.hh"
#include "HW/SerialIF.hh"
#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <assert.h>
#include "util/VerifyErrors.hh"

namespace FEI4{

  GlobalRegister::GlobalRegister(FECommands* commands): m_clkOutMode(false), m_commands(commands){
  }
  GlobalRegister::~GlobalRegister(){
  }
  void GlobalRegister::writeHW(){
    for(int i=m_rd->N_REG_REGS-1;i>=m_rd->LOW_REG;i--){
//      for(int i=LOW_REG;i<N_REG_REGS;i++){
      if(i==20)continue; //threshold register at the end
      assert(writeRegisterHW(i, m_regw[i])==0);
    }
  }
  void GlobalRegister::writeRegisterHW(int i){
    assert(writeRegisterHW(i, m_regw[i])==0);
  }
  void GlobalRegister::disableThresholdHW(bool dis){
    BitStream *bs=new BitStream;
    unsigned short val= dis? 0xffff : m_regw[20];
    m_commands->writeGlobalRegister(bs, 20, val);
    SerialIF::send(bs, SerialIF::WAITFORDATA); //Write register
    delete bs; 
  }

  void GlobalRegister::enableSrHW(bool on){
    unsigned short val;
    if(on)val=m_regw[m_rd->m_colpr_reg];
    else val=m_rd->m_colpr_disable;
    BitStream *bs=new BitStream;
    m_commands->writeGlobalRegister(bs, m_rd->m_colpr_reg, val);
    SerialIF::send(bs, SerialIF::WAITFORDATA); //Write register
    delete bs;
  }

  unsigned GlobalRegister::writeRegisterHW(int i, unsigned short val){
    unsigned retval=0;
    if(i<0 || i>=m_rd->N_W_REGS){
      retval=1;
    }else {
      m_regw[i]=val; // set register content

      BitStream *bs=new BitStream;
      m_commands->writeGlobalRegister(bs, i, val);

      SerialIF::send(bs, SerialIF::WAITFORDATA); //Write register
      if (i==m_rd->m_params8b10b.reg || i==m_rd->m_paramsDrate.reg){
	if(i==m_rd->m_paramsDrate.reg){
	  SerialIF::setDataRate(m_rd->m_rate[getField("CLK0")]);
	}
	if(getField("Clk2OutCnfg")==0){ //chip is not in clk output mode
	  bool mode=getField("no8b10b");
	  std::cout<<"Mode is "<<mode<<std::endl;
	  SerialIF::setOutputMode(mode);
	  m_clkOutMode=false;
	}else{
	  m_clkOutMode=true;
	}
      }
      delete bs;
    }
    return retval;
  }

  unsigned GlobalRegister::readRegisterHW(int i, unsigned short& val){
    unsigned retval=0;
    if(m_clkOutMode==true){
      retval=11;
    }else if(i>=0 && i<m_rd->N_R_REGS){
      BitStream *bs=new BitStream;
      std::vector<unsigned> retvec;
      m_commands->readGlobalRegister(bs, i);
      //      SerialIF::send(bs);
      std::cout<<"Size "<<retvec.size()<<std::endl;
      retval=SerialIF::readBlockData(*bs, retvec); 
      std::cout<<"Size "<<retvec.size()<<std::endl;
      if(retval==0){
	retval=decodeGlobalRecord(retvec,i,val);
      }
      delete bs;
    }else{
      retval=10;
    }
    return retval;
  }
  void GlobalRegister::readRegisterHW(int i){
    if(i>=0 && i<m_rd->N_R_REGS){
      BitStream *bs=new BitStream;
      m_commands->readGlobalRegister(bs, i);
      SerialIF::send(bs, SerialIF::WAITFORDATA);
      delete bs;
    }
  }
  int GlobalRegister::verifyConfigHW(std::vector<unsigned> & readback){
    int retval=0;
    for(int i=m_rd->LOW_REG;i<m_rd->N_REG_REGS;i++){
      if (i==22)continue; // reg 22 is colpr_mode and colpr_addr. Used to set up pixel reg => will be different in readback
      readback.clear();
      readRegisterHW(i);
      usleep(100);
      if(readback.size()==0){
	std::cout<<"Failed to read back register "<<std::dec<<i<<std::endl;
	retval|=ModuleVerify::GLOBAL_READBACK_FAILED;
      } else {
	m_regr[i]=readback[0];
	if(m_regw[i]!=m_regr[i]){
	  std::cout<<"Bad global register readback for register "<<std::dec<<i
		   <<": Read "<<std::hex<<m_regr[i]<<" should be "<<m_regw[i]<<std::dec<<std::endl;
	  retval|=ModuleVerify::GLOBAL_READBACK_DIFFERENT;
	} 
      }
    }
    return retval;
  }

  void GlobalRegister::setField(const char* name, unsigned vali, mode t){
    assert(m_rd->m_fields.find(name)!=m_rd->m_fields.end());
    FieldParams params=m_rd->m_fields[name];
    // set the proper register first
    unsigned val;
    if(params.reverse==true)val=flipBits(params.width, vali);
    else val=vali;
    setFieldFun(&m_regw[params.reg], params.bitpos, params.width, val);
    if(t==HW){
      writeRegisterHW(params.reg);
      if(params.bitpos+params.width>16)writeRegisterHW(params.reg+1);
    }
    //if(params.SRABbitpos!=-1) // AB shadow register
     // setFieldFun(&m_srab[params.SRABbitpos/16], params.SRABbitpos%16, params.width, val);
    //if(params.SRCbitpos!=-1) // C shadow register
    //  setFieldFun(&m_src[params.SRCbitpos/16], params.SRCbitpos%16, params.width, val);
  } 
  void GlobalRegister::setFieldFun(unsigned short* reg, int bitpos, int width, unsigned val){
    int pwidth=bitpos+width<=16 ? width : 16 - bitpos;
    unsigned short mask=(1<<pwidth)-1;
    *reg&=~(mask<<bitpos);
    *reg|=(val&mask)<<bitpos;
    if(pwidth!=width){ // extends over next register
      int rwidth=width-pwidth;
      mask=(1<<rwidth)-1;
      *(reg+1)&=~mask;
      *(reg+1)|=(val>>pwidth)&mask;
    }
  }

  unsigned GlobalRegister::getField(const char* name){
    assert(m_rd->m_fields.find(name)!=m_rd->m_fields.end());
    FieldParams params=m_rd->m_fields[name];
    unsigned retval=0;
    int pwidth=params.bitpos+params.width<=16 ? params.width : 16 - params.bitpos;
    unsigned short mask=(1<<pwidth)-1;
    retval=(m_regw[params.reg]>>params.bitpos)&mask;
    if(pwidth!=params.width){ // extends over next register
      int rwidth=params.width-pwidth;
      mask=(1<<rwidth)-1;
      retval|=(m_regw[params.reg+1]&mask)<<pwidth;
    }   
    if(params.reverse==false)
      return retval;
    else
      return flipBits(params.width, retval);
  }
  
  std::string GlobalRegister::lookupParameter(const char* name){
    // check if this is the last name used, return cached value
    if(std::string(name)==m_rd->m_cachedName)return m_rd->m_cachedField;
    // Now check if we can translate the name to a field name
    if(m_rd->m_parameters.find(name)!=m_rd->m_parameters.end()){
      //cache result
      m_rd->m_cachedName=name;
      m_rd->m_cachedField=m_rd->m_parameters[name]; 
      return m_rd->m_cachedField;
    // maybe it's a proper field name?
    } else if (m_rd->m_fields.find(name)!=m_rd->m_fields.end()){
      m_rd->m_cachedName=name;
      m_rd->m_cachedField=name;
      return m_rd->m_cachedField;
    }
    // not found.
    else return "";
  }
  void GlobalRegister::addParameter(const char* name, const char* field){
    m_rd->m_parameters[name]=field;
  }
  void GlobalRegister::addField(const char* name, int reg, int bitpos, int srab, int src, int width, bool reverse){
    FieldParams temp;
    temp.reg=reg;
    temp.bitpos=bitpos;
    temp.SRABbitpos=srab;
    temp.SRCbitpos=src;
    temp.width=width;
    temp.reverse=reverse;
    m_rd->m_fields[name]=temp;
  }

  void GlobalRegister::printRegisters(std::ostream &os){
    os<<"Global Registers:"<<std::endl;
    os<<"================="<<std::endl;
    char line[128];
    for (int i=0;i<m_rd->N_W_REGS;i++){
      sprintf(line, "Register %d: %04x\n",i, m_regw[i]);
      os<<line;
    }
  }
  void GlobalRegister::printShadowRegisterAB(std::ostream &os){
    os<<"Shadow Register AB:"<<std::endl;
    os<<"==================="<<std::endl;
    char line[128];
    for (int i=0;i<m_rd->SRAB_BITS/16;i++){
      sprintf(line, "Bit %d - Bit %d: %04x\n",i*16+15, i*16, m_srab[i]);
      os<<line;
    }
  }
  void GlobalRegister::printShadowRegisterC(std::ostream &os){
    os<<"Shadow Register C:"<<std::endl;
    os<<"==================="<<std::endl;
    char line[128];
    for (int i=0;i<m_rd->SRC_BITS/16;i++){
      sprintf(line, "Bit %d - Bit %d: %04x\n",i*16+15, i*16, m_src[i]);
      os<<line;
    }
  }

  unsigned GlobalRegister::decodeGlobalRecord(std::vector<unsigned> inpvec, int i, unsigned short& val){
    if(inpvec.size()==0)return FEI4ARecord::Empty;
    unsigned char* bytepointer=(unsigned char*)&inpvec[0];
    size_t size=inpvec.size()*sizeof(unsigned);
    unsigned char* last=bytepointer+size-3;
    val=0xffff;
    FEI4ARecord rec;
    bool addrrec=false;
    while(bytepointer<=last){
      rec.setRecord(bytepointer);
      if(rec.isAddressRecord()){ // address record
	if(!rec.isGlobal())return FEI4ARecord::NotGlobal; 
	if(rec.getAddress()!=(unsigned)i)return FEI4ARecord::WrongAddress;
	std::cout<<"Read address record"<<std::endl;
	addrrec=true;
      }else if(addrrec==true){
	if(!rec.isValueRecord())return FEI4ARecord::BadAddrValSeq; // addr not immediately followed by val
	std::cout<<"Value Record"<<std::endl;
	val=rec.getValue();
	addrrec=false;
      }else if(rec.isValueRecord()){ //val rec without addr rec
	printf("Value record: %08x\n",rec.getUnsigned());
	val=rec.getValue();
      }else if(rec.isServiceRecord()){
	printf("Service record. Error code: %d. Count: %d \n",rec.getErrorCode(), rec.getErrorCount());
      }else if(rec.isEmptyRecord()){
	// do nothing
      }else{
	std::cout<<"Unexpected record type."<<std::endl;
      }
      bytepointer+=3;
    }
    if(val==0xffff)return FEI4ARecord::NoValue; // no value record found.
    return FEI4ARecord::OK;
  }

};
