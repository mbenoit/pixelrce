# Call rootcint to make root dictionary.
$(objdir)/%_rootDict.o: $(objdir)/%_rootDict.cc 
	@echo "[CX] Compiling $<"
	$(quiet)$(CXX) $(incdirs_$(notdir $<)) $(DEFINES) $(CPPFLAGS) $(CFLAGS) -c $< -o $@



$(objdir)/%_rootDict.cc: %_Linkdef.hh $(GUIHEADERSDEP)
	@echo "[RC] Rootcint Preprocessing $<"
	$(quiet)rootcint -f $@ -c $(CPPFLAGS) $(guiheaders_$(subst _Linkdef.hh,,$<)) $<
