#ifndef COSMICEVENTRECEIVER_HH
#define COSMICEVENTRECEIVER_HH

#include "eudaq/Event.hh"

class CosmicEventReceiver{
public:
  CosmicEventReceiver(){s_receiver=this;}
  virtual ~CosmicEventReceiver(){}
  virtual void OnReceive(counted_ptr<eudaq::Event> ev)=0;
  static void receiveEvent(counted_ptr<eudaq::Event> ev){
    if(s_receiver!=0)s_receiver->OnReceive(ev);
  }
  static CosmicEventReceiver* s_receiver;
};

#endif
