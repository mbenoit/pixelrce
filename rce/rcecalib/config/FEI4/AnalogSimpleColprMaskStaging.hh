#ifndef FEI4ANALOGSIMPLECOLPRMASKSTAGING_HH
#define FEI4ANALOGSIMPLECOLPRMASKSTAGING_HH

#include "rcecalib/config/Masks.hh"
#include "rcecalib/config/MaskStaging.hh"

namespace FEI4{

  class Module;
  
  class AnalogSimpleColprMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    AnalogSimpleColprMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
    int m_nColStages;
    int m_colpr_mode;
  };
  
}
#endif
