#ifndef CFGFILEWRITER_HH
#define CFGFILEWRITER_HH

class TH2;
class TFile;
#include <string>

class CfgFileWriter{
public:
  CfgFileWriter(){}
  virtual ~CfgFileWriter(){}
  virtual void writeDacFile(const char* filename, TH2* his)=0;
  virtual void writeMaskFile(const char* filename, TH2* his)=0;
  std::string getPath(TFile*);
};

#endif 
