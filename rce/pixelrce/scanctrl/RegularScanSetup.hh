#ifndef REGULARSCANSETUP_HH
#define REGULARSCANSETUP_HH

#include "scanctrl/NestedLoop.hh"
#include "scanctrl/LoopSetup.hh"
#include "scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/ConfigIF.hh"

class RegularScanSetup: public LoopSetup{
public:
  RegularScanSetup():LoopSetup(){};
  virtual ~RegularScanSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
