#ifndef COSMICOFFLINEDATAPROC_HH
#define COSMICOFFLINEDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "eudaq/FileSerializer.hh"
#include <list>
#include <string>
#include <vector>
#include <fstream>
#include <map>

class CosmicEvent;
class CosmicEventIterator;
class AbsFormatter;
class AbsController;
class ModuleInfo;

class CosmicOfflineDataProc{
public:
  CosmicOfflineDataProc(AbsController* controller,
			std::vector<ModuleInfo*> modinfo,
			std::vector<int> rceAll,
			unsigned runnum, const std::string& outputFile, bool writeEudet, bool writeRaw);
  virtual ~CosmicOfflineDataProc();
  int processData(unsigned short link, unsigned char* data, int size);
protected:
  void resynch();
  inline int processModuleData(unsigned* data,int size, int link, int module, unsigned& l1id, int& bxfirst, int &bxlast, int &ntrg);
  void switchfile();
  enum constants{MAX_RCES=100, MAX_LINKS=32};

  int m_nModules;
  bool m_modsynch[64];
  int* m_l1id;
  int* m_bxfirst;
  int* m_swap;
  int **m_linkToIndex;
  int *m_rceToIndex;
  int m_tossed[64];
  unsigned m_runNo;
  eudaq::FileSerializer *m_file;
  std::ofstream* m_pfile;
  //std::ofstream m_file;
  int m_nfrag;
  CosmicEventIterator* m_iterator;
  int m_print;
  int m_hits;
  int m_event;
  int m_hitbus;
  AbsFormatter* m_formatter[64];
  AbsController* m_controller;
  unsigned m_sec;
  unsigned m_chunk;
  std::string m_filename;
};

#endif
