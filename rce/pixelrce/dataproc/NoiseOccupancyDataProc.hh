#ifndef NOISEOCCUPANCYDATAPROC_HH
#define NOISEOCCUPANCYDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include "dataproc/fit/AbsFit.hh"
#include <vector>
#include "util/RceHisto2d.cc"
#include "util/RceHisto1d.cc"

class NoiseOccupancyDataProc: public AbsDataProc{
public:
  NoiseOccupancyDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~NoiseOccupancyDataProc();
  int processData(unsigned link, unsigned *data, int size);

protected:

  std::vector<RceHisto2d<int, int>*>  m_histo_occ;
  std::vector<RceHisto1d<int, int>*>  m_histo_eventCount;
  unsigned int *m_l1id_last;
  unsigned short m_counter;
};

#endif
