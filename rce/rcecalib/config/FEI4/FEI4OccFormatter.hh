#ifndef FEI4OCCFORMATTER_HH
#define FEI4OCCFORMATTER_HH

#include "rcecalib/config/AbsFormatter.hh"
#include "rcecalib/util/RceHisto1d.cc"
#include "rcecalib/profiler/Profiler.hh"

class FEI4OccFormatter:public AbsFormatter{
public:
  FEI4OccFormatter(int id);
  virtual ~FEI4OccFormatter();
  int decode(const unsigned* data, int size, unsigned* parsedData, int &parsedsize, int &nL1A);
  void configure(boost::property_tree::ptree* scanOptions);

private:
  RceHisto1d<int, int>* m_errhist;
  Profiler::Timer m_timer;
};
#endif
