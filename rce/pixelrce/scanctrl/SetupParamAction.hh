#ifndef SETUPPARAMACTION_HH
#define SETUPPARAMACTION_HH
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include "scanctrl/LoopAction.hh"
#include "config/ConfigIF.hh"
#include <string>
#include <vector>

class SetupParamAction: public LoopAction{
public:
  SetupParamAction(std::string name, ConfigIF* cif, boost::property_tree::ptree *pt ):
    LoopAction(name),m_configIF(cif){
    try{
      m_scanParameter=pt->get<std::string>("scanParameter");
      int nPoints=pt->get<int>("nPoints");
      BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt->get_child("dataPoints")){
	int data = boost::lexical_cast<int>(v.second.data());
	dataPoints.push_back(data);
      }
      assert((int)dataPoints.size()==nPoints);
    }
    catch(boost::property_tree::ptree_bad_path ex){
      std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    }
  }

  int execute(int i){
    //std::cout<<"Parameter is "<<dataPoints[i]<<std::endl;
    return m_configIF->setupParameter(m_scanParameter.c_str(),dataPoints[i]);
  }

private:
  ConfigIF* m_configIF;
  std::string m_scanParameter;
  std::vector<int> dataPoints;
};

#endif
